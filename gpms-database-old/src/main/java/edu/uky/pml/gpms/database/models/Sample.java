package edu.uky.pml.gpms.database.models;

import edu.uky.pml.gpms.database.services.SampleService;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table( name = "sample" )
public class Sample {
    @Id
    private String id;

    @Column( name = "name" )
    private String name;

    @Column( name = "indir" )
    private String inDir;

    @Column( name = "outdir" )
    private String outDir;

    @ManyToOne
    @Fetch( FetchMode.SELECT )
    private Sequence sequence;

    public Sample() {
        this.id = java.util.UUID.randomUUID().toString();
    }

    public Sample(String name) {
        this();
        this.name = name;
    }

    public Sample(String name, Sequence sequence) {
        this(name);
        this.sequence = sequence;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getInDir() {
        return inDir;
    }
    public void setInDir(String inDir) {
        this.inDir = inDir;
    }

    public String getOutDir() {
        return outDir;
    }
    public void setOutDir(String outDir) {
        this.outDir = outDir;
    }

    public Sequence getSequence() {
        return sequence;
    }
    public void setSequence(Sequence sequence) {
        this.sequence = sequence;
    }

    public LogState getCurrentState() {
        SampleLog log;
        if ((log = SampleService.latestLog(this)) == null)
            return LogState.MISSING;
        return log.getState();
    }

    public int getCurrentStage() {
        SampleLog log;
        if ((log = SampleService.latestLog(this)) == null)
            return -1;
        return log.getStage();
    }

    public int getCurrentStep() {
        SampleLog log;
        if ((log = SampleService.latestLog(this)) == null)
            return -1;
        return log.getStep();
    }

    public long getStarted() {
        SampleLog log;
        if ((log = SampleService.firstLog(this)) == null)
            return -1;
        return log.getTs();
    }

    public String toJson() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");

        sb.append("\"id\":\"");
        sb.append(id);
        sb.append("\",");

        sb.append("\"name\":\"");
        sb.append(name);
        sb.append("\",");

        sb.append("\"max_stage\":");
        sb.append(SampleService.maxStage());
        sb.append(",");


        sb.append("\"stage\":");
        sb.append(getCurrentStage());
        sb.append(",");

        sb.append("\"max_step\":");
        sb.append(SampleService.maxStep());
        sb.append(",");

        sb.append("\"step\":");
        sb.append(getCurrentStep());
        sb.append(",");

        sb.append("\"started\":\"");
        sb.append(getStarted());
        sb.append("\"");

        sb.append("}");
        return sb.toString();
    }
}
