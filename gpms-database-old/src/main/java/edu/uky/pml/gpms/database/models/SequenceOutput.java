package edu.uky.pml.gpms.database.models;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "sequence_output" )
public class SequenceOutput {
    @Id
    private String id;

    @ManyToOne
    @Fetch( FetchMode.SELECT )
    private Sequence sequence;

    @ManyToOne
    @Fetch( FetchMode.SELECT )
    private Plugin plugin;

    @Column( name = "ts" )
    private Long ts;

    @Column( name = "perf_data" )
    private Boolean perfData;

    @Column( name = "log_output", columnDefinition = "longtext")
    private String logOutput;

    public SequenceOutput() {
        this.id = java.util.UUID.randomUUID().toString();
        this.ts = new Date().getTime();
        this.perfData = false;
    }

    public SequenceOutput(Plugin plugin, Sequence sequence) {
        this();
        this.plugin = plugin;
        this.sequence = sequence;
    }

    public SequenceOutput(Plugin plugin, Sequence sequence, String logOutput) {
        this(plugin, sequence);
        this.logOutput = logOutput;
    }

    public SequenceOutput(Plugin plugin, Sequence sequence, String logOutput, Boolean perfData) {
        this(plugin, sequence, logOutput);
        this.perfData = perfData;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public Sequence getSequence() {
        return sequence;
    }
    public void setSequence(Sequence sequence) {
        this.sequence = sequence;
    }

    public Plugin getPlugin() {
        return plugin;
    }
    public void setPlugin(Plugin plugin) {
        this.plugin = plugin;
    }

    public Long getTs() {
        return ts;
    }
    public Date getTsAsDate() {
        return new Date(ts);
    }
    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Boolean getPerfData() {
        return perfData;
    }
    public void setPerfData(Boolean perfData) {
        this.perfData = perfData;
    }

    public String getLogOutput() {
        return logOutput;
    }
    public void setLogOutput(String logOutput) {
        this.logOutput = logOutput;
    }
}
