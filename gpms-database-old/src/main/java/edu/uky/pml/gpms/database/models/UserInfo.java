package edu.uky.pml.gpms.database.models;

import com.google.gson.Gson;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table( name = "user_info" )
public class UserInfo {
    @Id
    private String id;

    @Column( name = "last_name" )
    private String lastName;

    @Column( name = "first_name" )
    private String firstName;

    @OneToOne
    @JoinColumn( name = "user_info_id" )
    private User user;

    public UserInfo() {
        this.id = java.util.UUID.randomUUID().toString();
    }

    public UserInfo(String lastName, String firstName) {
        this();
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public String toJson() {
        Map<String, Object> json = new HashMap<>();
        json.put("first_name", getFirstName());
        json.put("last_name", getLastName());
        return new Gson().toJson(json);
    }
}
