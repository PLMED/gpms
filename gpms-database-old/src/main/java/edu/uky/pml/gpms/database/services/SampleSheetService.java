package edu.uky.pml.gpms.database.services;

import edu.uky.pml.gpms.database.models.SampleSheet;
import edu.uky.pml.gpms.database.models.User;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SampleSheetService {
    private static final Logger logger = LoggerFactory.getLogger(SampleSheetService.class);

    @SuppressWarnings("unchecked")
    public static List<SampleSheet> all() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            List<SampleSheet> list = session.createQuery( "from SampleSheet" ).list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<SampleSheet> all(User owner) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            List<SampleSheet> list = session.createQuery( "from SampleSheet" ).list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static SampleSheet getById(String id) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "from SampleSheet where id = :id" );
            query.setString("id", id);
            SampleSheet object = (SampleSheet) query.uniqueResult();
            session.getTransaction().commit();
            return object;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static SampleSheet getByHash(String hash) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "from SampleSheet where hash = :hash" );
            query.setString("hash", hash);
            SampleSheet object = (SampleSheet) query.uniqueResult();
            session.getTransaction().commit();
            return object;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static SampleSheet getByContents(String contents) {
        return getByHash(DigestUtils.md5Hex(contents).toLowerCase());
    }

    /*public static synchronized SampleSheet create(User owner, String contents) {
        SampleSheet object = getByContents(contents);
        if (object != null)
            return object;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            object = new SampleSheet(owner, contents);
            session.save( object );
            session.getTransaction().commit();
            return object;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }*/

    public static synchronized SampleSheet create(User owner, String contents, String errors) {
        SampleSheet object = getByContents(contents);
        if (object != null)
            return object;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            if (errors != null)
                object = new SampleSheet(owner, contents, errors);
            else
                object = new SampleSheet(owner, contents);
            session.save( object );
            session.getTransaction().commit();
            return object;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            logger.error("create():\n{}", ExceptionUtils.getStackTrace(e));
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getCount(User owner) {
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            return -1L;
        }
        try {
            session.getTransaction().begin();
            String prune = "";
            if (owner != null)
                prune = " WHERE s.owner_id = :owner";
            String queryString = "SELECT s.id as samplesheet_id, s.created, u.id as user_id, u.username, s.errors FROM sample_sheet s join user u on s.owner_id = u.id " + prune;
            Query query = session.createSQLQuery(queryString);
            if (owner != null)
                query.setString("owner", owner.getId());
            final List list = query.list();
            session.getTransaction().commit();
            return list.size();
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            logger.error("getCount() Exception:\n{}", ExceptionUtils.getStackTrace(e));
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getFilteredCount(User owner, String filter) {
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            return -1L;
        }
        try {
            session.getTransaction().begin();
            String prune = "";
            if (owner != null)
                prune = " AND s.owner_id = :owner";
            String queryString = "SELECT s.id as samplesheet_id, s.created, u.id as user_id, u.username, s.errors FROM sample_sheet s join user u on s.owner_id = u.id WHERE (u.username LIKE :filter OR s.errors LIKE :filter)" + prune;
            Query query = session.createSQLQuery(queryString);
            query.setString("filter", "%" + filter + "%");
            if (owner != null)
                query.setString("owner", owner.getId());
            final List list = query.list();
            session.getTransaction().commit();
            return list.size();
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            logger.error("getFilteredCount() Exception:\n{}", ExceptionUtils.getStackTrace(e));
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Object[]> list(User owner, String sortBy, String dir, String filter, int start, int length) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            String prune = "";
            if (owner != null)
                prune = " AND s.owner_id = :owner";
            String queryString = "SELECT s.id as samplesheet_id, s.created, u.id as user_id, u.username, s.errors FROM sample_sheet s join user u on s.owner_id = u.id WHERE (u.username LIKE :filter OR s.errors LIKE :filter)" + prune + " ORDER BY " + sortBy + " " + dir;
            Query query = session.createSQLQuery(queryString);
            query.setString("filter", "%" + filter + "%");
            if (owner != null)
                query.setString("owner", owner.getId());
            query.setFirstResult(start);
            query.setMaxResults(length);
            final List list = query.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            if (logger != null)
                logger.error("list({}, {}, {}, {}, {})\n{}", sortBy, dir, filter, start, length, ExceptionUtils.getStackTrace(e));
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }
}
