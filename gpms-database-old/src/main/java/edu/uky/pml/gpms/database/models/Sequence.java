package edu.uky.pml.gpms.database.models;

import edu.uky.pml.gpms.database.services.SequenceService;

import javax.persistence.*;
import java.util.*;

@Entity
@Table( name = "sequence" )
public class Sequence {
    @Id
    private String id;

    @Column( name = "name" )
    private String name;

    @Column( name = "indir" )
    private String inDir;

    @Column( name = "outdir" )
    private String outDir;

    @OneToMany( fetch = FetchType.EAGER, cascade = CascadeType.ALL )
    @JoinColumn( name = "sequence_id" )
    private Set<Sample> samples = new HashSet<>();

    public Sequence() {
        this.id = java.util.UUID.randomUUID().toString();
    }

    public Sequence(String name) {
        this();
        this.name = name;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getInDir() {
        return inDir;
    }
    public void setInDir(String inDir) {
        this.inDir = inDir;
    }

    public String getOutDir() {
        return outDir;
    }
    public void setOutDir(String outDir) {
        this.outDir = outDir;
    }

    public Set<Sample> getSamples() {
        return samples;
    }
    public void setSamples(Set<Sample> samples) {
        this.samples = samples;
    }

    public Integer getCurrentStage() {
        SequenceLog log;
        if ((log = SequenceService.latestLog(this)) == null)
            return -1;
        return log.getStage();
    }

    public Integer getCurrentStep() {
        SequenceLog log;
        if ((log = SequenceService.latestLog(this)) == null)
            return -1;
        return log.getStep();
    }

    public Long getStarted() {
        SequenceLog log;
        if ((log = SequenceService.firstLog(this)) == null)
            return -1L;
        return log.getTs();
    }

    public String toJson() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");

        sb.append("\"id\":\"");
        sb.append(id);
        sb.append("\",");

        sb.append("\"name\":\"");
        sb.append(name);
        sb.append("\",");

        sb.append("\"max_stage\":");
        sb.append(SequenceService.maxStage());
        sb.append(",");


        sb.append("\"stage\":");
        sb.append(getCurrentStage());
        sb.append(",");

        sb.append("\"max_step\":");
        sb.append(SequenceService.maxStep());
        sb.append(",");

        sb.append("\"step\":");
        sb.append(getCurrentStep());
        sb.append(",");

        sb.append("\"started\":\"");
        sb.append(getStarted());
        sb.append("\"");

        sb.append("}");
        return sb.toString();
    }
}
