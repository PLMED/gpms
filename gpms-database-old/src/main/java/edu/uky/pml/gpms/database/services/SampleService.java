package edu.uky.pml.gpms.database.services;

import edu.uky.pml.gpms.database.models.*;
import edu.uky.pml.gpms.database.models.LogState;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SampleService {
    private static final Logger logger = LoggerFactory.getLogger(SampleService.class);
    private static final int FINAL_STAGE = 4;
    private static final int FINAL_STEP = 4;

    @SuppressWarnings("unchecked")
    public static List<Sample> all() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            List<Sample> list = session.createQuery( "from Sample" ).list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Sample getById(String id) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "from Sample where id = :id" );
            query.setString("id", id);
            Sample object = (Sample) query.uniqueResult();
            session.getTransaction().commit();
            return object;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Sample getByName(String name) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "from Sample where name = :name" );
            query.setString("name", name);
            Sample object = (Sample) query.uniqueResult();
            session.getTransaction().commit();
            return object;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized Sample create(String name, Sequence sequence) {
        Sample object = getByName(name);
        if (object != null)
            return object;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            object = new Sample(name, sequence);
            session.save( object );
            session.getTransaction().commit();
            return object;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Sample update(Sample object) {
        if (object == null)
            return null;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            session.update( object );
            session.getTransaction().commit();
            return object;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean delete(String id) {
        if (id == null)
            return false;
        Sample object = getById(id);
        if (object == null)
            return false;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return false;
        try {
            session.getTransaction().begin();
            Query deleteOutputs = session.createQuery("DELETE FROM SampleOutput WHERE sample = :sample");
            deleteOutputs.setParameter("sample", object);
            deleteOutputs.executeUpdate();
            Query deleteLogs = session.createQuery("DELETE FROM SampleLog WHERE sample = :sample");
            deleteLogs.setParameter("sample", object);
            deleteLogs.executeUpdate();
            session.delete( object );
            session.getTransaction().commit();
            return true;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return false;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static void addLog(Plugin plugin, Sample object, int stage, int step, String message, LogState state) {
        if (object == null)
            return;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return;
        try {
            session.getTransaction().begin();
            session.save( new SampleLog(plugin, object, stage, step, message, state) );
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static void addOutput(Plugin plugin, Sample sample, String output) {
        if (plugin == null)
            return;
        if (sample == null)
            return;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return;
        try {
            session.getTransaction().begin();
            session.save( new SampleOutput(plugin, sample, output) );
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static void addPerfData(Plugin plugin, Sample sample, String output) {
        if (plugin == null)
            return;
        if (sample == null)
            return;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return;
        try {
            session.getTransaction().begin();
            session.save( new SampleOutput(plugin, sample, output, true) );
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getCount() {
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            long count = (Long)session.createCriteria(Sample.class)
                    .setProjection(Projections.rowCount())
                    .uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getFilteredCount(String filter) {
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Sample.class);
            criteria.setProjection(Projections.rowCount());
            if (filter != null && !filter.equals(""))
                criteria.add(Restrictions.ilike("name", "%" + filter + "%"));
            long count = (Long)criteria.uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Object[]> list(String sortBy, String dir, String filter, int start, int length) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            String queryString = "select s.id as id, s.name as sample_name, se.id as sequence_id, se.name as sequence_name, sl.stage as stage, sl.step as step, sl2.ts as ts, sl.state as state from sample s join sample_log sl on s.id = sl.sample_id join sequence se on se.id = s.sequence_id join sample_log sl2 on s.id = sl2.sample_id where sl.ts = (select max(ts) from sample_log where sample_id = s.id) and sl2.ts = (select min(ts) from sample_log where sample_id = s.id) and s.name like :filter order by " + sortBy + " " + dir;
            if (!sortBy.equals("s.name")) {
                queryString += ", s.name asc";
            }
            Query query = session.createSQLQuery(queryString);
            query.setString("filter", "%" + filter + "%");
            query.setFirstResult(start);
            query.setMaxResults(length);
            final List list = query.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            if (logger != null)
                logger.error("list({}, {}, {}, {}, {}): {}", sortBy, dir, filter, start, length, e.getMessage());
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getLogCount(String id) {
        Sample sample = getById(id);
        if (sample == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            long count = (Long)session.createCriteria(SampleLog.class)
                    .add(Restrictions.eq("sample", sample))
                    .setProjection(Projections.rowCount())
                    .uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getFilteredLogCount(String id, String filter) {
        Sample sample = getById(id);
        if (sample == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(SampleLog.class);
            criteria.add(Restrictions.eq("sample", sample));
            criteria.setProjection(Projections.rowCount());
            if (filter != null && !filter.equals(""))
                criteria.add(Restrictions.ilike("message", "%" + filter + "%"));
            long count = (Long)criteria.uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<SampleLog> getLogs(String id, String sortBy, String order, String filter, int start, int length) {
        Sample sample = getById(id);
        if (sample == null)
            return new ArrayList<>();
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(SampleLog.class);
            if (id != null)
                criteria.add(Restrictions.eq("sample", sample));
            if (sortBy != null && order != null) {
                switch (order) {
                    case "asc":
                        criteria.addOrder(Order.asc(sortBy));
                        break;
                    case "desc":
                        criteria.addOrder(Order.desc(sortBy));
                        break;
                }
            }
            if (filter != null && !filter.equals(""))
                criteria.add(Restrictions.ilike("message", "%" + filter + "%"));
            if (start > -1)
                criteria.setFirstResult(start);
            if (length > -1)
                criteria.setMaxResults(length);
            final List<SampleLog> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getOutputCount(String id) {
        Sample sample = getById(id);
        if (sample == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            logger.error("getOutputCount({}): Session was null", id);
            return -1L;
        }
        try {
            session.getTransaction().begin();
            long count = (Long)session.createCriteria(SampleOutput.class)
                    .add(Restrictions.eq("sample", sample))
                    .add(Restrictions.eq("perfData", false))
                    .setProjection(Projections.rowCount())
                    .uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<SampleOutput> getOutputList(String id, String sortBy, String order, String filter, int start, int length) {
        Sample sample = getById(id);
        if (sample == null)
            return new ArrayList<>();
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(SampleOutput.class);
            if (id != null)
                criteria.add(Restrictions.eq("sample", sample));
            criteria.add(Restrictions.eq("perfData", false));
            if (sortBy != null && order != null) {
                switch (order) {
                    case "asc":
                        criteria.addOrder(Order.asc(sortBy));
                        break;
                    case "desc":
                        criteria.addOrder(Order.desc(sortBy));
                        break;
                }
            }
            if (start > -1)
                criteria.setFirstResult(start);
            if (length > -1)
                criteria.setMaxResults(length);
            final List<SampleOutput> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getPerfDataCount(String id) {
        Sample sample = getById(id);
        if (sample == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            logger.error("getOutputCount({}): Session was null", id);
            return -1L;
        }
        try {
            session.getTransaction().begin();
            long count = (Long)session.createCriteria(SampleOutput.class)
                    .add(Restrictions.eq("sample", sample))
                    .add(Restrictions.eq("perfData", true))
                    .setProjection(Projections.rowCount())
                    .uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<SampleOutput> getPerfDataList(String id, String sortBy, String order, String filter, int start, int length) {
        Sample sample = getById(id);
        if (sample == null)
            return new ArrayList<>();
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(SampleOutput.class);
            if (id != null)
                criteria.add(Restrictions.eq("sample", sample));
            criteria.add(Restrictions.eq("perfData", true));
            if (sortBy != null && order != null) {
                switch (order) {
                    case "asc":
                        criteria.addOrder(Order.asc(sortBy));
                        break;
                    case "desc":
                        criteria.addOrder(Order.desc(sortBy));
                        break;
                }
            }
            if (start > -1)
                criteria.setFirstResult(start);
            if (length > -1)
                criteria.setMaxResults(length);
            final List<SampleOutput> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static SampleOutput getOutputById(String id) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "from SampleOutput where id = :id" );
            query.setString("id", id);
            SampleOutput object = (SampleOutput) query.uniqueResult();
            session.getTransaction().commit();
            return object;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static int maxStep() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return -1;
        try {
            session.getTransaction().begin();
            Criteria criteria = session
                    .createCriteria(SampleLog.class)
                    .setProjection(Projections.max("step"));
            int max = (int) criteria.uniqueResult();
            session.getTransaction().commit();
            return max;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static int maxStage() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return -1;
        try {
            session.getTransaction().begin();
            Criteria criteria = session
                    .createCriteria(SampleLog.class)
                    .setProjection(Projections.max("stage"));
            int max = (int) criteria.uniqueResult();
            session.getTransaction().commit();
            return max;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static SampleLog firstLog(Sample object) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Criteria getTS = session
                    .createCriteria(SampleLog.class)
                    .add(Restrictions.eq("sample", object))
                    .addOrder(Order.asc("ts"))
                    .setMaxResults(1);
            List<SampleLog> logs = getTS.list();
            session.getTransaction().commit();
            if (logs.size() > 0)
                return logs.get(0);
            return null;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static SampleLog latestLog(Sample object) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Criteria getTS = session
                    .createCriteria(SampleLog.class)
                    .add(Restrictions.eq("sample", object))
                    .addOrder(Order.desc("ts"))
                    .setMaxResults(1);
            List<SampleLog> logs = getTS.list();
            session.getTransaction().commit();
            if (logs.size() > 0)
                return logs.get(0);
            return null;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static SampleLog latestLogForPlugin(Plugin object) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Criteria getTS = session
                    .createCriteria(SampleLog.class)
                    .add(Restrictions.eq("plugin", object))
                    .addOrder(Order.desc("ts"))
                    .setMaxResults(1);
            List<SampleLog> logs = getTS.list();
            session.getTransaction().commit();
            if (logs.size() > 0)
                return logs.get(0);
            return null;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    static boolean isFinished(Sample object) {
        if (object == null)
            return false;
        SampleLog log = latestLog(object);
        return (log != null && log.getStage() == FINAL_STAGE && log.getStep() == FINAL_STEP && log.getState() != LogState.ERROR);
    }
}
