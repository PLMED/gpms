package edu.uky.pml.gpms.database.models;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.researchworx.cresco.library.messaging.MsgEvent;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "request" )
public class Request {
    @Id
    private String id;

    @Column( name = "created" )
    private Long created;

    @Column( name = "submitted" )
    private Long submitted;

    @Column( name = "completed" )
    private Long completed;

    @Column( name = "failed" )
    private Long failed;

    @Column( name = "rank" )
    private Long order;

    @Column( name = "source_stage" )
    private Integer sourceStage;

    @Column( name = "source_step" )
    private Integer sourceStep;

    @Column( name = "destination_stage" )
    private Integer destinationStage;

    @Column( name = "sequence_name" )
    private String sequenceName;

    @Column( name = "sample_name" )
    private String sampleName;

    @ManyToOne
    @Fetch(FetchMode.SELECT)
    private Plugin plugin;

    @Column( name = "message", columnDefinition="text" )
    private String message;

    @Column( name = "error_message" )
    private String errorMessage;

    public Request() {
        setId(java.util.UUID.randomUUID().toString());
        setCreated(new Date().getTime());
    }

    public Request(int sourceStage, int sourceStep, int destinationStage, MsgEvent msgEvent, long order) {
        this();
        setSourceStage(sourceStage);
        setSourceStep(sourceStep);
        setDestinationStage(destinationStage);
        setMsgEvent(msgEvent);
        setOrder(order);
        if (msgEvent.getParam("seq_id") != null)
            setSequenceName(msgEvent.getParam("seq_id"));
        if (msgEvent.getParam("sample_id") != null)
            setSampleName(msgEvent.getParam("sample_id"));
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public Long getCreated() {
        return created;
    }
    public Date getCreatedAsDate() {
        return new Date(created);
    }
    public void setCreated(Long created) {
        this.created = created;
    }
    public void setCreatedAsDate(Date created) {
        this.created = created.getTime();
    }

    public Long getSubmitted() {
        return submitted;
    }
    public Date getSubmittedAsDate() {
        return new Date(submitted);
    }
    public void setSubmitted(Long submitted) {
        this.submitted = submitted;
    }
    public void setSubmittedAsDate(Date submitted) {
        this.submitted = submitted.getTime();
    }

    public Long getCompleted() {
        return completed;
    }
    public Date getCompletedAsDate() {
        return new Date(completed);
    }
    public void setCompleted(Long completed) {
        this.completed = completed;
    }
    public void setCompletedAsDate(Date completed) {
        this.completed = completed.getTime();
    }

    public Long getFailed() {
        return failed;
    }
    public Date getFailedAsDate() {
        return new Date(failed);
    }
    public void setFailed(Long failed) {
        this.failed = failed;
    }
    public void setFailedAsDate(Date failed) {
        this.failed = failed.getTime();
    }

    public Long getOrder() {
        return order;
    }
    public void setOrder(long order) {
        this.order = order;
    }

    public Integer getSourceStage() {
        return sourceStage;
    }
    public void setSourceStage(int sourceStage) {
        this.sourceStage = sourceStage;
    }

    public Integer getSourceStep() {
        return sourceStep;
    }
    public void setSourceStep(int sourceStep) {
        this.sourceStep = sourceStep;
    }

    public Integer getDestinationStage() {
        return destinationStage;
    }
    public void setDestinationStage(int destinationStage) {
        this.destinationStage = destinationStage;
    }

    public String getSequenceName() {
        return sequenceName;
    }
    public void setSequenceName(String sequenceName) {
        this.sequenceName = sequenceName;
    }

    public String getSampleName() {
        return sampleName;
    }
    public void setSampleName(String sampleName) {
        this.sampleName = sampleName;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        if (sampleName != null)
            return sampleName;
        if (sequenceName != null)
            return sequenceName;
        return "";
    }

    public MsgEvent getMsgEvent() {
        if (message == null) return null;
        Gson marshaller = new Gson();
        try {
            return marshaller.fromJson(message, MsgEvent.class);
        } catch (JsonSyntaxException e) {
            return null;
        }
    }
    public void setMsgEvent(MsgEvent message) {
        if (message == null) this.message = null;
        Gson unmarshaller = new Gson();
        this.message = unmarshaller.toJson(message);
    }

    public Plugin getPlugin() {
        return plugin;
    }
    public void setPlugin(Plugin plugin) {
        this.plugin = plugin;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
