package edu.uky.pml.gpms.database.models;

public enum UserAuditType {
    CREATED(0), UPDATED(2), REMOVED(4);

    private int value;

    UserAuditType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
