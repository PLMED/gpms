package edu.uky.pml.gpms.database.models;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "sample_sheet" )
public class SampleSheet {
    @Id
    private String id;

    @Column( name = "created" )
    private Long created;

    @ManyToOne
    @Fetch( FetchMode.SELECT )
    private User owner;

    @Column( name = "contents", columnDefinition = "longtext" )
    private String contents;

    @Column( name = "hash" )
    private String hash;

    @Column( name = "errors", columnDefinition = "longtext" )
    private String errors;

    public SampleSheet() {
        this.created = new Date().getTime();
        this.id = java.util.UUID.randomUUID().toString();
    }

    public SampleSheet(User owner, String contents) {
        this();
        this.owner = owner;
        this.contents = contents;
        this.hash = DigestUtils.md5Hex(contents).toLowerCase();
    }

    public SampleSheet(User owner, String contents, String errors) {
        this(owner, contents);
        this.errors = errors;
    }

    public String getId() {
        return id;
    }

    public Long getCreated() {
        return created;
    }
    public Date getCreatedDate() {
        return new Date(created);
    }

    public User getOwner() {
        return owner;
    }

    public String getContents() {
        return contents;
    }

    public String getHash() {
        return hash;
    }

    public String getErrors() {
        return errors;
    }
}
