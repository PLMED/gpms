package edu.uky.pml.gpms.database.models;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table( name = "user_audit" )
public class UserAudit {
    @Id
    @GeneratedValue
    private Long id;

    @Enumerated(EnumType.ORDINAL)
    @Column( name = "type" )
    private UserAuditType type;

    @ManyToOne
    private User operator;

    @ManyToOne
    private User target;

    @Temporal(TemporalType.TIMESTAMP)
    @Column( name = "ts" )
    private Date ts;

    @Column( name = "message" )
    private String message;

    public UserAudit() {
        this.ts = new Date();
    }

    public UserAudit(UserAuditType type, User operator, User target) {
        this();
        this.type = type;
        this.operator = operator;
        this.target = target;
    }

    public UserAudit(UserAuditType type, User operator, User target, String message) {
        this(type, operator, target);
        this.message = message;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public UserAuditType getType() {
        return type;
    }
    public void setType(UserAuditType type) {
        this.type = type;
    }

    public User getOperator() {
        return operator;
    }
    public void setOperator(User operator) {
        this.operator = operator;
    }

    public User getTarget() {
        return target;
    }
    public void setTarget(User target) {
        this.target = target;
    }

    public Date getTs() {
        return ts;
    }
    public void setTs(Date ts) {
        this.ts = ts;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}
