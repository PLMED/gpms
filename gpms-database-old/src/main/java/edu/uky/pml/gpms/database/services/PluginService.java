package edu.uky.pml.gpms.database.services;

import edu.uky.pml.gpms.database.models.Plugin;
import edu.uky.pml.gpms.database.models.PluginLog;
import edu.uky.pml.gpms.database.models.SampleLog;
import edu.uky.pml.gpms.database.models.SequenceLog;
import edu.uky.pml.gpms.database.models.LogState;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

public class PluginService {
    private static final Logger logger = LoggerFactory.getLogger(PluginService.class);
    private static final Object IDLE_LOCK = new Object();

    @SuppressWarnings("unchecked")
    public static List<Plugin> all() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            final List<Plugin> list = session.createQuery( "from Plugin" ).list();
            session.getTransaction().commit();
            return list;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Plugin getById(String id) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "from Plugin where id = :id" );
            query.setString("id", id);
            Plugin object = (Plugin) query.uniqueResult();
            session.getTransaction().commit();
            return object;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static Plugin getByName(String regionID, String agentID, String pluginID) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "from Plugin where regionID = :region and agentID = :agent and pluginID = :plugin" );
            query.setString("region", regionID);
            query.setString("agent", agentID);
            query.setString("plugin", pluginID);
            List<Plugin> results = query.list();
            Plugin object = null;
            if (results.size() > 0)
                object = results.get(0);
            session.getTransaction().commit();
            return object;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized Plugin create(String regionID, String agentID, String pluginID) {
        if (regionID == null || agentID == null || pluginID == null)
            return null;
        Plugin object = getByName(regionID, agentID, pluginID);
        if (object != null)
            return object;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            object = new Plugin(regionID, agentID, pluginID);
            session.save( object );
            session.getTransaction().commit();
            return object;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Plugin update(Plugin plugin) {
        if (plugin == null)
            return null;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            session.update( plugin );
            session.getTransaction().commit();
            return plugin;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return plugin;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static void delete(String id) {
        if (id == null)
            return;
        Plugin plugin = getById(id);
        if (plugin == null)
            return;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return;
        try {
            session.getTransaction().begin();
            session.delete( plugin );
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static void addLog(Plugin plugin, int stage, int step, String message, LogState state) {
        if (plugin == null)
            return;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return;
        try {
            session.getTransaction().begin();
            session.save( new PluginLog(plugin, stage, step, message, state) );
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getCount(boolean showAll) {
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            return -1;
        }
        try {
            String prune = "";
            if (!showAll)
                prune = " AND (pl.state = 1 OR pl.state = 2 OR pl.state = 3)";
            session.getTransaction().begin();
            //Query query = session.createSQLQuery("select p.id from plugin p join ((select ts, stage, state, plugin_id from plugin_log order by ts desc limit 100) union (select ts, stage, state, plugin_id from sequence_log order by ts desc limit 100) union (select ts, stage, state, plugin_id from sample_log order by ts desc limit 100)) logs on p.id = logs.plugin_id join (select max(ts) as ts, plugin_id from ((select ts, plugin_id from plugin_log order by ts desc limit 100) union (select ts, plugin_id from sequence_log order by ts desc limit 100) union (select ts, plugin_id from sample_log order by ts desc limit 100)) highest_logs group by plugin_id) highest on logs.ts = highest.ts " + prune);
            Query query = session.createSQLQuery("SELECT p.id FROM plugin p, plugin_log pl, (SELECT plugin_id, MAX(ts) as ts FROM plugin_log GROUP BY plugin_id) pm WHERE p.id = pl.plugin_id AND p.id = pm.plugin_id AND pl.ts = pm.ts" + prune);
            final List<Object[]> list = query.list();
            session.getTransaction().commit();
            return list.size();
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getFilteredCount(String filter, boolean showAll) {
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            return -1;
        }
        try {
            String prune = "";
            if (!showAll)
                prune = " AND (pl.state = 1 OR pl.state = 2 OR pl.state = 3) ";
            session.getTransaction().begin();
            //Query query = session.createSQLQuery("select p.id from plugin p join ((select ts, stage, state, plugin_id from plugin_log order by ts desc limit 100) union (select ts, stage, state, plugin_id from sequence_log order by ts desc limit 100) union (select ts, stage, state, plugin_id from sample_log order by ts desc limit 100)) logs on p.id = logs.plugin_id join (select max(ts) as ts, plugin_id from ((select ts, plugin_id from plugin_log order by ts desc limit 100) union (select ts, plugin_id from sequence_log order by ts desc limit 100) union (select ts, plugin_id from sample_log order by ts desc limit 100)) highest_logs group by plugin_id ) highest on logs.ts = highest.ts where (p.plugin_region like :filter or p.plugin_agent like :filter or p.plugin_ident like :filter)" + prune);
            Query query = session.createSQLQuery("SELECT p.id FROM plugin p, plugin_log pl, (SELECT plugin_id, MAX(ts) as ts FROM plugin_log GROUP BY plugin_id) pm WHERE p.id = pl.plugin_id AND p.id = pm.plugin_id AND pl.ts = pm.ts AND (p.plugin_region LIKE :filter OR p.plugin_agent LIKE :filter OR p.plugin_ident LIKE :filter)" + prune);
            query.setParameter("filter", "%" + filter + "%");
            final List<Object[]> list = query.list();
            session.getTransaction().commit();
            return list.size();
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Object[]> list(String sortBy, String dir, String filter, int start, int length, boolean showAll) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            String prune = "";
            if (!showAll)
                prune = " AND (pl.state = 1 OR pl.state = 2 OR pl.state = 3) ";
            session.getTransaction().begin();
            String queryString = "SELECT p.id as id, CONCAT(p.plugin_region, ' - ', p.plugin_agent, ' : ', p.plugin_ident) AS name, pl.stage AS stage, pl.state AS state, pl.ts AS ts FROM plugin p, plugin_log pl, (SELECT plugin_id, MAX(ts) as ts FROM plugin_log GROUP BY plugin_id) pm WHERE p.id = pl.plugin_id AND p.id = pm.plugin_id AND pl.ts = pm.ts AND (p.plugin_region LIKE :filter OR p.plugin_agent LIKE :filter OR p.plugin_ident LIKE :filter) " + prune + " ORDER BY " + sortBy + " " + dir;
            //String queryString = "select p.id as id, CONCAT(p.plugin_region, ' - ', p.plugin_agent, ' : ', p.plugin_ident) as name, logs.stage as stage, logs.state as state, logs.ts as ts from plugin p join ((select ts, stage, state, plugin_id from plugin_log order by ts desc limit 100) union (select ts, stage, state, plugin_id from sequence_log order by ts desc limit 100) union (select ts, stage, state, plugin_id from sample_log order by ts desc limit 100)) logs on p.id = logs.plugin_id join (select max(ts) as ts, plugin_id from ((select ts, plugin_id from plugin_log order by ts desc limit 100) union (select ts, plugin_id from sequence_log order by ts desc limit 100) union (select ts, plugin_id from sample_log order by ts desc limit 100)) highest_logs group by plugin_id ) highest on logs.ts = highest.ts where (p.plugin_region like :filter or p.plugin_agent like :filter or p.plugin_ident like :filter) " + prune + " order by " + sortBy + " " + dir;
            //String queryString = "select p.id as id, CONCAT(p.plugin_region, ' - ', p.plugin_agent, ' : ', p.plugin_ident) as name, logs.stage as stage, logs.state as state, logs.ts as ts from plugin p join (select ts, stage, state, plugin_id from plugin_log union select ts, stage, state, plugin_id from sequence_log union select ts, stage, state, plugin_id from sample_log) logs on p.id = logs.plugin_id join (select max(ts) as ts, plugin_id from (select ts, plugin_id from plugin_log union select ts, plugin_id from sequence_log union select ts, plugin_id from sample_log) highest_logs group by plugin_id ) highest on logs.ts = highest.ts where (p.plugin_region like :filter or p.plugin_agent like :filter or p.plugin_ident like :filter) " + prune + " order by " + sortBy + " " + dir;
            Query query = session.createSQLQuery(queryString);
            query.setString("filter", "%" + filter + "%");
            query.setFirstResult(start);
            query.setMaxResults(length);
            final List list = query.list();
            /*Criteria criteria = session.createCriteria(Plugin.class);
            if (sortBy != null && order != null) {
                switch (order) {
                    case "asc":
                        criteria.addOrder(Order.asc(sortBy));
                        break;
                    case "desc":
                        criteria.addOrder(Order.desc(sortBy));
                        break;
                }
            }
            if (filter != null && !filter.equals(""))
                criteria.add(Restrictions.ilike("name", "%" + filter + "%"));
            if (start > -1)
                criteria.setFirstResult(start);
            if (length > -1)
                criteria.setMaxResults(length);
            final List<Plugin> list = criteria.list();*/
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getLogCount(String id) {
        Plugin plugin = getById(id);
        if (plugin == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            return -1L;
        }
        try {
            session.getTransaction().begin();
            Query query = session.createQuery("select count(*) from PluginLog where plugin = :plugin");
            query.setParameter("plugin", plugin);
            final long count = (long) query.uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getFilteredLogCount(String id, String filter) {
        Plugin plugin = getById(id);
        if (plugin == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            return -1L;
        }
        try {
            session.getTransaction().begin();
            Query query = session.createQuery("select count(*) from PluginLog where plugin = :plugin and message like :filter");
            query.setParameter("plugin", plugin);
            query.setParameter("filter", "%" + filter + "%");
            final long count = (long) query.uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<PluginLog> getLogs(String id, String sortBy, String order, String filter, int start, int length) {
        Plugin plugin = getById(id);
        if (plugin == null)
            return new ArrayList<>();
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(PluginLog.class);
            if (id != null)
                criteria.add(Restrictions.eq("plugin", plugin));
            if (sortBy != null && order != null) {
                switch (order) {
                    case "asc":
                        criteria.addOrder(Order.asc(sortBy));
                        break;
                    case "desc":
                        criteria.addOrder(Order.desc(sortBy));
                        break;
                }
            }
            if (filter != null && !filter.equals(""))
                criteria.add(Restrictions.ilike("message", "%" + filter + "%"));
            if (start > -1)
                criteria.setFirstResult(start);
            if (length > -1)
                criteria.setMaxResults(length);
            final List<PluginLog> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getSequenceLogCount(String id) {
        Plugin plugin = getById(id);
        if (plugin == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            long count = (Long)session.createCriteria(SequenceLog.class)
                    .add(Restrictions.eq("plugin", plugin))
                    .setProjection(Projections.rowCount())
                    .uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getFilteredSequenceLogCount(String id, String filter) {
        Plugin plugin = getById(id);
        if (plugin == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(SequenceLog.class);
            criteria.add(Restrictions.eq("plugin", plugin));
            criteria.setProjection(Projections.rowCount());
            if (filter != null && !filter.equals(""))
                criteria.add(Restrictions.ilike("message", "%" + filter + "%"));
            long count = (Long)criteria.uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<SequenceLog> getSequenceLogs(String id, String sortBy, String dir, String filter, int start, int length) {
        Plugin plugin = getById(id);
        if (plugin == null)
            return new ArrayList<>();
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(SequenceLog.class);
            if (id != null)
                criteria.add(Restrictions.eq("plugin", plugin));
            if (sortBy != null && dir != null) {
                switch (dir) {
                    case "asc":
                        criteria.addOrder(Order.asc(sortBy));
                        break;
                    case "desc":
                        criteria.addOrder(Order.desc(sortBy));
                        break;
                }
            }
            if (filter != null && !filter.equals(""))
                criteria.add(Restrictions.ilike("message", "%" + filter + "%"));
            if (start > -1)
                criteria.setFirstResult(start);
            if (length > -1)
                criteria.setMaxResults(length);
            final List<SequenceLog> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getSampleLogCount(String id) {
        Plugin plugin = getById(id);
        if (plugin == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            long count = (Long)session.createCriteria(SampleLog.class)
                    .add(Restrictions.eq("plugin", plugin))
                    .setProjection(Projections.rowCount())
                    .uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getFilteredSampleLogCount(String id, String filter) {
        Plugin plugin = getById(id);
        if (plugin == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(SampleLog.class);
            criteria.add(Restrictions.eq("plugin", plugin));
            criteria.setProjection(Projections.rowCount());
            if (filter != null && !filter.equals(""))
                criteria.add(Restrictions.ilike("message", "%" + filter + "%"));
            long count = (Long)criteria.uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<SampleLog> getSampleLogs(String id, String sortBy, String dir, String filter, int start, int length) {
        Plugin plugin = getById(id);
        if (plugin == null)
            return new ArrayList<>();
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(SampleLog.class);
            if (id != null)
                criteria.add(Restrictions.eq("plugin", plugin));
            if (sortBy != null && dir != null) {
                switch (dir) {
                    case "asc":
                        criteria.addOrder(Order.asc(sortBy));
                        break;
                    case "desc":
                        criteria.addOrder(Order.desc(sortBy));
                        break;
                }
            }
            if (filter != null && !filter.equals(""))
                criteria.add(Restrictions.ilike("message", "%" + filter + "%"));
            if (start > -1)
                criteria.setFirstResult(start);
            if (length > -1)
                criteria.setMaxResults(length);
            final List<SampleLog> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static int maxStep() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return -1;
        try {
            session.getTransaction().begin();
            Criteria criteria = session
                    .createCriteria(PluginLog.class)
                    .setProjection(Projections.max("step"));
            int max = (int) criteria.uniqueResult();
            session.getTransaction().commit();
            return max;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static int maxStage() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return -1;
        try {
            session.getTransaction().begin();
            Criteria criteria = session
                    .createCriteria(PluginLog.class)
                    .setProjection(Projections.max("stage"));
            int max = (int) criteria.uniqueResult();
            session.getTransaction().commit();
            return max;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static PluginLog latestLog(Plugin object) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Criteria getTS = session
                    .createCriteria(PluginLog.class)
                    .add(Restrictions.eq("plugin", object))
                    .addOrder(Order.desc("ts"))
                    .setMaxResults(1);
            List<PluginLog> logs = getTS.list();
            session.getTransaction().commit();
            if (logs.size() > 0)
                return logs.get(0);
            return null;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Plugin getIdlePlugin(int pathStage, HashSet<String> used) {
        synchronized (IDLE_LOCK) {
            for (Plugin plugin : all()) {
                if (plugin.getCurrentStage() == pathStage && plugin.getCurrentState() == LogState.IDLE && !used.contains(plugin.getId())) {
                    addLog(plugin, pathStage, 0, "Sending process request", LogState.WORKING);
                    return plugin;
                }
            }
            return null;
        }
    }

    public static void cleanIdleLogs(int threshold) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return;
        Calendar endCap = Calendar.getInstance();
        endCap.add(Calendar.MINUTE, -1 * threshold);
        try {
            session.getTransaction().begin();
            Query query = session.createQuery("delete from PluginLog where ts < :endCap and ( message = :idlemessage or message like :scanmessage or message like :lastseenmessage )");
            query.setParameter("endCap", endCap.getTimeInMillis());
            query.setParameter("idlemessage", "Idle");
            query.setParameter("scanmessage", "%scan%");
            query.setParameter("lastseenmessage", "%15000%");
            query.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }
}
