package edu.uky.pml.gpms.database.utilities;

import org.mindrot.jbcrypt.BCrypt;

import java.io.IOException;

public class HashHelper {
    public static String createPassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public static boolean checkPassword(String candidate, String encryptedPassword) {
        if (candidate == null || encryptedPassword == null)
            return false;
        return BCrypt.checkpw(candidate, encryptedPassword);
    }
}
