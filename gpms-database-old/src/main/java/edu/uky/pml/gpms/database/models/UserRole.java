package edu.uky.pml.gpms.database.models;

import javax.persistence.*;

@Entity
@Table( name = "user_role" )
public class UserRole {
    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    @JoinColumn( name = "role_id" )
    private Role role;

    @OneToOne
    @JoinColumn( name = "user_id" )
    private User user;

    public UserRole() { }

    public UserRole(Role role, User user) {
        this();
        this.role = role;
        this.user = user;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }
    public void setRole(Role role) {
        this.role = role;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
}
