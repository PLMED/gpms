package edu.uky.pml.gpms.database.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "user_session" )
public class UserSession {
    @Id
    private String id;

    @Column( name = "last_seen" )
    private Long lastSeen;

    @Column( name = "remember_me" )
    private Boolean rememberMe;

    @ManyToOne
    private User user;

    public UserSession() {
        this.id = java.util.UUID.randomUUID().toString();
        this.lastSeen = new Date().getTime();
        this.rememberMe = false;
    }

    public UserSession(User user) {
        this();
        this.user = user;
    }

    public UserSession(User user, Boolean rememberMe) {
        this(user);
        this.rememberMe = rememberMe;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public long getLastSeen() {
        return lastSeen;
    }
    public Date getLastSeenAsDate() {
        return new Date(lastSeen);
    }
    public void setLastSeen(long lastSeen) {
        this.lastSeen = lastSeen;
    }

    public Boolean getRememberMe() {
        return rememberMe;
    }
    public void setRememberMe(Boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
}
