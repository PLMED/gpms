package edu.uky.pml.gpms.database.services;

import edu.uky.pml.gpms.database.models.*;
import edu.uky.pml.gpms.database.utilities.HashHelper;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import org.hibernate.*;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import java.util.ArrayList;
import java.util.List;

public class UserService {

    @SuppressWarnings("unchecked")
    public static List<User> all() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            final List<User> list = session.createQuery( "from User" ).list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
        return new ArrayList<>();
    }

    public static User getById(String id) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "from User where id = :id" );
            query.setString("id", id);
            final User user = (User) query.uniqueResult();
            session.getTransaction().commit();
            return user;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static User getByUsername(String username) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "from User where username = :username or email = :username" );
            query.setString("username", username);
            final User user = (User) query.uniqueResult();
            session.getTransaction().commit();
            return user;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static void updateUserRole(User operator, User user, Role role) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return;
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "update UserRole set role_id = :role_id where user_id = :user_id" );
            query.setString("user_id", user.getId());
            query.setLong("role_id", role.getId());
            query.executeUpdate();
            session.persist( new UserAudit(UserAuditType.UPDATED, operator, user,
                    String.format("Updated user role to [%s]", role.getName())) );
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized User create(User operator, String message, String username,
                              String email, String password, String lastName,
                              String firstName, Role role) {
        User existing = getByUsername(username);
        if (existing != null) {
            existing.setPassword(HashHelper.createPassword(password));
            existing.getInfo().setLastName(lastName);
            existing.getInfo().setFirstName(firstName);
            existing.setDecommissioned(false);
            silentUpdate(existing);
            return existing;
        }
        existing = getByUsername(email);
        if (existing != null) {
            existing.setPassword(HashHelper.createPassword(password));
            existing.getInfo().setLastName(lastName);
            existing.getInfo().setFirstName(firstName);
            existing.setDecommissioned(false);
            silentUpdate(existing);
            return existing;
        }
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            User user = new User( username.toLowerCase().trim(), email.toLowerCase().trim(), HashHelper.createPassword(password));
            user.setInfo( new UserInfo(lastName, firstName) );
            user.setUserRole( new UserRole(role, user) );
            session.persist( user );
            if (operator == null)
                operator = user;
            session.persist( new UserAudit(UserAuditType.CREATED, operator, user, message) );
            session.getTransaction().commit();
            return user;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static User update(User operator, String message, User user) {
        if (user == null)
            return null;
        if (operator == null)
            operator = user;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            session.update(user);
            session.persist( new UserAudit(UserAuditType.UPDATED, operator, user, message) );
            session.getTransaction().commit();
            return user;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static User silentUpdate(User user) {
        if (user == null)
            return null;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            session.update(user);
            session.getTransaction().commit();
            return user;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static void remove(User operator, String message, String id) {
        if (id == null)
            return;
        User user = getById(id);
        if (user == null)
            return;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return;
        try {
            session.getTransaction().begin();
            user.decommission();
            session.update(user);
            session.save( new UserAudit(UserAuditType.REMOVED, operator, user, message) );
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static User authenticate(String username, String password) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.beginTransaction();
            Query query = session.createQuery( "from User where username = :username or email = :username" );
            query.setString("username", username.toLowerCase().trim());
            final User user = (User) query.uniqueResult();
            if (user == null || user.getDecommissioned() || !HashHelper.checkPassword(password, user.getPassword()))
                return null;
            session.getTransaction().commit();
            return user;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }
}
