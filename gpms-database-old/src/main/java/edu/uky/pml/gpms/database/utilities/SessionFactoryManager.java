package edu.uky.pml.gpms.database.utilities;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.tool.schema.spi.SchemaManagementException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SessionFactoryManager {
    private static final Logger logger = LoggerFactory.getLogger(SessionFactoryManager.class);
    private static final String DB_PROPERTIES_PATH = "db.properties";
    private static SessionFactory factory;

    private static boolean buildSession() {
        logger.trace("buildSession()");
        Path props = Paths.get(DB_PROPERTIES_PATH);
        if (!Files.exists(props)) {
            logger.error("Database configuration file [{}] does not exist", props);
            return false;
        }
        logger.trace("Building registry object");
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure()
                .loadProperties(props.toFile())
                .build();
        try {
            logger.trace("Building session factory");
            factory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();
            return true;
        } catch (SchemaManagementException e) {
            logger.error("Invalid schema detected: {}", e.getMessage());
            StandardServiceRegistryBuilder.destroy( registry );
            return false;
        } catch (Exception e) {
            logger.error("General exception encountered:\n" + ExceptionUtils.getStackTrace(e));
            StandardServiceRegistryBuilder.destroy( registry );
            return false;
        }
    }

    public static Session getSession() {
        logger.trace("getSession()");
        if ( factory == null )
            if ( !buildSession() )
                return null;
        return factory.openSession();
    }

    public static void close() {
        logger.trace("close()");
        if ( factory != null )
            factory.close();
    }
}
