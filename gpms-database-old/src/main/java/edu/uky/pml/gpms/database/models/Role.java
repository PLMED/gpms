package edu.uky.pml.gpms.database.models;

import com.google.gson.Gson;

import javax.persistence.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Entity
@Table( name = "role" )
public class Role {
    @Id
    @GeneratedValue
    private Long id;

    @Column( name = "name", unique = true, nullable = false )
    private String name;

    @Column( name = "rank", unique = true, nullable = false )
    private Long rank;

    @OneToMany( cascade = CascadeType.DETACH )
    @JoinColumn( name = "role_id" )
    private Set<UserRole> users = new HashSet<>();

    public Role() { }

    public Role(String name) {
        this();
        this.name = name;
    }

    public Role(String name, Long rank) {
        this(name);
        this.rank = rank;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Long getRank() {
        return rank;
    }
    public void setRank(Long rank) {
        this.rank = rank;
    }

    public Set<UserRole> getUsers() {
        return users;
    }
    public void setUsers(Set<UserRole> users) {
        this.users = users;
    }

    public String toJson() {
        Map<String, Object> json = new HashMap<>();
        json.put("name", getName());
        return new Gson().toJson(json);
    }
}
