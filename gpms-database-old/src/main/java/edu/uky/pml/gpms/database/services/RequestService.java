package edu.uky.pml.gpms.database.services;

import com.researchworx.cresco.library.messaging.MsgEvent;
import edu.uky.pml.gpms.database.models.Plugin;
import edu.uky.pml.gpms.database.models.Request;
import edu.uky.pml.gpms.database.models.Sequence;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.*;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class RequestService {
    private static final Logger logger = LoggerFactory.getLogger(RequestService.class);
    private static final long ORDER_GAP = 100000L;

    @SuppressWarnings("unchecked")
    public static List<Request> all() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            final List<Request> list = session.createQuery( "from Request" ).list();
            session.getTransaction().commit();
            return list;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Request> pending() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Request.class);
            criteria.add(Restrictions.isNull("submitted"));
            criteria.add(Restrictions.isNull("completed"));
            criteria.add(Restrictions.isNull("failed"));
            List<Request> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Request> pendingForSequence(String name) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Request.class);
            criteria.add(Restrictions.isNull("submitted"));
            criteria.add(Restrictions.isNull("completed"));
            criteria.add(Restrictions.isNull("failed"));
            criteria.add(Restrictions.eq("sequenceName", name));
            criteria.addOrder(Order.desc("submitted"));
            List<Request> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Request> pendingForSample(String name) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Request.class);
            criteria.add(Restrictions.isNull("submitted"));
            criteria.add(Restrictions.isNull("completed"));
            criteria.add(Restrictions.isNull("failed"));
            criteria.add(Restrictions.eq("sampleName", name));
            criteria.addOrder(Order.desc("submitted"));
            List<Request> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Request> submitted() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Request.class);
            criteria.add(Restrictions.isNotNull("submitted"));
            criteria.add(Restrictions.isNull("completed"));
            criteria.add(Restrictions.isNull("failed"));
            List<Request> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Request> submittedForSequence(String name) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Request.class);
            criteria.add(Restrictions.isNotNull("submitted"));
            criteria.add(Restrictions.isNull("completed"));
            criteria.add(Restrictions.isNull("failed"));
            criteria.add(Restrictions.eq("sequenceName", name));
            criteria.addOrder(Order.desc("submitted"));
            List<Request> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Request> submittedForSample(String name) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Request.class);
            criteria.add(Restrictions.isNotNull("submitted"));
            criteria.add(Restrictions.isNull("completed"));
            criteria.add(Restrictions.isNull("failed"));
            criteria.add(Restrictions.eq("sampleName", name));
            criteria.addOrder(Order.desc("submitted"));
            List<Request> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Request> completed() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Request.class);
            criteria.add(Restrictions.isNotNull("submitted"));
            criteria.add(Restrictions.isNotNull("completed"));
            criteria.add(Restrictions.isNull("failed"));
            List<Request> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Request> completedForSequence(String name) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Request.class);
            criteria.add(Restrictions.isNotNull("submitted"));
            criteria.add(Restrictions.isNotNull("completed"));
            criteria.add(Restrictions.isNull("failed"));
            criteria.add(Restrictions.eq("sequenceName", name));
            criteria.addOrder(Order.desc("completed"));
            List<Request> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Request> completedForSample(String name) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Request.class);
            criteria.add(Restrictions.isNotNull("submitted"));
            criteria.add(Restrictions.isNotNull("completed"));
            criteria.add(Restrictions.isNull("failed"));
            criteria.add(Restrictions.eq("sampleName", name));
            criteria.addOrder(Order.desc("completed"));
            List<Request> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Request> failed() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Request.class);
            criteria.add(Restrictions.isNotNull("failed"));
            List<Request> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Request> failedForSequence(String name) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Request.class);
            criteria.add(Restrictions.isNotNull("failed"));
            criteria.add(Restrictions.eq("sequence_name", name));
            criteria.addOrder(Order.desc("submitted"));
            List<Request> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Request> failedForSample(String name) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Request.class);
            criteria.add(Restrictions.isNotNull("failed"));
            criteria.add(Restrictions.eq("sample_name", name));
            criteria.addOrder(Order.desc("submitted"));
            List<Request> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getRequestCount(boolean showAll) {
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            /*long count = (Long)session.createCriteria(Request.class)
                    .setProjection(Projections.rowCount())
                    .uniqueResult();*/
            Criteria query = session.createCriteria(Request.class)
                    .setProjection(Projections.rowCount());
            if (!showAll) {
                query.add(Restrictions.isNull("failed"));
                query.add(Restrictions.isNull("completed"));
            }
            long count = (Long)query.uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getFilteredRequestCount(String filter, boolean showAll) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return -1L;
        try {
            session.getTransaction().begin();
            String queryString = "SELECT DISTINCT r.id FROM plugin p, request r WHERE (CASE WHEN r.plugin_id <> '' THEN (r.plugin_id = p.id AND (r.sample_name LIKE :filter OR r.sequence_name LIKE :filter OR p.plugin_region LIKE :filter OR p.plugin_agent LIKE :filter OR p.plugin_ident LIKE :filter)) ELSE (r.sample_name LIKE :filter OR r.sequence_name LIKE :filter) END)";
            if (!showAll) {
                queryString += " AND r.completed IS NULL AND r.failed IS NULL";
            }
            Query query = session.createSQLQuery(queryString);
            query.setParameter("filter", "%" + filter + "%");
            long count = query.list().size();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Object[]> getRequests(String sort, String order, String filter, int start, int length, boolean showAll) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            String prune = "";
            if (!showAll) {
                prune = " AND r.completed IS NULL AND r.failed IS NULL";
            }
            String queryString = "SELECT DISTINCT r.id as id, (CASE WHEN r.sample_name <> '' THEN r.sample_name ELSE r.sequence_name END) as name, (CASE WHEN r.plugin_id <> '' THEN CONCAT_WS(':', p.plugin_region, p.plugin_agent, p.plugin_ident) ELSE '' END) as plugin, destination_stage as next, created, submitted, completed, failed FROM plugin p, request r WHERE (CASE WHEN r.plugin_id <> '' THEN r.plugin_id = p.id ELSE TRUE END) AND (r.sequence_name LIKE :filter OR r.sample_name LIKE :filter OR p.plugin_region LIKE :filter OR p.plugin_agent LIKE :filter OR p.plugin_ident LIKE :filter) " + prune + " ORDER BY " + sort + " " + order;
            if (sort.equals("r.sequence_name")) {
                queryString += ", r.sample_name " + order;
            }
            Query query = session.createSQLQuery(queryString);
            query.setString("filter", "%" + filter + "%");
            query.setFirstResult(start);
            query.setMaxResults(length);
            final List list = query.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Request getById(String id) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Request.class);
            criteria.add(Restrictions.eq("id", id));
            Request object = (Request) criteria.uniqueResult();
            session.getTransaction().commit();
            return object;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getRequestsForSequenceCount(String id, boolean showAll) {
        Sequence sequence = SequenceService.getById(id);
        if (sequence == null) {
            sequence = SequenceService.getByName(id);
            if (sequence == null) {
                return -1;
            }
        }
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            Criteria query = session.createCriteria(Request.class)
                    .setProjection(Projections.rowCount());
            query.add(Restrictions.eq("sequenceName", sequence.getName()));
            if (!showAll) {
                query.add(Restrictions.isNull("failed"));
                query.add(Restrictions.isNull("completed"));
            }
            long count = (Long)query.uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getFilteredRequestsForSequenceCount(String id, String filter, boolean showAll) {
        Sequence sequence = SequenceService.getById(id);
        if (sequence == null) {
            sequence = SequenceService.getByName(id);
            if (sequence == null) {
                return -1;
            }
        }
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return -1L;
        try {
            session.getTransaction().begin();
            String prune = "";
            if (!showAll) {
                prune = " AND r.completed IS NULL AND r.failed IS NULL";
            }
            String queryString = "SELECT DISTINCT r.id as id FROM plugin p, request r WHERE (CASE WHEN r.plugin_id <> '' THEN r.plugin_id = p.id ELSE TRUE END) AND r.sequence_name = :sequence AND (p.plugin_region LIKE :filter OR p.plugin_agent LIKE :filter OR p.plugin_ident LIKE :filter) " + prune;
            Query query = session.createSQLQuery(queryString);
            query.setString("sequence", sequence.getName());
            query.setString("filter", "%" + filter + "%");
            long count = query.list().size();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static List<Object[]> getRequestsForSequence(String id, String sort, String order, String filter, int start, int length, boolean showAll) {
        Sequence sequence = SequenceService.getById(id);
        if (sequence == null) {
            sequence = SequenceService.getByName(id);
            if (sequence == null) {
                return new ArrayList<>();
            }
        }
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            String prune = "";
            if (!showAll) {
                prune = " AND r.completed IS NULL AND r.failed IS NULL";
            }
            String queryString = "SELECT DISTINCT r.id as id, (CASE WHEN r.plugin_id <> '' THEN CONCAT_WS(':', p.plugin_region, p.plugin_agent, p.plugin_ident) ELSE '' END) as plugin, destination_stage as next, created, submitted, completed, failed, message FROM plugin p, request r WHERE (CASE WHEN r.plugin_id <> '' THEN r.plugin_id = p.id ELSE TRUE END) AND r.sequence_name = :sequence AND (p.plugin_region LIKE :filter OR p.plugin_agent LIKE :filter OR p.plugin_ident LIKE :filter) " + prune + " ORDER BY " + sort + " " + order;
            Query query = session.createSQLQuery(queryString);
            query.setString("sequence", sequence.getName());
            query.setString("filter", "%" + filter + "%");
            query.setFirstResult(start);
            query.setMaxResults(length);
            final List list = query.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    private static long getNextOrder() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return -1;
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Request.class);
            criteria.setProjection(Projections.max("order"));
            Long result =(Long)criteria.uniqueResult();
            session.getTransaction().commit();
            long order = ORDER_GAP;
            if (result != null)
                order = (ORDER_GAP + (result - (result % ORDER_GAP)));
            return order;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized Request create(int sourceStage, int sourceStep, int destinationStage, MsgEvent msgEvent) {
        if (sourceStage < 0 || sourceStep < 0 || destinationStage < 0 || msgEvent == null)
            return null;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Request object = new Request(sourceStage, sourceStep, destinationStage, msgEvent, getNextOrder());
            session.save( object );
            session.getTransaction().commit();
            return object;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Request update(Request object) {
        if (object == null)
            return null;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            session.update( object );
            session.getTransaction().commit();
            return object;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return object;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static void delete(String id) {
        if (id == null)
            return;
        Request object = getById(id);
        if (object == null)
            return;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return;
        try {
            session.getTransaction().begin();
            session.delete( object );
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Request moveBefore(Request toMove, Request beforeThis) {
        if (toMove == null || beforeThis == null || beforeThis.getOrder() < 1)
            return null;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        long newOrder = -1;
        try {
            session.getTransaction().begin();
            Criteria nextOrderBelow = session.createCriteria(Request.class);
            nextOrderBelow.add(Restrictions.lt("order", beforeThis.getOrder()));
            nextOrderBelow.setProjection(Projections.max("order"));
            Object result = nextOrderBelow.uniqueResult();
            session.getTransaction().commit();
            long nextOrderBelowValue = 0;
            if (result != null)
                nextOrderBelowValue = (long) result;
            newOrder = (beforeThis.getOrder() - nextOrderBelowValue) / 2;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
        if (newOrder > -1) {
            toMove.setOrder(newOrder);
            update(toMove);
        }
        return toMove;
    }

    public static Request moveAfter(Request toMove, Request afterThis) {
        if (toMove == null || afterThis == null || afterThis.getOrder() < 1)
            return null;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        long newOrder = -1;
        try {
            session.getTransaction().begin();
            Criteria nextOrderAbove = session.createCriteria(Request.class);
            nextOrderAbove.add(Restrictions.gt("order", afterThis.getOrder()));
            nextOrderAbove.setProjection(Projections.min("order"));
            Object result = nextOrderAbove.uniqueResult();
            session.getTransaction().commit();
            long nextOrderAboveValue = 0;
            if (result != null)
                nextOrderAboveValue = (long) result;
            newOrder = (nextOrderAboveValue - afterThis.getOrder()) / 2;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
        if (newOrder > -1) {
            toMove.setOrder(newOrder);
            update(toMove);
        }
        return toMove;
    }

    public static Request getNext(int stage, HashSet<Request> filled) {
        if (stage < 1)
            return null;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            DetachedCriteria lowestOrder = DetachedCriteria.forClass(Request.class);
            lowestOrder.add(Restrictions.eq("destinationStage", stage));
            lowestOrder.add(Restrictions.isNull("submitted"));
            lowestOrder.add(Restrictions.isNull("completed"));
            lowestOrder.add(Restrictions.isNull("failed"));
            lowestOrder.setProjection(Projections.min("order"));
            Criteria criteria = session.createCriteria(Request.class);
            criteria.add(Restrictions.eq("destinationStage", stage));
            criteria.add(Restrictions.isNull("submitted"));
            criteria.add(Restrictions.isNull("completed"));
            criteria.add(Restrictions.isNull("failed"));
            criteria.add(Property.forName("order").eq(lowestOrder));
            Request result = (Request) criteria.uniqueResult();
            session.getTransaction().commit();
            return !filled.contains(result) ? result : null;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Request submit(Request toSubmit, Plugin plugin) {
        if (toSubmit == null)
            return null;
        toSubmit.setSubmittedAsDate(new Date());
        toSubmit.setPlugin(plugin);
        update( toSubmit );
        return toSubmit;
    }

    public static Request complete(Request toComplete) {
        if (toComplete == null)
            return null;
        toComplete.setCompletedAsDate(new Date());
        update( toComplete );
        return toComplete;
    }

    public static Request fail(Request toFail, String errorMessage) {
        if (toFail == null)
            return null;
        toFail.setFailedAsDate(new Date());
        MsgEvent failedMsgEvent = toFail.getMsgEvent();
        failedMsgEvent.setMsgBody(errorMessage);
        toFail.setMsgEvent(failedMsgEvent);
        toFail.setErrorMessage(errorMessage);
        update( toFail );
        return toFail;
    }

    public static Request reSubmit(Request oldRequest, String submitter, String reason) {
        if (oldRequest == null)
            return null;
        MsgEvent newMsgEvent = oldRequest.getMsgEvent();
        newMsgEvent.setParam("submitter", submitter);
        newMsgEvent.setMsgBody(reason);
        return create(oldRequest.getSourceStage(), oldRequest.getSourceStep(), oldRequest.getDestinationStage(), newMsgEvent);
    }
}
