package edu.uky.pml.gpms.database.services;

import edu.uky.pml.gpms.database.models.Role;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RoleService {
    private static final Logger logger = LoggerFactory.getLogger(RoleService.class);
    public static HashMap<String, Long> DEFAULT_ROLES;
    static {
        DEFAULT_ROLES = new HashMap<>();
        DEFAULT_ROLES.put("admin", 4000L);
        DEFAULT_ROLES.put("user", 1000L);
        DEFAULT_ROLES.put("banned", -1000L);
    }

    @SuppressWarnings("unchecked")
    public static List<Role> all() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            final List<Role> list = session.createQuery( "from Role" ).list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized Role create(String name, Long rank) {
        Role role = getByName(name);
        if (role != null)
            return role;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            role = new Role(name, rank);
            session.persist( role );
            session.getTransaction().commit();
            return role;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Role getByName(String name) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "from Role where name = :name" );
            query.setString("name", name);
            Role role = (Role) query.uniqueResult();
            session.getTransaction().commit();
            return role;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }
}
