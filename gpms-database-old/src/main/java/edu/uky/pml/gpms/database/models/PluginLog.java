package edu.uky.pml.gpms.database.models;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "plugin_log" )
public class PluginLog {
    @Id
    private String id;

    @ManyToOne
    @Fetch(FetchMode.SELECT)
    private Plugin plugin;

    @Column( name = "stage" )
    private Integer stage;

    @Column( name = "step" )
    private Integer step;

    @Column( name = "state" )
    @Enumerated(EnumType.ORDINAL)
    private LogState state;

    @Column( name = "ts" )
    private Long ts;

    @Column( name = "message", columnDefinition = "longtext" )
    private String message;

    public PluginLog() {
        this.id = java.util.UUID.randomUUID().toString();
        this.ts = new Date().getTime();
    }

    public PluginLog(Plugin plugin) {
        this();
        this.plugin = plugin;
    }

    public PluginLog(Plugin plugin, int stage, int step, String message) {
        this(plugin);
        this.stage = stage;
        this.step = step;
        this.message = message;
    }

    public PluginLog(Plugin plugin, int stage, int step, String message, LogState state) {
        this(plugin, stage, step, message);
        this.state = state;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public Plugin getPlugin() {
        return plugin;
    }
    public void setPlugin(Plugin plugin) {
        this.plugin = plugin;
    }

    public int getStage() {
        return stage;
    }
    public void setStage(int stage) {
        this.stage = stage;
    }

    public int getStep() {
        return step;
    }
    public void setStep(int step) {
        this.step = step;
    }

    public LogState getState() {
        return state;
    }
    public void setState(LogState state) {
        this.state = state;
    }

    public long getTs() {
        return ts;
    }
    public Date getTsAsDate() {
        return new Date(ts);
    }
    public void setTs(long ts) {
        this.ts = ts;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}
