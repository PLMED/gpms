package edu.uky.pml.gpms.database.models;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "sample_output" )
public class SampleOutput {
    @Id
    private String id;

    @ManyToOne
    @Fetch( FetchMode.SELECT )
    private Sample sample;

    @ManyToOne
    @Fetch( FetchMode.SELECT )
    private Plugin plugin;

    @Column( name = "ts" )
    private Long ts;

    @Column( name = "perf_data" )
    private Boolean perfData;

    @Column( name = "log_output", columnDefinition = "longtext")
    private String logOutput;

    public SampleOutput() {
        this.id = java.util.UUID.randomUUID().toString();
        this.ts = new Date().getTime();
        this.perfData = false;
    }

    public SampleOutput(Plugin plugin, Sample sample) {
        this();
        this.plugin = plugin;
        this.sample = sample;
    }

    public SampleOutput(Plugin plugin, Sample sample, String logOutput) {
        this(plugin, sample);
        this.logOutput = logOutput;
    }

    public SampleOutput(Plugin plugin, Sample sample, String logOutput, Boolean perfData) {
        this(plugin, sample, logOutput);
        this.perfData = perfData;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public Sample getSample() {
        return sample;
    }
    public void setSample(Sample sample) {
        this.sample = sample;
    }

    public Plugin getPlugin() {
        return plugin;
    }
    public void setPlugin(Plugin plugin) {
        this.plugin = plugin;
    }

    public Long getTs() {
        return ts;
    }
    public Date getTsAsDate() {
        return new Date(ts);
    }
    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Boolean getPerfData() {
        return perfData;
    }
    public void setPerfData(Boolean perfData) {
        this.perfData = perfData;
    }

    public String getLogOutput() {
        return logOutput;
    }
    public void setLogOutput(String logOutput) {
        this.logOutput = logOutput;
    }
}
