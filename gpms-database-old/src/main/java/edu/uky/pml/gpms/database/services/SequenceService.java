package edu.uky.pml.gpms.database.services;

import edu.uky.pml.gpms.database.models.*;
import edu.uky.pml.gpms.database.models.LogState;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class SequenceService {
    private static final Logger logger = LoggerFactory.getLogger(SequenceService.class);

    @SuppressWarnings("unchecked")
    public static List<Sequence> all() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            final List<Sequence> list = session.createQuery( "from Sequence" ).list();
            session.getTransaction().commit();
            return list;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Sequence getById(String id) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "from Sequence where id = :id" );
            query.setString("id", id);
            final Sequence object = (Sequence) query.uniqueResult();
            session.getTransaction().commit();
            return object;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Sequence getByName(String name) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "from Sequence where name = :name" );
            query.setString("name", name);
            final Sequence object = (Sequence) query.uniqueResult();
            session.getTransaction().commit();
            return object;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized Sequence create(String name) {
        Sequence object = getByName(name);
        if (object != null)
            return object;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            object = new Sequence(name);
            session.save( object );
            session.getTransaction().commit();
            return object;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Sequence update(Sequence object) {
        if (object == null)
            return null;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            session.update( object );
            session.getTransaction().commit();
            return object;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean delete(String id) {
        if (id == null)
            return false;
        Sequence object = getById(id);
        if (object == null)
            return false;
        for (Sample sample : object.getSamples())
            SampleService.delete(sample.getId());
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return false;
        try {
            session.getTransaction().begin();
            Query deleteOutputs = session.createQuery("DELETE FROM SequenceOutput WHERE sequence = :sequence");
            deleteOutputs.setParameter("sequence", object);
            deleteOutputs.executeUpdate();
            Query deleteLogs = session.createQuery("DELETE FROM SequenceLog WHERE sequence = :sequence");
            deleteLogs.setParameter("sequence", object);
            deleteLogs.executeUpdate();
            session.delete( object );
            session.getTransaction().commit();
            return true;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return false;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static void addLog(Plugin plugin, Sequence object, int stage, int step, String message, LogState state) {
        if (object == null)
            return;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return;
        try {
            session.getTransaction().begin();
            session.save( new SequenceLog(plugin, object, stage, step, message, state) );
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static void addOutput(Plugin plugin, Sequence sequence, String output) {
        if (plugin == null)
            return;
        if (sequence == null)
            return;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return;
        try {
            session.getTransaction().begin();
            session.save( new SequenceOutput(plugin, sequence, output) );
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static void addPerfData(Plugin plugin, Sequence sequence, String output) {
        if (plugin == null)
            return;
        if (sequence == null)
            return;
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return;
        try {
            session.getTransaction().begin();
            session.save( new SequenceOutput(plugin, sequence, output, true) );
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getCount() {
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            long count = (Long)session.createCriteria(Sequence.class)
                    .setProjection(Projections.rowCount())
                    .uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getFilteredCount(String filter) {
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Sequence.class);
            criteria.setProjection(Projections.rowCount());
            if (filter != null && !filter.equals(""))
                criteria.add(Restrictions.ilike("name", "%" + filter + "%"));
            long count = (Long)criteria.uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Object[]> list(String sortBy, String dir, String filter, int start, int length) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            String queryString = "select s.id as id, s.name as name, sl.stage as stage, sl.step as step, sl.state as state, sl2.ts as ts from sequence s join sequence_log sl on s.id = sl.sequence_id join sequence_log sl2 on s.id = sl2.sequence_id where sl.ts = (select max(ts) from sequence_log where sequence_id = s.id) and sl2.ts = (select min(ts) from sequence_log where sequence_id = s.id) and name like :filter order by " + sortBy + " " + dir;
            Query query = session.createSQLQuery(queryString);
            query.setString("filter", "%" + filter + "%");
            query.setFirstResult(start);
            query.setMaxResults(length);
            final List list = query.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            logger.error("Exception in SequenceService::list() : {}", e.getMessage());
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getLogCount(String id) {
        Sequence sequence = getById(id);
        if (sequence == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            long count = (Long)session.createCriteria(SequenceLog.class)
                    .add(Restrictions.eq("sequence", sequence))
                    .setProjection(Projections.rowCount())
                    .uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getFilteredLogCount(String id, String filter) {
        Sequence sequence = getById(id);
        if (sequence == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(SequenceLog.class);
            criteria.add(Restrictions.eq("sequence", sequence));
            criteria.setProjection(Projections.rowCount());
            if (filter != null && !filter.equals(""))
                criteria.add(Restrictions.ilike("message", "%" + filter + "%"));
            long count = (Long)criteria.uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<SequenceLog> getLogs(String id, String sortBy, String dir, String filter, int start, int length) {
        Sequence sequence = getById(id);
        if (sequence == null)
            return new ArrayList<>();
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(SequenceLog.class);
            if (id != null)
                criteria.add(Restrictions.eq("sequence", sequence));
            if (sortBy != null && dir != null) {
                switch (dir) {
                    case "asc":
                        criteria.addOrder(Order.asc(sortBy));
                        break;
                    case "desc":
                        criteria.addOrder(Order.desc(sortBy));
                        break;
                }
            }
            if (filter != null && !filter.equals(""))
                criteria.add(Restrictions.ilike("message", "%" + filter + "%"));
            if (start > -1)
                criteria.setFirstResult(start);
            if (length > -1)
                criteria.setMaxResults(length);
            final List<SequenceLog> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println(e.getMessage());
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getSampleCount(String id) {
        Sequence sequence = getById(id);
        if (sequence == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            long count = (Long)session.createCriteria(Sample.class)
                    .add(Restrictions.eq("sequence", sequence))
                    .setProjection(Projections.rowCount())
                    .uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getFilteredSampleCount(String id, String filter) {
        Sequence sequence = getById(id);
        if (sequence == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            System.out.println("Session was null");
            return -1L;
        }
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Sample.class);
            criteria.add(Restrictions.eq("sequence", sequence));
            criteria.setProjection(Projections.rowCount());
            if (filter != null && !filter.equals(""))
                criteria.add(Restrictions.ilike("name", "%" + filter + "%"));
            long count = (Long)criteria.uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static Collection<Sample> getSamples(String id, String sortBy, String dir, String filter, int start, int length) {
        Sequence sequence = getById(id);
        if (sequence == null)
            return new ArrayList<>();
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(Sample.class);
            if (id != null)
                criteria.add(Restrictions.eq("sequence", sequence));
            if (sortBy != null && dir != null) {
                switch (dir) {
                    case "asc":
                        criteria.addOrder(Order.asc(sortBy));
                        break;
                    case "desc":
                        criteria.addOrder(Order.desc(sortBy));
                        break;
                }
            }
            if (filter != null && !filter.equals(""))
                criteria.add(Restrictions.ilike("name", "%" + filter + "%"));
            if (start > -1)
                criteria.setFirstResult(start);
            if (length > -1)
                criteria.setMaxResults(length);
            final Collection<Sample> list = new LinkedHashSet(criteria.list());
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println(e.getMessage());
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getOutputCount(String id) {
        Sequence sequence = getById(id);
        if (sequence == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            logger.error("getOutputCount({}): Session was null", id);
            return -1L;
        }
        try {
            session.getTransaction().begin();
            long count = (Long)session.createCriteria(SequenceOutput.class)
                    .add(Restrictions.eq("sequence", sequence))
                    .add(Restrictions.eq("perfData", false))
                    .setProjection(Projections.rowCount())
                    .uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<SequenceOutput> getOutputList(String id, String sortBy, String order, String filter, int start, int length) {
        Sequence sequence = getById(id);
        if (sequence == null)
            return new ArrayList<>();
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(SequenceOutput.class);
            if (id != null)
                criteria.add(Restrictions.eq("sequence", sequence));
            criteria.add(Restrictions.eq("perfData", false));
            if (sortBy != null && order != null) {
                switch (order) {
                    case "asc":
                        criteria.addOrder(Order.asc(sortBy));
                        break;
                    case "desc":
                        criteria.addOrder(Order.desc(sortBy));
                        break;
                }
            }
            if (start > -1)
                criteria.setFirstResult(start);
            if (length > -1)
                criteria.setMaxResults(length);
            final List<SequenceOutput> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getPerfDataCount(String id) {
        Sequence sequence = getById(id);
        if (sequence == null)
            return -1L;
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            logger.error("getOutputCount({}): Session was null", id);
            return -1L;
        }
        try {
            session.getTransaction().begin();
            long count = (Long)session.createCriteria(SequenceOutput.class)
                    .add(Restrictions.eq("sequence", sequence))
                    .add(Restrictions.eq("perfData", true))
                    .setProjection(Projections.rowCount())
                    .uniqueResult();
            session.getTransaction().commit();
            return count;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<SequenceOutput> getPerfDataList(String id, String sortBy, String order, String filter, int start, int length) {
        Sequence sequence = getById(id);
        if (sequence == null)
            return new ArrayList<>();
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            Criteria criteria = session.createCriteria(SequenceOutput.class);
            if (id != null)
                criteria.add(Restrictions.eq("sequence", sequence));
            criteria.add(Restrictions.eq("perfData", true));
            if (sortBy != null && order != null) {
                switch (order) {
                    case "asc":
                        criteria.addOrder(Order.asc(sortBy));
                        break;
                    case "desc":
                        criteria.addOrder(Order.desc(sortBy));
                        break;
                }
            }
            if (start > -1)
                criteria.setFirstResult(start);
            if (length > -1)
                criteria.setMaxResults(length);
            final List<SequenceOutput> list = criteria.list();
            session.getTransaction().commit();
            return list;
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static SequenceOutput getOutputById(String id) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "from SequenceOutput where id = :id" );
            query.setString("id", id);
            SequenceOutput object = (SequenceOutput) query.uniqueResult();
            session.getTransaction().commit();
            return object;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static int maxStep() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return -1;
        try {
            session.getTransaction().begin();
            Criteria criteria = session
                    .createCriteria(SequenceLog.class)
                    .setProjection(Projections.max("step"));
            int max = (int) criteria.uniqueResult();
            session.getTransaction().commit();
            return max;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static int maxStage() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return -1;
        try {
            session.getTransaction().begin();
            Criteria criteria = session
                    .createCriteria(SequenceLog.class)
                    .setProjection(Projections.max("stage"));
            int max = (int) criteria.uniqueResult();
            session.getTransaction().commit();
            return max;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return -1;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static SequenceLog firstLog(Sequence object) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Criteria getTS = session
                    .createCriteria(SequenceLog.class)
                    .add(Restrictions.eq("sequence", object))
                    .addOrder(Order.asc("ts"))
                    .setMaxResults(1);
            List<SequenceLog> logs = getTS.list();
            session.getTransaction().commit();
            if (logs.size() > 0)
                return logs.get(0);
            return null;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static SequenceLog latestLog(Sequence object) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Criteria getTS = session
                    .createCriteria(SequenceLog.class)
                    .add(Restrictions.eq("sequence", object))
                    .addOrder(Order.desc("ts"))
                    .setMaxResults(1);
            List<SequenceLog> logs = getTS.list();
            session.getTransaction().commit();
            if (logs.size() > 0)
                return logs.get(0);
            return null;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static SequenceLog latestLogForPlugin(Plugin object) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Criteria getTS = session
                    .createCriteria(SequenceLog.class)
                    .add(Restrictions.eq("plugin", object))
                    .addOrder(Order.desc("ts"))
                    .setMaxResults(1);
            List<SequenceLog> logs = getTS.list();
            session.getTransaction().commit();
            if (logs.size() > 0)
                return logs.get(0);
            return null;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static List<Sample> samplesForSequence(Sequence object) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Criteria criteria = session
                    .createCriteria(Sample.class)
                    .add(Restrictions.eq("sequence", object));
            List<Sample> results = criteria.list();
            session.getTransaction().commit();
            if (results.size() > 0)
                return results;
            return null;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isFinished(Sequence object) {
        if (object == null)
            return false;
        List<Sample> samples = samplesForSequence(object);
        if (samples == null)
            return false;
        for (Sample sample : samples)
            if (!SampleService.isFinished(sample))
                return false;
        return true;
    }
}
