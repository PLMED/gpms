package edu.uky.pml.gpms.database.models;

import edu.uky.pml.gpms.database.services.PluginService;
import edu.uky.pml.gpms.database.services.SampleService;
import edu.uky.pml.gpms.database.services.SequenceService;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "plugin" )
public class Plugin {
    private static final int MISSING_THRESHOLD = 15 * 1000;

    @Id
    private String id;

    @Column( name = "plugin_region" )
    private String regionID;

    @Column( name = "plugin_agent" )
    private String agentID;

    @Column( name = "plugin_ident" )
    private String pluginID;

    @Column( name = "transfer_status_file" )
    private String transfer_status_file;

    @Column( name = "transfer_watch_file" )
    private String transfer_watch_file;

    @Column( name = "bucket_name" )
    private String bucket_name;

    @Column( name = "endpoint" )
    private String endpoint;

    public Plugin() {
        this.id = java.util.UUID.randomUUID().toString();
    }

    public Plugin(String regionID, String agentID, String pluginID) {
        this();
        this.regionID = regionID;
        this.agentID = agentID;
        this.pluginID = pluginID;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getRegionID() {
        return regionID;
    }
    public void setRegionID(String regionID) {
        this.regionID = regionID;
    }

    public String getAgentID() {
        return agentID;
    }
    public void setAgentID(String agentID) {
        this.agentID = agentID;
    }

    public String getPluginID() {
        return pluginID;
    }
    public void setPluginID(String pluginID) {
        this.pluginID = pluginID;
    }

    public String getName() {
        return regionID + " - " + agentID + " : " + pluginID;
    }

    public String getTransferStatusFile() {
        return transfer_status_file;
    }
    public void setTransferStatusFile(String transfer_status_file) {
        this.transfer_status_file = transfer_status_file;
    }

    public String getTransferWatchFile() {
        return transfer_watch_file;
    }
    public void setTransferWatchFile(String transfer_watch_file) {
        this.transfer_watch_file = transfer_watch_file;
    }

    public String getBucketName() {
        return bucket_name;
    }
    public void setBucketName(String bucket_name) {
        this.bucket_name = bucket_name;
    }

    public String getEndpoint() {
        return endpoint;
    }
    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public int getCurrentStage() {
        PluginLog latest;
        if ((latest = PluginService.latestLog(this)) == null)
            return -1;
        return latest.getStage();
    }

    public int getCurrentStep() {
        PluginLog latest;
        if ((latest = PluginService.latestLog(this)) == null)
            return -1;
        return latest.getStep();
    }

    public LogState getCurrentState() {
        PluginLog latestPluginLog = PluginService.latestLog(this);
        if (latestPluginLog == null)
            return LogState.SHUTDOWN;
        Date latestTS;
        LogState latestState;
        latestTS = latestPluginLog.getTsAsDate();
        latestState = latestPluginLog.getState();
        SequenceLog latestSequenceLog = SequenceService.latestLogForPlugin(this);
        if (latestSequenceLog != null && latestSequenceLog.getTsAsDate().after(latestTS)) {
            latestTS = latestSequenceLog.getTsAsDate();
            latestState = latestSequenceLog.getState();
        }
        SampleLog latestSampleLog = SampleService.latestLogForPlugin(this);
        if (latestSampleLog != null && latestSampleLog.getTsAsDate().after(latestTS)) {
            latestState = latestSampleLog.getState();
        }
        return latestState;
    }

    public String toJson() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");

        sb.append("\"id\":\"");
        sb.append(id);
        sb.append("\",");

        sb.append("\"region\":\"");
        if (regionID != null) { sb.append(regionID); }
        sb.append("\",");

        sb.append("\"agent\":\"");
        if (agentID != null) { sb.append(agentID); }
        sb.append("\",");

        sb.append("\"pluginID\":\"");
        if (pluginID != null) { sb.append(pluginID); }
        sb.append("\",");

        sb.append("\"transfer_status_file\":\"");
        if (transfer_status_file != null) { sb.append(transfer_status_file); }
        sb.append("\",");

        sb.append("\"transfer_watch_file\":\"");
        if (transfer_watch_file != null) { sb.append(transfer_watch_file); }
        sb.append("\",");

        sb.append("\"bucket_name\":\"");
        if (bucket_name != null) { sb.append(bucket_name); }
        sb.append("\",");

        sb.append("\"endpoint\":\"");
        if (endpoint != null) { sb.append(endpoint); }
        sb.append("\",");

        sb.append("\"state\":\"");
        sb.append(getCurrentState());
        sb.append("\",");

        sb.append("\"max_stage\":");
        sb.append(PluginService.maxStage());
        sb.append(",");

        sb.append("\"stage\":");
        sb.append(getCurrentStage());
        sb.append(",");

        sb.append("\"max_step\":");
        sb.append(PluginService.maxStep());
        sb.append(",");

        sb.append("\"step\":");
        sb.append(getCurrentStep());

        sb.append("}");
        return sb.toString();
    }
}
