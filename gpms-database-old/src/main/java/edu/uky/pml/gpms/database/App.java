package edu.uky.pml.gpms.database;

import ch.qos.logback.classic.Level;
import edu.uky.pml.gpms.database.models.*;
import edu.uky.pml.gpms.database.services.*;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import edu.uky.pml.gpms.utilities.logging.BasicGPMSLogger;
import edu.uky.pml.gpms.utilities.logging.GPMSLogger;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static net.sourceforge.argparse4j.impl.Arguments.storeTrue;

public class App {
    //private static final Logger logger = LoggerFactory.getLogger(App.class);
    private static final GPMSLogger logger = new BasicGPMSLogger(App.class);

    /**
     * App method.
     * @param args - Main execution arguments
     */
    public static void main(String[] args) {
        System.setProperty("com.mchange.v2.log.MLog", "com.mchange.v2.log.FallbackMLog");
        System.setProperty("com.mchange.v2.log.FallbackMLog.DEFAULT_CUTOFF_LEVEL", "WARNING");

        ArgumentParser parser = ArgumentParsers.newFor("java -jar gpms-database-old.jar").build()
                .defaultHelp(true)
                .description("Manager for data in old version of UKHC GPMS database.");
        parser.addArgument("-d", "--debug").action(storeTrue())
                .help("Turn on debug logging");
        parser.addArgument("-a").dest("action")
                .choices("list", "get", "create", "delete", "update")
                .setDefault("list")
                .help("Action to perform");
        parser.addArgument("-t").dest("type")
                .choices("user", "role", "sequence", "sample", "samplesheet", "request", "plugin")
                .setDefault("user")
                .help("Type of data for action");
        parser.addArgument("params").nargs("*");
        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }

        if (ns.getBoolean("debug")) {
            ((ch.qos.logback.classic.Logger)LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).setLevel(Level.TRACE);
        }

        logger.trace("Connecting/verifying database");
        Session verify = SessionFactoryManager.getSession();
        if (verify == null) {
            logger.error("Failed to connect to the database");
            System.exit(1);
        }
        verify.close();
        try {
            String action = ns.getString("action"),
                    type = ns.get("type");
            List<String> params = ns.getList("params");

            switch (type) {
                case "user":
                    switch (action) {
                        case "list":
                            logger.trace("Listing users");
                            for (User user : UserService.all())
                                logger.info("User: {}", user.getUsername());
                            logger.trace("Listing users (again)");
                            for (User user : UserService.all())
                                logger.info("User: {}", user.getUsername());
                            break;
                        case "get":
                            break;
                        case "create":
                            break;
                        case "delete":
                            break;
                        case "update":
                            break;
                    }
                    break;
                case "role":
                    switch (action) {
                        case "list":
                            break;
                        case "get":
                            break;
                        case "create":
                            break;
                        case "delete":
                            break;
                        case "update":
                            break;
                    }
                    break;
                case "sequence":
                    switch (action) {
                        case "list":
                            break;
                        case "get":
                            break;
                        case "create":
                            break;
                        case "delete":
                            break;
                        case "update":
                            break;
                    }
                    break;
                case "sample":
                    switch (action) {
                        case "list":
                            break;
                        case "get":
                            break;
                        case "create":
                            break;
                        case "delete":
                            break;
                        case "update":
                            break;
                    }
                    break;
                case "samplesheet":
                    switch (action) {
                        case "list":
                            break;
                        case "get":
                            break;
                        case "create":
                            break;
                        case "delete":
                            break;
                        case "update":
                            break;
                    }
                    break;
                case "request":
                    switch (action) {
                        case "list":
                            break;
                        case "get":
                            break;
                        case "create":
                            break;
                        case "delete":
                            break;
                        case "update":
                            break;
                    }
                    break;
                case "plugin":
                    switch (action) {
                        case "list":
                            break;
                        case "get":
                            break;
                        case "create":
                            break;
                        case "delete":
                            break;
                        case "update":
                            break;
                    }
                    break;
            }
        } catch (Exception e) {
            logger.error("Exception:\n{}", ExceptionUtils.getStackTrace(e));
        } finally {
            SessionFactoryManager.close();
        }
    }
}

