package edu.uky.pml.gpms.database.models;

import com.google.gson.Gson;

import javax.persistence.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Entity
@Table( name = "user" )
public class User {
    @Id
    private String id;

    @Column( name = "decommissioned" )
    private Boolean decommissioned;

    @Column( name = "username", unique = true, nullable = false )
    private String username;
    @Column( name = "email", unique = true )
    private String email;
    @Column( name = "password", nullable = false )
    private String password;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn( name = "user_info_id" )
    private UserInfo info;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn( name = "user_role_id" )
    private UserRole userRole;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn( name = "user_id" )
    private Set<UserSession> sessions = new HashSet<>();

    @OneToMany(cascade = CascadeType.DETACH)
    @JoinColumn( name = "operator_id" )
    private Set<UserAudit> auditsBy = new HashSet<>();

    @OneToMany(cascade = CascadeType.DETACH)
    @JoinColumn( name = "target_id" )
    private Set<UserAudit> auditsOn = new HashSet<>();

    public User() {
        this.id = java.util.UUID.randomUUID().toString();
        this.decommissioned = false;
    }

    public User(String username, String email, String password) {
        this();
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public boolean getDecommissioned() {
        return decommissioned;
    }
    public void setDecommissioned(boolean decommissioned) {
        this.decommissioned = decommissioned;
    }
    public void decommission() {
        this.decommissioned = true;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public UserInfo getInfo() {
        return info;
    }
    public void setInfo(UserInfo info) {
        this.info = info;
        this.info.setUser(this);
    }

    public UserRole getUserRole() {
        return userRole;
    }
    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public Role getRole() {
        return userRole.getRole();
    }

    public Set<UserSession> getSessions() {
        return sessions;
    }
    public void setSessions(Set<UserSession> sessions) {
        this.sessions = sessions;
    }

    public Set<UserAudit> getAuditsBy() {
        return auditsBy;
    }
    public void setAuditsBy(Set<UserAudit> auditsBy) {
        this.auditsBy = auditsBy;
    }

    public Set<UserAudit> getAuditsOn() {
        return auditsOn;
    }
    public void setAuditsOn(Set<UserAudit> auditsOn) {
        this.auditsOn = auditsOn;
    }

    public String toJson() {
        Map<String, Object> json = new HashMap<>();
        json.put("id", getId());
        json.put("username", getUsername());
        json.put("email", getEmail());
        json.put("role", getRole().toJson());
        json.put("info", getInfo().toJson());
        return new Gson().toJson(json);
    }

    public String toInfoJson() {
        Map<String, Object> json = new HashMap<>();
        json.put("username", getUsername());
        json.put("info", getInfo().toJson());
        return new Gson().toJson(json);
    }
}
