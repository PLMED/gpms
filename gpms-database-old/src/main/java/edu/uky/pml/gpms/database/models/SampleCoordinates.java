package edu.uky.pml.gpms.database.models;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table( name = "sample_coordinates" )
public class SampleCoordinates {
    public static final String NO_COORDINATES_DEFAULT = "21    99999999    .   .   .";

    @Id
    private String id;

    @Column( name = "name" )
    private String name;

    @Column( name = "created" )
    private Long created;

    @Column( name = "line_number" )
    private int lineNumber;

    @ManyToOne
    @Fetch( FetchMode.SELECT )
    private SampleSheet sample_sheet;

    @Column( name = "has_coordinates" )
    private Boolean hasCoordinates;

    @Column( name = "coordinates", columnDefinition = "longtext" )
    private String coordinates;

    /*@OneToMany( fetch = FetchType.EAGER )
    @JoinColumn( name = "sample_coordinates_id" )
    private Set<Sample> samples = new HashSet<>();*/

    public SampleCoordinates() {
        this.created = new Date().getTime();
        this.id = java.util.UUID.randomUUID().toString();
    }

    public SampleCoordinates(String name, boolean hasCoordinates, String coordinates) {
        this();
        this.name = name;
        this.hasCoordinates = hasCoordinates;
        this.coordinates = coordinates;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getCreated() {
        return created;
    }
    public Date getCreatedAsDate() {
        return new Date(created);
    }

    public int getLineNumber() {
        return lineNumber;
    }
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public SampleSheet getSampleSheet() {
        return sample_sheet;
    }

    public void setSampleSheet(SampleSheet sample_sheet) {
        this.sample_sheet = sample_sheet;
    }

    public Boolean getHasCoordinates() {
        return hasCoordinates;
    }

    public String getCoordinates() {
        return (hasCoordinates) ? coordinates : NO_COORDINATES_DEFAULT;
    }

    /*public Set<Sample> getSamples() {
        return samples;
    }*/
}
