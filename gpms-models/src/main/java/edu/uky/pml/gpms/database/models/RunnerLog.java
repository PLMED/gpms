package edu.uky.pml.gpms.database.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "runner_log" )
public class RunnerLog extends GenericLogModel {
    @NotNull
    @ManyToOne
    @JoinColumn( name = "runner_id", nullable = false )
    private Runner runner;

    public RunnerLog() {
        super();
    }

    public RunnerLog(Runner runner, TaskType taskType, int step, LogState state, String message) {
        super(taskType, step, state, message);
        this.runner = runner;
    }

    public Runner getRunner() {
        return runner;
    }
}
