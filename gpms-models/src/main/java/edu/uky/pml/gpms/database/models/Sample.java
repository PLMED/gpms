package edu.uky.pml.gpms.database.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "sample" )
public class Sample extends GenericModel {
    @NotNull
    @Column( name = "name", nullable = false )
    private String name;

    @ManyToOne
    @JoinColumn( name = "flow_cell_id" )
    private FlowCell flowCell;

    @ManyToOne
    @JoinColumn( name = "sample_sheet_id" )
    private SampleSheet sampleSheet;

    @OneToMany( mappedBy = "sample" )
    private transient List<SampleLog> logs = new ArrayList<>();

    @OneToOne
    private SampleLog latest;

    public Sample() {
        super();
    }

    public Sample(String name) {
        this();
        this.name = name;
    }

    public Sample(FlowCell flowCell, String name) {
        this(name);
        this.flowCell = flowCell;
    }

    public Sample(SampleSheet sampleSheet, String name) {
        this(name);
        this.sampleSheet = sampleSheet;
    }

    public String getName() {
        return name;
    }

    public FlowCell getFlowCell() {
        return flowCell;
    }
    public void setFlowCell(FlowCell flowCell) {
        this.flowCell = flowCell;
    }

    public SampleSheet getSampleSheet() {
        return sampleSheet;
    }
    public void setSampleSheet(SampleSheet sampleSheet) {
        this.sampleSheet = sampleSheet;
    }

    public List<SampleLog> getLogs() {
        return logs;
    }

    public SampleLog getLatest() {
        return latest;
    }

    public void setLatest(SampleLog latest) {
        this.latest = latest;
    }
}
