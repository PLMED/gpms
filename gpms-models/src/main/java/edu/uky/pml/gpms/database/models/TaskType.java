package edu.uky.pml.gpms.database.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "task_type" )
public class TaskType extends GenericModel {
    @NotNull
    @Size( max = 36 )
    @Column( name = "tag", nullable = false )
    private String tag;

    @NotNull
    @Size( max = 255 )
    @Column( name = "name", nullable = false )
    private String name;

    public TaskType() {
        super();
    }

    public TaskType(String tag, String name) {
        this();
        this.tag = tag;
        this.name = name;
    }

    public String getTag() {
        return tag;
    }

    public String getName() {
        return name;
    }
}
