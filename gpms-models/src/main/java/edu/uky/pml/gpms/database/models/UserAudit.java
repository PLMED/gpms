package edu.uky.pml.gpms.database.models;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "user_audit" )
public class UserAudit extends GenericModel {
    public enum Type {
        CREATED(0), UPDATED(1), REMOVED(2);
        private int value;
        Type(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    @Column( name = "type", nullable = false )
    private Type type;

    @ManyToOne
    @JoinColumn( name = "operator_id" )
    private User operator;

    @NotNull
    @ManyToOne
    @JoinColumn( name = "user_id", nullable = false )
    private User user;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column( name = "ts" )
    private Date ts;

    @Column( name = "message" )
    private String message;

    public UserAudit() { }

    public UserAudit(Type type, User operator, User user) {
        this();
        this.type = type;
        this.operator = operator;
        this.user = user;
    }

    public UserAudit(Type type, User operator, User target, String message) {
        this(type, operator, target);
        this.message = message;
    }

    public Type getType() {
        return type;
    }

    public User getOperator() {
        return operator;
    }

    public User getUser() {
        return user;
    }

    public Date getTs() {
        return ts;
    }

    public String getMessage() {
        return message;
    }
}
