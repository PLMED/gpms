package edu.uky.pml.gpms.database.models;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "sample_sheet" )
public class SampleSheet extends GenericModel {
    @NotNull
    @ManyToOne
    @JoinColumn( name = "owner_id", nullable = false )
    private User owner;

    @CreationTimestamp
    @Temporal( TemporalType.TIMESTAMP )
    @Column( name = "created" )
    private Date created;

    @Column( name = "errors", columnDefinition = "longtext" )
    private String errors;

    @NotNull
    @Column( name = "contents", columnDefinition = "longtext", nullable = false )
    private String contents;

    @NotNull
    @Size( max = 32 )
    @Column( name = "hash", nullable = false, unique = true )
    private String hash;

    @OneToMany( mappedBy = "sampleSheet" )
    private transient List<Sample> samples = new ArrayList<>();

    public SampleSheet() { }

    public SampleSheet(User owner, String contents) {
        this();
        this.owner = owner;
        this.contents = contents;
        this.hash = DigestUtils.md5Hex(contents).toLowerCase();
    }

    public SampleSheet(User owner, String contents, String errors) {
        this(owner, contents);
        this.errors = errors;
    }

    public User getOwner() {
        return owner;
    }

    public Date getCreated() {
        return created;
    }

    public String getErrors() {
        return errors;
    }

    public String getContents() {
        return contents;
    }

    public String getHash() {
        return hash;
    }

    public List<Sample> getSamples() {
        return samples;
    }
}
