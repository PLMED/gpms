package edu.uky.pml.gpms.database.models;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "runner" )
public class Runner extends GenericModel {
    @Size( max = 255 )
    @Column( name = "region" )
    private String region;

    @Size( max = 255 )
    @Column( name = "agent" )
    private String agent;

    @Size( max = 255 )
    @Column( name = "plugin" )
    private String plugin;

    @Size( max = 36 )
    @Column( name = "instance", unique = true )
    private String instance;

    @OneToMany( mappedBy = "runner" )
    private transient List<RunnerLog> logs = new ArrayList<>();

    @OneToOne
    private RunnerLog latest;

    public Runner() { }

    public Runner(String instance) {
        this();
        this.instance = instance;
    }

    public Runner(String region, String agent, String plugin) {
        this();
        this.region = region;
        this.agent = agent;
        this.plugin = plugin;
    }

    public Runner(String region, String agent, String plugin, String instance) {
        this(region, agent, plugin);
        this.instance = instance;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getPlugin() {
        return plugin;
    }

    public void setPlugin(String plugin) {
        this.plugin = plugin;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public List<RunnerLog> getLogs() {
        return logs;
    }

    public RunnerLog getLatest() {
        return latest;
    }

    public void setLatest(RunnerLog latest) {
        this.latest = latest;
    }
}
