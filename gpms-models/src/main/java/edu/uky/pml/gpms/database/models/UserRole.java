package edu.uky.pml.gpms.database.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "user_role" )
public class UserRole extends GenericModel {
    @NotNull
    @ManyToOne
    @JoinColumn( name = "user_id", nullable = false )

    private User user;
    @NotNull
    @ManyToOne
    @JoinColumn( name = "role_id", nullable = false )
    private Role role;

    public UserRole() {
        super();
    }

    public UserRole(User user, Role role) {
        this();
        this.user = user;
        this.role = role;
    }

    public User getUser() {
        return user;
    }

    public Role getRole() {
        return role;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserRole i = (UserRole) o;
        return Objects.equals(getId(), i.getId())
                && Objects.equals(getUser(), i.getUser())
                && Objects.equals(getRole(), i.getRole());
    }
}
