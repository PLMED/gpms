package edu.uky.pml.gpms.database.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "task" )
public class Task extends GenericModel {
    @NotNull
    @ManyToOne
    @JoinColumn( name = "task_type_id" )
    private TaskType taskType;

    @ManyToOne
    @JoinColumn( name = "sample_id" )
    private Sample sample;

    @ManyToOne
    @JoinColumn( name = "flow_cell_id" )
    private FlowCell flowCell;

    @OneToMany( mappedBy = "task" )
    private transient List<TaskLog> logs = new ArrayList<>();

    @OneToOne
    private TaskLog latest;

    public Task() {
        super();
    }

    public Task(TaskType taskType) {
        this();
        this.taskType = taskType;
    }

    public Task(TaskType taskType, Sample sample) {
        this(taskType);
        this.sample = sample;
    }

    public Task(TaskType taskType, FlowCell flowCell) {
        this(taskType);
        this.flowCell = flowCell;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public Sample getSample() {
        return sample;
    }

    public FlowCell getFlowCell() {
        return flowCell;
    }

    public List<TaskLog> getLogs() {
        return logs;
    }

    public TaskLog getLatest() {
        return latest;
    }

    public void setLatest(TaskLog latest) {
        this.latest = latest;
    }
}
