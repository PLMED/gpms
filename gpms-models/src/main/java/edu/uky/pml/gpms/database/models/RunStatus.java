package edu.uky.pml.gpms.database.models;

public enum RunStatus {
    PENDING(0), SUBMITTED(1), FINISHED(2), CANCELLED(3), FAILED(4);
    private int value;
    RunStatus(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}
