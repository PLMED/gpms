package edu.uky.pml.gpms.database.models;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "run" )
public class Run extends GenericModel {
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column( name = "created" )
    private Date created;

    @Enumerated(EnumType.ORDINAL)
    @Column( name = "status" )
    private RunStatus status;

    @ManyToOne
    @JoinColumn( name = "runner_id" )
    private Runner runner;

    @NotNull
    @ManyToOne
    @JoinColumn( name = "task_id" )
    private Task task;

    @OneToMany( mappedBy = "run" )
    private List<RunLog> logs = new ArrayList<>();

    @OneToOne
    private RunLog latest;

    @OneToMany( mappedBy = "run" )
    private List<RunOutput> outputs = new ArrayList<>();

    public Run() {
        super();
    }

    public Run(Task task) {
        this();
        this.task = task;
    }

    public Date getCreated() {
        return created;
    }

    public RunStatus getStatus() {
        return status;
    }

    public void setStatus(RunStatus status) {
        this.status = status;
    }

    public Runner getRunner() {
        return runner;
    }

    public void setRunner(Runner runner) {
        this.runner = runner;
    }

    public Task getTask() {
        return task;
    }

    public List<RunLog> getLogs() {
        return logs;
    }

    public RunLog getLatest() {
        return latest;
    }
}
