package edu.uky.pml.gpms.database.models;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "run_log" )
public class RunLog extends GenericModel {
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column( name = "ts" )
    private Date ts;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    @Column( name = "state" )
    private LogState state;

    @NotNull
    @Column( name = "step" )
    private int step;

    @NotNull
    @Column( name = "message", columnDefinition = "longtext", nullable = false )
    private String message;

    @NotNull
    @ManyToOne
    @JoinColumn( name = "run_id" )
    private Run run;

    public RunLog() {
        super();
    }

    public RunLog(Run run, LogState state, int step, String message) {
        this();
        this.run = run;
        this.state = state;
        this.step = step;
        this.message = message;
    }

    public Date getTs() {
        return ts;
    }

    public LogState getState() {
        return state;
    }

    public int getStep() {
        return step;
    }

    public String getMessage() {
        return message;
    }

    public Run getRun() {
        return run;
    }
}
