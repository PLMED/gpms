package edu.uky.pml.gpms.database.models;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "sample_log" )
public class SampleLog extends GenericLogModel {
    @NotNull
    @ManyToOne
    @JoinColumn( name = "sample_id", nullable = false )
    private Sample sample;

    public SampleLog() {
        super();
    }

    public SampleLog(Sample sample, TaskType taskType, int step, LogState state, String message) {
        super(taskType, step, state, message);
        this.sample = sample;
    }

    public Sample getSample() {
        return sample;
    }
}
