package edu.uky.pml.gpms.database.models;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "task_log" )
public class TaskLog extends GenericModel {
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column( name = "ts" )
    private Date ts;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    @Column( name = "state" )
    private LogState state;

    @NotNull
    @Column( name = "message", columnDefinition = "longtext", nullable = false )
    private String message;

    @ManyToOne
    @JoinColumn( name = "task_id" )
    private Task task;

    public TaskLog() {
        super();
    }

    public TaskLog(Task task, LogState state, String message) {
        this();
        this.task = task;
        this.state = state;
        this.message = message;
    }

    public Date getTs() {
        return ts;
    }

    public LogState getState() {
        return state;
    }

    public String getMessage() {
        return message;
    }

    public Task getTask() {
        return task;
    }
}
