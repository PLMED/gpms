package edu.uky.pml.gpms.database.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "user" )
public class User extends GenericModel {
    @NotNull
    @Size(min = 4, max = 20)
    @Column( name = "username", unique = true, nullable = false )
    private String username;

    @NotNull
    @Email
    @Size(min = 3, max = 255)
    @Column( name = "email", unique = true, nullable = false)
    private String email;

    @Size(max = 255)
    @Column( name = "last_name" )
    private String lastName;

    @Size(max = 255)
    @Column( name = "first_name" )
    private String firstName;

    @OneToMany( mappedBy = "user" )
    private transient List<UserRole> roles = new ArrayList<>();

    @OneToMany( mappedBy = "user" )
    private transient List<UserSession> sessions = new ArrayList<>();

    @OneToMany( mappedBy = "user" )
    private transient List<UserAudit> auditsOn = new ArrayList<>();

    @OneToMany( mappedBy = "operator" )
    private transient List<UserAudit> auditsBy = new ArrayList<>();

    @OneToMany( mappedBy = "owner" )
    private transient List<SampleSheet> sampleSheets = new ArrayList<>();

    public User() {
        super();
    }

    public User(String username, String email) {
        this();
        this.username = username;
        this.email = email;
    }

    public User (String username, String email, String firstName, String lastName) {
        this(username, email);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public List<UserRole> getRoles() {
        return roles;
    }

    public List<UserSession> getSessions() {
        return sessions;
    }

    public List<UserAudit> getAuditsOn() {
        return auditsOn;
    }

    public List<UserAudit> getAuditsBy() {
        return auditsBy;
    }

    public List<SampleSheet> getSampleSheets() {
        return sampleSheets;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        User i = (User) o;
        return Objects.equals(getId(), i.getId())
                && Objects.equals(getUsername(), i.getUsername())
                && Objects.equals(getEmail(), i.getEmail())
                && Objects.equals(getFirstName(), i.getFirstName())
                && Objects.equals(getLastName(), i.getLastName());
    }
}
