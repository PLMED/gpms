package edu.uky.pml.gpms.database.models;

import com.google.gson.Gson;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@MappedSuperclass
public class GenericModel {
    @Id
    @GeneratedValue( generator = "UUID" )
    @GenericGenerator( name = "UUID", strategy = "org.hibernate.id.UUIDGenerator" )
    @Column( name = "id", updatable = false, nullable = false )
    private UUID id;

    public GenericModel() { }

    public UUID getId() {
        return id;
    }

    public String json() {
        return new Gson().toJson(this);
    }
}
