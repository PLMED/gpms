package edu.uky.pml.gpms.database.models;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "run_output" )
public class RunOutput extends GenericModel {
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column( name = "ts" )
    private Date ts;

    @NotNull
    @Column( name = "output", columnDefinition = "longtext" )
    private String output;

    @NotNull
    @ManyToOne
    @JoinColumn( name = "run_id" )
    private Run run;

    public RunOutput() {
        super();
    }

    public RunOutput(Run run, String output) {
        this();
        this.run = run;
        this.output = output;
    }

    public Date getTs() {
        return ts;
    }

    public String getOutput() {
        return output;
    }

    public Run getRun() {
        return run;
    }
}
