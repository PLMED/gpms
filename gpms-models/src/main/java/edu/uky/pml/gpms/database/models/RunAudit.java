package edu.uky.pml.gpms.database.models;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "run_audit" )
public class RunAudit extends GenericModel {
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column( name = "ts" )
    private Date ts;

    @Enumerated(EnumType.ORDINAL)
    @Column( name = "status" )
    private RunStatus status;

    @ManyToOne
    @JoinColumn( name = "operator_id" )
    private User operator;

    @NotNull
    @Column( name = "message" )
    private String message;

    @NotNull
    @ManyToOne
    @JoinColumn( name = "run_id" )
    private Run run;

    public RunAudit() {
        super();
    }

    public RunAudit(Run run, RunStatus status, User operator, String message) {
        this();
        this.run = run;
        this.status = status;
        this.operator = operator;
        this.message = message;
    }

    public Date getTs() {
        return ts;
    }

    public RunStatus getStatus() {
        return status;
    }

    public User getOperator() {
        return operator;
    }

    public String getMessage() {
        return message;
    }

    public Run getRun() {
        return run;
    }
}
