package edu.uky.pml.gpms.database.models;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings("unused")
@Entity
@Table( name = "flow_cell_log" )
public class FlowCellLog extends GenericLogModel {
    @NotNull
    @ManyToOne
    @JoinColumn( name = "flow_cell_id", nullable = false )
    private FlowCell flowCell;

    public FlowCellLog() {
        super();
    }

    public FlowCellLog(FlowCell flowCell, TaskType taskType, int step, LogState state, String message) {
        super(taskType, step, state, message);
        this.flowCell = flowCell;
    }

    public FlowCell getFlowCell() {
        return flowCell;
    }
}
