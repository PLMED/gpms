package edu.uky.pml.gpms.database.models;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@SuppressWarnings({"unused", "WeakerAccess"})
public class GenericLogModel extends GenericModel {
    @CreationTimestamp
    @Temporal( TemporalType.TIMESTAMP )
    @Column( name = "ts" )
    private Date ts;

    @NotNull
    @ManyToOne
    @JoinColumn( name = "task_type_id", nullable = false )
    private TaskType taskType;

    @NotNull
    @Column( name = "step", nullable = false )
    private int step;

    @NotNull
    @Enumerated( EnumType.ORDINAL )
    @Column( name = "state", nullable = false )
    private LogState state;

    @NotNull
    @Column( name = "message", columnDefinition = "longtext", nullable = false )
    private String message;

    public GenericLogModel() {
        super();
    }

    public GenericLogModel(TaskType taskType, int step, LogState state, String message) {
        this();
        this.taskType = taskType;
        this.step = step;
        this.state = state;
        this.message = message;
    }

    public Date getTs() {
        return ts;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public int getStep() {
        return step;
    }

    public LogState getState() {
        return state;
    }

    public String getMessage() {
        return message;
    }
}
