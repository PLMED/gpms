package edu.uky.pml.gpms.database.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
@Entity
@Table( name = "flow_cell" )
public class FlowCell extends GenericModel {
    @NotNull
    @Column( name = "name", nullable = false )
    private String name;

    @OneToMany( mappedBy = "flowCell" )
    private transient List<FlowCellLog> logs = new ArrayList<>();

    @OneToOne
    private FlowCellLog latest;

    public FlowCell() {
        super();
    }

    public FlowCell(String name) {
        this();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<FlowCellLog> getLogs() {
        return logs;
    }

    public FlowCellLog getLatest() {
        return latest;
    }

    public void setLatest(FlowCellLog latest) {
        this.latest = latest;
    }
}
