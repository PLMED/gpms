package edu.uky.pml.gpms.database.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;

@SuppressWarnings({"unused", "WeakerAccess"})
@Entity
@Table( name = "role" )
public class Role extends GenericModel {
    public enum Type {
        Administrator, Director, User, Viewer
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    private Type type;

    @OneToMany( mappedBy = "role" )
    private transient List<UserRole> users = new ArrayList<>();

    public Role() { }

    public Role(Type type) {
        this();
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Role i = (Role) o;
        return Objects.equals(getId(), i.getId())
                && Objects.equals(getType(), i.getType());
    }
}
