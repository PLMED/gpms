package edu.uky.pml.gpms.database.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@SuppressWarnings({"WeakerAccess","unused"})
@Entity
@Table( name = "user_session" )
public class UserSession extends GenericModel {
    @Temporal(TemporalType.TIMESTAMP)
    @Column( name = "last_seen" )
    private Date lastSeen;

    @Column( name = "remember_me" )
    private Boolean rememberMe;

    @NotNull
    @ManyToOne
    @JoinColumn( name = "user_id", nullable = false )
    private User user;

    public UserSession() {
        this.rememberMe = false;
        updateSeen();
    }

    public UserSession(User user) {
        this();
        this.user = user;
    }

    public UserSession(User user, Boolean rememberMe) {
        this(user);
        this.rememberMe = rememberMe;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void updateSeen() {
        this.lastSeen = new Date();
    }

    public Boolean getRememberMe() {
        return rememberMe;
    }

    public User getUser() {
        return user;
    }
}
