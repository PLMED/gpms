package edu.uky.pml.gpms.database.models;

import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class UserRoleTest {
    private static Validator validator;

    @BeforeClass
    public static void setUpValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void validationTest() {
        UserRole validUserRole = new UserRole(new User("test", "test@test.com"), new Role(Role.Type.Administrator));
        Set<ConstraintViolation<UserRole>> violations = validator.validate(validUserRole);
        assertEquals(0, violations.size());
        UserRole nullUser = new UserRole(null, new Role(Role.Type.Administrator));
        violations = validator.validate(nullUser);
        assertEquals(1, violations.size());
        assertEquals("user", violations.iterator().next().getPropertyPath().toString());
        assertEquals("must not be null", violations.iterator().next().getMessage());
        UserRole nullRole = new UserRole(new User("test", "test@test.com"), null);
        violations = validator.validate(nullRole);
        assertEquals(1, violations.size());
        assertEquals("role", violations.iterator().next().getPropertyPath().toString());
        assertEquals("must not be null", violations.iterator().next().getMessage());
    }

    @Test
    public void equalsTest() {
        User user = new User("test", "test@test.com");
        Role role = new Role(Role.Type.Administrator);
        UserRole a = new UserRole(user, role);
        UserRole b = new UserRole(user, role);
        assertEquals(a, b);
    }
}
