package edu.uky.pml.gpms.database.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.lang.reflect.Type;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class RoleTest {
    @Test
    public void equalTest() {
        Role a = new Role(Role.Type.Administrator);
        Role b = new Role(Role.Type.Administrator);
        assertEquals(a, b);
    }

    @Test
    public void jsonTest() {
        Role admin = new Role(Role.Type.Administrator);
        Type type = new TypeToken<Map<String, Object>>(){}.getType();
        Map<String, Object> props = new Gson().fromJson(admin.json(), type);
        assertEquals(props.get("type"), admin.getType().name());
    }
}
