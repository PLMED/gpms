package edu.uky.pml.gpms.database.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import edu.uky.pml.gpms.database.models.User;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class UserTest {
    private static Validator validator;

    @BeforeClass
    public static void setUpValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void validationTest() {
        User validUser = new User("test", "test@test.com");
        Set<ConstraintViolation<User>> violations = validator.validate(validUser);
        assertEquals(0, violations.size());
        User nullUsername = new User(null, "test@test.com");
        violations = validator.validate(nullUsername);
        assertEquals(1, violations.size());
        assertEquals("must not be null", violations.iterator().next().getMessage());
        User nullEmail = new User("test", null);
        violations = validator.validate(nullEmail);
        assertEquals(1, violations.size());
        assertEquals("must not be null", violations.iterator().next().getMessage());
        User invalidEmail = new User("test", "test");
        violations = validator.validate(invalidEmail);
        assertEquals(1, violations.size());
        assertEquals("must be a well-formed email address", violations.iterator().next().getMessage());
        User tooLongUsername = new User("SomeReallyLongUsernameThatIsInvalid", "test@test.com");
        violations = validator.validate(tooLongUsername);
        assertEquals(1, violations.size());
        assertEquals("size must be between 4 and 20", violations.iterator().next().getMessage());
        User tooLongEmail = new User("test",
                "SomeReallyLongUsernameThatIsInvalidSomeReallyLongUsernameThatIsInvalidSomeReallyLongUsername" +
                        "ThatIsInvalidSomeReallyLongUsernameThatIsInvalidSomeReallyLongUsernameThatIsInvalid" +
                        "SomeReallyLongUsernameThatIsInvalidSomeReallyLongUsernameThatIsInvalidSomeReallyLongUsername" +
                        "ThatIsInvalidSomeReallyLongUsernameThatIsInvalidSomeReallyLongUsernameThatIsInvalid@test.com");
        violations = validator.validate(tooLongEmail);
        assertEquals(2, violations.size());
        for (ConstraintViolation<User> violation : violations) {
            if (!violation.getMessage().equals("size must be between 3 and 255") &&
                    !violation.getMessage().equals("must be a well-formed email address"))
                fail(String.format("expected:<[%s]> or <[%s]> but was <[%s]>",
                        "size must be between 3 and 255", "must be a well-formed email address", violation.getMessage()));
        }
        User tooLongFirstName = new User("test", "test@test.com",
                "SomeReallyLongUsernameThatIsInvalidSomeReallyLongUsernameThatIsInvalidSomeReallyLongUsername" +
                "ThatIsInvalidSomeReallyLongUsernameThatIsInvalidSomeReallyLongUsernameThatIsInvalid" +
                "SomeReallyLongUsernameThatIsInvalidSomeReallyLongUsernameThatIsInvalidSomeReallyLongUsername" +
                "ThatIsInvalidSomeReallyLongUsernameThatIsInvalidSomeReallyLongUsernameThatIsInvalid", "test");
        violations = validator.validate(tooLongFirstName);
        assertEquals(1, violations.size());
        assertEquals("size must be between 0 and 255", violations.iterator().next().getMessage());
        User tooLongLastName = new User("test", "test@test.com", "test",
                "SomeReallyLongUsernameThatIsInvalidSomeReallyLongUsernameThatIsInvalidSomeReallyLongUsername" +
                        "ThatIsInvalidSomeReallyLongUsernameThatIsInvalidSomeReallyLongUsernameThatIsInvalid" +
                        "SomeReallyLongUsernameThatIsInvalidSomeReallyLongUsernameThatIsInvalidSomeReallyLongUsername" +
                        "ThatIsInvalidSomeReallyLongUsernameThatIsInvalidSomeReallyLongUsernameThatIsInvalid");
        violations = validator.validate(tooLongLastName);
        assertEquals(1, violations.size());
        assertEquals("size must be between 0 and 255", violations.iterator().next().getMessage());
    }

    @Test
    public void equalsTest() {
        User a = new User("test", "test");
        User b = new User("test", "test");
        assertEquals(a, b);
        a = new User("test", "test","test", "test");
        b = new User("test", "test","test", "test");
        assertEquals(a, b);
    }

    @Test
    public void jsonTest() {
        User a = new User("test", "test", "test", "test");
        Type type = new TypeToken<Map<String, Object>>(){}.getType();
        Map<String, Object> props = new Gson().fromJson(a.json(), type);
        assertEquals(props.get("username"), a.getUsername());
        assertEquals(props.get("email"), a.getEmail());
        assertEquals(props.get("firstName"), a.getFirstName());
        assertEquals(props.get("lastName"), a.getLastName());
    }
}
