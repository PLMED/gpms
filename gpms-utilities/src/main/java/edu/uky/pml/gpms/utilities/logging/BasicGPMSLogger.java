package edu.uky.pml.gpms.utilities.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@SuppressWarnings({"unused", "WeakerAccess"})
public class BasicGPMSLogger implements GPMSLogger {
    private static final Logger msgEventLogger = LoggerFactory.getLogger("MSGEVENT");
    private static final String FLOW_CELL_ID_PARAM_NAME = "seq_id";
    private static final String SAMPLE_ID_PARAM_NAME = "sample_id";
    private static final String FLOW_CELL_STEP_PARAM_NAME = "sstep";
    private static final String SAMPLE_STEP_PARAM_NAME = "ssstep";
    private static final String MESSAGE_PARAM_NAME = "msg";

    private Logger logger;
    private String flowCellID;
    private String sampleID;
    private String requestID;
    private int stage;
    private int step;

    public BasicGPMSLogger() {
        this(BasicGPMSLogger.class, null, null, null, 0, 0);
    }

    public BasicGPMSLogger(String flowCellID, String requestID) {
        this(BasicGPMSLogger.class, flowCellID, null, requestID, 0, 0);
    }

    public BasicGPMSLogger(String flowCellID, String requestID, int stage) {
        this(BasicGPMSLogger.class, flowCellID, null, requestID, stage, 0);
    }

    public BasicGPMSLogger(String flowCellID, String requestID, int stage, int step) {
        this(BasicGPMSLogger.class, flowCellID, null, requestID, stage, step);
    }

    public BasicGPMSLogger(Class logClass) {
        this(logClass, null, null, null, 0, 0);
    }

    public BasicGPMSLogger(Class logClass, String flowCellID, String requestID) {
        this(logClass, flowCellID, null, requestID, 0, 0);
    }

    public BasicGPMSLogger(Class logClass, String flowCellID, String requestID, int stage) {
        this(logClass, flowCellID, null, requestID, stage, 0);
    }

    public BasicGPMSLogger(Class logClass, String flowCellID, String requestID, int stage, int step) {
        this(logClass, flowCellID, null, requestID, stage, step);
    }

    public BasicGPMSLogger(String flowCellID, String sampleID, String requestID) {
        this(BasicGPMSLogger.class, flowCellID, sampleID, requestID, 0, 0);
    }

    public BasicGPMSLogger(String flowCellID, String sampleID, String requestID, int stage) {
        this(BasicGPMSLogger.class, flowCellID, sampleID, requestID, stage, 0);
    }

    public BasicGPMSLogger(String flowCellID, String sampleID, String requestID, int stage, int step) {
        this(BasicGPMSLogger.class, flowCellID, sampleID, requestID, stage, step);
    }

    public BasicGPMSLogger(Class logClass, String flowCellID, String sampleID, String requestID) {
        this(logClass, flowCellID, sampleID, requestID, 0, 0);
    }

    public BasicGPMSLogger(Class logClass, String flowCellID, String sampleID, String requestID, int stage) {
        this(logClass, flowCellID, sampleID, requestID, stage, 0);
    }

    public BasicGPMSLogger(Class logClass, String flowCellID, String sampleID, String requestID, int stage, int step) {
        this.logger = LoggerFactory.getLogger(logClass);
        setFlowCellID(flowCellID);
        setSampleID(sampleID);
        setRequestID(requestID);
        setStage(stage);
        setStep(step);
    }

    public void trace(String message) {
        logger.trace(message);
    }
    public void trace(String message, Object... objects) {
        logger.trace(message, objects);
    }

    public void debug(String message) {
        logger.debug(message);
    }
    public void debug(String message, Object... objects) {
        logger.debug(message, objects);
    }

    public void info(String message) {
        logger.info(message);
    }
    public void info(String message, Object... objects) {
        logger.info(message, objects);
    }

    public void warn(String message) {
        logger.warn(message);
    }
    public void warn(String message, Object... objects) {
        logger.warn(message, objects);
    }

    public void error(String message) {
        logger.error(message);
    }
    public void error(String message, Object... objects) {
        logger.error(message, objects);
    }

    public void gpmsInfo(Map<String, String> customParams) {
        if (getSampleID() == null)
            msgEventLogger.info(String.format("[%s][%s][%d][%d]\n%s", getFlowCellID(), getRequestID(), getStage(), getStep(), customParams));
        else
            msgEventLogger.info(String.format("[%s][%s][%s][%d][%d]\n%s", getFlowCellID(), getSampleID(), getRequestID(), getStage(), getStep(), customParams));
    }
    public void gpmsInfo(String message) {
        if (getSampleID() == null)
            msgEventLogger.info(String.format("[%s][%s][%d][%d] %s", getFlowCellID(), getRequestID(), getStage(), getStep(), message));
        else
            msgEventLogger.info(String.format("[%s][%s][%s][%d][%d] %s", getFlowCellID(), getSampleID(), getRequestID(), getStage(), getStep(), message));
    }
    public void gpmsInfo(Map<String, String> customParams, String message) {
        if (getSampleID() == null)
            msgEventLogger.info(String.format("[%s][%s][%d][%d] %s\n%s", getFlowCellID(), getRequestID(), getStage(), getStep(), message, customParams));
        else
            msgEventLogger.info(String.format("[%s][%s][%s][%d][%d] %s\n%s", getFlowCellID(), getSampleID(), getRequestID(), getStage(), getStep(), message, customParams));
    }
    public void gpmsInfo(String message, Object... objects) {
        gpmsInfo(replaceBrackets(message, objects));
    }
    public void gpmsInfo(Map<String, String> customParams, String message, Object... objects) {
        gpmsInfo(replaceBrackets(message, objects), customParams);
    }

    public void gpmsError(Map<String, String> customParams) {
        if (getSampleID() == null)
            msgEventLogger.error(String.format("[%s][%s][%d][%d]\n%s", getFlowCellID(), getRequestID(), getStage(), getStep(), customParams));
        else
            msgEventLogger.error(String.format("[%s][%s][%s][%d][%d]\n%s", getFlowCellID(), getSampleID(), getRequestID(), getStage(), getStep(), customParams));
    }
    public void gpmsError(String message) {
        if (getSampleID() == null)
            msgEventLogger.error(String.format("[%s][%s][%d][%d] %s", getFlowCellID(), getRequestID(), getStage(), getStep(), message));
        else
            msgEventLogger.error(String.format("[%s][%s][%s][%d][%d] %s", getFlowCellID(), getSampleID(), getRequestID(), getStage(), getStep(), message));
    }
    public void gpmsError(Map<String, String> customParams, String message) {
        if (getSampleID() == null)
            msgEventLogger.error(String.format("[%s][%s][%d][%d] %s\n%s", getFlowCellID(), getRequestID(), getStage(), getStep(), message, customParams));
        else
            msgEventLogger.error(String.format("[%s][%s][%s][%d][%d] %s\n%s", getFlowCellID(), getSampleID(), getRequestID(), getStage(), getStep(), message, customParams));
    }
    public void gpmsError(String message, Object... objects) {
        gpmsError(replaceBrackets(message, objects));
    }
    public void gpmsError(Map<String, String> customParams, String message, Object... objects) {
        gpmsError(replaceBrackets(message, objects), customParams);
    }

    public void gpmsFailure(String message) {
        gpmsError(message);
    }
    public void gpmsFailure(String message, Object... objects) {
        gpmsError(message, objects);
    }

    public GPMSLogger cloneLogger(Class clazz) {
        return new BasicGPMSLogger(clazz, getFlowCellID(), getSampleID(), getRequestID(), getStage(), getStep());
    }

    public String getFlowCellID() {
        return flowCellID;
    }

    public void setFlowCellID(String flowCellID) {
        this.flowCellID = flowCellID;
    }

    public String getSampleID() {
        return sampleID;
    }

    public void setSampleID(String sampleID) {
        this.sampleID = sampleID;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public void incrementStep() {
        this.step++;
    }

    private String replaceBrackets(String logMessage, Object... params) {
        int replaced = 0;
        while (logMessage.contains("{}") && replaced < params.length) {
            logMessage = logMessage.replaceFirst("\\{}", String.valueOf(params[replaced]).replace("\\", "\\\\"));
            replaced++;
        }
        return logMessage;
    }
}
