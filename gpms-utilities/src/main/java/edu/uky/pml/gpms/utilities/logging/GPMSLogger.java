package edu.uky.pml.gpms.utilities.logging;

import java.util.Map;

@SuppressWarnings("unused")
public interface GPMSLogger {
    void trace(String message);
    void trace(String message, Object... objects);

    void debug(String message);
    void debug(String message, Object... objects);

    void info(String message);
    void info(String message, Object... objects);

    void warn(String message);
    void warn(String message, Object... objects);

    void error(String message);
    void error(String message, Object... objects);

    void gpmsInfo(Map<String, String> customParams);
    void gpmsInfo(String message);
    void gpmsInfo(Map<String, String> customParams, String message);
    void gpmsInfo(String message, Object... objects);
    void gpmsInfo(Map<String, String> customParams, String message, Object... objects);

    void gpmsError(Map<String, String> customParams);
    void gpmsError(String message);
    void gpmsError(Map<String, String> customParams, String message);
    void gpmsError(String message, Object... objects);
    void gpmsError(Map<String, String> customParams, String message, Object... objects);

    void gpmsFailure(String message);
    void gpmsFailure(String message, Object... objects);

    GPMSLogger cloneLogger(Class clazz);

    String getFlowCellID();
    void setFlowCellID(String flowCellID);

    String getSampleID();
    void setSampleID(String sampleID);

    String getRequestID();
    void setRequestID(String requestID);

    int getStage();
    void setStage(int stage);

    int getStep();
    void setStep(int step);
    void incrementStep();
}
