package edu.uky.pml.gpms.utilities.helpers;

import com.google.gson.Gson;

import java.util.List;

@SuppressWarnings("WeakerAccess")
public class GenomicsJSON {
    public String Sample_Name;
    public ProcessingDetails Processing_details;
    public List<Target> target;
    public List<String> Download_exclusion_list;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{\n");
        sb.append(String.format("\t\"Sample_Name\": %s,\n", Sample_Name != null ? String.format("\"%s\"", Sample_Name) : "null"));
        sb.append(String.format("\t\"Processing_details\": %s", Processing_details != null ? Processing_details : "null"));
        if (target == null)
            sb.append("\t\"target\": null,\n");
        else {
            sb.append("\t\"target\": [\n");
            for (Target targetEntry : target) {
                sb.append(targetEntry);
            }
            sb.append("\t],\n");
        }
        if (Download_exclusion_list == null)
            sb.append("\t\"Download_exclusion_list\": null\n");
        else {
            sb.append("\t\"Download_exclusion_list\": [,\n");
            for (String exclusion : Download_exclusion_list)
                sb.append(String.format("\t\t\"%s\",\n", exclusion));
            if (Download_exclusion_list.size() > 0)
                sb.deleteCharAt(sb.lastIndexOf(","));
            sb.append("\t]\n");
        }
        sb.append("}");
        return sb.toString();
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public class ProcessingDetails {
        public String Docker_container;
        public String AWS_instance_name;

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder("{\n");
            sb.append(String.format("\t\t\"Docker_container\": %s,\n", Docker_container != null ? String.format("\"%s\"", Docker_container) : "null"));
            sb.append(String.format("\t\t\"AWS_instance_name\": %s,\n", AWS_instance_name != null ? String.format("\"%s\"", AWS_instance_name) : "null"));
            sb.append("\t},\n");
            return sb.toString();
        }
    }

    public class Target {
        public String target_name;
        public String target_command;

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder("\t\t{\n");
            sb.append(String.format("\t\t\t\"target_name\": %s,\n", target_name != null ? String.format("\"%s\"", target_name) : "null"));
            sb.append(String.format("\t\t\t\"target_command\": %s,\n", target_command != null ? String.format("\"%s\"", target_command) : "null"));
            sb.append("\t\t},\n");
            return sb.toString();
        }
    }
}
