package edu.uky.pml.gpms.utilities.helpers;

public class GPMSStatics {
    // Archiver
    public static final String DEFAULT_BAGIT_TYPE = "standard";
    public static final String DEFAULT_BAGIT_HASHING = "md5";
    public static final boolean DEFAULT_HIDDEN_FILES = true;
    public static final String DEFAULT_ARCHIVE_COMPRESSION = "tar";

    // ObjectStorage
    public static final String UNCOMPRESSED_SIZE_METADATA_TAG_KEY = "uncompressedsize";
    public static final String PART_SIZE_METADATA_TAG_KEY = "partsize";
    public static final int DEFAULT_PART_SIZE = 5;

    // DataManager
    public static final String PREPROCESSED_FLOWCELL_CONFIG_ERROR_LOG_FILE = "batch_pipeline_config_error_log.txt";
    public static final String PREPROCESSED_FLOWCELL_CONFIG_JSON_FILE = "batch_pipeline_config.json";
    public static final String PROCESSED_CONFIG_FILES_DIRECTORY = "config_files";
    public static final String PROCESSED_PIPELINE_CONFIG_ERROR_LOG_FILE = "pipeline_config_error_log.txt";
    public static final String PROCESSED_PIPELINE_CONFIG_JSON_FILE = "pipeline_config.json";
    public static final String PROCESSED_RESULTS_COMPLETE_ARCHIVE_SUFFIX = "complete";

    // ProcessManager
    public static final String DEFAULT_DOCKER_REGISTRY = "intrepo.uky.edu:5000";
    public static final String DEFAULT_DOCKER_CONTAINER_VERSION = "latest";
    public static final int PREPROCESSING_STAGE = 2;
    public static final String PREPROCESSING_CONTAINER_COMMAND = "/opt/pretools/raw_data_processing.pl";
    public static final String PREPROCESSING_CLINICAL_RESULTS_FOLDER_NAME = "clinical";
    public static final String PREPROCESSING_RESEARCH_RESULTS_FOLDER_NAME = "research";
    public static final String CONFIG_GENERATION_CONTAINER_COMMAND = "/opt/pretools/pipeline_config_generator_main.pl";
    public static final String CONFIG_GENERATION_CLINICAL_RESULTS_FOLDER_NAME = "clinical";
    public static final String CONFIG_GENERATION_RESEARCH_RESULTS_FOLDER_NAME = "research";
    public static final int PROCESSING_STAGE = 4;
    public static final String PROCESSING_CONTAINER_COMMAND = "/gdata/input/commands_main.sh";
    public static final String PROCESSING_COMMANDS_FILE = "commands_main.sh";
    public static final String PROCESSING_CONFIGS_FOLDER = "config_files";
    public static final String PROCESSING_JSON_PIPELINE_CONFIG_FILE = "pipeline_config.json";
    public static final String DEFAULT_MRD_VARIANTS = "21   99999999    .   .   .";
    public static final int POSTPROCESSING_STAGE = 5;
    public static final int RESULTS_DOWNLOAD_STAGE = 6;
}
