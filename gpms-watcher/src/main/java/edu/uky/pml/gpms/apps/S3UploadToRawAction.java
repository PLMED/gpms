package edu.uky.pml.gpms.apps;

import edu.uky.pml.gpms.data.DataManager;
import edu.uky.pml.gpms.watcher.*;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class S3UploadToRawAction implements ReadyAction {
    private static final Logger logger = LoggerFactory.getLogger(S3UploadToRawAction.class);

    private Handler handler;
    private WatchFolder watchFolder;
    private DataManager dataManager;
    private ExecutorService exec = Executors.newFixedThreadPool(1);

    public S3UploadToRawAction(WatchFolder watchFolder, DataManager dataManager) {
        logger.trace("S3UploadToRawAction()");
        this.watchFolder = watchFolder;
        this.dataManager = dataManager;
    }

    @Override
    public void actOn(FlowCellFolder toActOn) {
        logger.trace("actOn('{}')", toActOn.getFlowCellID());
        exec.submit(new S3UploadActionWorker(toActOn));
    }

    @Override
    public void setHandler(Handler handler) {
        logger.trace("setHandler()");
        this.handler = handler;
    }

    @Override
    public void stop() {
        logger.trace("stop()");
        exec.shutdownNow();
    }

    private class S3UploadActionWorker implements Runnable {
        private final Logger logger = LoggerFactory.getLogger(S3UploadActionWorker.class);
        private FlowCellFolder folder;

        public S3UploadActionWorker(FlowCellFolder folder) {
            logger.trace("Adding transfer task for [{}]", folder.getFlowCellID());
            this.folder = folder;
        }

        @Override
        public void run() {
            String flowCellID = folder.getFlowCellID();
            try {
                logger.trace("Running: {}", flowCellID);
                watchFolder.updateFlowCellFolderStatus(flowCellID, TransferStatus.TRANSFERRING);
                if (dataManager.uploadFlowCellRawFiles(flowCellID, watchFolder.getWatchFolder().resolve(flowCellID)))
                    watchFolder.updateFlowCellFolderStatus(flowCellID, TransferStatus.COMPLETE);
                else
                    watchFolder.updateFlowCellFolderStatus(flowCellID, TransferStatus.FAILED);
                if (handler != null)
                    handler.handled(folder);
            } catch (Exception e) {
                logger.error("[{}] transfer was interrupted\n{}", flowCellID, ExceptionUtils.getStackTrace(e));
            }
        }
    }
}
