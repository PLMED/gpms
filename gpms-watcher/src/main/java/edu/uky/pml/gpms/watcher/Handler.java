package edu.uky.pml.gpms.watcher;

import edu.uky.pml.gpms.utilities.logging.BasicGPMSLogger;
import edu.uky.pml.gpms.utilities.logging.GPMSLogger;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class Handler {
    private GPMSLogger logger;
    private WatchFolder watchFolder;
    private ReadyAction readyAction;
    private Timer handlerTimer = new Timer("Handler");
    private Set<String> handling = new HashSet<>();
    private long handlerTimerDelay = 5000L;
    private long handlerTimerPeriod = 5000L;

    public Handler(WatchFolder watchFolder, ReadyAction readyAction) {
        this.logger = new BasicGPMSLogger(Handler.class);
        logger.trace("Handler()");
        this.watchFolder = watchFolder;
        this.readyAction = readyAction;
        this.readyAction.setHandler(this);
    }

    public Handler(WatchFolder watchFolder, ReadyAction readyAction, GPMSLogger logger) {
        setLogger(logger);
        this.logger.trace("Handler()");
        this.watchFolder = watchFolder;
        this.readyAction = readyAction;
        this.readyAction.setHandler(this);
    }

    public Handler(WatchFolder watchFolder, ReadyAction readyAction, long handlerTimerDelay, long handlerTimerPeriod) {
        this(watchFolder, readyAction);
        this.handlerTimerDelay = handlerTimerDelay;
        this.handlerTimerPeriod = handlerTimerPeriod;
    }

    public Handler(WatchFolder watchFolder, ReadyAction readyAction, GPMSLogger logger, long handlerTimerDelay, long handlerTimerPeriod) {
        this(watchFolder, readyAction, logger);
        this.handlerTimerDelay = handlerTimerDelay;
        this.handlerTimerPeriod = handlerTimerPeriod;
    }

    public void start() {
        logger.trace("start()");
        handlerTimer.scheduleAtFixedRate(new HandlerTask(), handlerTimerDelay, handlerTimerPeriod);
    }

    public void stop() {
        logger.trace("stop()");
        readyAction.stop();
        handlerTimer.cancel();
    }

    public GPMSLogger getLogger() {
        return logger;
    }

    public void setLogger(GPMSLogger logger) {
        this.logger = logger.cloneLogger(Handler.class);
    }

    public void updateLogger(GPMSLogger logger) {
        this.logger.setFlowCellID(logger.getFlowCellID());
        this.logger.setSampleID(logger.getSampleID());
        this.logger.setRequestID(logger.getRequestID());
        this.logger.setStage(logger.getStage());
        this.logger.setStep(logger.getStep());
    }

    private class HandlerTask extends TimerTask {
        @Override
        public void run() {
            logger.trace("run()");
            for (FlowCellFolder folder : watchFolder.getFolders())
                if (folder.getStatus() == TransferStatus.READY && !handling.contains(folder.getFlowCellID())) {
                    handling.add(folder.getFlowCellID());
                    readyAction.actOn(folder);
                }
        }
    }

    public void handled(FlowCellFolder handled) {
        handling.remove(handled.getFlowCellID());
    }
}
