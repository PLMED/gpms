package edu.uky.pml.gpms.apps;

import edu.uky.pml.gpms.data.DataManager;
import edu.uky.pml.gpms.data.encapsulation.Archiver;
import edu.uky.pml.gpms.data.transfer.ObjectStorage;
import edu.uky.pml.gpms.watcher.*;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.ion.NullValueException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class GPMSWatcherApp {
    private static final Logger logger = LoggerFactory.getLogger(GPMSWatcherApp.class);
    private static final Logger simpleLogger = LoggerFactory.getLogger("message-only");

    public static void main(String[] args) {
        ArgumentParser parser = ArgumentParsers.newFor("Genomic Pipeline Management System Data Extractor").build()
                .defaultHelp(true)
                .description("Downloads and restores genomics pipeline data to the local filesystem.");
        parser.addArgument("-C", "--credentials")
                .setDefault("config.properties")
                .help("Configuration file holding credential information.");
        parser.addArgument("-A", "--accesskey");
        parser.addArgument("-S", "--secretkey");
        parser.addArgument("-E", "--endpoint");
        parser.addArgument("-R", "--region");
        parser.addArgument("--raw").help("Genomic raw file S3 bucket name");
        parser.addArgument("--clinical").help("Genomic clinical file S3 bucket name");
        parser.addArgument("--research").help("Genomic research file S3 bucket name");
        parser.addArgument("--processed").help("Genomic processed file S3 bucket name");
        parser.addArgument("--results").help("Genomic results file S3 bucket name");
        parser.addArgument("command").nargs("?");
        parser.addArgument("directories").nargs("*");
        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        String command = ns.getString("command"), credentials = ns.getString("credentials"),
                accessKey = ns.getString("accesskey"), secretKey = ns.getString("secretkey"),
                endpoint = ns.getString("endpoint"), region = ns.getString("region"),
                rawBucketName = ns.getString("raw"), clinicalBucketName = ns.getString("clinical"),
                researchBucketName = ns.getString("research"), processedBucketName = ns.getString("processed"),
                resultsBucketName = ns.getString("results")
                        ;
        List<String> directories = ns.getList("directories");

        if (command == null) {
            simpleLogger.info("Commands:");
            simpleLogger.info("\t- watch <dir>");
            return;
        }

        switch (command) {
            case "watch":
            {
                if (directories.size() < 1 || directories.size() > 1) {
                    simpleLogger.info("Usage: {} <dir>", command);
                    return;
                }
                Path dir = Paths.get(directories.get(0));
                if (!Files.exists(dir)) {
                    simpleLogger.error("Directory [{}] does not exist", dir);
                    return;
                }
            }
            break;
        }

        logger.trace("Checking for config.properties file");
        File configFile = new File(credentials);
        if (configFile.exists()) {
            Configurations configs = new Configurations();
            try {
                Configuration config = configs.properties(configFile);
                if (config.containsKey("access.key"))
                    accessKey = config.getString("access.key");
                if (config.containsKey("secret.key"))
                    secretKey = config.getString("secret.key");
                if (config.containsKey("endpoint"))
                    endpoint = config.getString("endpoint");
                if (config.containsKey("region"))
                    region = config.getString("region");
                if (config.containsKey("bucket.raw"))
                    rawBucketName = config.getString("bucket.raw");
                if (config.containsKey("bucket.clinical"))
                    clinicalBucketName = config.getString("bucket.clinical");
                if (config.containsKey("bucket.research"))
                    researchBucketName = config.getString("bucket.research");
                if (config.containsKey("bucket.processed"))
                    processedBucketName = config.getString("bucket.processed");
                if (config.containsKey("bucket.results"))
                    resultsBucketName = config.getString("bucket.results");
            } catch (ConfigurationException e) {
                logger.error("Error with your config.properties file.");
            }
        }
        ObjectStorage objectStorage;
        try {
            objectStorage = new ObjectStorage(accessKey, secretKey, endpoint, region);
        } catch (NullValueException | IllegalArgumentException e) {
            logger.error("Error building objectStorage : {}", e.getMessage());
            return;
        }
        Archiver archiver;
        try {
            archiver = new Archiver();
        } catch (NullValueException | IllegalArgumentException e) {
            logger.error("Error building archiver : {}", e.getMessage());
            return;
        }
        DataManager dataManager;
        try {
            dataManager = new DataManager(objectStorage, archiver, rawBucketName, clinicalBucketName,
                    researchBucketName, processedBucketName, resultsBucketName);
        } catch (NullValueException | IllegalArgumentException e) {
            logger.error("Error building data manager : {}", e.getMessage());
            return;
        }
        switch (command) {
            case "watch":
                {
                    Path dir = Paths.get(directories.get(0));
                    ReadyCheck manualReadyCheck = new ManualReadyCheck();
                    WatchFolder watchFolder = new WatchFolder(dir, manualReadyCheck);
                    Watcher watcher = new Watcher(watchFolder);
                    ReadyAction s3UploadAction = new S3UploadToRawAction(watchFolder, dataManager);
                    Handler handler = new Handler(watchFolder, s3UploadAction);
                    watcher.start();
                    handler.start();
                    System.out.println("Press any key to stop");
                    try {
                        System.in.read();
                        handler.stop();
                        watcher.stop();
                    } catch (IOException e) {
                        logger.error("FAILURE: {}", e.getMessage());
                    }
                }
                break;
        }
    }
}
