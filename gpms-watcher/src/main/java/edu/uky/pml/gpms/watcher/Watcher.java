package edu.uky.pml.gpms.watcher;

import edu.uky.pml.gpms.utilities.logging.BasicGPMSLogger;
import edu.uky.pml.gpms.utilities.logging.GPMSLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

public class Watcher {
    private GPMSLogger logger;
    private WatchFolder watchFolder;
    private Timer watchTimer = new Timer("Watcher");
    private long watcherTimerDelay = 5000L;
    private long watcherTimerPeriod = 5000L;

    public Watcher(WatchFolder watchFolder) {
        this.logger = new BasicGPMSLogger(Watcher.class);
        this.watchFolder = watchFolder;
    }

    public Watcher(WatchFolder watchFolder, GPMSLogger logger) {
        setLogger(logger);
        this.watchFolder = watchFolder;
    }

    public Watcher(WatchFolder watchFolder, long watcherTimerDelay, long watcherTimerPeriod) {
        this(watchFolder);
        this.watcherTimerDelay = watcherTimerDelay;
        this.watcherTimerPeriod = watcherTimerPeriod;
    }

    public Watcher(WatchFolder watchFolder, GPMSLogger logger, long watcherTimerDelay, long watcherTimerPeriod) {
        this(watchFolder, logger);
        this.watcherTimerDelay = watcherTimerDelay;
        this.watcherTimerPeriod = watcherTimerPeriod;
    }

    public void start() {
        logger.trace("start()");
        if (watchFolder == null)
            return;
        watchTimer.scheduleAtFixedRate(new WatcherTask(), watcherTimerDelay, watcherTimerPeriod);
    }

    public void stop() {
        logger.trace("stop()");
        watchTimer.cancel();
    }

    public GPMSLogger getLogger() {
        return logger;
    }

    public void setLogger(GPMSLogger logger) {
        this.logger = logger.cloneLogger(Watcher.class);
    }

    public void updateLogger(GPMSLogger logger) {
        this.logger.setFlowCellID(logger.getFlowCellID());
        this.logger.setSampleID(logger.getSampleID());
        this.logger.setRequestID(logger.getRequestID());
        this.logger.setStage(logger.getStage());
        this.logger.setStep(logger.getStep());
    }

    private class WatcherTask extends TimerTask {
        public WatcherTask() { }
        @Override
        public void run() {
            logger.trace("run()");
            Path toWatch = watchFolder.getWatchFolder();
            if (Files.exists(toWatch)) {
                try {
                    watchFolder.update(Files.list(toWatch).filter(Files::isDirectory).map(p -> p.getFileName().toString()).collect(Collectors.toList()));
                } catch (IOException e) {
                    logger.gpmsError("Failed to update list of folders in watch directory: {}", e.getMessage());
                }
            }
        }
    }
}
