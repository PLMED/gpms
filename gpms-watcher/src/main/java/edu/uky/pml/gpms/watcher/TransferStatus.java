package edu.uky.pml.gpms.watcher;

import com.google.gson.annotations.SerializedName;

public enum TransferStatus {
    @SerializedName("NOTREADY")
    NOTREADY(0),
    @SerializedName("READY")
    READY(1),
    @SerializedName("TRANSFERRING")
    TRANSFERRING(2),
    @SerializedName("COMPLETE")
    COMPLETE(3),
    @SerializedName("FAILED")
    FAILED(4);
    private final int value;
    public int getValue() {
        return value;
    }

    private TransferStatus(int value) {
        this.value = value;
    }
}
