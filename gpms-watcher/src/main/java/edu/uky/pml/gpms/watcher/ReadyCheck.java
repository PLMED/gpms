package edu.uky.pml.gpms.watcher;

import java.nio.file.Path;

public interface ReadyCheck {
    boolean isReady(Path toCheck);
}
