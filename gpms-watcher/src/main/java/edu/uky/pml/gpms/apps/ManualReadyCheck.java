package edu.uky.pml.gpms.apps;

import edu.uky.pml.gpms.watcher.ReadyCheck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;

public class ManualReadyCheck implements ReadyCheck {
    private static final Logger logger = LoggerFactory.getLogger(ManualReadyCheck.class);

    @Override
    public boolean isReady(Path toCheck) {
        logger.trace("isReady('{}')", toCheck);
        return false;
    }
}
