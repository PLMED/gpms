package edu.uky.pml.gpms.watcher;

import java.nio.file.Path;

public interface ReadyAction {
    public void actOn(FlowCellFolder toActOn);
    public void setHandler(Handler handler);
    public void stop();
}
