package edu.uky.pml.gpms.watcher;

import com.google.gson.*;
import edu.uky.pml.gpms.utilities.logging.BasicGPMSLogger;
import edu.uky.pml.gpms.utilities.logging.GPMSLogger;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class WatchFolder {
    private static final String WATCHFILENAME = "watching.json";

    private transient GPMSLogger logger;
    private List<String> statusTypes = new ArrayList<>();
    private Date lastUpdated;
    private Path configFile;
    private Path watchFolder;
    private List<FlowCellFolder> folders = new ArrayList<>();
    private transient ReadyCheck readyCheck;
    private transient Set<String> folderNames = new HashSet<>();

    public WatchFolder(Path watchFolder, ReadyCheck readyCheck) {
        this.logger = new BasicGPMSLogger(WatchFolder.class);
        statusTypes.add("NOTREADY");
        statusTypes.add("READY");
        statusTypes.add("TRANFERRING");
        statusTypes.add("COMPLETE");
        statusTypes.add("FAILED");
        lastUpdated = new Date();
        this.watchFolder = watchFolder;
        this.configFile = watchFolder.resolve(WATCHFILENAME);
        this.readyCheck = readyCheck;
        read();
    }

    public WatchFolder(Path watchFolder, ReadyCheck readyCheck, GPMSLogger logger) {
        setLogger(logger);
        statusTypes.add("NOTREADY");
        statusTypes.add("READY");
        statusTypes.add("TRANFERRING");
        statusTypes.add("COMPLETE");
        statusTypes.add("FAILED");
        lastUpdated = new Date();
        this.watchFolder = watchFolder;
        this.configFile = watchFolder.resolve(WATCHFILENAME);
        this.readyCheck = readyCheck;
        read();
    }

    public WatchFolder(Path configFolder, Path watchFolder, ReadyCheck readyCheck) {
        this.logger = new BasicGPMSLogger(WatchFolder.class);
        statusTypes.add("NOTREADY");
        statusTypes.add("READY");
        statusTypes.add("TRANFERRING");
        statusTypes.add("COMPLETE");
        statusTypes.add("FAILED");
        lastUpdated = new Date();
        this.watchFolder = watchFolder;
        this.configFile = configFolder.resolve(WATCHFILENAME);
        this.readyCheck = readyCheck;
        read();
    }

    public WatchFolder(Path configFolder, Path watchFolder, ReadyCheck readyCheck, GPMSLogger logger) {
        setLogger(logger);
        statusTypes.add("NOTREADY");
        statusTypes.add("READY");
        statusTypes.add("TRANFERRING");
        statusTypes.add("COMPLETE");
        statusTypes.add("FAILED");
        lastUpdated = new Date();
        this.watchFolder = watchFolder;
        this.configFile = configFolder.resolve(WATCHFILENAME);
        this.readyCheck = readyCheck;
        read();
    }

    public void addFlowCellFolder(FlowCellFolder folder) {
        logger.gpmsInfo("Adding [{}]", folder.getFlowCellID());
        boolean updated = false;
        if (!folderNames.contains(folder.getFlowCellID())) {
            folderNames.add(folder.getFlowCellID());
            folders.add(folder);
            updated = true;
        } else
            logger.trace("[{}] already exists", folder.getFlowCellID());
        if (updated)
            write();
    }

    public void removeFlowCellFolder(FlowCellFolder folder) {
        logger.gpmsInfo("Removing [{}]", folder.getFlowCellID());
        if (folderNames.contains(folder.getFlowCellID())) {
            folderNames.remove(folder.getFlowCellID());
            folders.remove(folder);
            write();
        }
    }

    public synchronized void updateFlowCellFolderStatus(String flowcell, TransferStatus status) {
        logger.trace("updateFlowCellFolderStatus('{}','{}')", flowcell, status);
        boolean updated = false;
        for (FlowCellFolder folder : folders) {
            if (folder.getFlowCellID().equals(flowcell) && folder.getStatus() != status) {
                logger.gpmsInfo("Setting status [{}] -> [{}]", folder.getFlowCellID(), status);
                folder.setStatus(status);
                folder.setLastUpdated(new Date());
                updated = true;
            }
        }
        if (updated)
            write();
    }

    public List<FlowCellFolder> getFolders() {
        return folders;
    }

    public void setFolders(List<FlowCellFolder> folders) {
        this.folders = folders;
    }

    public FlowCellFolder getFolder(String folderName) {
        FlowCellFolder folder = null;
        for (FlowCellFolder flowCellFolder : folders)
            if (flowCellFolder.getFlowCellID().equals(folderName))
                folder = flowCellFolder;
        return folder;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public List<String> getStatusTypes() {
        return statusTypes;
    }

    public void setStatusTypes(List<String> statusTypes) {
        this.statusTypes = statusTypes;
    }

    public Path getWatchFolder() {
        return watchFolder;
    }

    public void setWatchFolder(Path watchFolder) {
        this.watchFolder = watchFolder;
    }

    public Path getConfigFile() {
        return configFile;
    }

    public GPMSLogger getLogger() {
        return logger;
    }

    public void setLogger(GPMSLogger logger) {
        this.logger = logger.cloneLogger(WatchFolder.class);
    }

    public void updateLogger(GPMSLogger logger) {
        this.logger.setFlowCellID(logger.getFlowCellID());
        this.logger.setSampleID(logger.getSampleID());
        this.logger.setRequestID(logger.getRequestID());
        this.logger.setStage(logger.getStage());
        this.logger.setStep(logger.getStep());
    }

    public synchronized void write() {
        logger.trace("write()");
        try {
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .disableHtmlEscaping()
                    .serializeNulls()
                    .registerTypeHierarchyAdapter(Path.class, new PathConverter())
                    .create();
            logger.trace("{}", gson.toJson(this));
            try (FileWriter writer = new FileWriter(configFile.toString())) {
                setLastUpdated(new Date());
                gson.toJson(this, writer);
            } catch (IOException e) {
                logger.error("write():{}", e.getMessage());
            }
        } catch (Exception e) {
            logger.error("Exception encountered: {}:{}", e.getClass().getCanonicalName(), e.getMessage());
        }
    }

    public synchronized void read() {
        logger.trace("read()");
        //Path watchFile = watchFolder.resolve(WATCHFILENAME);
        if (!Files.exists(configFile)) {
            write();
            return;
        }
        Gson gson = new GsonBuilder()
                .registerTypeHierarchyAdapter(Path.class, new PathConverter())
                .create();
        WatchFolder tmpWatchFolder;
        try {
            tmpWatchFolder = gson.fromJson(String.join(" ", Files.readAllLines(configFile)), WatchFolder.class);
        } catch(IOException e) {
            logger.error("Failed to read contents of [{}] : {}", configFile, e.getMessage());
            return;
        } catch (JsonSyntaxException e) {
            logger.error("JSON error in [{}] : {}", configFile, e.getMessage());
            return;
        }
        for (FlowCellFolder folder : tmpWatchFolder.getFolders()) {
            if (folderNames.contains(folder.getFlowCellID()))
                updateFlowCellFolderStatus(folder.getFlowCellID(), folder.getStatus());
            else
                addFlowCellFolder(folder);
        }
    }

    public void update(List<String> folders) {
        logger.trace("update()");
        read();
        if (folders != null && folders.size() > 0) {
            for (String folder : folders)
                if (!folderNames.contains(folder))
                    addFlowCellFolder(new FlowCellFolder(folder));
        }
        if (getFolders().size() > 0) {
            List<FlowCellFolder> toRemove = new ArrayList<>();
            for (FlowCellFolder folder : getFolders())
                if (folders == null || folders.size() == 0 || !folders.contains(folder.getFlowCellID()))
                    toRemove.add(folder);
            for (FlowCellFolder folder : toRemove)
                removeFlowCellFolder(folder);
        }
        checkForReadyFolders();
    }

    private void checkForReadyFolders() {
        if (readyCheck == null)
            return;
        for (FlowCellFolder folder : folders) {
            if (folder.getStatus() == TransferStatus.NOTREADY) {
                Path toCheck = watchFolder.resolve(folder.getFlowCellID());
                if (readyCheck.isReady(toCheck))
                    updateFlowCellFolderStatus(folder.getFlowCellID(), TransferStatus.READY);
            }
        }
    }

    public String getWatchFileContents() {
        try {
            return StringUtils.join(Files.readAllLines(configFile), "");
        } catch(IOException e) {
            logger.error("Failed to read contents of [{}] : {}", configFile, e.getMessage());
            return null;
        }
    }

    private class PathConverter implements JsonDeserializer<Path>, JsonSerializer<Path> {
        @Override
        public Path deserialize(JsonElement jsonElement, Type type,
                                JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            return Paths.get(jsonElement.getAsString());
        }

        @Override
        public JsonElement serialize(Path path, Type type, JsonSerializationContext jsonSerializationContext) {
            return new JsonPrimitive(path.toString());
        }
    }
}
