package edu.uky.pml.gpms.watcher;

import java.util.Date;

public class FlowCellFolder {
    private TransferStatus status;
    private String flowCellID;
    private Date lastUpdated;

    public FlowCellFolder(String flowCellID) {
        this.flowCellID = flowCellID;
        this.status = TransferStatus.NOTREADY;
        this.lastUpdated = new Date();
    }

    public TransferStatus getStatus() {
        return status;
    }

    public void setStatus(TransferStatus status) {
        this.status = status;
    }

    public String getFlowCellID() {
        return flowCellID;
    }

    public void setFlowCellID(String flowCellID) {
        this.flowCellID = flowCellID;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
