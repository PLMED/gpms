package edu.uky.pml.gpms.processing;

import edu.uky.pml.gpms.data.DataManager;
import edu.uky.pml.gpms.data.encapsulation.Archiver;
import edu.uky.pml.gpms.data.transfer.ObjectStorage;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.ion.NullValueException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static net.sourceforge.argparse4j.impl.Arguments.storeTrue;

public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    private static final Logger simpleLogger = LoggerFactory.getLogger("message-only");
    private static final String DEFAULT_DOCKER_REGISTRY = "850408476861.dkr.ecr.us-east-1.amazonaws.com";
    private static final String DEFAULT_DOCKER_CONTAINER_VERSION = "latest";
    //private static final String DEFAULT_PREPROCESSING_IMAGE = "intrepo.uky.edu:5000/gbase_preprocessing_1.0";
    private static final String DEFAULT_PREPROCESSING_IMAGE = "gbase_preprocessing_1.1";
    private static final String DEFAULT_PREPROCESSING_COMMAND = "/opt/pretools/raw_data_processing.pl";
    //private static final String DEFAULT_CONFIG_GENERATION_IMAGE = "intrepo.uky.edu:5000/gbase_config_generation_1.0";
    private static final String DEFAULT_CONFIG_GENERATION_IMAGE = "gbase_config_generation_1.1";
    private static final String DEFAULT_CONFIG_GENERATION_COMMAND = "/opt/pretools/pipeline_config_generator_main.pl";
    //private static final String DEFAULT_PROCESSING_IMAGE = "intrepo.uky.edu:5000/gbase_1.2.1";
    private static final String DEFAULT_PROCESSING_IMAGE = "gbase_1.2.1";
    private static final String DEFAULT_PROCESSING_COMMAND = "/gdata/input/commands_main.sh";

    public static void main(String[] args) {
        ArgumentParser parser = ArgumentParsers.newFor("Genomic Pipeline Management Manual Processor").build()
                .defaultHelp(true)
                .description("Manually performs GPMS processing tasks.");
        parser.addArgument("-C", "--credentials")
                .setDefault("config.properties")
                .help("Configuration file holding credential information.");
        parser.addArgument("-A", "--accesskey");
        parser.addArgument("-S", "--secretkey");
        parser.addArgument("-E", "--endpoint");
        parser.addArgument("-R", "--region");
        parser.addArgument("--raw").help("Genomic raw file S3 bucket name");
        parser.addArgument("--clinical").help("Genomic clinical file S3 bucket name");
        parser.addArgument("--research").help("Genomic research file S3 bucket name");
        parser.addArgument("--processed").help("Genomic processed file S3 bucket name");
        parser.addArgument("--results").help("Genomic results file S3 bucket name");
        parser.addArgument("--bagittype").help("BagIt type");
        parser.addArgument("--bagithash").help("BagIt hashing method");
        parser.addArgument("--bagithidden").help("Include hidden files in BagIt").action(storeTrue());
        parser.addArgument("--compression").help("Compression algorithm");
        parser.addArgument("--download").help("Download input file(s) from S3").action(storeTrue());
        parser.addArgument("--upload").help("Upload output file(s) from S3").action(storeTrue());
        parser.addArgument("--registry").help("Docker registry for images")
                .setDefault(DEFAULT_DOCKER_REGISTRY);
        parser.addArgument("--containerversion").help("Version of Docker images to use")
                .setDefault(DEFAULT_DOCKER_CONTAINER_VERSION);
        parser.addArgument("command").nargs("?");
        parser.addArgument("arguments").nargs("*");
        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        String command = ns.getString("command"), credentials = ns.getString("credentials"),
                accessKey = ns.getString("accesskey"), secretKey = ns.getString("secretkey"),
                endpoint = ns.getString("endpoint"), region = ns.getString("region"),
                rawBucketName = ns.getString("raw"), clinicalBucketName = ns.getString("clinical"),
                researchBucketName = ns.getString("research"),
                processedBucketName = ns.getString("processed"),
                resultsBucketName = ns.getString("results"), bagitType = ns.getString("bagittype"),
                bagitHash = ns.getString("bagithash"), compression = ns.getString("compression"),
                registry = ns.getString("registry"), containerVersion = ns.getString("containerversion"),
                preprocessingContainerName = DEFAULT_PREPROCESSING_IMAGE,
                preprocessingContainerCommand = DEFAULT_PREPROCESSING_COMMAND,
                configGenerationContainerName = DEFAULT_CONFIG_GENERATION_IMAGE,
                configGenerationContainerCommand = DEFAULT_CONFIG_GENERATION_COMMAND,
                processingContainerName = DEFAULT_PROCESSING_IMAGE,
                processingContainerCommand = DEFAULT_PROCESSING_COMMAND
                ;
        boolean bagitHidden = ns.getBoolean("bagithidden"), downloadFromS3 = ns.getBoolean("download"),
                uploadToS3 = ns.getBoolean("upload")
                ;
        List<String> arguments = ns.getList("arguments");

        if (command == null) {
            simpleLogger.info("Commands:");
            simpleLogger.info("\t- preprocess <flow_cell_id> <input_dir> <output_dir> [preprocess_container_name] [preprocess_container_command] [config_container_name] [config_container_command]");
            simpleLogger.info("\t- process <flow_cell_id> <sample_id> <input_dir> <output_dir> <gpackage_dir> <mrd_variant_file> [container_name] [container_command]");
            simpleLogger.info("\t- postprocess <flow_cell_id> <input_dir> <gpackage_dir>");
            simpleLogger.info("\t- results <flow_cell_id> <input_dir>");
            return;
        }

        /*logger.trace("Checking for config.properties file");
        File configFile = new File(credentials);
        if (configFile.exists()) {
            Configurations configs = new Configurations();
            try {
                Configuration config = configs.properties(configFile);
                if (config.containsKey("access.key"))
                    accessKey = config.getString("access.key");
                if (config.containsKey("secret.key"))
                    secretKey = config.getString("secret.key");
                if (config.containsKey("endpoint"))
                    endpoint = config.getString("endpoint");
                if (config.containsKey("region"))
                    region = config.getString("region");
                if (config.containsKey("bucket.raw"))
                    rawBucketName = config.getString("bucket.raw");
                if (config.containsKey("bucket.clinical"))
                    clinicalBucketName = config.getString("bucket.clinical");
                if (config.containsKey("bucket.research"))
                    researchBucketName = config.getString("bucket.research");
                if (config.containsKey("bucket.processed"))
                    processedBucketName = config.getString("bucket.processed");
                if (config.containsKey("bucket.results"))
                    resultsBucketName = config.getString("bucket.results");
                if (config.containsKey("bagit.type"))
                    bagitType = config.getString("bagit.type");
                if (config.containsKey("bagit.hashing"))
                    bagitHash = config.getString("bagit.hashing");
                if (config.containsKey("bagit.includehidden"))
                    bagitHidden = config.getBoolean("bagit.includehidden");
                if (config.containsKey("compression"))
                    compression = config.getString("compression");
                if (config.containsKey("download"))
                    downloadFromS3 = config.getBoolean("download");
                if (config.containsKey("upload"))
                    uploadToS3 = config.getBoolean("upload");
                if (config.containsKey("registry"))
                    registry = config.getString("docker_registry");
                if (config.containsKey("preprocessing_container_name"))
                    preprocessingContainerName = config.getString("preprocessing_container_name");
                if (config.containsKey("preprocessing_container_command"))
                    preprocessingContainerCommand = config.getString("preprocessing_container_command");
                if (config.containsKey("config_generation_container_name"))
                    configGenerationContainerName = config.getString("config_generation_container_name");
                if (config.containsKey("config_generation_container_command"))
                    configGenerationContainerCommand = config.getString("config_generation_container_command");
                if (config.containsKey("processing_container_name"))
                    processingContainerName = config.getString("processing_container_name");
                if (config.containsKey("processing_container_command"))
                    processingContainerCommand = config.getString("processing_container_command");
            } catch (ConfigurationException e) {
                logger.error("Error with your config.properties file.");
            }
        }
        ObjectStorage objectStorage;
        try {
            objectStorage = new ObjectStorage(accessKey, secretKey, endpoint, region);
        } catch (NullValueException | IllegalArgumentException e) {
            logger.error("Error building object storage manager: {}", e.getMessage());
            return;
        }
        Archiver archiver;
        try {
            archiver = new Archiver(bagitType, bagitHash, bagitHidden, compression);
        } catch (NullValueException | IllegalArgumentException e) {
            logger.error("Error building archiver: {}", e.getMessage());
            return;
        }
        DataManager dataManager;
        try {
            dataManager = new DataManager(objectStorage, archiver, rawBucketName, clinicalBucketName, researchBucketName,
                    processedBucketName, resultsBucketName);
        } catch (NullValueException | IllegalArgumentException e) {
            logger.error("Error building data manager: {}", e.getMessage());
            return;
        }
        ProcessManager processManager;
        try {
            processManager = new ProcessManager(dataManager, downloadFromS3, uploadToS3);
            processManager.setDockerRegistry(registry);
            processManager.setDockerContainerVersion(containerVersion);
        } catch (NullValueException | IllegalArgumentException e) {
            logger.error("Error building genomics processor: {}", e.getMessage());
            return;
        }
        String requestID = "command-line";*/
        switch (command) {
            case "preprocess":
                {
                    if (arguments.size() < 3) {
                        simpleLogger.error("Usage: preprocess <flow_cell_id> <input_dir> <output_dir> [preprocess_container_name] [preprocess_container_command] [config_container_name] [config_container_command]");
                        return;
                    }
                    logger.trace("Checking for config.properties file");
                    File configFile = new File(credentials);
                    if (configFile.exists()) {
                        Configurations configs = new Configurations();
                        try {
                            Configuration config = configs.properties(configFile);
                            if (config.containsKey("access.key"))
                                accessKey = config.getString("access.key");
                            if (config.containsKey("secret.key"))
                                secretKey = config.getString("secret.key");
                            if (config.containsKey("endpoint"))
                                endpoint = config.getString("endpoint");
                            if (config.containsKey("region"))
                                region = config.getString("region");
                            if (config.containsKey("bucket.raw"))
                                rawBucketName = config.getString("bucket.raw");
                            if (config.containsKey("bucket.clinical"))
                                clinicalBucketName = config.getString("bucket.clinical");
                            if (config.containsKey("bucket.research"))
                                researchBucketName = config.getString("bucket.research");
                            if (config.containsKey("bucket.processed"))
                                processedBucketName = config.getString("bucket.processed");
                            if (config.containsKey("bucket.results"))
                                resultsBucketName = config.getString("bucket.results");
                            if (config.containsKey("bagit.type"))
                                bagitType = config.getString("bagit.type");
                            if (config.containsKey("bagit.hashing"))
                                bagitHash = config.getString("bagit.hashing");
                            if (config.containsKey("bagit.includehidden"))
                                bagitHidden = config.getBoolean("bagit.includehidden");
                            if (config.containsKey("compression"))
                                compression = config.getString("compression");
                            if (config.containsKey("download"))
                                downloadFromS3 = config.getBoolean("download");
                            if (config.containsKey("upload"))
                                uploadToS3 = config.getBoolean("upload");
                            if (config.containsKey("registry"))
                                registry = config.getString("docker_registry");
                            if (config.containsKey("preprocessing_container_name"))
                                preprocessingContainerName = config.getString("preprocessing_container_name");
                            if (config.containsKey("preprocessing_container_command"))
                                preprocessingContainerCommand = config.getString("preprocessing_container_command");
                            if (config.containsKey("config_generation_container_name"))
                                configGenerationContainerName = config.getString("config_generation_container_name");
                            if (config.containsKey("config_generation_container_command"))
                                configGenerationContainerCommand = config.getString("config_generation_container_command");
                            if (config.containsKey("processing_container_name"))
                                processingContainerName = config.getString("processing_container_name");
                            if (config.containsKey("processing_container_command"))
                                processingContainerCommand = config.getString("processing_container_command");
                        } catch (ConfigurationException e) {
                            logger.error("Error with your config.properties file.");
                        }
                    }
                    ObjectStorage objectStorage;
                    try {
                        objectStorage = new ObjectStorage(accessKey, secretKey, endpoint, region);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building object storage manager: {}", e.getMessage());
                        return;
                    }
                    Archiver archiver;
                    try {
                        archiver = new Archiver(bagitType, bagitHash, bagitHidden, compression);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building archiver: {}", e.getMessage());
                        return;
                    }
                    DataManager dataManager;
                    try {
                        dataManager = new DataManager(objectStorage, archiver, rawBucketName, clinicalBucketName, researchBucketName,
                                processedBucketName, resultsBucketName);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building data manager: {}", e.getMessage());
                        return;
                    }
                    ProcessManager processManager;
                    try {
                        processManager = new ProcessManager(dataManager, downloadFromS3, uploadToS3);
                        processManager.setDockerRegistry(registry);
                        processManager.setDockerContainerVersion(containerVersion);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building genomics processor: {}", e.getMessage());
                        return;
                    }
                    String requestID = "cli-preprocess-request";
                    String flowCellID = arguments.get(0);
                    Path inputDir = Paths.get(arguments.get(1));
                    Path outputDir = Paths.get(arguments.get(2));
                    if (arguments.size() > 3)
                        preprocessingContainerName = arguments.get(3);
                    if (arguments.size() > 4)
                        preprocessingContainerCommand = arguments.get(4);
                    if (arguments.size() > 5)
                        configGenerationContainerName = arguments.get(5);
                    if (arguments.size() > 6)
                        configGenerationContainerCommand = arguments.get(6);
                    processManager.preprocessBaggedFlowCell(flowCellID, requestID, inputDir, outputDir,
                            preprocessingContainerName, preprocessingContainerCommand,
                            configGenerationContainerName, configGenerationContainerCommand);
                }
                break;
            /*case "preprocess-config":
                {
                    if (arguments.size() < 2) {
                        simpleLogger.error("Usage: preprocess-config <flow_cell_id> <output_dir>");
                        return;
                    }
                    String flowCellID = arguments.get(0);
                    Path outputDir = Paths.get(arguments.get(1));
                    processManager.preprocessBaggedFlowCellTestUploadConfig(flowCellID, outputDir);
                }
                break;*/
            case "process":
                {
                    if (arguments.size() < 6) {
                        simpleLogger.error("Usage: process <flow_cell_id> <sample_id> <input_dir> <output_dir> <gpackage_dir> <mrd_variant_file> [container_name] [container_command]");
                        return;
                    }
                    logger.trace("Checking for config.properties file");
                    File configFile = new File(credentials);
                    if (configFile.exists()) {
                        Configurations configs = new Configurations();
                        try {
                            Configuration config = configs.properties(configFile);
                            if (config.containsKey("access.key"))
                                accessKey = config.getString("access.key");
                            if (config.containsKey("secret.key"))
                                secretKey = config.getString("secret.key");
                            if (config.containsKey("endpoint"))
                                endpoint = config.getString("endpoint");
                            if (config.containsKey("region"))
                                region = config.getString("region");
                            if (config.containsKey("bucket.raw"))
                                rawBucketName = config.getString("bucket.raw");
                            if (config.containsKey("bucket.clinical"))
                                clinicalBucketName = config.getString("bucket.clinical");
                            if (config.containsKey("bucket.research"))
                                researchBucketName = config.getString("bucket.research");
                            if (config.containsKey("bucket.processed"))
                                processedBucketName = config.getString("bucket.processed");
                            if (config.containsKey("bucket.results"))
                                resultsBucketName = config.getString("bucket.results");
                            if (config.containsKey("bagit.type"))
                                bagitType = config.getString("bagit.type");
                            if (config.containsKey("bagit.hashing"))
                                bagitHash = config.getString("bagit.hashing");
                            if (config.containsKey("bagit.includehidden"))
                                bagitHidden = config.getBoolean("bagit.includehidden");
                            if (config.containsKey("compression"))
                                compression = config.getString("compression");
                            if (config.containsKey("download"))
                                downloadFromS3 = config.getBoolean("download");
                            if (config.containsKey("upload"))
                                uploadToS3 = config.getBoolean("upload");
                            if (config.containsKey("registry"))
                                registry = config.getString("docker_registry");
                            if (config.containsKey("preprocessing_container_name"))
                                preprocessingContainerName = config.getString("preprocessing_container_name");
                            if (config.containsKey("preprocessing_container_command"))
                                preprocessingContainerCommand = config.getString("preprocessing_container_command");
                            if (config.containsKey("config_generation_container_name"))
                                configGenerationContainerName = config.getString("config_generation_container_name");
                            if (config.containsKey("config_generation_container_command"))
                                configGenerationContainerCommand = config.getString("config_generation_container_command");
                            if (config.containsKey("processing_container_name"))
                                processingContainerName = config.getString("processing_container_name");
                            if (config.containsKey("processing_container_command"))
                                processingContainerCommand = config.getString("processing_container_command");
                        } catch (ConfigurationException e) {
                            logger.error("Error with your config.properties file.");
                        }
                    }
                    ObjectStorage objectStorage;
                    try {
                        objectStorage = new ObjectStorage(accessKey, secretKey, endpoint, region);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building object storage manager: {}", e.getMessage());
                        return;
                    }
                    Archiver archiver;
                    try {
                        archiver = new Archiver(bagitType, bagitHash, bagitHidden, compression);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building archiver: {}", e.getMessage());
                        return;
                    }
                    DataManager dataManager;
                    try {
                        dataManager = new DataManager(objectStorage, archiver, rawBucketName, clinicalBucketName, researchBucketName,
                                processedBucketName, resultsBucketName);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building data manager: {}", e.getMessage());
                        return;
                    }
                    ProcessManager processManager;
                    try {
                        processManager = new ProcessManager(dataManager, downloadFromS3, uploadToS3);
                        processManager.setDockerRegistry(registry);
                        processManager.setDockerContainerVersion(containerVersion);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building genomics processor: {}", e.getMessage());
                        return;
                    }
                    String requestID = "cli-process-request";
                    String flowCellID = arguments.get(0);
                    String sampleID = arguments.get(1);
                    Path inputDir = Paths.get(arguments.get(2));
                    Path outputDir = Paths.get(arguments.get(3));
                    Path gPackageDir = Paths.get(arguments.get(4));
                    Path mrdVariantFile = Paths.get(arguments.get(5));
                    if (!Files.exists(mrdVariantFile)) {
                        simpleLogger.error("MRD variant file [{}] does not exist", mrdVariantFile);
                        return;
                    }
                    String mrdVariants = null;
                    try {
                        mrdVariants = new String(Files.readAllBytes(mrdVariantFile));
                    } catch (IOException e) {
                        simpleLogger.error("Failed to read in MRD variants from file [{}]", mrdVariantFile);
                        return;
                    }
                    if (arguments.size() > 6)
                        processingContainerName = arguments.get(6);
                    if (arguments.size() > 7)
                        processingContainerCommand = arguments.get(7);
                    processManager.processBaggedSample(flowCellID, sampleID, requestID, inputDir, outputDir, gPackageDir,
                            mrdVariants, processingContainerName, processingContainerCommand);
                }
                break;
            /*case "process-targets":
                {
                    if (arguments.size() < 5) {
                        simpleLogger.error("Usage: process-target <flow_cell_id> <sample_id> <input_dir> <output_dir> <gpackage_base>");
                        return;
                    }
                    String flowCellID = arguments.get(0);
                    String sampleID = arguments.get(1);
                    Path inputDir = Paths.get(arguments.get(2));
                    Path outputDir = Paths.get(arguments.get(3));
                    Path gPackageBase = Paths.get(arguments.get(4));
                    processManager.testProcessBaggedSampleRunTargets(flowCellID, sampleID, inputDir, outputDir, gPackageBase);
                }
                break;
            case "upload-process-results":
                {
                    if (arguments.size() < 3) {
                        simpleLogger.error("Usage: upload-process-results <flow_cell_id> <sample_id> <upload_dir>");
                        return;
                    }
                    String flowCellID = arguments.get(0);
                    String sampleID = arguments.get(1);
                    Path uploadDir = Paths.get(arguments.get(2));
                    processManager.processBaggedSampleUploadResults(flowCellID, sampleID, uploadDir, new ArrayList<>());
                }
                break;*/
            case "postprocess":
                {
                    if (arguments.size() < 3) {
                        simpleLogger.error("Usage: postprocess <flow_cell_id> <input_dir> <gpackage_dir>");
                        return;
                    }
                    logger.trace("Checking for config.properties file");
                    File configFile = new File(credentials);
                    if (configFile.exists()) {
                        Configurations configs = new Configurations();
                        try {
                            Configuration config = configs.properties(configFile);
                            if (config.containsKey("access.key"))
                                accessKey = config.getString("access.key");
                            if (config.containsKey("secret.key"))
                                secretKey = config.getString("secret.key");
                            if (config.containsKey("endpoint"))
                                endpoint = config.getString("endpoint");
                            if (config.containsKey("region"))
                                region = config.getString("region");
                            if (config.containsKey("bucket.raw"))
                                rawBucketName = config.getString("bucket.raw");
                            if (config.containsKey("bucket.clinical"))
                                clinicalBucketName = config.getString("bucket.clinical");
                            if (config.containsKey("bucket.research"))
                                researchBucketName = config.getString("bucket.research");
                            if (config.containsKey("bucket.processed"))
                                processedBucketName = config.getString("bucket.processed");
                            if (config.containsKey("bucket.results"))
                                resultsBucketName = config.getString("bucket.results");
                            if (config.containsKey("bagit.type"))
                                bagitType = config.getString("bagit.type");
                            if (config.containsKey("bagit.hashing"))
                                bagitHash = config.getString("bagit.hashing");
                            if (config.containsKey("bagit.includehidden"))
                                bagitHidden = config.getBoolean("bagit.includehidden");
                            if (config.containsKey("compression"))
                                compression = config.getString("compression");
                            if (config.containsKey("download"))
                                downloadFromS3 = config.getBoolean("download");
                            if (config.containsKey("upload"))
                                uploadToS3 = config.getBoolean("upload");
                            if (config.containsKey("registry"))
                                registry = config.getString("docker_registry");
                            if (config.containsKey("preprocessing_container_name"))
                                preprocessingContainerName = config.getString("preprocessing_container_name");
                            if (config.containsKey("preprocessing_container_command"))
                                preprocessingContainerCommand = config.getString("preprocessing_container_command");
                            if (config.containsKey("config_generation_container_name"))
                                configGenerationContainerName = config.getString("config_generation_container_name");
                            if (config.containsKey("config_generation_container_command"))
                                configGenerationContainerCommand = config.getString("config_generation_container_command");
                            if (config.containsKey("processing_container_name"))
                                processingContainerName = config.getString("processing_container_name");
                            if (config.containsKey("processing_container_command"))
                                processingContainerCommand = config.getString("processing_container_command");
                        } catch (ConfigurationException e) {
                            logger.error("Error with your config.properties file.");
                        }
                    }
                    ObjectStorage objectStorage;
                    try {
                        objectStorage = new ObjectStorage(accessKey, secretKey, endpoint, region);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building object storage manager: {}", e.getMessage());
                        return;
                    }
                    Archiver archiver;
                    try {
                        archiver = new Archiver(bagitType, bagitHash, bagitHidden, compression);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building archiver: {}", e.getMessage());
                        return;
                    }
                    DataManager dataManager;
                    try {
                        dataManager = new DataManager(objectStorage, archiver, rawBucketName, clinicalBucketName, researchBucketName,
                                processedBucketName, resultsBucketName);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building data manager: {}", e.getMessage());
                        return;
                    }
                    ProcessManager processManager;
                    try {
                        processManager = new ProcessManager(dataManager, downloadFromS3, uploadToS3);
                        processManager.setDockerRegistry(registry);
                        processManager.setDockerContainerVersion(containerVersion);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building genomics processor: {}", e.getMessage());
                        return;
                    }
                    String requestID = "cli-postprocess-request";
                    String flowCellID = arguments.get(0);
                    Path inputDir = Paths.get(arguments.get(1));
                    Path gPackageDir = Paths.get(arguments.get(2));
                    processManager.postprocessFlowCell(flowCellID, requestID, inputDir, gPackageDir);
                }
                break;
            case "results":
                {
                    if (arguments.size() < 2) {
                        simpleLogger.error("Usage: results <flow_cell_id> <input_dir>");
                        return;
                    }
                    logger.trace("Checking for config.properties file");
                    File configFile = new File(credentials);
                    if (configFile.exists()) {
                        Configurations configs = new Configurations();
                        try {
                            Configuration config = configs.properties(configFile);
                            if (config.containsKey("access.key"))
                                accessKey = config.getString("access.key");
                            if (config.containsKey("secret.key"))
                                secretKey = config.getString("secret.key");
                            if (config.containsKey("endpoint"))
                                endpoint = config.getString("endpoint");
                            if (config.containsKey("region"))
                                region = config.getString("region");
                            if (config.containsKey("bucket.raw"))
                                rawBucketName = config.getString("bucket.raw");
                            if (config.containsKey("bucket.clinical"))
                                clinicalBucketName = config.getString("bucket.clinical");
                            if (config.containsKey("bucket.research"))
                                researchBucketName = config.getString("bucket.research");
                            if (config.containsKey("bucket.processed"))
                                processedBucketName = config.getString("bucket.processed");
                            if (config.containsKey("bucket.results"))
                                resultsBucketName = config.getString("bucket.results");
                            if (config.containsKey("bagit.type"))
                                bagitType = config.getString("bagit.type");
                            if (config.containsKey("bagit.hashing"))
                                bagitHash = config.getString("bagit.hashing");
                            if (config.containsKey("bagit.includehidden"))
                                bagitHidden = config.getBoolean("bagit.includehidden");
                            if (config.containsKey("compression"))
                                compression = config.getString("compression");
                            if (config.containsKey("download"))
                                downloadFromS3 = config.getBoolean("download");
                            if (config.containsKey("upload"))
                                uploadToS3 = config.getBoolean("upload");
                            if (config.containsKey("registry"))
                                registry = config.getString("docker_registry");
                            if (config.containsKey("preprocessing_container_name"))
                                preprocessingContainerName = config.getString("preprocessing_container_name");
                            if (config.containsKey("preprocessing_container_command"))
                                preprocessingContainerCommand = config.getString("preprocessing_container_command");
                            if (config.containsKey("config_generation_container_name"))
                                configGenerationContainerName = config.getString("config_generation_container_name");
                            if (config.containsKey("config_generation_container_command"))
                                configGenerationContainerCommand = config.getString("config_generation_container_command");
                            if (config.containsKey("processing_container_name"))
                                processingContainerName = config.getString("processing_container_name");
                            if (config.containsKey("processing_container_command"))
                                processingContainerCommand = config.getString("processing_container_command");
                        } catch (ConfigurationException e) {
                            logger.error("Error with your config.properties file.");
                        }
                    }
                    ObjectStorage objectStorage;
                    try {
                        objectStorage = new ObjectStorage(accessKey, secretKey, endpoint, region);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building object storage manager: {}", e.getMessage());
                        return;
                    }
                    Archiver archiver;
                    try {
                        archiver = new Archiver(bagitType, bagitHash, bagitHidden, compression);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building archiver: {}", e.getMessage());
                        return;
                    }
                    DataManager dataManager;
                    try {
                        dataManager = new DataManager(objectStorage, archiver, rawBucketName, clinicalBucketName, researchBucketName,
                                processedBucketName, resultsBucketName);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building data manager: {}", e.getMessage());
                        return;
                    }
                    ProcessManager processManager;
                    try {
                        processManager = new ProcessManager(dataManager, downloadFromS3, uploadToS3);
                        processManager.setDockerRegistry(registry);
                        processManager.setDockerContainerVersion(containerVersion);
                    } catch (NullValueException | IllegalArgumentException e) {
                        logger.error("Error building genomics processor: {}", e.getMessage());
                        return;
                    }
                    String requestID = "cli-results-request";
                    String flowCellID = arguments.get(0);
                    Path inputDir = Paths.get(arguments.get(1));
                    processManager.downloadFlowCellResults(flowCellID, requestID, inputDir);
                }
                break;
            default:
                simpleLogger.error("Unknown command [{}]", command);
                break;
        }
    }
}
