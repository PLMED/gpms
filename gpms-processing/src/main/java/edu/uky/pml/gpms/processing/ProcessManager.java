package edu.uky.pml.gpms.processing;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogMessage;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.exceptions.*;
import com.spotify.docker.client.messages.*;
import edu.uky.pml.gpms.data.DataManager;
import edu.uky.pml.gpms.utilities.helpers.FlowCellPipelineConfig;
import edu.uky.pml.gpms.utilities.helpers.GPMSStatics;
import edu.uky.pml.gpms.utilities.helpers.GenomicsJSON;
import edu.uky.pml.gpms.utilities.helpers.SamplePipelineConfig;
import edu.uky.pml.gpms.utilities.logging.BasicGPMSLogger;
import edu.uky.pml.gpms.utilities.logging.GPMSLogger;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"SameParameterValue", "WeakerAccess", "BooleanMethodIsAlwaysInverted"})
public class ProcessManager {
    private DataManager dataManager;
    private GPMSLogger logger;

    private boolean downloadFromS3 = true;
    private boolean uploadToS3 = true;

    private String dockerRegistry;
    private String dockerContainerVersion;

    private StringBuilder output = new StringBuilder();

    private DockerClient dockerClient = null;
    private String dockerRunningID = null;

    // Constructors

    public ProcessManager(DataManager dataManager) {
        this(dataManager, new BasicGPMSLogger(ProcessManager.class));
    }

    public ProcessManager(DataManager dataManager, GPMSLogger logger) {
        this.dataManager = dataManager;
        setLogger(logger);
    }

    public ProcessManager(DataManager dataManager, boolean downloadFromS3, boolean uploadToS3) {
        this(dataManager);
        setDownloadFromS3(downloadFromS3);
        setUploadToS3(uploadToS3);
    }

    /*public ProcessManager(DataManager dataManager, GPMSLogger logger, boolean downloadFromS3, boolean uploadToS3) {
        this(dataManager, logger);
        setDownloadFromS3(downloadFromS3);
        setUploadToS3(uploadToS3);
    }*/

    // Getters/Setters

    public GPMSLogger getLogger() {
        return logger;
    }

    public void setDownloadFromS3(boolean downloadFromS3) {
        this.downloadFromS3 = downloadFromS3;
    }

    public boolean getDownloadFromS3() {
        return downloadFromS3;
    }

    public void setUploadToS3(boolean uploadToS3) {
        this.uploadToS3 = uploadToS3;
    }

    public boolean getUploadToS3() {
        return uploadToS3;
    }

    public String getDockerRegistry() {
        return dockerRegistry;
    }

    public void setDockerRegistry(String dockerRegistry) {
        this.dockerRegistry = dockerRegistry;
    }

    public String getDockerContainerVersion() {
        return dockerContainerVersion;
    }

    public void setDockerContainerVersion(String dockerContainerVersion) {
        this.dockerContainerVersion = dockerContainerVersion;
    }

    public String getOutput() {
        return output.toString();
    }

    public String getOutputTail(int nLines) {
        String[] lines = output.toString().split("\n");
        StringBuilder ret = new StringBuilder();
        int start = (lines.length > nLines) ? (lines.length - nLines) : 0;
        for (int i = start; i < lines.length; i++)
            ret.append(String.format("%s\n", lines[i]));
        return ret.toString();
    }

    // Flow Cell Preprocessing

    /*public void preprocessBaggedFlowCell(String flowCellID, String requestID, Path inputDir, Path outputDir,
                                         String preprocessing_container_name) {
        preprocessBaggedFlowCell(flowCellID, requestID, inputDir, outputDir, preprocessing_container_name,
                GPMSStatics.PREPROCESSING_CONTAINER_COMMAND);
    }*/

    /*public void preprocessBaggedFlowCell(String flowCellID, String requestID, Path inputDir, Path outputDir,
                                         String preprocessing_container_name, String preprocessing_container_command) {
        logger.debug("preprocessBaggedFlowCell('{}','{}','{}','{}','{}')", flowCellID, inputDir, outputDir,
                preprocessing_container_name, preprocessing_container_command);
        logger.setFlowCellID(flowCellID);
        logger.setRequestID(requestID);
        logger.setStage(GPMSStatics.PREPROCESSING_STAGE);
        logger.setStep(0);
        try {
            logger.gpmsInfo("Starting to preprocess flow cell");
            output.append("Starting to preprocess flow cell\n");
            Thread.sleep(1000);
            if (!preprocessBaggedFlowCellCheckAndPrepare(flowCellID, inputDir, outputDir,
                    preprocessing_container_name, preprocessing_container_command)) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed sequence preprocessor initialization");
                return;
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Preprocessor initialization was interrupted");
            return;
        } catch (Exception e) {
            logger.gpmsFailure("preprocessBaggedFlowCell exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return;
        }
        try {
            output.append("Downloading raw flow cell file(s)\n");
            Thread.sleep(1000);
            if (getDownloadFromS3() && !preprocessBaggedFlowCellDownloadFlowCell(flowCellID, inputDir)) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed to download flow cell file");
                return;
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Flow cell download was interrupted");
            return;
        } catch (Exception e) {
            logger.gpmsFailure("preprocessBaggedFlowCell exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return;
        }
        try {
            Thread.sleep(1000);
            long retCode = preprocessBaggedFlowCellRunContainer(flowCellID, inputDir, outputDir,
                    preprocessing_container_name, preprocessing_container_command, "preprocessing");
            if (retCode != 0) {
                Thread.sleep(1000);
                if (retCode == -1)
                    logger.gpmsFailure("Failed to preprocess flow cell");
                else {
                    switch ((int)retCode) {
                        case 1:     // Container error encountered
                            logger.gpmsFailure("General Docker Error Encountered (Err: 1)");
                            break;
                        case 100:   // Script failure encountered
                            logger.gpmsFailure("Processing Pipeline encountered an error (Err: 100)");
                            break;
                        case 125:   // Container failed to run
                            logger.gpmsFailure("Container failed to run (Err: 125)");
                            break;
                        case 126:   // Container command cannot be invoked
                            logger.gpmsFailure("Container command failed to be invoked (Err: 126)");
                            break;
                        case 127:   // Container command cannot be found
                            logger.gpmsFailure("Container command could not be found (Err: 127)");
                            break;
                        case 137:   // Container was killed
                            logger.gpmsFailure("Container was manually stopped (Err: 137)");
                            break;
                        case 404:
                            logger.gpmsFailure("Docker image [{}] does not exist on this machine",
                                    preprocessing_container_name);
                        default:    // Other return code encountered
                            logger.gpmsError("Unknown container return code (Err: {})", retCode);
                            break;
                    }
                }
                return;
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Flow cell preprocessing was interrupted");
            return;
        } catch (Exception e) {
            logger.gpmsFailure("preprocessBaggedFlowCell exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return;
        }
        try {
            Thread.sleep(1000);
            if (getUploadToS3() && !preprocessBaggedFlowCellUploadResults(flowCellID, outputDir)) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed to upload flow cell results");
                return;
            }
            logger.incrementStep();
            Thread.sleep(1000);
            logger.gpmsInfo("Flow cell preprocessing complete");
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Flow cell results upload was interrupted");
        } catch (Exception e) {
            logger.gpmsFailure("preprocessBaggedFlowCell exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
        }
    }*/

    /*private boolean preprocessBaggedFlowCellCheckAndPrepare(String flowCellID, Path inputDir, Path outputDir,
                                                            String container_name, String container_command) {
        logger.debug("preprocessBaggedFlowCellCheckAndPrepare('{}','{}','{}','{}','{}')",
                flowCellID, inputDir, outputDir, container_name, container_command);
        logger.gpmsInfo("Checking and preparing flow cell preprocessor");
        try {
            if (downloadFromS3) {
                if (Files.exists(inputDir))
                    if (!deleteDirectory(inputDir)) {
                        logger.gpmsError("Failed to remove existing input directory [{}]", inputDir);
                        return false;
                    }
                try {
                    Files.createDirectories(inputDir);
                } catch (IOException e) {
                    logger.gpmsError("Failed to create input directory [{}]", inputDir);
                    return false;
                }
            } else {
                Path flowCellDir = inputDir.resolve(flowCellID);
                if (!Files.exists(inputDir) || !Files.exists(flowCellDir)) {
                    logger.gpmsError("Input file(s) [{}] do not exist", flowCellDir);
                    return false;
                }
            }
            if (Files.exists(outputDir))
                if (!deleteDirectory(outputDir)) {
                    logger.gpmsError("Failed to remove existing output directory [{}]", outputDir);
                    return false;
                }
            try {
                Files.createDirectories(outputDir);
            } catch (IOException e) {
                logger.gpmsError("Failed to create output directory [{}]", outputDir);
                return false;
            }
            Path clinicalResultsDir = outputDir.resolve(GPMSStatics.PREPROCESSING_CLINICAL_RESULTS_FOLDER_NAME);
            try {
                Files.createDirectories(clinicalResultsDir);
            } catch (IOException e) {
                logger.gpmsError("Failed to create clinical results directory [{}]", clinicalResultsDir);
                return false;
            }
            Path researchResultsDir = outputDir.resolve(GPMSStatics.PREPROCESSING_RESEARCH_RESULTS_FOLDER_NAME);
            try {
                Files.createDirectories(researchResultsDir);
            } catch (IOException e) {
                logger.gpmsError("Failed to create research results directory [{}]", researchResultsDir);
                return false;
            }
            if (!hasDockerImage(container_name)) {
                logger.gpmsError("Genomic preprocessing container [{}] does not exist on this machine", container_name);
                return false;
            }
            if (container_command == null || StringUtils.isBlank(container_command)) {
                logger.gpmsError("Genomic preprocessing container command not supplied");
                return false;
            }
            return true;
        } catch (Exception e) {
            logger.gpmsError("preprocessBaggedFlowCellCheckAndPrepare Exception [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return false;
        }
    }*/

    public void preprocessBaggedFlowCell(String flowCellID, String requestID, Path inputDir, Path outputDir,
                                         String preprocessing_container_name, String preprocessing_container_command,
                                         String config_generation_container_name, String config_generation_container_command) {
        logger.debug("preprocessBaggedFlowCell('{}','{}','{}','{}','{}','{}','{}')", flowCellID, inputDir, outputDir,
                preprocessing_container_name, preprocessing_container_command,
                config_generation_container_name, config_generation_container_command);
        logger.setFlowCellID(flowCellID);
        logger.setRequestID(requestID);
        logger.setStage(GPMSStatics.PREPROCESSING_STAGE);
        logger.setStep(0);
        int existingPreprocessorRegistryIdx = preprocessing_container_name.indexOf("/") + 1;
        preprocessing_container_name = String.format("%s/%s:%s", getDockerRegistry(),
                preprocessing_container_name.substring(existingPreprocessorRegistryIdx),
                getDockerContainerVersion());
        int existingConfigGenRegistryIdx = config_generation_container_name.indexOf("/") + 1;
        config_generation_container_name = String.format("%s/%s:%s", getDockerRegistry(),
                config_generation_container_name.substring(existingConfigGenRegistryIdx), getDockerContainerVersion());
        try {
            logger.gpmsInfo("Starting to preprocess flow cell");
            output.append("Starting to preprocess flow cell\n");
            Thread.sleep(1000);
            if (!preprocessBaggedFlowCellCheckAndPrepare(flowCellID, inputDir, outputDir,
                    preprocessing_container_name, preprocessing_container_command,
                    config_generation_container_name, config_generation_container_command)) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed sequence preprocessor initialization");
                return;
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Preprocessor initialization was interrupted");
            return;
        } catch (Exception e) {
            logger.gpmsFailure("preprocessBaggedFlowCell exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return;
        }
        try {
            output.append("Downloading raw flow cell file(s)\n");
            Thread.sleep(1000);
            if (getDownloadFromS3() && !preprocessBaggedFlowCellDownloadFlowCell(flowCellID, inputDir)) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed to download flow cell file");
                return;
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Flow cell download was interrupted");
            return;
        } catch (Exception e) {
            logger.gpmsFailure("preprocessBaggedFlowCell exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return;
        }
        try {
            Thread.sleep(1000);
            long retCode = preprocessBaggedFlowCellRunContainer(flowCellID, inputDir, outputDir,
                    preprocessing_container_name, preprocessing_container_command, "preprocessing");
            if (retCode != 0) {
                Thread.sleep(1000);
                if (retCode == -1)
                    logger.gpmsFailure("Failed to preprocess flow cell");
                else {
                    switch ((int)retCode) {
                        case 1:     // Container error encountered
                            logger.gpmsFailure("General Docker Error Encountered (Err: 1)");
                            break;
                        case 100:   // Script failure encountered
                            logger.gpmsFailure("Processing Pipeline encountered an error (Err: 100)");
                            break;
                        case 125:   // Container failed to run
                            logger.gpmsFailure("Container failed to run (Err: 125)");
                            break;
                        case 126:   // Container command cannot be invoked
                            logger.gpmsFailure("Container command failed to be invoked (Err: 126)");
                            break;
                        case 127:   // Container command cannot be found
                            logger.gpmsFailure("Container command could not be found (Err: 127)");
                            break;
                        case 137:   // Container was killed
                            logger.gpmsFailure("Container was manually stopped (Err: 137)");
                            break;
                        case 404:
                            logger.gpmsFailure("Docker image [{}] does not exist on this machine",
                                    preprocessing_container_name);
                        default:    // Other return code encountered
                            logger.gpmsFailure("Unknown container return code (Err: {})", retCode);
                            break;
                    }
                }
                return;
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Flow cell preprocessing was interrupted");
            return;
        } catch (Exception e) {
            logger.gpmsFailure("preprocessBaggedFlowCell exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return;
        }
        try {
            Thread.sleep(1000);
            Path clinicalResultsDir = outputDir.resolve(GPMSStatics.PREPROCESSING_CLINICAL_RESULTS_FOLDER_NAME);
            Path flowCellResultsDir = clinicalResultsDir.resolve(flowCellID);
            if (Files.exists(flowCellResultsDir) && Files.isDirectory(flowCellResultsDir)) {
                long retCode = preprocessBaggedFlowCellRunContainer(flowCellID, inputDir, outputDir,
                        config_generation_container_name, config_generation_container_command, "config generation");
                if (retCode != 0) {
                    Thread.sleep(1000);
                    if (retCode == -1)
                        logger.gpmsFailure("Failed to generate sample configs");
                    else {
                        switch ((int) retCode) {
                            case 1:     // Container error encountered
                                logger.gpmsFailure("General Docker Error Encountered (Err: 1)");
                                break;
                            case 100:   // Script failure encountered
                                logger.gpmsFailure("Processing Pipeline encountered an error (Err: 100)");
                                break;
                            case 125:   // Container failed to run
                                logger.gpmsFailure("Container failed to run (Err: 125)");
                                break;
                            case 126:   // Container command cannot be invoked
                                logger.gpmsFailure("Container command failed to be invoked (Err: 126)");
                                break;
                            case 127:   // Container command cannot be found
                                logger.gpmsFailure("Container command could not be found (Err: 127)");
                                break;
                            case 137:   // Container was killed
                                logger.gpmsFailure("Container was manually stopped (Err: 137)");
                                break;
                            case 404:
                                logger.gpmsFailure("Docker image [{}] does not exist on this machine",
                                        config_generation_container_name);
                            default:    // Other return code encountered
                                logger.gpmsError("Unknown container return code (Err: {})", retCode);
                                break;
                        }
                    }
                    return;
                }
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Flow cell preprocessing was interrupted");
            return;
        } catch (Exception e) {
            logger.gpmsFailure("preprocessBaggedFlowCell exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return;
        }
        try {
            Thread.sleep(1000);
            if (getUploadToS3() && !preprocessBaggedFlowCellUploadResults(flowCellID, outputDir)) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed to upload flow cell results");
                return;
            }
            logger.incrementStep();
            Thread.sleep(1000);
            logger.gpmsInfo("Flow cell preprocessing complete");
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Flow cell results upload was interrupted");
        } catch (Exception e) {
            logger.gpmsFailure("preprocessBaggedFlowCell exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
        }
    }

    private boolean preprocessBaggedFlowCellCheckAndPrepare(String flowCellID, Path inputDir, Path outputDir,
                                                            String preprocessing_container_name, String preprocessing_container_command,
                                                            String config_generation_container_name, String config_generation_container_command) {
        logger.debug("preprocessBaggedFlowCellCheckAndPrepare('{}','{}','{}','{}','{}')",
                flowCellID, inputDir, outputDir,
                preprocessing_container_name, preprocessing_container_command,
                config_generation_container_name, config_generation_container_command);
        logger.gpmsInfo("Checking and preparing flow cell preprocessor");
        try {
            if (downloadFromS3) {
                if (Files.exists(inputDir))
                    if (!deleteDirectory(inputDir)) {
                        logger.gpmsError("Failed to remove existing input directory [{}]", inputDir);
                        return false;
                    }
                try {
                    Files.createDirectories(inputDir);
                } catch (IOException e) {
                    logger.gpmsError("Failed to create input directory [{}]", inputDir);
                    return false;
                }
            } else {
                Path flowCellDir = inputDir.resolve(flowCellID);
                if (!Files.exists(inputDir) || !Files.exists(flowCellDir)) {
                    logger.gpmsError("Input file(s) [{}] do not exist", flowCellDir);
                    return false;
                }
            }
            if (Files.exists(outputDir))
                if (!deleteDirectory(outputDir)) {
                    logger.gpmsError("Failed to remove existing output directory [{}]", outputDir);
                    return false;
                }
            try {
                Files.createDirectories(outputDir);
            } catch (IOException e) {
                logger.gpmsError("Failed to create output directory [{}]", outputDir);
                return false;
            }
            Path clinicalResultsDir = outputDir.resolve(GPMSStatics.PREPROCESSING_CLINICAL_RESULTS_FOLDER_NAME);
            try {
                Files.createDirectories(clinicalResultsDir);
            } catch (IOException e) {
                logger.gpmsError("Failed to create clinical results directory [{}]", clinicalResultsDir);
                return false;
            }
            Path researchResultsDir = outputDir.resolve(GPMSStatics.PREPROCESSING_RESEARCH_RESULTS_FOLDER_NAME);
            try {
                Files.createDirectories(researchResultsDir);
            } catch (IOException e) {
                logger.gpmsError("Failed to create research results directory [{}]", researchResultsDir);
                return false;
            }
            if (!hasDockerImage(preprocessing_container_name) && !pullDockerImage(preprocessing_container_name)) {
                logger.gpmsError("Genomic preprocessing container [{}] does not exist on this machine", preprocessing_container_name);
                return false;
            }
            if (preprocessing_container_command == null || StringUtils.isBlank(preprocessing_container_command)) {
                logger.gpmsError("Genomic preprocessing container command not supplied");
                return false;
            }
            if (!hasDockerImage(config_generation_container_name) && !pullDockerImage(config_generation_container_name)) {
                logger.gpmsError("Genomic sample config generation container [{}] does not exist on this machine", config_generation_container_name);
                return false;
            }
            if (config_generation_container_command == null || StringUtils.isBlank(config_generation_container_command)) {
                logger.gpmsError("Genomic sample config generation container command not supplied");
                return false;
            }
            return true;
        } catch (Exception e) {
            logger.gpmsError("preprocessBaggedFlowCellCheckAndPrepare Exception [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return false;
        }
    }

    private boolean preprocessBaggedFlowCellDownloadFlowCell(String flowCellID, Path inputDir) {
        logger.debug("preprocessBaggedFlowCellDownloadFlowCell('{}','{}')", flowCellID, inputDir);
        dataManager.updateLogger(logger);
        logger.gpmsInfo("Downloading flow cell file(s)");
        try {
            if (!dataManager.downloadFlowCellRawFiles(flowCellID, inputDir)) {
                logger.gpmsError("Failed to download raw flow cell file(s)");
                return false;
            }
            return true;
        } catch (Exception e) {
            logger.gpmsError("Flow cell download exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return false;
        }
    }

    private long preprocessBaggedFlowCellRunContainer(String flowCellID, Path inputDir, Path outputDir,
                                                     String container_name, String container_command, String phase) {
        logger.debug("preprocessBaggedFlowCellRunContainer('{}','{}','{}','{}','{}')",
                flowCellID, inputDir, outputDir, container_name, container_command);
        logger.gpmsInfo("Starting [{}] pipeline via Docker container", phase);
        Path clinicalResultsDir = outputDir.resolve(GPMSStatics.PREPROCESSING_CLINICAL_RESULTS_FOLDER_NAME);
        Path researchResultsDir = outputDir.resolve(GPMSStatics.PREPROCESSING_RESEARCH_RESULTS_FOLDER_NAME);
        output = new StringBuilder();
        output.append(String.format("Executing: docker run --rm -t " +
                        "-v %s:/gdata/output/clinical " +
                        "-v %s:/gdata/output/research " +
                        "-v %s:/gdata/input " +
                        "-e INPUT_FOLDER_PATH=/gdata/input/%s " +
                        "-t %s %s\n",
                clinicalResultsDir, researchResultsDir, inputDir, flowCellID, container_name, container_command));
        final HostConfig hostConfig = HostConfig.builder()
                .appendBinds(String.format("%s:/gdata/output/clinical", clinicalResultsDir))
                .appendBinds(String.format("%s:/gdata/output/research", researchResultsDir))
                .appendBinds(String.format("%s:/gdata/input", inputDir))
                .build();
        final ContainerConfig containerConfig = ContainerConfig.builder()
                .hostConfig(hostConfig)
                .env(String.format("INPUT_FOLDER_PATH=/gdata/input/%s", flowCellID))
                .image(container_name)
                .cmd(container_command)
                .build();
        return runDockerContainer(containerConfig, phase);
    }

    private GenomicsJSON grabSampleJSONConfig(Path dir) {
        Path sampleConfigDir = dir.resolve(GPMSStatics.PROCESSING_CONFIGS_FOLDER);
        Path sampleJSONPipelineConfigFile = sampleConfigDir.resolve(GPMSStatics.PROCESSING_JSON_PIPELINE_CONFIG_FILE);
        if (!Files.exists(sampleConfigDir) || !Files.exists(sampleJSONPipelineConfigFile)) {
            logger.gpmsInfo("Sample configuration file(s) [{}] does not exist",
                    (Files.exists(sampleConfigDir)) ?
                            sampleJSONPipelineConfigFile
                            :
                            sampleConfigDir
            );
            return null;
        }
        try {
            Gson gson = new Gson();
            String json = new String(Files.readAllBytes(sampleJSONPipelineConfigFile));
            GenomicsJSON genomicsJSON = gson.fromJson(json, GenomicsJSON.class);
            String containerName = genomicsJSON.Processing_details.Docker_container;
            if (containerName == null) {
                logger.gpmsInfo("No valid Docker image given in JSON configuration file [{}]",
                        sampleJSONPipelineConfigFile);
                return null;
            }
            return genomicsJSON;
        } catch (JsonSyntaxException e) {
            logger.gpmsError("Improperly formatted JSON configuration file [{}]: {}",
                    sampleJSONPipelineConfigFile, e.getMessage());
            return null;
        } catch (IOException e) {
            logger.gpmsInfo("Failed to read JSON configuration file [{}]", sampleJSONPipelineConfigFile);
            return null;
        }
    }

    /*public void preprocessBaggedFlowCellTestUploadConfig(String flowCellID, Path outputDir) {
        logger.debug("preprocessBaggedFlowCellTestUploadConfig('{}','{}')", flowCellID, outputDir);
        dataManager.updateLogger(logger);
        try {
            Path clinicalResultsDir = outputDir.resolve(GPMSStatics.PREPROCESSING_CLINICAL_RESULTS_FOLDER_NAME).resolve(flowCellID);
            if (Files.exists(clinicalResultsDir)) {
                try {
                    if (!dataManager.uploadFlowCellClinicalPipelineFiles(flowCellID, clinicalResultsDir)) {
                        logger.gpmsError("Failed to upload flow cell pipeline configuration files");
                    }
                } catch (IllegalArgumentException e) {
                    logger.gpmsError("Flow cell pipeline configuration files error: {}", e.getMessage());
                }
            } else {
                logger.gpmsError("Output directory [{}] does not exist", outputDir);
            }
        } catch (Exception e) {
            logger.gpmsError("Test config upload exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
        }
    }*/

    private boolean preprocessBaggedFlowCellUploadResults(String flowCellID, Path outputDir) {
        logger.debug("preprocessBaggedFlowCellUploadResults('{}','{}')", flowCellID, outputDir);
        dataManager.updateLogger(logger);
        try {
            Path clinicalResultsDir = outputDir.resolve(GPMSStatics.PREPROCESSING_CLINICAL_RESULTS_FOLDER_NAME).resolve(flowCellID);
            if (Files.exists(clinicalResultsDir)) {
                try {
                    if (!dataManager.uploadFlowCellClinicalPipelineFiles(flowCellID, clinicalResultsDir)) {
                        logger.gpmsError("Failed to upload flow cell pipeline configuration files");
                        return false;
                    }
                } catch (IllegalArgumentException e) {
                    logger.gpmsError("Flow cell pipeline configuration files error: {}", e.getMessage());
                    return false;
                }
                String sampleList = getSampleList(clinicalResultsDir);
                Map<String, GenomicsJSON> sampleConfigs = new HashMap<>();
                List<String> sampleNames = new ArrayList<>();
                if (sampleList != null) {
                    logger.gpmsInfo("Transferring Clinical Results Directory. Sample list: {}", sampleList);
                    String[] samples = sampleList.split(",");
                    boolean uploaded = true;
                    for (String sampleID : samples) {
                        String sampleName = String.join("/", flowCellID, sampleID);
                        logger.setSampleID(sampleName);
                        dataManager.updateLogger(logger);
                        Path samplePath = clinicalResultsDir.resolve(sampleName);
                        GenomicsJSON genomicsJSON = grabSampleJSONConfig(samplePath);
                        if (genomicsJSON != null)
                            sampleConfigs.put(sampleName, genomicsJSON);
                        try {
                            if (dataManager.uploadSampleClinicalFiles(flowCellID, sampleID, samplePath)) {
                                logger.gpmsInfo("Clinical sample [{}] upload complete", sampleID);
                                sampleNames.add(sampleName);
                            } else {
                                logger.gpmsError("Failed to upload clinical sample [{}]", sampleID);
                                uploaded = false;
                            }
                        } catch (Exception e) {
                            logger.gpmsError("processBaggedFlowCellUploadResults exception encountered [{}:{}]",
                                    e.getClass().getCanonicalName(), e.getMessage());
                            return false;
                        }
                    }
                    logger.setSampleID(null);
                    dataManager.updateLogger(logger);
                    if (uploaded) {
                        Map<String, String> customParams = new HashMap<String, String>()
                        {{
                            put("sample_list", String.join(",", sampleNames));
                        }};
                        for (Map.Entry<String, GenomicsJSON> sampleConfig : sampleConfigs.entrySet()) {
                            if (sampleConfig.getValue() != null)
                                customParams.put(String.format("%s_config", sampleConfig.getKey()), new String(Base64.encodeBase64(sampleConfig.getValue().toJson().getBytes())));
                        }
                        logger.gpmsInfo(customParams, "Sending sample list and configs");
                        logger.gpmsInfo("Clinical results directory transfer complete");
                    } else {
                        logger.gpmsError("Some sample(s) failed to upload");
                        return false;
                    }
                } else {
                    logger.gpmsInfo("No samples found in clinical results folder");
                }
            } else {
                logger.gpmsInfo("No clinical results generated");
            }
            logger.setSampleID(null);
            dataManager.updateLogger(logger);
            Path researchResultsDir = outputDir.resolve(GPMSStatics.PREPROCESSING_RESEARCH_RESULTS_FOLDER_NAME).resolve(flowCellID);
            if (Files.exists(researchResultsDir)) {
                logger.gpmsInfo("Transferring Research Results Directory");
                if (dataManager.uploadFlowCellResearchFiles(flowCellID, researchResultsDir)) {
                    logger.gpmsInfo("Research results directory uploaded");
                } else {
                    logger.gpmsError("Failed to upload research results [{}]", flowCellID);
                    return false;
                }
            } else {
                logger.gpmsInfo("No research results generated");
            }
            return true;
        } catch (Exception e) {
            logger.gpmsError("Results upload exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return false;
        }
    }

    // Sample Processing

    public void processBaggedSample(String flowCellID, String rawSampleID, String requestID, Path inputDir, Path outputDir,
                                    Path gPackageBase, String mrdVariants) {
        logger.debug("processBaggedSample('{}','{}','{}','{}','{}','{}')", flowCellID, rawSampleID, requestID,
                inputDir, outputDir, gPackageBase);
        logger.setFlowCellID(flowCellID);
        logger.setSampleID(rawSampleID);
        logger.setRequestID(requestID);
        logger.setStage(GPMSStatics.PROCESSING_STAGE);
        logger.setStep(0);
        int sampleIDPrefix = rawSampleID.lastIndexOf("/") + 1;
        final String sampleID = rawSampleID.substring(sampleIDPrefix);
        try {
            logger.gpmsInfo("Starting to process sample");
            output.append("Starting to process sample\n");
            Thread.sleep(1000);
            if (!processBaggedSampleCheckAndPrepare(flowCellID, sampleID, inputDir, outputDir, gPackageBase,
                    mrdVariants)) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed sample processor initialization");
                return;
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Sample processing was interrupted");
            return;
        } catch (Exception e) {
            logger.gpmsFailure("processBaggedSample exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return;
        }
        try {
            Thread.sleep(1000);
            output.append("Downloading sample from S3\n");
            if (getDownloadFromS3() && !processBaggedSampleDownloadSample(flowCellID, sampleID, inputDir)) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed to download sample file");
                return;
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Sample processing was interrupted");
            return;
        } catch (Exception e) {
            logger.gpmsFailure("processBaggedSample exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return;
        }
        logger.gpmsInfo("Checking sample JSON configuration information");
        SamplePipelineConfig samplePipelineConfig = processBaggedSampleGrabConfig(flowCellID, sampleID, inputDir);
        if (samplePipelineConfig == null) {
            logger.gpmsFailure("Failed to load pipeline JSON configuration file");
            return;
        }
        String container_name = samplePipelineConfig.Processing_details.Docker_container;
        Path gPackageDir = gPackageBase.resolve(container_name).resolve("gpackage");
        logger.gpmsInfo("Using gpackage directory [{}]", gPackageDir);
        if (!Files.exists(gPackageDir) || !Files.isDirectory(gPackageDir)) {
            logger.gpmsError("Genomic package directory [{}] does not exist or is not a directory",
                    gPackageDir);
            return;
        }
        String container_command = GPMSStatics.PROCESSING_CONTAINER_COMMAND;
        int existingRegistryIdx = container_name.indexOf("/") + 1;
        container_name = String.format("%s/%s:%s", getDockerRegistry(), container_name.substring(existingRegistryIdx),
                getDockerContainerVersion());
        if (!hasDockerImage(container_name) && !pullDockerImage(container_name)) {
            logger.gpmsFailure("No valid Docker container provided");
            return;
        }
        logger.gpmsInfo("Writing MRD variant file");
        output.append("Writing MRD variant file\n");
        Path flowCellDir = inputDir.resolve(flowCellID);
        Path sampleDir = flowCellDir.resolve(sampleID);
        Path sampleMRDVariantFile = sampleDir.resolve(String.format("%s_mrd_list.txt", sampleID));
        try {
            if (Files.exists(sampleMRDVariantFile))
                Files.delete(sampleMRDVariantFile);
            Files.write(sampleMRDVariantFile, mrdVariants.getBytes());
        } catch (IOException e) {
            logger.gpmsFailure(String.format("Failed to write out MRD variant file [%s] with contents:\n",
                    sampleMRDVariantFile) + mrdVariants);
            return;
        }
        try {
            Thread.sleep(1000);
            output.append(String.format("Running Docker image [%s] with command [%s]\n",
                    container_name, container_command));
            long retCode = processBaggedSampleRunContainer(flowCellID, sampleID, gPackageDir, inputDir,
                    outputDir, container_name, container_command, "processing");
            if (retCode != 0) {
                Thread.sleep(1000);
                if (retCode == -1)
                    logger.gpmsFailure("Failed to process sample");
                else {
                    switch ((int)retCode) {
                        case 1:     // Container error encountered
                            logger.gpmsFailure("General Docker Error Encountered (Err: 1)");
                            break;
                        case 100:   // Script failure encountered
                            logger.gpmsFailure("Processing Pipeline encountered an error (Err: 100)");
                            break;
                        case 125:   // Container failed to run
                            logger.gpmsFailure("Container failed to run (Err: 125)");
                            break;
                        case 126:   // Container command cannot be invoked
                            logger.gpmsFailure("Container command failed to be invoked (Err: 126)");
                            break;
                        case 127:   // Container command cannot be found
                            logger.gpmsFailure("Container command could not be found (Err: 127)");
                            break;
                        case 137:   // Container was killed
                            logger.gpmsFailure("Container was manually stopped (Err: 137)");
                            break;
                        case 404:
                            logger.gpmsFailure("Docker image [{}] does not exist on this machine",
                                    samplePipelineConfig.Processing_details.Docker_container);
                        default:    // Other return code encountered
                            logger.gpmsFailure("Unknown container return code (Err: {})", retCode);
                            break;
                    }
                }
                return;
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Sample processing was interrupted");
            return;
        } catch (Exception e) {
            logger.gpmsFailure("processBaggedSample exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return;
        }
        try {
            logger.gpmsInfo("Running processing target(s)");
            output.append("Running the processing target(s)\n");
            Thread.sleep(1000);
            Path flowCellResultsDir = outputDir.resolve(flowCellID);
            String panel = null;
            Path sampleResultsDir = flowCellResultsDir.resolve(sampleID);
            if (!Files.exists(sampleResultsDir)) {
                try {
                    List<Path> typedSampleResultsDirs = new ArrayList<>();
                    Files.walk(flowCellResultsDir).filter(Files::isDirectory).forEach(path -> {
                        if (path.getFileName().toString().equals(sampleID))
                            typedSampleResultsDirs.add(path);
                    });
                    if (typedSampleResultsDirs.size() == 1) {
                        sampleResultsDir = typedSampleResultsDirs.get(0);
                        panel = sampleResultsDir.getParent().getFileName().toString();
                    } else {
                        if (typedSampleResultsDirs.size() > 1)
                            logger.gpmsFailure("Found multiple possible results folders: {}",
                                    StringUtils.join(typedSampleResultsDirs, ", "));
                        else
                            logger.gpmsFailure("Failed to find results directory named [{}] in path [{}]",
                                    sampleID, flowCellResultsDir);
                        return;
                    }
                } catch (IOException e) {
                    logger.gpmsFailure("Exception trying to find sample results directory [{}]", e.getMessage());
                    logger.error("processBaggedSample IOException:\n" + ExceptionUtils.getStackTrace(e));
                    return;
                }
            }
            for (SamplePipelineConfig.Target target : samplePipelineConfig.target) {
                String containerName = target.Docker_container;
                String containerCommand = target.target_command;
                String phase = target.target_name;
                gPackageDir = gPackageBase.resolve(containerName).resolve("gpackage");
                logger.gpmsInfo("Using gpackage directory [{}]", gPackageDir);
                if (!Files.exists(gPackageDir) || !Files.isDirectory(gPackageDir)) {
                    logger.gpmsError("Genomic package directory [{}] does not exist or is not a directory",
                            gPackageDir);
                    return;
                }
                existingRegistryIdx = containerName.indexOf("/") + 1;
                containerName = String.format("%s/%s:%s", getDockerRegistry(), containerName.substring(existingRegistryIdx),
                        getDockerContainerVersion());
                if (!hasDockerImage(containerName) && !pullDockerImage(containerName)) {
                    Thread.sleep(1000);
                    logger.gpmsFailure("Required Docker image [{}] does not exist on this machine", containerName);
                    return;
                }
                output.append(String.format("Running Docker container [%s] with command [%s]\n",
                        containerName, containerCommand));
                long retCode = processBaggedSampleRunTarget(flowCellID, sampleID, gPackageDir, sampleResultsDir,
                        containerName, containerCommand, phase);
                if (retCode != 0) {
                    Thread.sleep(1000);
                    if (retCode == -1)
                        logger.gpmsFailure("Failed to process sample");
                    else {
                        switch ((int)retCode) {
                            case 1:     // Container error encountered
                                logger.gpmsFailure("General Docker Error Encountered (Err: 1)");
                                break;
                            case 100:   // Script failure encountered
                                logger.gpmsFailure("Processing Pipeline encountered an error (Err: 100)");
                                break;
                            case 125:   // Container failed to run
                                logger.gpmsFailure("Container failed to run (Err: 125)");
                                break;
                            case 126:   // Container command cannot be invoked
                                logger.gpmsFailure("Container command failed to be invoked (Err: 126)");
                                break;
                            case 127:   // Container command cannot be found
                                logger.gpmsFailure("Container command could not be found (Err: 127)");
                                break;
                            case 137:   // Container was killed
                                logger.gpmsFailure("Container was manually stopped (Err: 137)");
                                break;
                            case 404:
                                logger.gpmsFailure("Docker image [{}] does not exist on this machine",
                                        containerName);
                                break;
                            default:    // Other return code encountered
                                logger.gpmsFailure("Unknown container return code (Err: {})", retCode);
                                break;
                        }
                    }
                    return;
                }
            }
            logger.incrementStep();
        } catch (InterruptedException e) {
            logger.gpmsFailure("Pipeline target execution interrupted");
        } catch (Exception e) {
            logger.gpmsFailure("processBaggedSample target execution exception encountered:\n" +
                    ExceptionUtils.getStackTrace(e));
        }
        try {
            Thread.sleep(1000);
            output.append("Uploading processed sample to S3\n");
            if (getUploadToS3() && !processBaggedSampleUploadResults(flowCellID, sampleID, outputDir,
                    samplePipelineConfig.Download_exclusion_list)) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed to upload sample results");
                return;
            }
            logger.incrementStep();
            Thread.sleep(1000);
            logger.gpmsInfo("Sample processing complete");
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Sample processing was interrupted");
        } catch (Exception e) {
            logger.gpmsFailure("processBaggedSample exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
        }
    }

    private SamplePipelineConfig processBaggedSampleGrabConfig(String flowCellID, String sampleID, Path inputDir) {
        logger.debug("processBaggedSampleGrabConfig('{}','{}','{}')", flowCellID, sampleID, inputDir);
        Path flowCellInputDir = inputDir.resolve(flowCellID);
        Path sampleInputDir = flowCellInputDir.resolve(sampleID);
        Path sampleConfigDir = sampleInputDir.resolve(GPMSStatics.PROCESSING_CONFIGS_FOLDER);
        Path samplePipelineConfigFile = sampleConfigDir.resolve(GPMSStatics.PROCESSING_JSON_PIPELINE_CONFIG_FILE);
        if (!Files.exists(sampleConfigDir) || !Files.exists(samplePipelineConfigFile)) {
            logger.gpmsInfo("Sample configuration file(s) [{}] does not exist",
                            (Files.exists(sampleConfigDir)) ?
                                    samplePipelineConfigFile
                                    :
                                    sampleConfigDir
                    );
            return null;
        }
        try {
            Gson gson = new Gson();
            String json = new String(Files.readAllBytes(samplePipelineConfigFile));
            SamplePipelineConfig samplePipelineConfig = gson.fromJson(json, SamplePipelineConfig.class);
            String containerName = samplePipelineConfig.Processing_details.Docker_container;
            if (containerName == null) {
                logger.gpmsInfo("No valid Docker image given in JSON configuration file [{}]",
                        samplePipelineConfigFile);
                return null;
            }
            int existingRegistryIdx = containerName.indexOf("/") + 1;
            containerName = String.format("%s/%s:%s", getDockerRegistry(), containerName.substring(existingRegistryIdx),
                    getDockerContainerVersion());
            if (!hasDockerImage(containerName) && !pullDockerImage(containerName)) {
                logger.gpmsInfo("No valid Docker image given in JSON configuration file [{}]",
                        samplePipelineConfigFile);
                return null;
            }
            return samplePipelineConfig;
        } catch (IOException e) {
            logger.gpmsInfo("Failed to read JSON configuration file [{}]", samplePipelineConfigFile);
            return null;
        }
    }

    /*private boolean processBaggedSampleCheckAndPrepare(String flowCellID, String sampleID,
                                                       Path inputDir, Path outputDir, Path gPackageDir) {
        logger.debug("processBaggedSampleCheckAndPrepare('{}','{}','{}','{}')",
                flowCellID, sampleID, inputDir, outputDir, gPackageDir);
        logger.gpmsInfo("Checking and preparing sample processor");
        try {
            if (downloadFromS3) {
                if (Files.exists(inputDir))
                    if (!deleteDirectory(inputDir)) {
                        logger.gpmsError("Failed to remove existing input directory [{}]", inputDir);
                        return false;
                    }
                try {
                    Files.createDirectories(inputDir);
                } catch (IOException e) {
                    logger.gpmsError("Failed to create input directory [{}]", inputDir);
                    return false;
                }
            } else {
                Path flowCellInputDir = inputDir.resolve(flowCellID);
                Path sampleInputDir = flowCellInputDir.resolve(sampleID);
                if (!Files.exists(inputDir) || !Files.exists(flowCellInputDir) || !Files.exists(sampleInputDir)) {
                    logger.gpmsError("Input file(s) [{}] do not exist",
                                    (Files.exists(inputDir)) ?
                                            (Files.exists(flowCellInputDir)) ?
                                                    sampleInputDir
                                                    :
                                                    flowCellInputDir
                                            :
                                            inputDir
                            );
                    return false;
                }
            }
            if (Files.exists(outputDir))
                if (!deleteDirectory(outputDir)) {
                    logger.gpmsError("Failed to remove existing output directory [{}]", outputDir);
                    return false;
                }
            try {
                Files.createDirectories(outputDir);
            } catch (IOException e) {
                logger.gpmsError("Failed to create output directory [{}]", outputDir);
                return false;
            }
            if (!Files.exists(gPackageDir) || !Files.isDirectory(gPackageDir)) {
                logger.gpmsError("Genomic package directory [{}] does not exist or is not a directory", gPackageDir);
                return false;
            }
            return true;
        } catch (Exception e) {
            logger.gpmsError("Check and prepare exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return false;
        }
    }*/

    public void processBaggedSample(String flowCellID, String rawSampleID, String requestID, Path inputDir, Path outputDir,
                                    Path gPackageBase, String mrdVariants, String container_name) {
        processBaggedSample(flowCellID, rawSampleID, requestID, inputDir, outputDir, gPackageBase, mrdVariants,
                container_name, GPMSStatics.PROCESSING_CONTAINER_COMMAND);
    }

    public void processBaggedSample(String flowCellID, String rawSampleID, String requestID, Path inputDir, Path outputDir,
                                    Path gPackageBase, String mrdVariants, String container_name,
                                    String container_command) {
        logger.debug("processBaggedSample('{}','{}','{}','{}','{}','{}','{}')", flowCellID, rawSampleID, requestID,
                inputDir, outputDir, container_name, container_command);
        logger.setFlowCellID(flowCellID);
        logger.setSampleID(rawSampleID);
        logger.setRequestID(requestID);
        logger.setStage(GPMSStatics.PROCESSING_STAGE);
        logger.setStep(0);
        int sampleIDPrefix = rawSampleID.lastIndexOf("/") + 1;
        final String sampleID = rawSampleID.substring(sampleIDPrefix);
        try {
            logger.gpmsInfo("Starting to process sample");
            output.append("Starting to process sample\n");
            Thread.sleep(1000);
            if (!processBaggedSampleCheckAndPrepare(flowCellID, sampleID, inputDir, outputDir, gPackageBase,
                    mrdVariants)) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed sample processor initialization");
                return;
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Sample processing was interrupted");
            return;
        } catch (Exception e) {
            logger.gpmsFailure("processBaggedSample exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return;
        }
        try {
            Thread.sleep(1000);
            output.append("Downloading sample from S3\n");
            if (getDownloadFromS3() && !processBaggedSampleDownloadSample(flowCellID, sampleID, inputDir)) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed to download sample file");
                return;
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Sample processing was interrupted");
            return;
        } catch (Exception e) {
            logger.gpmsFailure("processBaggedSample exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return;
        }
        logger.gpmsInfo("Checking sample JSON configuration information");
        output.append("Checking sample JSON configuration information\n");
        SamplePipelineConfig samplePipelineConfig = processBaggedSampleGrabConfig(flowCellID, sampleID, inputDir);
        if (samplePipelineConfig == null) {
            logger.gpmsFailure("Failed to find sample pipeline configuration file");
            return;
        }
        container_name = samplePipelineConfig.Processing_details.Docker_container;
        List<String> exclusions = samplePipelineConfig.Download_exclusion_list;
        Path gPackageDir = gPackageBase.resolve(container_name).resolve("gpackage");
        logger.gpmsInfo("Using gpackage directory [{}]", gPackageDir);
        if (!Files.exists(gPackageDir) || !Files.isDirectory(gPackageDir)) {
            logger.gpmsFailure("Genomic package directory [{}] does not exist or is not a directory",
                    gPackageDir);
            return;
        }
        int existingRegistryIdx = container_name.indexOf("/") + 1;
        container_name = String.format("%s/%s:%s", getDockerRegistry(), container_name.substring(existingRegistryIdx),
                getDockerContainerVersion());
        if (!hasDockerImage(container_name) && !pullDockerImage(container_name)) {
            logger.gpmsFailure("No valid Docker container provided");
            return;
        }
        logger.gpmsInfo("Writing MRD variant file");
        output.append("Writing MRD variant file\n");
        Path flowCellDir = inputDir.resolve(flowCellID);
        Path sampleDir = flowCellDir.resolve(sampleID);
        Path sampleMRDVariantFile = sampleDir.resolve(String.format("%s_mrd_list.txt", sampleID));
        try {
            if (Files.exists(sampleMRDVariantFile))
                Files.delete(sampleMRDVariantFile);
            Files.write(sampleMRDVariantFile, mrdVariants.getBytes());
        } catch (IOException e) {
            logger.gpmsFailure(String.format("Failed to write out MRD variant file [%s] with contents:\n",
                    sampleMRDVariantFile) + mrdVariants);
            return;
        }
        try {
            Thread.sleep(1000);
            output.append(String.format("Running Docker container [%s] with command [%s]\n",
                    container_name, container_command));
            long retCode = processBaggedSampleRunContainer(flowCellID, sampleID, gPackageDir, inputDir,
                    outputDir, container_name, container_command, "processing");
            if (retCode != 0) {
                Thread.sleep(1000);
                if (retCode == -1)
                    logger.gpmsFailure("Failed to process sample");
                else {
                    switch ((int)retCode) {
                        case 1:     // Container error encountered
                            logger.gpmsFailure("General Docker Error Encountered (Err: 1)");
                            break;
                        case 100:   // Script failure encountered
                            logger.gpmsFailure("Processing Pipeline encountered an error (Err: 100)");
                            break;
                        case 125:   // Container failed to run
                            logger.gpmsFailure("Container failed to run (Err: 125)");
                            break;
                        case 126:   // Container command cannot be invoked
                            logger.gpmsFailure("Container command failed to be invoked (Err: 126)");
                            break;
                        case 127:   // Container command cannot be found
                            logger.gpmsFailure("Container command could not be found (Err: 127)");
                            break;
                        case 137:   // Container was killed
                            logger.gpmsFailure("Container was manually stopped (Err: 137)");
                            break;
                        case 404:
                            logger.gpmsFailure("Docker image [{}] does not exist on this machine",
                                    container_name);
                        default:    // Other return code encountered
                            logger.gpmsFailure("Unknown container return code (Err: {})", retCode);
                            break;
                    }
                }
                return;
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Sample processing was interrupted");
            return;
        } catch (Exception e) {
            logger.gpmsFailure("processBaggedSample exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return;
        }
        try {
            logger.gpmsInfo("Running processing target(s)");
            output.append("Running the processing target(s)\n");
            Thread.sleep(1000);
            Path flowCellResultsDir = outputDir.resolve(flowCellID);
            String panel = null;
            Path sampleResultsDir = flowCellResultsDir.resolve(sampleID);
            if (!Files.exists(sampleResultsDir)) {
                try {
                    List<Path> typedSampleResultsDirs = new ArrayList<>();
                    Files.walk(flowCellResultsDir).filter(Files::isDirectory).forEach(path -> {
                        if (path.getFileName().toString().equals(sampleID))
                            typedSampleResultsDirs.add(path);
                    });
                    if (typedSampleResultsDirs.size() == 1) {
                        sampleResultsDir = typedSampleResultsDirs.get(0);
                        panel = sampleResultsDir.getParent().getFileName().toString();
                    } else {
                        if (typedSampleResultsDirs.size() > 1)
                            logger.gpmsFailure("Found multiple possible results folders: {}",
                                    StringUtils.join(typedSampleResultsDirs, ", "));
                        else
                            logger.gpmsFailure("Failed to find results directory named [{}] in path [{}]",
                                    sampleID, flowCellResultsDir);
                        return;
                    }
                } catch (IOException e) {
                    logger.gpmsFailure("Exception trying to find sample results directory [{}]", e.getMessage());
                    logger.error("processBaggedSample IOException:\n" + ExceptionUtils.getStackTrace(e));
                    return;
                }
            }
            for (SamplePipelineConfig.Target target : samplePipelineConfig.target) {
                String containerName = target.Docker_container;
                String containerCommand = target.target_command;
                String phase = target.target_name;
                gPackageDir = gPackageBase.resolve(containerName).resolve("gpackage");
                logger.gpmsInfo("Using gpackage directory [{}]", gPackageDir);
                if (!Files.exists(gPackageDir) || !Files.isDirectory(gPackageDir)) {
                    logger.gpmsError("Genomic package directory [{}] does not exist or is not a directory",
                            gPackageDir);
                    return;
                }
                existingRegistryIdx = containerName.indexOf("/") + 1;
                containerName = String.format("%s/%s:%s", getDockerRegistry(), containerName.substring(existingRegistryIdx),
                        getDockerContainerVersion());
                if (!hasDockerImage(containerName) && !pullDockerImage(containerName)) {
                    Thread.sleep(1000);
                    logger.gpmsFailure("Required Docker image [{}] does not exist on this machine", containerName);
                    return;
                }
                output.append(String.format("Running Docker container [%s] with command [%s]\n",
                        containerName, containerCommand));
                long retCode = processBaggedSampleRunTarget(flowCellID, sampleID, gPackageDir, sampleResultsDir,
                        containerName, containerCommand, phase);
                if (retCode != 0) {
                    Thread.sleep(1000);
                    if (retCode == -1)
                        logger.gpmsFailure("Failed to process sample");
                    else {
                        switch ((int)retCode) {
                            case 1:     // Container error encountered
                                logger.gpmsFailure("General Docker Error Encountered (Err: 1)");
                                break;
                            case 100:   // Script failure encountered
                                logger.gpmsFailure("Processing Pipeline encountered an error (Err: 100)");
                                break;
                            case 125:   // Container failed to run
                                logger.gpmsFailure("Container failed to run (Err: 125)");
                                break;
                            case 126:   // Container command cannot be invoked
                                logger.gpmsFailure("Container command failed to be invoked (Err: 126)");
                                break;
                            case 127:   // Container command cannot be found
                                logger.gpmsFailure("Container command could not be found (Err: 127)");
                                break;
                            case 137:   // Container was killed
                                logger.gpmsFailure("Container was manually stopped (Err: 137)");
                                break;
                            case 404:
                                logger.gpmsFailure("Docker image [{}] does not exist on this machine",
                                        containerName);
                                break;
                            default:    // Other return code encountered
                                logger.gpmsFailure("Unknown container return code (Err: {})", retCode);
                                break;
                        }
                    }
                    return;
                }
            }
            logger.incrementStep();
        } catch (InterruptedException e) {
            logger.gpmsFailure("Pipeline target execution interrupted");
        } catch (Exception e) {
            logger.gpmsFailure("processBaggedSample target execution exception encountered:\n" +
                    ExceptionUtils.getStackTrace(e));
        }
        try {
            Thread.sleep(1000);
            output.append("Uploading processed sample results to S3\n");
            if (getUploadToS3() && !processBaggedSampleUploadResults(flowCellID, sampleID, outputDir, exclusions)) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed to upload sample results");
                return;
            }
            logger.incrementStep();
            Thread.sleep(1000);
            logger.gpmsInfo("Sample processing complete");
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Sample processing was interrupted");
        } catch (Exception e) {
            logger.gpmsFailure("processBaggedSample exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
        }
    }

    /*public void testProcessBaggedSampleRunTargets(String flowCellID, String sampleID, Path inputDir, Path outputDir,
                                                  Path gPackageBase) {
        SamplePipelineConfig samplePipelineConfig = processBaggedSampleGrabConfig(flowCellID, sampleID, inputDir);
        if (samplePipelineConfig == null) {
            logger.gpmsFailure("Failed to find sample pipeline configuration file");
            return;
        }
        try {
            output.append("Running the processing target\n");
            Thread.sleep(1000);
            Path flowCellResultsDir = outputDir.resolve(flowCellID);
            String panel = null;
            Path sampleResultsDir = flowCellResultsDir.resolve(sampleID);
            if (!Files.exists(sampleResultsDir)) {
                try {
                    List<Path> typedSampleResultsDirs = new ArrayList<>();
                    Files.walk(flowCellResultsDir).filter(Files::isDirectory).forEach(path -> {
                        if (path.getFileName().toString().equals(sampleID))
                            typedSampleResultsDirs.add(path);
                    });
                    if (typedSampleResultsDirs.size() == 1) {
                        sampleResultsDir = typedSampleResultsDirs.get(0);
                        panel = sampleResultsDir.getParent().getFileName().toString();
                    } else {
                        if (typedSampleResultsDirs.size() > 1)
                            logger.gpmsFailure("Found multiple possible results folders: {}",
                                    StringUtils.join(typedSampleResultsDirs, ", "));
                        else
                            logger.gpmsFailure("Failed to find results directory named [{}] in path [{}]",
                                    sampleID, flowCellResultsDir);
                        return;
                    }
                } catch (IOException e) {
                    logger.gpmsFailure("Exception trying to find sample results directory [{}]", e.getMessage());
                    logger.error("processBaggedSample IOException:\n" + ExceptionUtils.getStackTrace(e));
                    return;
                }
            }
            for (SamplePipelineConfig.Target target : samplePipelineConfig.target) {
                String containerName = target.Docker_container;
                String containerCommand = target.target_command;
                String phase = target.target_name;
                Path gPackageDir = gPackageBase.resolve(containerName).resolve("gpackage");
                logger.gpmsInfo("Using gpackage directory [{}]", gPackageDir);
                if (!Files.exists(gPackageDir) || !Files.isDirectory(gPackageDir)) {
                    logger.gpmsError("Genomic package directory [{}] does not exist or is not a directory",
                            gPackageDir);
                    return;
                }
                int existingRegistryIdx = containerName.indexOf("/") + 1;
                containerName = String.format("%s/%s:%s", getDockerRegistry(), containerName.substring(existingRegistryIdx),
                        getDockerContainerVersion());
                if (!hasDockerImage(containerName) && !pullDockerImage(containerName)) {
                    Thread.sleep(1000);
                    logger.gpmsFailure("Required Docker image [{}] does not exist on this machine", containerName);
                    return;
                }
                output.append(String.format("Running Docker container [%s] with command [%s]\n",
                        containerName, containerCommand));
                long retCode = processBaggedSampleRunTarget(flowCellID, sampleID, gPackageDir, sampleResultsDir,
                        containerName, containerCommand, phase);
                if (retCode != 0) {
                    Thread.sleep(1000);
                    if (retCode == -1)
                        logger.gpmsFailure("Failed to process sample");
                    else {
                        switch ((int)retCode) {
                            case 1:     // Container error encountered
                                logger.gpmsFailure("General Docker Error Encountered (Err: 1)");
                                break;
                            case 100:   // Script failure encountered
                                logger.gpmsFailure("Processing Pipeline encountered an error (Err: 100)");
                                break;
                            case 125:   // Container failed to run
                                logger.gpmsFailure("Container failed to run (Err: 125)");
                                break;
                            case 126:   // Container command cannot be invoked
                                logger.gpmsFailure("Container command failed to be invoked (Err: 126)");
                                break;
                            case 127:   // Container command cannot be found
                                logger.gpmsFailure("Container command could not be found (Err: 127)");
                                break;
                            case 137:   // Container was killed
                                logger.gpmsFailure("Container was manually stopped (Err: 137)");
                                break;
                            case 404:
                                logger.gpmsFailure("Docker image [{}] does not exist on this machine",
                                        containerName);
                                break;
                            default:    // Other return code encountered
                                logger.gpmsFailure("Unknown container return code (Err: {})", retCode);
                                break;
                        }
                    }
                    return;
                }
            }
            logger.incrementStep();
        } catch (InterruptedException e) {
            logger.gpmsFailure("Pipeline target execution");
        } catch (Exception e) {
            logger.gpmsFailure("postprocessFlowCell target execution exception encountered:\n" +
                    ExceptionUtils.getStackTrace(e));
        }
    }*/

    private boolean processBaggedSampleCheckAndPrepare(String flowCellID, String sampleID, Path inputDir, Path outputDir,
                                                       Path gPackageBase, String mrdVariants) {
        logger.debug("processBaggedSampleCheckAndPrepare('{}','{}','{}','{}','{}')",
                flowCellID, sampleID, inputDir, outputDir, gPackageBase);
        logger.gpmsInfo("Checking and preparing sample processor");
        try {
            if (downloadFromS3) {
                if (Files.exists(inputDir))
                    if (!deleteDirectory(inputDir)) {
                        logger.gpmsError("Failed to remove existing input directory [{}]", inputDir);
                        return false;
                    }
                try {
                    Files.createDirectories(inputDir);
                } catch (IOException e) {
                    logger.gpmsError("Failed to create input directory [{}]", inputDir);
                    return false;
                }
            } else {
                Path flowCellInputDir = inputDir.resolve(flowCellID);
                Path sampleInputDir = flowCellInputDir.resolve(sampleID);
                if (!Files.exists(inputDir) || !Files.exists(flowCellInputDir) || !Files.exists(sampleInputDir)) {
                    logger.gpmsError("Input file(s) [{}] do not exist",
                            (Files.exists(inputDir)) ?
                                    (Files.exists(flowCellInputDir)) ?
                                            sampleInputDir
                                            :
                                            flowCellInputDir
                                    :
                                    inputDir
                    );
                    return false;
                }
            }
            if (Files.exists(outputDir))
                if (!deleteDirectory(outputDir)) {
                    logger.gpmsError("Failed to remove existing output directory [{}]", outputDir);
                    return false;
                }
            try {
                Files.createDirectories(outputDir);
            } catch (IOException e) {
                logger.gpmsError("Failed to create output directory [{}]", outputDir);
                return false;
            }
            if (!Files.exists(gPackageBase) || !Files.isDirectory(gPackageBase)) {
                logger.gpmsError("Genomic package base directory [{}] does not exist or is not a directory", gPackageBase);
                return false;
            }
            if (mrdVariants == null || StringUtils.isBlank(mrdVariants)) {
                logger.gpmsError("No valid MRD variant(s) provided for this sample");
                return false;
            }
            return true;
        } catch (Exception e) {
            logger.gpmsError("Check and prepare exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return false;
        }
    }

    /*private boolean processBaggedSampleCheckAndPrepare(String flowCellID, String sampleID,
                                                       Path inputDir, Path outputDir, Path gPackageDir,
                                                       String container_name, String container_command) {
        logger.debug("processBaggedSampleCheckAndPrepare('{}','{}','{}','{}','{}','{}')",
                flowCellID, sampleID, inputDir, outputDir, gPackageDir, container_name, container_command);
        logger.gpmsInfo("Checking and preparing sample processor");
        try {
            if (downloadFromS3) {
                if (Files.exists(inputDir))
                    if (!deleteDirectory(inputDir)) {
                        logger.gpmsError("Failed to remove existing input directory [{}]", inputDir);
                        return false;
                    }
                try {
                    Files.createDirectories(inputDir);
                } catch (IOException e) {
                    logger.gpmsError("Failed to create input directory [{}]", inputDir);
                    return false;
                }
            } else {
                if (!Files.exists(inputDir) || !Files.exists(inputDir.resolve(flowCellID))) {
                    logger.gpmsError("Input file(s) [{}] does not exist", inputDir.resolve(flowCellID));
                    return false;
                }
            }
            if (Files.exists(outputDir))
                if (!deleteDirectory(outputDir)) {
                    logger.gpmsError("Failed to remove existing output directory [{}]", outputDir);
                    return false;
                }
            try {
                Files.createDirectories(outputDir);
            } catch (IOException e) {
                logger.gpmsError("Failed to create output directory [{}]", outputDir);
                return false;
            }
            if (!Files.exists(gPackageDir) || !Files.isDirectory(gPackageDir)) {
                logger.gpmsError("Genomic package directory [{}] does not exist or is not a directory", gPackageDir);
                return false;
            }
            if (getDockerImageID(container_name) == null) {
                logger.gpmsError("Genomic processing container [{}] does not exist on this machine", container_name);
                return false;
            }
            return true;
        } catch (Exception e) {
            logger.gpmsError("Check and prepare exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return false;
        }
    }*/

    private boolean processBaggedSampleDownloadSample(String flowCellID, String sampleID, Path inputDir) {
        logger.debug("processBaggedSampleDownloadSample('{}','{}','{}')",
                flowCellID, sampleID, inputDir);
        dataManager.updateLogger(logger);
        //logger.gpmsInfo("Downloading sample file(s)");
        try {
            if (!dataManager.downloadSampleClinicalFiles(flowCellID, sampleID, inputDir)) {
                logger.gpmsError("Failed to download clinical sample file(s)");
                return false;
            }
            return true;
        } catch (Exception e) {
            logger.gpmsInfo("Sample download exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return false;
        }
    }

    private long processBaggedSampleRunContainer(String flowCellID, String sampleID,
                                                  Path gPackageDir, Path inputDir, Path outputDir,
                                                  String container_name, String container_command, String phase) {
        logger.debug("processBaggedSampleRunContainer('{}','{}','{}','{}','{}','{}','{}','{}')", flowCellID,
                sampleID, gPackageDir, inputDir, outputDir, container_name, container_command, phase);
        logger.gpmsInfo("Starting pipeline via Docker container");
        Path flowCellDir = inputDir.resolve(flowCellID);
        Path sampleDir = flowCellDir.resolve(sampleID);
        Path commands_main = sampleDir.resolve("commands_main.sh");
        logger.trace("Checking for commands_main.sh at [{}]", commands_main);
        if (!Files.exists(commands_main) || !Files.isRegularFile(commands_main)) {
            logger.gpmsError("Commands file is missing");
            return -1;
        }
        if (!commands_main.toFile().setExecutable(true)) {
            logger.gpmsError("Failed to make commands file executable");
            return -1;
        }

        output = new StringBuilder();
        output.append(String.format("Executing: docker run --rm -t " +
                        "-v %s:/gdata/output " +
                        "-v %s:/gdata/input " +
                        "-v %s:/gpackage " +
                        "-t %s %s\n",
                outputDir, sampleDir, gPackageDir, container_name, container_command));
        final HostConfig hostConfig = HostConfig.builder()
                .appendBinds(String.format("%s:/gdata/output", outputDir))
                .appendBinds(String.format("%s:/gdata/input", sampleDir))
                .appendBinds(String.format("%s:/gpackage", gPackageDir))
                .build();
        final ContainerConfig containerConfig = ContainerConfig.builder()
                .hostConfig(hostConfig)
                .image(container_name)
                .cmd(container_command)
                .build();
        return runDockerContainer(containerConfig, phase);
    }

    private long processBaggedSampleRunTarget(String flowCellID, String sampleID,
                                                 Path gPackageDir, Path outputDir,
                                                 String container_name, String container_command, String phase) {
        logger.debug("processBaggedSampleRunContainer('{}','{}','{}','{}','{}','{}','{}')", flowCellID,
                sampleID, gPackageDir, outputDir, container_name, container_command, phase);
        logger.gpmsInfo("Starting pipeline via Docker container");
        output = new StringBuilder();
        output.append(String.format("Executing: docker run --rm -t -v %s:/gdata -v %s:/gpackage %s sh -c %s\n",
                outputDir, gPackageDir, container_name, container_command));
        final HostConfig hostConfig = HostConfig.builder()
                .appendBinds(String.format("%s:/gdata", outputDir))
                .appendBinds(String.format("%s:/gpackage", gPackageDir))
                .build();
        final ContainerConfig containerConfig = ContainerConfig.builder()
                .hostConfig(hostConfig)
                .image(container_name)
                .cmd("sh", "-c", container_command)
                .build();
        return runDockerContainer(containerConfig, phase);
    }

    private boolean processBaggedSampleUploadResults(String flowCellID, String sampleID, Path outputDir,
                                                    List<String> exclusions) {
        logger.debug("processBaggedSampleUploadResults('{}','{}','{}','{}')", flowCellID, sampleID, outputDir,
                String.join(";", exclusions));
        dataManager.updateLogger(logger);
        logger.gpmsInfo("Uploading processed sample file(s)");
        try {
            Path flowCellResultsDir = outputDir.resolve(flowCellID);
            Path sampleResultsDir = flowCellResultsDir.resolve(sampleID);
            if (Files.exists(sampleResultsDir)) {
                if (!dataManager.uploadSampleProcessedFiles(flowCellID, sampleID, sampleResultsDir, exclusions)) {
                    logger.gpmsError("Failed to upload processing results");
                    return false;
                }
                logger.gpmsInfo("Processed sample [{}] upload complete", sampleID);
                return true;
            } else {
                try {
                    List<Path> typedSampleResultsDirs = new ArrayList<>();
                    Files.walk(flowCellResultsDir).filter(Files::isDirectory).forEach(path -> {
                        if (path.getFileName().toString().equals(sampleID))
                            typedSampleResultsDirs.add(path);
                    });
                    if (typedSampleResultsDirs.size() == 1) {
                        Path typedSampleResultsDir = typedSampleResultsDirs.get(0);
                        String type = typedSampleResultsDir.getParent().getFileName().toString();
                        if (!dataManager.uploadTypedSampleProcessedFiles(flowCellID, type, sampleID,
                                typedSampleResultsDir, exclusions)) {
                            logger.gpmsError("Failed to upload processing results");
                            return false;
                        }
                        logger.gpmsInfo("Processed sample [{}] upload complete", sampleID);
                        return true;
                    } else {
                        if (typedSampleResultsDirs.size() > 1)
                            logger.gpmsError("Found multiple possible results folders: {}",
                                    StringUtils.join(typedSampleResultsDirs, ", "));
                        else
                            logger.gpmsError("Failed to find results directory named [{}] in path [{}]",
                                    sampleID, flowCellResultsDir);
                        return false;
                    }
                } catch (IOException e) {
                    logger.gpmsError("Exception trying to find sample results directory [{}]", e.getMessage());
                    logger.error("processBaggedSampleUploadResults IOException:\n" + ExceptionUtils.getStackTrace(e));
                    return false;
                }
            }
        } catch (Exception e) {
            logger.gpmsError("Results upload exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return false;
        }
    }

    // Flow Cell Postprocessing

    public void postprocessFlowCell(String flowCellID, String requestID, Path inputDir, Path gPackageBase) {
        logger.debug("postprocessFlowCell('{}','{}')", flowCellID, inputDir, gPackageBase);
        logger.setFlowCellID(flowCellID);
        logger.setRequestID(requestID);
        logger.setStage(GPMSStatics.POSTPROCESSING_STAGE);
        logger.setStep(0);
        try {
            logger.gpmsInfo("Starting to download processed sample results");
            output.append("Starting to download processed sample results\n");
            Thread.sleep(1000);
            if (!postprocessFlowCellCheckAndPrepare(flowCellID, inputDir, gPackageBase)) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed sequence preprocessor initialization");
                return;
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Postprocessing initialization was interrupted");
        } catch (Exception e) {
            logger.gpmsFailure("postprocessFlowCell initialization exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
        }
        try {
            if (getDownloadFromS3()) {
                output.append("Downloading processed sample file(s)\n");
                Thread.sleep(1000);
                if (!postprocessFlowCellDownloadSequence(flowCellID, inputDir)) {
                    Thread.sleep(1000);
                    logger.gpmsFailure("Failed to download flow cell file(s)");
                    return;
                }
            }
            logger.incrementStep();
            Thread.sleep(1000);
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Flow cell download was interrupted");
        } catch (Exception e) {
            logger.gpmsFailure("postprocessFlowCell download exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
        }
        try {
            output.append("Running the postprocessing container\n");
            Thread.sleep(1000);
            FlowCellPipelineConfig flowCellPipelineConfig = postprocessFlowCellGrabConfig(flowCellID, inputDir);
            if (flowCellPipelineConfig == null) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed to find any valid pipeline configuration files");
                return;
            }
            for (FlowCellPipelineConfig.Target target : flowCellPipelineConfig.target) {
                String containerName = target.Docker_container;
                String containerCommand = target.target_command.replace("sudo ", "")
                        .replace("${GPACKAGE}", "/gpackage")
                        .replace("${OUTPUT}", String.format("/gdata/output/%s", flowCellID));
                String phase = target.target_name;
                Path gPackageDir = gPackageBase.resolve(containerName).resolve("gpackage");
                logger.gpmsInfo("Using gpackage directory [{}]", gPackageDir);
                if (!Files.exists(gPackageDir) || !Files.isDirectory(gPackageDir)) {
                    logger.gpmsError("Genomic package directory [{}] does not exist or is not a directory",
                            gPackageDir);
                    return;
                }
                int existingRegistryIdx = containerName.indexOf("/") + 1;
                containerName = String.format("%s/%s:%s", getDockerRegistry(), containerName.substring(existingRegistryIdx),
                        getDockerContainerVersion());
                if (!hasDockerImage(containerName) && !pullDockerImage(containerName)) {
                    Thread.sleep(1000);
                    logger.gpmsFailure("Required Docker image [{}] does not exist on this machine", containerName);
                    return;
                }
                output.append(String.format("Running Docker container [%s] with command [%s]\n",
                        containerName, containerCommand));
                long retCode = postprocessBaggedSampleRunContainer(flowCellID, gPackageDir, inputDir, containerName,
                        containerCommand, phase);
                if (retCode != 0) {
                    Thread.sleep(1000);
                    if (retCode == -1)
                        logger.gpmsFailure("Failed to process sample");
                    else {
                        switch ((int)retCode) {
                            case 1:     // Container error encountered
                                logger.gpmsFailure("General Docker Error Encountered (Err: 1)");
                                break;
                            case 100:   // Script failure encountered
                                logger.gpmsFailure("Processing Pipeline encountered an error (Err: 100)");
                                break;
                            case 125:   // Container failed to run
                                logger.gpmsFailure("Container failed to run (Err: 125)");
                                break;
                            case 126:   // Container command cannot be invoked
                                logger.gpmsFailure("Container command failed to be invoked (Err: 126)");
                                break;
                            case 127:   // Container command cannot be found
                                logger.gpmsFailure("Container command could not be found (Err: 127)");
                                break;
                            case 137:   // Container was killed
                                logger.gpmsFailure("Container was manually stopped (Err: 137)");
                                break;
                            case 404:
                                logger.gpmsFailure("Docker image [{}] does not exist on this machine",
                                        containerName);
                                break;
                            default:    // Other return code encountered
                                logger.gpmsFailure("Unknown container return code (Err: {})", retCode);
                                break;
                        }
                    }
                    return;
                }
            }
            logger.incrementStep();
        } catch (InterruptedException e) {
            logger.gpmsFailure("Pipeline target execution");
        } catch (Exception e) {
            logger.gpmsFailure("postprocessFlowCell target execution exception encountered:\n" +
                    ExceptionUtils.getStackTrace(e));
        }
        try {
            if (getUploadToS3()) {
                Thread.sleep(1000);
                output.append("Uploading processed sample results to S3\n");
                if (!postprocessFlowCellUploadResults(flowCellID, inputDir)) {
                    Thread.sleep(1000);
                    logger.gpmsFailure("Failed to upload flow cell results");
                    return;
                }
            }
            logger.incrementStep();
            Thread.sleep(1000);
            logger.gpmsInfo("Flow cell postprocessing complete");
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Flow cell postprocessing was interrupted");
        } catch (Exception e) {
            logger.gpmsFailure("processBaggedSample exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
        }
    }

    private boolean postprocessFlowCellCheckAndPrepare(String flowCellID, Path inputDir, Path gPackageBase) {
        logger.debug("postprocessFlowCellCheckAndPrepare('{}','{}','{}')",
                flowCellID, inputDir, gPackageBase);
        logger.gpmsInfo("Checking and preparing flow cell postprocessor");
        try {
            if (downloadFromS3) {
                if (Files.exists(inputDir))
                    if (!deleteDirectory(inputDir)) {
                        logger.gpmsError("Failed to remove existing input directory [{}]", inputDir);
                        return false;
                    }
                try {
                    Files.createDirectories(inputDir);
                } catch (IOException e) {
                    logger.gpmsError("Failed to create input directory [{}]", inputDir);
                    return false;
                }
            } else {
                if (!Files.exists(inputDir) || !Files.exists(inputDir.resolve(flowCellID))) {
                    logger.gpmsError("Input file(s) [{}] do not exist", inputDir.resolve(flowCellID));
                    return false;
                }
            }
            if (!Files.exists(gPackageBase) || !Files.isDirectory(gPackageBase)) {
                logger.gpmsError("Genomic package directory [{}] does not exist or is not a directory",
                        gPackageBase);
                return false;
            }
            return true;
        } catch (Exception e) {
            logger.gpmsError("Check and prepare exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return false;
        }
    }

    private boolean postprocessFlowCellDownloadSequence(String flowCellID, Path inputDir) {
        logger.debug("postprocessFlowCellDownloadSequence('{}','{}')", flowCellID, inputDir);
        dataManager.updateLogger(logger);
        logger.gpmsInfo("Downloading processed sample results");
        try {
            if (!dataManager.downloadFlowCellProcessedFiles(flowCellID, inputDir)) {
                logger.gpmsError("Failed to download processed flow cell file(s)");
                return false;
            }
            return true;
        } catch (Exception e) {
            logger.gpmsError("Sequence download exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return false;
        }
    }

    private FlowCellPipelineConfig postprocessFlowCellGrabConfig(String flowCellID, Path inputDir) {
        logger.debug("postprocessFlowCellGrabConfig('{}','{}')", flowCellID, inputDir);
        logger.gpmsInfo("Searching for valid flow cell pipeline configuration file");
        Path flowCellDir = inputDir.resolve(flowCellID);
        Path flowCellPipelineConfigPath = flowCellDir.resolve(GPMSStatics.PREPROCESSED_FLOWCELL_CONFIG_JSON_FILE);
        if (!Files.exists(flowCellDir) || !Files.exists(flowCellPipelineConfigPath)) {
            logger.gpmsInfo("Sample configuration file(s) [{}] does not exist",
                    (Files.exists(flowCellPipelineConfigPath)) ?
                            flowCellPipelineConfigPath
                            :
                            flowCellDir
            );
            return null;
        }
        try {
            Gson gson = new Gson();
            String json = new String(Files.readAllBytes(flowCellPipelineConfigPath));
            return gson.fromJson(json, FlowCellPipelineConfig.class);
        } catch (JsonSyntaxException e) {
            logger.gpmsError("Improperly formatted JSON configuration file [{}]: {}",
                    flowCellPipelineConfigPath, e.getMessage());
            return null;
        } catch (IOException e) {
            logger.gpmsInfo("Failed to read JSON configuration file [{}]", flowCellPipelineConfigPath);
            return null;
        }
    }

    private long postprocessBaggedSampleRunContainer(String flowCellID, Path gPackageDir, Path inputDir,
                                                     String container_name, String container_command, String phase) {
        logger.debug("postprocessBaggedSampleRunContainer('{}','{}','{}','{}','{}','{}')", flowCellID,
                gPackageDir, inputDir, container_name, container_command, phase);
        logger.gpmsInfo("Starting pipeline via Docker container");
        output = new StringBuilder();
        output.append(String.format("Executing: docker run --rm -t -v %s:/gdata/output -v %s:/gpackage %s sh -c %s\n",
                inputDir, gPackageDir, container_name, container_command));
        final HostConfig hostConfig = HostConfig.builder()
                .appendBinds(String.format("%s:/gdata/output", inputDir))
                .appendBinds(String.format("%s:/gpackage", gPackageDir))
                .build();
        final ContainerConfig containerConfig = ContainerConfig.builder()
                .hostConfig(hostConfig)
                .image(container_name)
                .cmd("sh", "-c", container_command)
                .build();
        return runDockerContainer(containerConfig, phase);
    }

    public boolean postprocessFlowCellUploadResults(String flowCellID, Path outputDir) {
        logger.debug("postprocessFlowCellUploadResults('{}','{}')", flowCellID, outputDir);
        dataManager.updateLogger(logger);
        logger.gpmsInfo("Uploading postprocessed flow cell file(s)");
        try {
            Path flowCellResultsDir = outputDir.resolve(flowCellID);
            if (Files.exists(flowCellResultsDir)) {
                if (!dataManager.uploadFlowCellResultsFiles(flowCellID, flowCellResultsDir)) {
                    logger.gpmsError("Failed to upload postprocessing results");
                    return false;
                }
                logger.gpmsInfo("Postprocessed flow cell [{}] upload complete", flowCellID);
                return true;
            } else {
                logger.gpmsError("Postprocessing results folder [{}] does not exist", flowCellResultsDir);
                return false;
            }
        } catch (Exception e) {
            logger.gpmsError("Results upload exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return false;
        }
    }

    // Results Download

    public void downloadFlowCellResults(String flowCellID, String requestID, Path inputDir) {
        logger.debug("downloadFlowCellResults('{}','{}')", flowCellID, inputDir);
        logger.setFlowCellID(flowCellID);
        logger.setRequestID(requestID);
        logger.setStage(GPMSStatics.RESULTS_DOWNLOAD_STAGE);
        logger.setStep(0);
        try {
            logger.gpmsInfo("Starting to download postprocessed flowcell results");
            Thread.sleep(1000);
            if (!downloadFlowCellResultsCheckAndPrepare(flowCellID, inputDir)) {
                Thread.sleep(1000);
                logger.gpmsFailure("Failed sequence preprocessor initialization");
                return;
            }
            logger.incrementStep();
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Postprocessing initialization was interrupted");
        } catch (Exception e) {
            logger.gpmsFailure("downloadFlowCellResults initialization exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
        }
        try {
            if (downloadFromS3) {
                Thread.sleep(1000);
                if (!downloadFlowCellResultsDownloadFlowCell(flowCellID, inputDir)) {
                    Thread.sleep(1000);
                    logger.gpmsFailure("Failed to download flow cell file(s)");
                    return;
                }
            }
            logger.incrementStep();
            Thread.sleep(1000);
            logger.gpmsInfo("Flow cell results download complete");
        } catch (InterruptedException ie) {
            logger.gpmsFailure("Flow cell download was interrupted");
        } catch (Exception e) {
            logger.gpmsFailure("downloadFlowCellResults download exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
        }
    }

    private boolean downloadFlowCellResultsCheckAndPrepare(String flowCellID, Path inputDir) {
        logger.debug("downloadFlowCellResultsCheckAndPrepare('{}','{}','{}')", flowCellID, inputDir);
        logger.gpmsInfo("Checking and preparing flow cell postprocessor");
        try {
            try {
                Files.createDirectories(inputDir);
            } catch (IOException e) {
                logger.gpmsError("Failed to create input directory [{}]", inputDir);
                return false;
            }
            return true;
        } catch (Exception e) {
            logger.gpmsError("Check and prepare exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return false;
        }
    }

    private boolean downloadFlowCellResultsDownloadFlowCell(String flowCellID, Path inputDir) {
        logger.debug("downloadFlowCellResultsDownloadFlowCell('{}','{}')", flowCellID, inputDir);
        dataManager.updateLogger(logger);
        logger.gpmsInfo("Downloading processed sample results");
        try {
            if (!dataManager.downloadFlowCellResultsFiles(flowCellID, inputDir)) {
                logger.gpmsError("Failed to download processed flow cell file(s)");
                return false;
            }
            return true;
        } catch (Exception e) {
            logger.gpmsError("Sequence download exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            return false;
        }
    }

    private String getSampleList(Path inDir) {
        String sampleList = null;
        try {
            ArrayList<String> samples = new ArrayList<>();
            logger.trace("Processing Sequence Directory [{}]", inDir);
            for (Path subDir : Files.walk(inDir, 1).filter(Files::isDirectory).collect(Collectors.toList())) {
                if (subDir.equals(inDir)) continue;
                logger.trace("Processing Sequence SubDirectory [{}]", subDir);
                for (Path subSubDir : Files.walk(subDir, 1).filter(Files::isDirectory).collect(Collectors.toList())) {
                    if (subSubDir.equals(subDir)) continue;
                    logger.trace("Processing Sample SubDirectory [{}]", subSubDir);
                    Path commandsMainFile = subSubDir.resolve(GPMSStatics.PROCESSING_COMMANDS_FILE);
                    Path configFilesDirectory = subSubDir.resolve(GPMSStatics.PROCESSING_CONFIGS_FOLDER);
                    logger.trace("Commands file: [exists={},isRegularFile={}]",
                            Files.exists(commandsMainFile), Files.isRegularFile(commandsMainFile));
                    logger.trace("Config directory: [exists={},isDirectory={}]",
                            Files.exists(configFilesDirectory), Files.isDirectory(configFilesDirectory));
                    if (Files.exists(commandsMainFile) && Files.isRegularFile(commandsMainFile) &&
                            Files.exists(configFilesDirectory) && Files.isDirectory(configFilesDirectory)) {
                        logger.trace("Directory [{}] contains commands file and config folder", subSubDir);
                        samples.add(subSubDir.getFileName().toString());
                    }
                }
            }
            if (!samples.isEmpty())
                sampleList = String.join(",", samples);
        } catch (Exception e) {
            logger.error("getSampleList exception encountered  [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
        }
        return sampleList;
    }

    private boolean hasDockerImage(String imageName) {
        logger.debug("hasDockerImage({})", imageName);
        try (DockerClient tmpDockerClient = DefaultDockerClient.fromEnv().build()) {
            List<Image> dockerImages = tmpDockerClient.listImages(DockerClient.ListImagesParam.byName(imageName));
            return dockerImages.size() > 0;
        } catch (DockerCertificateException e) {
            logger.gpmsError("Docker certificates improperly configured on this machine");
            return false;
        } catch (DockerException e) {
            logger.gpmsError("Docker exception encountered: {}", e.getMessage());
            return false;
        } catch (InterruptedException e) {
            logger.gpmsError("Docker command interrupted");
            return false;
        }
    }

    private boolean pullDockerImage(String imageName) {
        logger.debug("pullDockerImage({})", imageName);
        try (DockerClient tmpDockerClient = DefaultDockerClient.fromEnv().build()) {
            logger.gpmsInfo("Pulling Docker image [{}]", imageName);
            tmpDockerClient.pull(imageName);
            return true;
        } catch (DockerCertificateException e) {
            logger.gpmsError("Docker certificates improperly configured on this machine");
            return false;
        } catch(ImageNotFoundException e) {
            logger.gpmsError("Docker registry does not have the image [{}]", imageName);
            return false;
        } catch (DockerException e) {
            logger.gpmsError("Docker exception encountered: {}", e.getMessage());
            return false;
        } catch (InterruptedException e) {
            logger.gpmsError("Docker command interrupted");
            return false;
        }
    }

    private long runDockerContainer(ContainerConfig containerConfig, String phase) {
        if (containerConfig == null || phase == null || StringUtils.isBlank(phase))
            return -1;
        StringBuilder toRun = new StringBuilder("docker run --rm -t");
        HostConfig hostConfig = containerConfig.hostConfig();
        if (hostConfig != null) {
            ImmutableList<String> binds = hostConfig.binds();
            if (binds != null && !binds.isEmpty()) {
                for (String volume : binds) {
                    toRun.append(String.format(" -v %s", volume));
                }
            }
        }
        toRun.append(String.format(" %s", containerConfig.image()));
        ImmutableList<String> cmd = containerConfig.cmd();
        if (cmd != null && !cmd.isEmpty()) {
            toRun.append(String.format(" %s", cmd.get(0)));
        }
        logger.gpmsInfo("Running Docker: " + toRun);
        try {
            dockerClient = DefaultDockerClient.fromEnv().build();
            final ContainerCreation creation = dockerClient.createContainer(containerConfig);
            dockerRunningID = creation.id();
            dockerClient.startContainer(dockerRunningID);
            try (final LogStream logStream = dockerClient.logs(dockerRunningID, DockerClient.LogsParam.follow(),
                    DockerClient.LogsParam.stdout(), DockerClient.LogsParam.stderr())) {
                String stagePhase = "uninit";
                while (logStream.hasNext()) {
                    final LogMessage msg = logStream.next();
                    final String outputLine = StandardCharsets.UTF_8.decode(msg.content()).toString();
                    output.append(String.format("%s", outputLine));

                    String[] outputStr = outputLine.split("\\|\\|");

                    for (int i = 0; i < outputStr.length; i++) {
                        outputStr[i] = outputStr[i].trim();
                    }

                    if ((outputStr.length == 5) &&
                            ((outputLine.toLowerCase().startsWith("info")) ||
                                    (outputLine.toLowerCase().startsWith("error")))) {
                        if (outputStr[0].toLowerCase().equals("info")) {
                            if (!stagePhase.equals(outputStr[3]))
                                logger.gpmsInfo("Pipeline is now in phase: " + outputStr[3]);
                            stagePhase = outputStr[3];
                        } else if (outputStr[0].toLowerCase().equals("error"))
                            logger.gpmsError("Pipeline error: " + outputLine);
                    }
                    logger.info("Docker: " + outputLine.trim());
                }
            }
            logger.gpmsInfo("Container has finished, gathering information");
            ContainerState state = dockerClient.inspectContainer(dockerRunningID).state();
            logger.trace("Building output message");
            Map<String, String> customParams = new HashMap<String, String>()
            {{
                put("output_phase", phase);
                put("output_log", output.toString());
            }};
            logger.gpmsInfo(customParams, "Sending output log");
            Thread.sleep(2000);
            output = new StringBuilder();
            logger.gpmsInfo("Pipeline has completed");
            Long retValue = state.exitCode();
            return (retValue != null) ? retValue : -1;
        } catch (ImageNotFoundException e) {
            //logger.gpmsError("Docker image [{}] does not exist on this machine", container_name);
            return 404;
        } catch (BadParamException e) {
            logger.gpmsError("Docker parameter error: " + e.getMessage());
            logger.error(ExceptionUtils.getStackTrace(e));
            return 125;
        } catch(ContainerNotFoundException e) {
            logger.gpmsError("Docker container failed creation: " + e.getMessage());
            logger.error(ExceptionUtils.getStackTrace(e));
            return 125;
        } catch (DockerException e) {
            logger.gpmsError("Docker container failed to run: " + e.getMessage());
            logger.error(ExceptionUtils.getStackTrace(e));
            return 125;
        } catch (InterruptedException e) {
            //logger.gpmsError("Docker container interrupted");
            return 137;
        } catch (Exception e) {
            logger.gpmsError("Run container exception encountered [{}:{}]",
                    e.getClass().getCanonicalName(), e.getMessage());
            logger.error(ExceptionUtils.getStackTrace(e));
            return -1;
        } finally {
            try {
                if (dockerClient != null) {
                    if (dockerRunningID != null) {
                        final List<Container> containers = dockerClient.listContainers();
                        for (Container container : containers) {
                            if (container.id().equals(dockerRunningID)) {
                                dockerClient.removeContainer(dockerRunningID);
                            }
                        }
                        dockerRunningID = null;
                    }
                    dockerClient.close();
                    dockerClient = null;
                }
            } catch (DockerException e) {
                logger.gpmsError("Failed to close Docker client: " + e.getMessage());
                logger.error(ExceptionUtils.getStackTrace(e));
            } catch (InterruptedException e) {
                logger.gpmsError("Docker client closure was interrupted: " + e.getMessage());
                logger.error(ExceptionUtils.getStackTrace(e));
            } catch (Exception e) {
                logger.gpmsError("Docker client closure execption encountered [{}:{}]",
                        e.getClass().getCanonicalName(), e.getMessage());
                logger.error(ExceptionUtils.getStackTrace(e));
            }
        }
    }

    public boolean isDockerRunning() {
        if (dockerClient != null) {
            if (dockerRunningID != null) {
                try {
                    ContainerState state = dockerClient.inspectContainer(dockerRunningID).state();
                    return state.running();
                } catch (DockerException | InterruptedException e) {
                    return false;
                }
            }
            return false;
        }
        return false;
    }

    public boolean stopDockerContainer() {
        try {
            if (dockerClient != null && dockerRunningID != null) {
                dockerClient.killContainer(dockerRunningID);
                return true;
            }
        } catch (DockerException e) {
            logger.gpmsError("Failed to kill container [{}] on this machine: {}", dockerRunningID, e.getMessage());
        } catch (InterruptedException e) {
            logger.gpmsError("Task to kill container [{}] was interrupted");
        }
        return false;
    }

    public void setLogger(GPMSLogger logger) {
        this.logger = logger.cloneLogger(ProcessManager.class);
        this.dataManager.setLogger(logger);
    }

    /**
     * Deletes an entire folder structure
     * @param folder Path of the folder to delete
     * @return Whether the folder was successfully deleted
     */
    private boolean deleteDirectory(Path folder) {
        logger.trace("deleteFolder({})", folder);
        try {
            Files.walkFileTree(folder, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
            return true;
        } catch (IOException e) {
            logger.gpmsError("Failed to delete directory [{}]", folder);
            return false;
        }
    }
}
