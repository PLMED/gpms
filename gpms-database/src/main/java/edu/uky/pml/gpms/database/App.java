package edu.uky.pml.gpms.database;

import edu.uky.pml.gpms.database.models.Role;
import edu.uky.pml.gpms.database.models.User;
import edu.uky.pml.gpms.database.services.RoleService;
import edu.uky.pml.gpms.database.services.UserService;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.SessionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityExistsException;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.logging.LogManager;

public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main( String[] args ) {

        Thread.currentThread().setName("App");
        System.setProperty("com.mchange.v2.log.MLog", "com.mchange.v2.log.FallbackMLog");
        System.setProperty("com.mchange.v2.log.FallbackMLog.DEFAULT_CUTOFF_LEVEL", "WARNING");
        LogManager.getLogManager().reset();
        try {
            logger.info("Priming the DB pump...");
            SessionFactoryManager.getSession();
            logger.info("Executing test queries");
            List<Role> roles = RoleService.all();
            /*if (roles.size() == 0) {
                for (Role.Type type : Role.Type.values()) {
                    logger.info("Adding role: {}:{}", type.name(), type.getValue());
                    Role role = RoleService.create(type.getValue());
                    if (role == null)
                        logger.error("Failed to add role [{}]", type.getValue());
                    else
                        logger.info("Added role [{}] with id [{}]", type.getValue(), role.getId());
                }
            } else {*/
                logger.info("Getting admin role:");
                Role admin = RoleService.getByType(Role.Type.Administrator);
                if (admin != null)
                    logger.info("\t{}", admin.json());
                logger.info("Listing roles:");
                for (Role role : roles) {
                    logger.info("\t{} - {}", role.json(), role.getType());
                }
            /*}
            Role admin = RoleService.getByType(Role.Type.ADMINISTRATOR);
            if (admin != null) {
                logger.info("{}", admin.json());
            } else {
                logger.error("Failed to get admin role");
            }
            User created = null;*/
            List<User> users = UserService.all();
            /*if (users.size() == 0) {
                if ((created = UserService.createUser("TestUsername", "TestEmail")) != null)
                    logger.info("Initial user created successfully: {}", created.json());
            } else {*/
                logger.info("Listing users:");
                for (User user : users) {
                    //created = user;
                    logger.info("\t{}", user.json());
                }
            /*}
            if (created != null) {
                UserService.addRole(created, admin);
                logger.info("User is admin: {}", UserService.hasRole(created, RoleService.getByType(Role.Type.ADMINISTRATOR)));
                logger.info("User is admin: {}", UserService.hasRole(created, RoleService.getByType(Role.Type.DIRECTOR)));
            } else {
                logger.error("Failed to get admin user");
            }*/
            User controller = UserService.getByUsername("controller");
            logger.info("controller is admin: {}", UserService.hasRole(controller, admin));
            if (controller != null) {
                User controllerFromID = UserService.getById(controller.getId().toString());
                if (controllerFromID != null)
                    logger.info("controller from ID: {} = {}", controller.getId(), controllerFromID.json());
                try {
                    User created = UserService.create(controller, "test", "test@test.com");
                    if (created != null)
                        logger.info("\t{}", created.json());
                } catch (SessionException e) {
                    logger.error("Hibernate session exception: {}", e.getMessage());
                } catch (EntityExistsException e) {
                    logger.error("Cannot create user: {}", e.getMessage());
                } catch (ConstraintViolationException e) {
                    logger.error("Invalid user: {}", e.getMessage());
                }
            }
            logger.info("List: {}", UserService.list(1, "firstName", "asc", "test", 0, 10));
            User retrieved = UserService.getByUsername("test");
            //if (retrieved != null)
            //    UserService.delete(retrieved);

            /*
                Role Testing:
                Todo: All
                Todo: ByType
             */

            /*
                User Testing:
                Todo: Create (w/ Username, Email)
                Todo: Create (w/ Username, Email, Firstname, Lastname)
                Todo: Create (w/ UserObject)
                Todo: All
                Todo: ById
                Todo: ByUsername
                Todo: ByEmail
                Todo: Update
                Todo: UpdateByOther
                Todo: Delete
             */

            /*
                UserRole Testing:
                Todo: Add
                Todo: Has
                Todo: Remove
             */

            /*
                UserSession Testing:
                Todo: Create
                Todo: All
                Todo: ById
                Todo: Delete
             */

            /*
                SampleSheet Testing:
                Todo: Create
                Todo: Create (w/ Errors)
                Todo: All
                Todo: ForUser
                Todo: ById
                Todo: ByHash
                Todo: ByContent
             */

            /*
                TaskType Testing:
                Todo: Create
                Todo: All
                Todo: ById
                Todo: ByTag
                Todo: ByName
                Todo: Update
                Todo: Delete
             */

            /*
                FlowCell Testing:
                Todo: Create
                Todo: All
                Todo: ById
                Todo: ByName
                Todo: Update
                Todo: Delete
             */

            /*
                Sample Testing:
                Todo: Create
                Todo: All
                Todo: AllForFlowCell
                Todo: ById
                Todo: ByName
                Todo: Update
                Todo: Delete
             */

            /*
                Task Testing:
                Todo: Create (w/ FlowCell)
                Todo: Create (w/ Sample)
                Todo: All
                Todo: AllForFlowCell
                Todo: AllForSample
                Todo: ById
                Todo: ByTypeAndFlowCell
                Todo: ByTypeAndSample
                Todo: Update
                Todo: Delete
             */

            /*
                Runner Testing:
                Todo: Create
                Todo: All
                Todo: ById
                Todo: ByRegionAgentPlugin
                Todo: ByInstance
                Todo: Update
                Todo: Delete
             */

            /*
                Run Testing:
                Todo: Create
                Todo: All
                Todo: AllForTask
                Todo: NonCompleteForTask
                Todo: ById
                Todo: Update
                Todo: Delete
             */

            SessionFactoryManager.close();
        } catch (Exception e) {
            logger.error("Exception encountered: {}", ExceptionUtils.getStackTrace(e));
        }
    }
}
