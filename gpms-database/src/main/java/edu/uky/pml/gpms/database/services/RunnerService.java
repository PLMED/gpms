package edu.uky.pml.gpms.database.services;

import edu.uky.pml.gpms.database.models.Runner;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.HibernateException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityExistsException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@SuppressWarnings({"unused", "WeakerAccess", "UnusedReturnValue"})
public class RunnerService {
    private static final Logger logger = LoggerFactory.getLogger(RunnerService.class);

    public synchronized static Runner create(String region, String agent, String plugin, String instance) {
        logger.trace("Call to {}('{}','{}','{}','{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                region, agent, plugin, instance);
        if (region == null || region.equals(""))
            throw new IllegalArgumentException("region cannot be null or empty");
        if (agent == null || agent.equals(""))
            throw new IllegalArgumentException("agent cannot be null or empty");
        if (plugin == null || plugin.equals(""))
            throw new IllegalArgumentException("plugin cannot be null or empty");
        if (instance == null || instance.equals(""))
            throw new IllegalArgumentException("instance cannot be null or empty");
        if (getByRegionAgentPlugin(region, agent, plugin) != null)
            throw new EntityExistsException("runner with supplied region, agent, and plugin already exists");
        if (getByInstance(instance) != null)
            throw new EntityExistsException("runner with supplied instance already exists");
        Runner o = new Runner( region, agent, plugin, instance );
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Runner>> errors = validator.validate(o);
        if (errors.size() > 0)
            throw new ConstraintViolationException(errors);
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            session.save( o );
            session.getTransaction().commit();
            return o;
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    public static List<Runner> all() {
        logger.trace("Call to {}()", new Object(){}.getClass().getEnclosingMethod().getName());
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Runner> c = cb.createQuery(Runner.class);
            Root<Runner> r = c.from(Runner.class);
            c.select(r);
            Query<Runner> query = session.createQuery(c);
            List<Runner> l = query.list();
            session.getTransaction().commit();
            return l;
        } catch (IllegalArgumentException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return new ArrayList<>();
    }

    public static Runner getById(String id) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(), id);
        if (id == null)
            throw new IllegalArgumentException("id cannot be null");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Runner> c = cb.createQuery(Runner.class);
            Root<Runner> r = c.from(Runner.class);
            c.select(r)
                    .where(
                            cb.equal(r.get("id"), UUID.fromString(id))
                    );
            Query<Runner> query = session.createQuery(c);
            Runner o = query.uniqueResult();
            session.getTransaction().commit();
            return o;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    public static Runner getByRegionAgentPlugin(String region, String agent, String plugin) {
        logger.trace("Call to {}('{}','{}','{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                region, agent, plugin);
        if (region == null || region.equals(""))
            throw new IllegalArgumentException("region cannot be null or empty");
        if (agent == null || agent.equals(""))
            throw new IllegalArgumentException("agent cannot be null or empty");
        if (plugin == null || plugin.equals(""))
            throw new IllegalArgumentException("plugin cannot be null or empty");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Runner> c = cb.createQuery(Runner.class);
            Root<Runner> r = c.from(Runner.class);
            c.select(r)
                    .where(cb.and(
                            cb.equal(r.get("region"), region),
                            cb.equal(r.get("agent"), agent),
                            cb.equal(r.get("plugin"), plugin)
                    ));
            Query<Runner> query = session.createQuery(c);
            Runner o = query.uniqueResult();
            session.getTransaction().commit();
            return o;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    public static Runner getByInstance(String instance) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(), instance);
        if (instance == null)
            throw new IllegalArgumentException("instance cannot be null");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Runner> c = cb.createQuery(Runner.class);
            Root<Runner> r = c.from(Runner.class);
            c.select(r)
                    .where(
                            cb.equal(r.get("instance"), instance)
                    );
            Query<Runner> query = session.createQuery(c);
            Runner o = query.uniqueResult();
            session.getTransaction().commit();
            return o;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    // Todo: List (DataTables)
    // Todo: Idle (forTask)

    public static boolean update(Runner o) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (o != null) ? o.getId() : "NULL");
        if (o == null)
            throw new IllegalArgumentException("runner cannot be null");
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Runner>> errors = validator.validate(o);
        if (errors.size() > 0)
            throw new ConstraintViolationException(errors);
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            session.saveOrUpdate( o );
            session.getTransaction().commit();
            return true;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return false;
    }

    public static boolean delete(Runner o) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (o != null) ? o.getId() : "NULL");
        if (o == null)
            throw new IllegalArgumentException("runner cannot be null");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            session.delete( o );
            session.getTransaction().commit();
            return true;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return false;
    }
}
