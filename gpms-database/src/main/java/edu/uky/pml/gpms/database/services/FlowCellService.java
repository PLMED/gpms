package edu.uky.pml.gpms.database.services;

import com.google.gson.Gson;
import edu.uky.pml.gpms.database.models.FlowCell;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.*;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityExistsException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.*;

@SuppressWarnings({"unused", "WeakerAccess", "UnusedReturnValue"})
public class FlowCellService {
    private static final Logger logger = LoggerFactory.getLogger(FlowCellService.class);

    public synchronized static FlowCell create(String name) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(), name);
        if (name == null || name.equals(""))
            throw new IllegalArgumentException("name cannot be null or empty");
        if (getByName(name) != null)
            throw new EntityExistsException("flow cell with supplied name already exists");
        FlowCell o = new FlowCell( name );
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<FlowCell>> errors = validator.validate(o);
        if (errors.size() > 0)
            throw new ConstraintViolationException(errors);
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            session.save( o );
            session.getTransaction().commit();
            return o;
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    public static List<FlowCell> all() {
        logger.trace("Call to {}()", new Object(){}.getClass().getEnclosingMethod().getName());
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<FlowCell> c = cb.createQuery(FlowCell.class);
            Root<FlowCell> r = c.from(FlowCell.class);
            c.select(r);
            Query<FlowCell> query = session.createQuery(c);
            List<FlowCell> l = query.list();
            session.getTransaction().commit();
            return l;
        } catch (IllegalArgumentException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return new ArrayList<>();
    }

    public static String list(int draw, String sortBy, String dir, String filter, int start, int length) {
        logger.trace("Call to {}()", new Object(){}.getClass().getEnclosingMethod().getName());
        Map<String, Object> ret = new HashMap<>();
        ret.put("draw", draw);
        ret.put("recordsTotal", 0);
        ret.put("recordsFiltered", 0);
        ret.put("data", new ArrayList<FlowCell>());
        if (sortBy == null)
            sortBy = "username";
        if (dir == null)
            dir = "desc";
        if (filter == null)
            filter = "";
        filter = String.format("%%%s%%", filter);
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            Long total = (Long) session.createQuery("select count(u.id) from User u").uniqueResult();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<FlowCell> c = cb.createQuery(FlowCell.class);
            Root<FlowCell> r = c.from(FlowCell.class);
            c.select(r)
                    .where(cb.like(r.get("name"), filter));
            if (dir.toLowerCase().equals("asc"))
                c.orderBy(cb.asc(r.get(sortBy)));
            else
                c.orderBy(cb.desc(r.get(sortBy)));
            Query<FlowCell> query = session.createQuery(c);
            ScrollableResults cursor = query.scroll(ScrollMode.SCROLL_INSENSITIVE);
            cursor.last();
            int totalFiltered = cursor.getRowNumber() + 1;
            cursor.close();
            query.setFirstResult(start).setMaxResults(length);
            List<FlowCell> l = query.list();
            session.getTransaction().commit();
            ret.put("recordsTotal", total);
            ret.put("recordsFiltered", totalFiltered);
            ret.put("data", l);
            return new Gson().toJson(ret);
        } catch (IllegalArgumentException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return new Gson().toJson(ret);
    }

    public static FlowCell getById(String id) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(), id);
        if (id == null)
            throw new IllegalArgumentException("id cannot be null");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<FlowCell> c = cb.createQuery(FlowCell.class);
            Root<FlowCell> r = c.from(FlowCell.class);
            c.select(r)
                    .where(
                            cb.equal(r.get("id"), UUID.fromString(id))
                    );
            Query<FlowCell> query = session.createQuery(c);
            FlowCell o = query.uniqueResult();
            session.getTransaction().commit();
            return o;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    public static FlowCell getByName(String name) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(), name);
        if (name == null || name.equals(""))
            throw new IllegalArgumentException("name cannot be null or empty");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<FlowCell> c = cb.createQuery(FlowCell.class);
            Root<FlowCell> r = c.from(FlowCell.class);
            c.select(r)
                    .where(
                            cb.equal(r.get("name"), name)
                    );
            Query<FlowCell> query = session.createQuery(c);
            FlowCell o = query.uniqueResult();
            session.getTransaction().commit();
            return o;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    public static boolean update(FlowCell o) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (o != null) ? o.getId() : "NULL");
        if (o == null)
            throw new IllegalArgumentException("flow cell cannot be null");
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<FlowCell>> errors = validator.validate(o);
        if (errors.size() > 0)
            throw new ConstraintViolationException(errors);
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            session.saveOrUpdate( o );
            session.getTransaction().commit();
            return true;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return false;
    }

    public static boolean delete(FlowCell o) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (o != null) ? o.getId() : "NULL");
        if (o == null)
            throw new IllegalArgumentException("flow cell cannot be null");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            session.delete( o );
            session.getTransaction().commit();
            return true;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return false;
    }
}
