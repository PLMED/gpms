package edu.uky.pml.gpms.database.services;

import edu.uky.pml.gpms.database.models.Run;
import edu.uky.pml.gpms.database.models.RunStatus;
import edu.uky.pml.gpms.database.models.Task;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.HibernateException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityExistsException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@SuppressWarnings({"unused", "WeakerAccess", "UnusedReturnValue"})
public class RunService {
    private static final Logger logger = LoggerFactory.getLogger(RunService.class);

    public synchronized static Run create(Task task) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (task != null) ? task.getId() : "NULL");
        if (task == null)
            throw new IllegalArgumentException("task cannot be null");
        if (getNonCompleteForTask(task) != null)
            throw new EntityExistsException("non-complete run for supplied task already exists");
        Run o = new Run( task );
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Run>> errors = validator.validate(o);
        if (errors.size() > 0)
            throw new ConstraintViolationException(errors);
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            session.save( o );
            session.getTransaction().commit();
            return o;
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    public static List<Run> all() {
        logger.trace("Call to {}()", new Object(){}.getClass().getEnclosingMethod().getName());
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Run> c = cb.createQuery(Run.class);
            Root<Run> r = c.from(Run.class);
            c.select(r);
            Query<Run> query = session.createQuery(c);
            List<Run> l = query.list();
            session.getTransaction().commit();
            return l;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return new ArrayList<>();
    }

    public static List<Run> allForTask(Task task) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (task != null) ? task.getId() : "NULL");
        if (task == null)
            throw new IllegalArgumentException("task cannot be null");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Run> c = cb.createQuery(Run.class);
            Root<Run> r = c.from(Run.class);
            c.select(r)
                    .where(
                            cb.equal(r.get("task"), task)
                    );
            Query<Run> query = session.createQuery(c);
            List<Run> l = query.list();
            session.getTransaction().commit();
            return l;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return new ArrayList<>();
    }

    public static Run getNonCompleteForTask(Task task) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (task != null) ? task.getId() : "NULL");
        if (task == null)
            throw new IllegalArgumentException("task cannot be null");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Run> c = cb.createQuery(Run.class);
            Root<Run> r = c.from(Run.class);
            c.select(r)
                    .where(cb.and(
                            cb.equal(r.get("task"), task),
                            cb.or(
                                    cb.equal(r.get("status"), RunStatus.PENDING),
                                    cb.equal(r.get("status"), RunStatus.SUBMITTED)
                            )
                    ));
            Query<Run> query = session.createQuery(c);
            Run o = query.uniqueResult();
            session.getTransaction().commit();
            return o;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    public static Run getById(String id) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(), id);
        if (id == null || id.equals(""))
            throw new IllegalArgumentException("id cannot be null or empty");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Run> c = cb.createQuery(Run.class);
            Root<Run> r = c.from(Run.class);
            c.select(r)
                    .where(
                            cb.equal(r.get("id"), UUID.fromString(id))
                    );
            Query<Run> query = session.createQuery(c);
            Run o = query.uniqueResult();
            session.getTransaction().commit();
            return o;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    // Todo: List (DataTables)
    // Todo: ListForTask (DataTables)

    public static boolean update(Run o) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (o != null) ? o.getId() : "NULL");
        if (o == null)
            throw new IllegalArgumentException("run cannot be null");
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Run>> errors = validator.validate(o);
        if (errors.size() > 0)
            throw new ConstraintViolationException(errors);
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            session.saveOrUpdate( o );
            session.getTransaction().commit();
            return true;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return false;
    }

    public static boolean delete(Run o) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (o != null) ? o.getId() : "NULL");
        if (o == null)
            throw new IllegalArgumentException("run cannot be null");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            session.delete( o );
            session.getTransaction().commit();
            return true;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return false;
    }
}
