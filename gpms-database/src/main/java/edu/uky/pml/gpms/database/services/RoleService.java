package edu.uky.pml.gpms.database.services;

import edu.uky.pml.gpms.database.models.Role;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.HibernateException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"unused", "WeakerAccess", "UnusedReturnValue"})
public class RoleService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    public static List<Role> all() {
        logger.trace("Call to {}()", new Object(){}.getClass().getEnclosingMethod().getName());
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to aquire session");
        try {
            session.getTransaction().begin();
            Query<Role> query = session.createQuery( "from Role", Role.class);
            List<Role> l = query.list();
            session.getTransaction().commit();
            return l;
        } catch (IllegalArgumentException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return new ArrayList<>();
    }

    public static Role getByType(Role.Type type) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (type != null) ? type : "NULL");
        if (type == null)
            throw new IllegalArgumentException("type cannot be null");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to aquire session");
        try {
            session.getTransaction().begin();
            Query<Role> query = session.createQuery( "from Role where type = :type", Role.class );
            query.setParameter("type", type);
            Role o = query.uniqueResult();
            session.getTransaction().commit();
            return o;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }
}
