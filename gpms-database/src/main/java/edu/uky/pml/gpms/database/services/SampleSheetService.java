package edu.uky.pml.gpms.database.services;

import edu.uky.pml.gpms.database.models.SampleSheet;
import edu.uky.pml.gpms.database.models.User;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@SuppressWarnings({"unused", "WeakerAccess", "UnusedReturnValue"})
public class SampleSheetService {
    private static final Logger logger = LoggerFactory.getLogger(SampleSheetService.class);

    public synchronized static SampleSheet create(User owner, String content) {
        logger.trace("Call to {}('{}','{}','{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (owner != null) ? owner.getId() : "NULL", "<content>");
        if (owner == null)
            throw new IllegalArgumentException("owner cannot be null");
        if (content == null || content.equals(""))
            throw new IllegalArgumentException("content cannot be null or empty");
        SampleSheet o = new SampleSheet(owner, content);
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<SampleSheet>> validationErrors = validator.validate(o);
        if (validationErrors.size() > 0)
            throw new ConstraintViolationException(validationErrors);
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            session.save( o );
            session.getTransaction().commit();
            return o;
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    public synchronized static SampleSheet create(User owner, String content, String errors) {
        logger.trace("Call to {}('{}','{}','{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (owner != null) ? owner.getId() : "NULL", "<content>", errors);
        if (owner == null)
            throw new IllegalArgumentException("owner cannot be null");
        if (content == null || content.equals(""))
            throw new IllegalArgumentException("content cannot be null or empty");
        SampleSheet o;
        if (errors == null || errors.equals(""))
            o = new SampleSheet(owner, content);
        else
            o = new SampleSheet(owner, content, errors);
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<SampleSheet>> validationErrors = validator.validate(o);
        if (validationErrors.size() > 0)
            throw new ConstraintViolationException(validationErrors);
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            session.save( o );
            session.getTransaction().commit();
            return o;
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    public static List<SampleSheet> all() {
        logger.trace("Call to {}()", new Object(){}.getClass().getEnclosingMethod().getName());
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            Query<SampleSheet> query = session.createQuery( "from SampleSheet", SampleSheet.class);
            List<SampleSheet> l = query.list();
            session.getTransaction().commit();
            return l;
        } catch (IllegalArgumentException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return new ArrayList<>();
    }

    public static List<SampleSheet> forUser(User owner) {
        logger.trace("Call to {}()", new Object(){}.getClass().getEnclosingMethod().getName());
        if (owner == null)
            throw new IllegalArgumentException("owner cannot be null");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            Query<SampleSheet> query = session.createQuery( "from SampleSheet where owner = :owner",
                    SampleSheet.class).setParameter("owner", owner);
            List<SampleSheet> l = query.list();
            session.getTransaction().commit();
            return l;
        } catch (IllegalArgumentException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return new ArrayList<>();
    }

    public static SampleSheet getById(String id) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            Query<SampleSheet> query = session.createQuery( "from SampleSheet where id = :id",
                    SampleSheet.class ).setParameter("id", UUID.fromString(id));
            SampleSheet o = query.uniqueResult();
            session.getTransaction().commit();
            return o;
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    public static SampleSheet getByHash(String hash) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            Query<SampleSheet> query = session.createQuery( "from SampleSheet where hash = :hash",
                    SampleSheet.class ).setParameter("hash", hash);
            SampleSheet o = query.uniqueResult();
            session.getTransaction().commit();
            return o;
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    public static SampleSheet getByContents(String contents) {
        return getByHash(DigestUtils.md5Hex(contents).toLowerCase());
    }
}
