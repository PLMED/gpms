package edu.uky.pml.gpms.database.services;

import edu.uky.pml.gpms.database.models.FlowCell;
import edu.uky.pml.gpms.database.models.Sample;
import edu.uky.pml.gpms.database.utilities.SessionFactoryManager;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.HibernateException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityExistsException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@SuppressWarnings({"unused", "WeakerAccess", "UnusedReturnValue"})
public class SampleService {
    private static final Logger logger = LoggerFactory.getLogger(SampleService.class);

    public synchronized static Sample create(String name, FlowCell flowCell) {
        logger.trace("Call to {}('{}','{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                name, (flowCell != null) ? flowCell.getId() : "NULL");
        if (name == null || name.equals(""))
            throw new IllegalArgumentException("name cannot be null or empty");
        Sample existing = getByName(name);
        if (existing != null) {
            if (existing.getFlowCell().equals(flowCell)) {
                return existing;
            } else if (existing.getFlowCell() == null) {
                existing.setFlowCell(flowCell);
                if (update(existing))
                    return existing;
                else
                    return null;
            } else
                throw new EntityExistsException(
                        String.format("sample with different flow cell [%s] already exists",
                                existing.getFlowCell().getName())
                );
        }
        Sample o = new Sample( flowCell, name );
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Sample>> errors = validator.validate(o);
        if (errors.size() > 0)
            throw new ConstraintViolationException(errors);
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            session.save( o );
            session.getTransaction().commit();
            return o;
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    public static List<Sample> all() {
        logger.trace("Call to {}()", new Object(){}.getClass().getEnclosingMethod().getName());
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Sample> c = cb.createQuery(Sample.class);
            Root<Sample> r = c.from(Sample.class);
            c.select(r);
            Query<Sample> query = session.createQuery(c);
            List<Sample> l = query.list();
            session.getTransaction().commit();
            return l;
        } catch (IllegalArgumentException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return new ArrayList<>();
    }

    public static List<Sample> allForFlowCell(FlowCell flowCell) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (flowCell != null) ? flowCell.getId() : "NULL");
        if (flowCell == null)
            throw new IllegalArgumentException("flow cell cannot be null");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Sample> c = cb.createQuery(Sample.class);
            Root<Sample> r = c.from(Sample.class);
            c.select(r)
                    .where(
                            cb.equal(r.get("flowCell"), flowCell)
                    );
            Query<Sample> query = session.createQuery(c);
            List<Sample> l = query.list();
            session.getTransaction().commit();
            return l;
        } catch (IllegalArgumentException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return new ArrayList<>();
    }

    public static Sample getById(String id) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(), id);
        if (id == null)
            throw new IllegalArgumentException("id cannot be null");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Sample> c = cb.createQuery(Sample.class);
            Root<Sample> r = c.from(Sample.class);
            c.select(r)
                    .where(
                            cb.equal(r.get("id"), UUID.fromString(id))
                    );
            Query<Sample> query = session.createQuery(c);
            Sample o = query.uniqueResult();
            session.getTransaction().commit();
            return o;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }

    public static Sample getByName(String name) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(), name);
        if (name == null || name.equals(""))
            throw new IllegalArgumentException("name cannot be null or empty");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Sample> c = cb.createQuery(Sample.class);
            Root<Sample> r = c.from(Sample.class);
            c.select(r)
                    .where(
                            cb.equal(r.get("name"), name)
                    );
            Query<Sample> query = session.createQuery(c);
            Sample o = query.uniqueResult();
            session.getTransaction().commit();
            return o;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return null;
    }
    // Todo: List (DataTables)
    // Todo: ListForFlowCell (DataTables)

    public static boolean update(Sample o) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (o != null) ? o.getId() : "NULL");
        if (o == null)
            throw new IllegalArgumentException("sample cannot be null");
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Sample>> errors = validator.validate(o);
        if (errors.size() > 0)
            throw new ConstraintViolationException(errors);
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            session.saveOrUpdate( o );
            session.getTransaction().commit();
            return true;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return false;
    }

    public static boolean delete(Sample o) {
        logger.trace("Call to {}('{}')", new Object(){}.getClass().getEnclosingMethod().getName(),
                (o != null) ? o.getId() : "NULL");
        if (o == null)
            throw new IllegalArgumentException("sample cannot be null");
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            throw new SessionException("failed to acquire session");
        try {
            session.getTransaction().begin();
            session.delete( o );
            session.getTransaction().commit();
            return true;
        } catch (IllegalArgumentException | NonUniqueResultException e) {
            logger.error("{} in {}() - {}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), e.getMessage());
        } catch (Exception e) {
            logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                    new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                logger.error("{} in {}()\n{}", e.getClass().getCanonicalName(),
                        new Object(){}.getClass().getEnclosingMethod().getName(), ExceptionUtils.getStackTrace(e));
            }
        }
        return false;
    }
}
