package edu.uky.pml.gpms.data;

import edu.uky.pml.gpms.data.encapsulation.Archiver;
import edu.uky.pml.gpms.data.transfer.ObjectStorage;
import edu.uky.pml.gpms.utilities.helpers.GPMSStatics;
import edu.uky.pml.gpms.utilities.logging.BasicGPMSLogger;
import edu.uky.pml.gpms.utilities.logging.GPMSLogger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;

@SuppressWarnings({"unused", "WeakerAccess", "BooleanMethodIsAlwaysInverted", "SameParameterValue"})
public class DataManager {
    private GPMSLogger logger;
    private Archiver archiver;
    private ObjectStorage objectStorage;

    private String rawBucketName;
    private String clinicalBucketName;
    private String researchBucketName;
    private String processedBucketName;
    private String resultsBucketName;

    /*
        Constructors
     */

    public DataManager(ObjectStorage objectStorage, Archiver archiver) {
        this(objectStorage, archiver, new BasicGPMSLogger(DataManager.class));
    }

    public DataManager(ObjectStorage objectStorage, Archiver archiver, GPMSLogger logger) throws IllegalArgumentException {
        setLogger(logger);
        setObjectStorage(objectStorage);
        setArchiver(archiver);
    }

    public DataManager(ObjectStorage objectStorage, Archiver archiver, String rawBucketName,
                       String clinicalBucketName, String researchBucketName, String processedBucketName,
                       String resultsBucketName) {
        this(objectStorage, archiver, rawBucketName, clinicalBucketName, researchBucketName, processedBucketName,
                resultsBucketName, new BasicGPMSLogger(DataManager.class));
    }

    public DataManager(ObjectStorage objectStorage, Archiver archiver, String rawBucketName,
                       String clinicalBucketName, String researchBucketName, String processedBucketName, String resultsBucketName,
                       GPMSLogger logger) throws IllegalArgumentException {
        setLogger(logger);
        /*if (objectStorage == null)
            throw new IllegalArgumentException("ObjectStorage instance cannot be null");
        this.objectStorage = objectStorage;*/
        setObjectStorage(objectStorage);
        /*if (archiver == null)
            throw new IllegalArgumentException("Archiver instance cannot be null");
        this.archiver = archiver;*/
        setArchiver(archiver);
        /*if (rawBucketName == null || StringUtils.isBlank(rawBucketName) || !objectStorage.doesBucketExist(rawBucketName))
            throw new IllegalArgumentException("Genomic raw files S3 bucket name is invalid");
        this.rawBucketName = rawBucketName;*/
        setRawBucketName(rawBucketName);
        /*if (clinicalBucketName == null || StringUtils.isBlank(clinicalBucketName) || !objectStorage.doesBucketExist(clinicalBucketName))
            throw new IllegalArgumentException("Genomic clinical files S3 bucket name is invalid");
        this.clinicalBucketName = clinicalBucketName;*/
        setClinicalBucketName(clinicalBucketName);
        /*if (researchBucketName == null || StringUtils.isBlank(researchBucketName) || !objectStorage.doesBucketExist(researchBucketName))
            throw new IllegalArgumentException("Genomic research files S3 bucket name is invalid");
        this.researchBucketName = researchBucketName;*/
        setResearchBucketName(researchBucketName);
        /*if (processedBucketName == null || StringUtils.isBlank(processedBucketName) || !objectStorage.doesBucketExist(processedBucketName))
            throw new IllegalArgumentException("Genomic processed files S3 bucket name is invalid");
        this.processedBucketName = processedBucketName;*/
        setProcessedBucketName(processedBucketName);
        /*if (resultsBucketName == null || StringUtils.isBlank(resultsBucketName) || !objectStorage.doesBucketExist(resultsBucketName))
            throw new IllegalArgumentException("Genomic results files S3 bucket name is invalid");
        this.resultsBucketName = resultsBucketName;*/
        setResultsBucketName(resultsBucketName);
    }

    /*
        PathStage Specific Methods
     */

    public boolean downloadFlowCellRawFiles(String flowCellID, Path downloadDirectory) {
        logger.trace("downloadFlowCellRawFiles('{}','{}')", flowCellID, downloadDirectory);
        if (!Files.exists(downloadDirectory)) {
            try {
                Files.createDirectories(downloadDirectory);
            } catch (IOException e) {
                logger.gpmsError("Failed to create download directory [{}]", downloadDirectory);
                return false;
            }
        }
        return downloadBaggedGenomicsFile(rawBucketName, archiver.getFlowCellObjectName(flowCellID), downloadDirectory);
    }

    public boolean downloadFlowCellClinicalFiles(String flowCellID, Path downloadDirectory) {
        logger.trace("downloadFlowCellClinicalFiles('{}','{}')", flowCellID, downloadDirectory);
        if (!Files.exists(downloadDirectory)) {
            try {
                Files.createDirectories(downloadDirectory);
            } catch (IOException e) {
                logger.gpmsError("Failed to create download directory [{}]", downloadDirectory);
                return false;
            }
        }
        return downloadDirectoryOfBaggedGenomicsFiles(clinicalBucketName, flowCellID, downloadDirectory);
    }

    public boolean downloadSampleClinicalFiles(String flowCellID, String sampleID, Path downloadDirectory) {
        logger.trace("downloadSampleClinicalFiles('{}','{}','{}')", flowCellID, sampleID, downloadDirectory);
        Path flowCellDir = downloadDirectory.resolve(flowCellID);
        if (!Files.exists(flowCellDir)) {
            try {
                Files.createDirectories(flowCellDir);
            } catch (IOException e) {
                logger.gpmsError("Failed to create download directory [{}]", flowCellDir);
                return false;
            }
        }
        return downloadBaggedGenomicsFile(clinicalBucketName, archiver.getSampleObjectName(flowCellID, sampleID), flowCellDir);
    }

    public boolean downloadFlowCellResearchFiles(String flowCellID, Path downloadDirectory) {
        logger.trace("downloadFlowCellResearchFiles('{}','{}')", flowCellID, downloadDirectory);
        if (!Files.exists(downloadDirectory)) {
            try {
                Files.createDirectories(downloadDirectory);
            } catch (IOException e) {
                logger.gpmsError("Failed to create download directory [{}]", downloadDirectory);
                return false;
            }
        }
        /*
            Todo
                Currently research on AWS is saved as /<research-bucket>/<seqID>/<seqID>.tar
                When this is fixed, this will need to be as well
        */
        return downloadBaggedGenomicsFile(researchBucketName, archiver.getFlowCellObjectName(flowCellID), downloadDirectory);
    }

    public boolean downloadFlowCellProcessedFiles(String flowCellID, Path downloadDirectory) {
        logger.trace("downloadFlowCellProcessedFiles('{}','{}')", flowCellID, downloadDirectory);
        if (!Files.exists(downloadDirectory)) {
            try {
                Files.createDirectories(downloadDirectory);
            } catch (IOException e) {
                logger.gpmsError("Failed to create download directory [{}]", downloadDirectory);
                return false;
            }
        }
        return downloadDirectoryOfBaggedGenomicsFiles(processedBucketName, flowCellID, downloadDirectory);
    }

    public boolean downloadSampleProcessedFiles(String flowCellID, String sampleID, Path downloadDirectory) {
        logger.trace("downloadSampleProcessedFiles('{}','{}','{}')", flowCellID, sampleID, downloadDirectory);
        Path flowCellDir = downloadDirectory.resolve(flowCellID);
        if (!Files.exists(flowCellDir)) {
            try {
                Files.createDirectories(flowCellDir);
            } catch (IOException e) {
                logger.gpmsError("Failed to create download directory [{}]", flowCellDir);
                return false;
            }
        }
        String processedSampleObject = objectStorage.findBucketObject(getProcessedBucketName(), flowCellID,
                sampleID + archiver.getCompressionFileExtension());
        if (processedSampleObject != null) {
            String innerFolders = processedSampleObject
                    .replace(flowCellID + "/", "")
                    .replace(sampleID + archiver.getCompressionFileExtension(), "");
            if (innerFolders.length() > 0) {
                flowCellDir = flowCellDir.resolve(innerFolders);
                try {
                    Files.createDirectories(flowCellDir);
                } catch (IOException e) {
                    logger.gpmsError("Failed to create download directory [{}]", flowCellDir);
                    return false;
                }
            }
            return downloadBaggedGenomicsFile(processedBucketName, processedSampleObject, flowCellDir);
        }
        logger.gpmsError("Failed to find sample [{}] in processed files of [{}]", sampleID, flowCellID);
        return false;
    }

    public boolean downloadFlowCellResultsFiles(String flowCellID, Path downloadDirectory) {
        logger.trace("downloadFlowCellResultsFiles('{}','{}')", flowCellID, downloadDirectory);
        if (!Files.exists(downloadDirectory)) {
            try {
                Files.createDirectories(downloadDirectory);
            } catch (IOException e) {
                logger.gpmsError("Failed to create download directory [{}]", downloadDirectory);
                return false;
            }
        }
        //return downloadDirectoryOfBaggedGenomicsFiles(resultsBucketName, flowCellID, downloadDirectory);
        return downloadBaggedGenomicsFile(resultsBucketName, archiver.getFlowCellObjectName(flowCellID), downloadDirectory);
    }

    public boolean downloadSampleResultsFiles(String flowCellID, String sampleID, Path downloadDirectory) {
        logger.trace("downloadSampleResultsFiles('{}','{}','{}')", flowCellID, sampleID, downloadDirectory);
        Path flowCellDir = downloadDirectory.resolve(flowCellID);
        if (!Files.exists(flowCellDir)) {
            try {
                Files.createDirectories(flowCellDir);
            } catch (IOException e) {
                logger.gpmsError("Failed to create download directory [{}]", flowCellDir);
                return false;
            }
        }
        return downloadBaggedGenomicsFile(resultsBucketName, archiver.getSampleObjectName(flowCellID, sampleID), flowCellDir);
    }

    public boolean uploadFlowCellRawFiles(String flowCellID, Path uploadDirectory) {
        logger.trace("uploadFlowCellRawFiles('{}','{}')", flowCellID, uploadDirectory);
        if (StringUtils.isBlank(flowCellID)) {
            logger.gpmsError("You must supply a flow cell ID!");
            return false;
        }
        if (!Files.exists(uploadDirectory)) {
            logger.gpmsError("Directory to upload [{}] does not exist!", uploadDirectory);
            return false;
        }
        if (!Files.isDirectory(uploadDirectory)) {
            logger.gpmsError("Directory to upload [{}] is not a directory!", uploadDirectory);
            return false;
        }
        return uploadBaggedGenomicsDirectory(uploadDirectory, rawBucketName, archiver.getFlowCellObjectName(flowCellID));
    }

    public boolean uploadFlowCellClinicalPipelineFiles(String flowCellID, Path uploadDirectory) throws IllegalArgumentException {
        logger.trace("uploadFlowCellClinicalPipelineFiles('{}','{}')", flowCellID, uploadDirectory);
        if (StringUtils.isBlank(flowCellID))
            throw new IllegalArgumentException("You must supply a flow cell ID!");
        if (!Files.exists(uploadDirectory))
            throw new IllegalArgumentException(String.format("Results directory [%s] does not exist!", uploadDirectory));
        Path finalResultsDir = uploadDirectory.resolve(flowCellID);
        if (!Files.exists(finalResultsDir))
            throw new IllegalArgumentException(String.format("Finalized directory [%s] does not exist!", finalResultsDir));
        boolean uploaded = true;
        Path flowCellPipelineConfigFile = finalResultsDir.resolve(GPMSStatics.PREPROCESSED_FLOWCELL_CONFIG_JSON_FILE);
        if (!Files.exists(flowCellPipelineConfigFile))
            throw new IllegalArgumentException(String.format("Pipeline configuration file [%s] does not exist!", flowCellPipelineConfigFile));
        try {
            long flowCellPipelineConfigFileSize = Files.size(flowCellPipelineConfigFile);
            if (!objectStorage.uploadFile(flowCellPipelineConfigFile, processedBucketName,
                    String.format("%s/%s", flowCellID, flowCellPipelineConfigFile.getFileName().toString()), flowCellPipelineConfigFileSize,
                    false))
                uploaded = false;
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Failed to get pipeline config file [%s] size", flowCellPipelineConfigFile));
        }
        Path flowCellPipelineConfigErrorFile = finalResultsDir.resolve(GPMSStatics.PREPROCESSED_FLOWCELL_CONFIG_ERROR_LOG_FILE);
        if (!Files.exists(flowCellPipelineConfigErrorFile))
            throw new IllegalArgumentException(String.format("Pipeline configuration error file [%s] does not exist!", flowCellPipelineConfigErrorFile));
        try {
            long flowCellPipelineConfigErrorFileSize = Files.size(flowCellPipelineConfigErrorFile);
            if (!objectStorage.uploadFile(flowCellPipelineConfigErrorFile, processedBucketName,
                    String.format("%s/%s", flowCellID, flowCellPipelineConfigErrorFile.getFileName().toString()), flowCellPipelineConfigErrorFileSize,
                    false))
                uploaded = false;
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Failed to get pipeline config error file [%s] size", flowCellPipelineConfigErrorFile));
        }
        return uploaded;
    }

    public boolean uploadSampleClinicalFiles(String flowCellID, String sampleID, Path uploadDirectory) {
        logger.trace("uploadSampleClinicalFiles('{}','{}','{}')", flowCellID, sampleID, uploadDirectory);
        if (StringUtils.isBlank(flowCellID)) {
            logger.gpmsError("You must supply a flow cell ID!");
            return false;
        }
        if (StringUtils.isBlank(sampleID)) {
            logger.gpmsError("You must supply a sample ID!");
            return false;
        }
        if (!Files.exists(uploadDirectory)) {
            logger.gpmsError("Directory to upload [{}] does not exist!", uploadDirectory);
            return false;
        }
        if (!Files.isDirectory(uploadDirectory)) {
            logger.gpmsError("Directory to upload [{}] is not a directory!", uploadDirectory);
            return false;
        }
        return uploadBaggedGenomicsDirectory(uploadDirectory, clinicalBucketName, archiver.getSampleObjectName(flowCellID, sampleID));
    }

    public boolean uploadFlowCellResearchFiles(String flowCellID, Path uploadDirectory) {
        logger.trace("uploadFlowCellResearchFiles('{}','{}')", flowCellID, uploadDirectory);
        if (StringUtils.isBlank(flowCellID)) {
            logger.gpmsError("You must supply a flow cell ID!");
            return false;
        }
        if (!Files.exists(uploadDirectory)) {
            logger.gpmsError("Directory to upload [{}] does not exist!", uploadDirectory);
            return false;
        }
        if (!Files.isDirectory(uploadDirectory)) {
            logger.gpmsError("Directory to upload [{}] is not a directory!", uploadDirectory);
            return false;
        }
        return uploadBaggedGenomicsDirectory(uploadDirectory, researchBucketName, archiver.getFlowCellObjectName(flowCellID));
    }

    public boolean uploadSampleProcessedFiles(String flowCellID, String sampleID, Path uploadDirectory, List<String> exclusions) {
        logger.trace("uploadSampleClinicalFiles('{}','{}','{}','{}')", flowCellID, sampleID, uploadDirectory, String.join(";", exclusions));
        if (StringUtils.isBlank(flowCellID)) {
            logger.gpmsError("You must supply a flow cell ID!");
            return false;
        }
        if (StringUtils.isBlank(sampleID)) {
            logger.gpmsError("You must supply a sample ID!");
            return false;
        }
        if (!Files.exists(uploadDirectory)) {
            logger.gpmsError("Directory to upload [{}] does not exist!", uploadDirectory);
            return false;
        }
        if (!Files.isDirectory(uploadDirectory)) {
            logger.gpmsError("Directory to upload [{}] is not a directory!", uploadDirectory);
            return false;
        }
        if (exclusions.size() > 0) {
            boolean uploadedTotal = uploadBaggedGenomicsDirectory(uploadDirectory, processedBucketName, archiver.getSampleObjectName(flowCellID, sampleID, GPMSStatics.PROCESSED_RESULTS_COMPLETE_ARCHIVE_SUFFIX));
            if (!uploadedTotal) {
                logger.gpmsError("Failed to upload complete processed sample results");
            }
            Path flowCellResultsDir = uploadDirectory.getParent();
            Path inputDir = flowCellResultsDir.getParent();
            for (String exclusion : exclusions) {
                Path exclusionPath = flowCellResultsDir.resolve(exclusion);
                Path exclusionMoveToPath = inputDir.resolve(exclusion);
                try {
                    Files.createDirectories(exclusionMoveToPath);
                    logger.trace("Path to exclude: {}", exclusionPath);
                    logger.trace("moveToFolder('{}','{}')", exclusionPath, exclusionMoveToPath);
                    moveToFolder(exclusionPath, exclusionMoveToPath);
                    Files.deleteIfExists(exclusionPath);
                } catch (IOException e) {
                    logger.error("Failed to create directories for exclusion [{}]", exclusion);
                }
            }
            boolean uploadedExcluded = uploadBaggedGenomicsDirectory(uploadDirectory, processedBucketName, archiver.getSampleObjectName(flowCellID, sampleID));
            if (!uploadedExcluded) {
                logger.gpmsError("Failed to upload filtered processed sample results");
            }
            for (String exclusion : exclusions) {
                Path exclusionPath = flowCellResultsDir.resolve(exclusion);
                Path exclusionMoveToPath = inputDir.resolve(exclusion);
                try {
                    Files.createDirectories(exclusionPath);
                    logger.trace("Path to exclude: {}", exclusionPath);
                    logger.trace("moveToFolder('{}','{}')", exclusionPath, exclusionMoveToPath);
                    moveToFolder(exclusionMoveToPath, exclusionPath);
                    Files.deleteIfExists(exclusionMoveToPath);
                } catch (IOException e) {
                    logger.error("Failed to create directories for exclusion restoration [{}]", exclusion);
                }
            }
            return uploadedTotal & uploadedExcluded;
        } else {
            return uploadBaggedGenomicsDirectory(uploadDirectory, processedBucketName, archiver.getSampleObjectName(flowCellID, sampleID));
        }
    }

    public boolean uploadTypedSampleProcessedFiles(String flowCellID, String type, String sampleID, Path uploadDirectory,
                                              List<String> exclusions) {
        logger.trace("uploadTypedSampleClinicalFiles('{}','{}','{}','{}','{}')", flowCellID, type, sampleID,
                uploadDirectory, String.join(";", exclusions));
        if (StringUtils.isBlank(flowCellID)) {
            logger.gpmsError("You must supply a flow cell ID!");
            return false;
        }
        if (StringUtils.isBlank(sampleID)) {
            logger.gpmsError("You must supply a sample ID!");
            return false;
        }
        if (!Files.exists(uploadDirectory)) {
            logger.gpmsError("Directory to upload [{}] does not exist!", uploadDirectory);
            return false;
        }
        if (!Files.isDirectory(uploadDirectory)) {
            logger.gpmsError("Directory to upload [{}] is not a directory!", uploadDirectory);
            return false;
        }
        if (exclusions.size() > 0) {
            boolean uploadedTotal = uploadBaggedGenomicsDirectory(uploadDirectory, processedBucketName,
                    archiver.getTypedSampleObjectName(flowCellID, type, sampleID,
                            GPMSStatics.PROCESSED_RESULTS_COMPLETE_ARCHIVE_SUFFIX));
            if (!uploadedTotal) {
                logger.gpmsError("Failed to upload complete processed sample results");
            }
            Path flowCellResultsDir = uploadDirectory.getParent();
            Path inputDir = flowCellResultsDir.getParent();
            for (String exclusion : exclusions) {
                Path exclusionPath = flowCellResultsDir.resolve(exclusion);
                Path exclusionMoveToPath = inputDir.resolve(exclusion);
                try {
                    Files.createDirectories(exclusionMoveToPath);
                    logger.trace("Path to exclude: {}", exclusionPath);
                    logger.trace("moveToFolder('{}','{}')", exclusionPath, exclusionMoveToPath);
                    moveToFolder(exclusionPath, exclusionMoveToPath);
                    Files.deleteIfExists(exclusionPath);
                } catch (IOException e) {
                    logger.error("Failed to create directories for exclusion [{}]", exclusion);
                }
            }
            boolean uploadedExcluded = uploadBaggedGenomicsDirectory(uploadDirectory, processedBucketName,
                    archiver.getTypedSampleObjectName(flowCellID, type, sampleID));
            if (!uploadedExcluded) {
                logger.gpmsError("Failed to upload filtered processed sample results");
            }
            for (String exclusion : exclusions) {
                Path exclusionPath = flowCellResultsDir.resolve(exclusion);
                Path exclusionMoveToPath = inputDir.resolve(exclusion);
                try {
                    Files.createDirectories(exclusionPath);
                    logger.trace("Path to exclude: {}", exclusionPath);
                    logger.trace("moveToFolder('{}','{}')", exclusionPath, exclusionMoveToPath);
                    moveToFolder(exclusionMoveToPath, exclusionPath);
                    Files.deleteIfExists(exclusionMoveToPath);
                } catch (IOException e) {
                    logger.error("Failed to create directories for exclusion restoration [{}]", exclusion);
                }
            }
            return uploadedTotal & uploadedExcluded;
        } else {
            return uploadBaggedGenomicsDirectory(uploadDirectory, processedBucketName,
                    archiver.getTypedSampleObjectName(flowCellID, type, sampleID));
        }
    }

    public boolean uploadFlowCellResultsFiles(String flowCellID, Path uploadDirectory) {
        logger.trace("uploadFlowCellResultsFiles('{}','{}')", flowCellID, uploadDirectory);
        if (StringUtils.isBlank(flowCellID)) {
            logger.gpmsError("You must supply a flow cell ID!");
            return false;
        }
        if (!Files.exists(uploadDirectory)) {
            logger.gpmsError("Directory to upload [{}] does not exist!", uploadDirectory);
            return false;
        }
        if (!Files.isDirectory(uploadDirectory)) {
            logger.gpmsError("Directory to upload [{}] is not a directory!", uploadDirectory);
            return false;
        }
        return uploadBaggedGenomicsDirectory(uploadDirectory, resultsBucketName, archiver.getFlowCellObjectName(flowCellID));
    }

    /*
        Public Instance Methods
     */

    public boolean uploadBaggedGenomicsDirectory(Path inPath, String bucket, String s3Prefix) {
        logger.trace("uploadBaggedGenomicsDirectory('{}','{}','{}')", inPath, bucket, s3Prefix);
        if (!checkSetup(bucket, inPath)) {
            logger.gpmsError("Failed bagged upload setup verification");
            return false;
        }
        if (s3Prefix == null) {
            logger.gpmsError("No S3 prefix supplied for bagged upload");
            return false;
        }
        long uncompressedSize = FileUtils.sizeOfDirectory(inPath.toFile());
        logger.trace("uncompressedSize: {}", humanReadableByteCount(uncompressedSize));
        try {
            long freeSpace = Files.getFileStore(inPath).getUsableSpace();
            logger.trace("freeSpace: {}", humanReadableByteCount(freeSpace));
            long requiredSpace = uncompressedSize + (1024 * 1024 * 1024);
            logger.trace("requiredSpace: {}", humanReadableByteCount(requiredSpace));
            if (requiredSpace > freeSpace) {
                logger.gpmsError("Not enough free space to bag up [{}], needs [{}] has [{}]",
                                inPath.toAbsolutePath(), humanReadableByteCount(requiredSpace),
                                humanReadableByteCount(freeSpace));
                return false;
            }
        } catch (IOException e) {
            logger.gpmsError("Failed to locate path to upload [{}]", inPath);
            return false;
        }
        logger.gpmsInfo("Bagging up [{}]", inPath);
        Path bagged = archiver.bagItUp(inPath);
        if (bagged == null || !Files.exists(bagged)) {
            logger.gpmsError("Failed to bag up directory [{}]", inPath);
            return false;
        }
        logger.gpmsInfo("Verifying bagging on [{}]", inPath);
        if (!archiver.verifyBag(inPath)) {
            logger.gpmsError("Failed to bag up directory [{}]", inPath);
            archiver.debagify(inPath);
            return false;
        }
        logger.gpmsInfo("Boxing up [{}]", inPath.toAbsolutePath());
        Path boxed = archiver.archive(bagged.toFile());
        logger.gpmsInfo("Reverting bagging on directory [{}]", inPath);
        archiver.debagify(inPath);
        if (boxed == null || !Files.exists(boxed)) {
            logger.gpmsError("Failed to box up directory [{}]", inPath);
            return false;
        }
        logger.gpmsInfo("Uploading [{}] to [{}/{}]", boxed, bucket, s3Prefix);
        boolean success = false;
        try {
            success = objectStorage.uploadFile(boxed, bucket, s3Prefix, uncompressedSize, false);
            Files.delete(boxed);
            return success;
        } catch (IOException e) {
            logger.gpmsError("Failed to upload file [{}] to S3 [{}/{}]", boxed, bucket, s3Prefix);
            logger.debug("IOException: {}", ExceptionUtils.getStackTrace(e));
            return success;
        }
    }

    public boolean downloadDirectoryOfBaggedGenomicsFiles(String bucket, String s3Prefix, Path outPath) {
        logger.trace("downloadDirectoryOfBaggedGenomicsFiles('{}','{}','{}')", bucket, s3Prefix, outPath);
        if (!checkSetup(bucket, outPath)) {
            logger.gpmsError("Failed bagged download setup verification");
            return false;
        }
        if (!s3Prefix.endsWith("/"))
            s3Prefix += "/";
        long downloadSize = 0L;
        long largestObject = 0L;
        for (Map.Entry<String, Long> object : objectStorage.listBucketObjectsWithSize(bucket, s3Prefix).entrySet()) {
            downloadSize += object.getValue();
            if (object.getValue() > largestObject)
                largestObject = object.getValue();
        }
        logger.trace("downloadSize: {}", humanReadableByteCount(downloadSize));
        logger.trace("largestObject: {}", humanReadableByteCount(largestObject));
        try {
            long freeSpace = Files.getFileStore(outPath).getUsableSpace();
            logger.trace("freeSpace: {}", humanReadableByteCount(freeSpace));
            long requiredSpace = downloadSize + largestObject + (1024 * 1024 * 1024);
            logger.trace("requiredSpace: {}", humanReadableByteCount(requiredSpace));
            if (requiredSpace > freeSpace) {
                logger.gpmsError("Not enough free space in [{}], needs [{}] has [{}]",
                        outPath.toAbsolutePath(), humanReadableByteCount(requiredSpace),
                        humanReadableByteCount(freeSpace));
                return false;
            }
        } catch (IOException e) {
            logger.gpmsError("Invalid path provided: {}", outPath.toAbsolutePath());
            return false;
        }
        for (String objectName : objectStorage.listBucketObjects(bucket, s3Prefix)) {
            Path boxedPath = outPath.resolve(objectName);
            Path parentFolder = boxedPath.getParent();
            try {
                Files.createDirectories(parentFolder);
            } catch (IOException e) {
                logger.gpmsError("Failed to create local parent directory [{}] for sample [{}]",
                        parentFolder, objectName);
                return false;
            }
            boolean success = downloadBaggedGenomicsFile(bucket, objectName, parentFolder);
            if (!success) {
                logger.gpmsError("Failed to download sample [{}] to [{}]", objectName, parentFolder);
                return false;
            }
        }
        return true;
    }

    public boolean downloadBaggedGenomicsFile(String bucket, String objectName, Path outPath) {
        logger.trace("downloadBaggedGenomicsFile('{}','{}','{}')", bucket, objectName, outPath);
        logger.trace("Checking setup");
        if (!checkSetup(bucket, outPath)) {
            logger.gpmsError("Failed bagged download setup verification");
            return false;
        }
        logger.trace("Getting object size");
        long downloadSize = objectStorage.getObjectSize(bucket, objectName);
        if (downloadSize < 0L) {
            logger.gpmsError("Object [{}/{}] does not exist or has an invalid size", bucket, objectName);
            return false;
        }
        logger.trace("Getting object uncompressed size tag");
        long uncompressedSize = 2 * downloadSize;
        String uncompressedSizeString = objectStorage.getObjectTag(bucket, objectName, GPMSStatics.UNCOMPRESSED_SIZE_METADATA_TAG_KEY);
        if (uncompressedSizeString == null)
            logger.warn("[{}/{}] is missing the 'uncompressedSize' metadata tag, assuming twice the download size to be safe",
                    bucket, objectName);
        else {
            try {
                uncompressedSize = Long.valueOf(uncompressedSizeString);
            } catch (NumberFormatException e) {
                logger.gpmsError("[{}/{}] has an invalid 'uncompressedSize' metadata tag [{}], assuming twice the download size to be safe",
                        bucket, objectName, uncompressedSizeString);
            }
        }
        logger.trace("Formatting paths");
        int prefix = objectName.lastIndexOf("/") + 1;
        String object = objectName.substring(prefix);
        Path boxedPath = outPath.resolve(object);
        String folder = objectName;
        int suffix = objectName.lastIndexOf(".tar");
        if (suffix == -1)
            suffix = objectName.lastIndexOf(".tgz");
        if (suffix > 0)
            folder = objectName.substring(prefix, suffix);
        Path unboxedPath = outPath.resolve(folder);
        if (Files.exists(unboxedPath)) {
            long existingSize = FileUtils.sizeOfDirectory(unboxedPath.toFile());
            if (uncompressedSize == existingSize) {
                logger.gpmsInfo("Files for [s3://{}/{}] exist at [{}] and are of the correct size, skipping. To re-download, delete existing files and resubmit request.",
                        bucket, objectName, unboxedPath);
                return true;
            } else {
                deleteDirectory(unboxedPath);
            }
        }
        logger.trace("Checking free space");
        try {
            long freeSpace = Files.getFileStore(outPath).getUsableSpace();
            logger.trace("freeSpace: {}", humanReadableByteCount(freeSpace));
            long requiredSpace = downloadSize + uncompressedSize + (1024 * 1024 * 1024);
            logger.trace("requiredSpace: {}", humanReadableByteCount(requiredSpace));
            if (requiredSpace > freeSpace) {
                logger.gpmsError("Not enough free space in [{}], needs [{}] has [{}]",
                        outPath.toAbsolutePath(), humanReadableByteCount(requiredSpace),
                        humanReadableByteCount(freeSpace));
                return false;
            }
        } catch (IOException e) {
            logger.gpmsError("Invalid path provided: {}", outPath);
            return false;
        }
        /*logger.trace("Formatting paths");
        int prefix = objectName.lastIndexOf("/") + 1;
        String object = objectName.substring(prefix);
        Path boxedPath = outPath.resolve(object);*/
        logger.gpmsInfo("Downloading [{}/{}] to [{}]", bucket, objectName, boxedPath);
        if (!objectStorage.downloadObject(bucket, objectName, outPath) || !Files.exists(boxedPath) ||
                !Files.isRegularFile(boxedPath)) {
            logger.gpmsError("Failed to download [{}/{}] to [{}]", bucket, objectName, boxedPath);
            return false;
        }
        /*String folder = objectName;
        int suffix = objectName.lastIndexOf(".tar");
        if (suffix == -1)
            suffix = objectName.lastIndexOf(".tgz");
        if (suffix > 0)
            folder = objectName.substring(prefix, suffix);
        Path unboxedPath = outPath.resolve(folder);*/
        if (archiver.isArchive(boxedPath)) {
            logger.gpmsInfo("Unboxing [{}]", boxedPath);
            if (!archiver.unarchive(boxedPath, outPath) || !Files.exists(unboxedPath) ||
                    !Files.isDirectory(unboxedPath)) {
                logger.gpmsError("Failed to unarchive [{}] to [{}]", boxedPath, unboxedPath);
                return false;
            }
            try {
                Files.deleteIfExists(boxedPath);
            } catch (IOException e) {
                logger.gpmsError("Failed to clean up downloaded object [{}]", boxedPath);
            }
            logger.gpmsInfo("Verifying [{}] using BagIt data", unboxedPath);
            if (!archiver.isBag(unboxedPath) || !archiver.verifyBag(unboxedPath)) {
                logger.gpmsError("[{}] contains missing or invalid BagIt data", unboxedPath);
                return false;
            }
            logger.gpmsInfo("Reverting [{}] to original format", unboxedPath);
            archiver.debagify(unboxedPath);
        }
        return true;
    }

    public GPMSLogger getLogger() {
        return logger;
    }

    public void setLogger(GPMSLogger logger) {
        this.logger = logger.cloneLogger(DataManager.class);
    }

    public void updateLogger(GPMSLogger logger) {
        this.logger.setFlowCellID(logger.getFlowCellID());
        this.logger.setSampleID(logger.getSampleID());
        this.logger.setRequestID(logger.getRequestID());
        this.logger.setStage(logger.getStage());
        this.logger.setStep(logger.getStep());
        this.objectStorage.updateLogger(logger);
        this.archiver.updateLogger(logger);
    }

    public ObjectStorage getObjectStorage() {
        return objectStorage;
    }
    public void setObjectStorage(ObjectStorage objectStorage) throws IllegalArgumentException {
        if (objectStorage == null)
            throw new IllegalArgumentException("ObjectStorage instance cannot be null");
        this.objectStorage = objectStorage;
    }

    public Archiver getArchiver() {
        return archiver;
    }
    public void setArchiver(Archiver archiver) {
        if (archiver == null)
            throw new IllegalArgumentException("Archiver instance cannot be null");
        this.archiver = archiver;
    }

    public String getRawBucketName() {
        return rawBucketName;
    }
    public void setRawBucketName(String rawBucketName) throws IllegalArgumentException {
        if (rawBucketName == null || StringUtils.isBlank(rawBucketName) || !objectStorage.doesBucketExist(rawBucketName))
            throw new IllegalArgumentException("Genomic raw files S3 bucket name is invalid");
        this.rawBucketName = rawBucketName;
    }

    public String getClinicalBucketName() {
        return clinicalBucketName;
    }
    public void setClinicalBucketName(String clinicalBucketName) throws IllegalArgumentException {
        if (clinicalBucketName == null || StringUtils.isBlank(clinicalBucketName) || !objectStorage.doesBucketExist(clinicalBucketName))
            throw new IllegalArgumentException("Genomic clinical files S3 bucket name is invalid");
        this.clinicalBucketName = clinicalBucketName;
    }

    public String getResearchBucketName() {
        return researchBucketName;
    }
    public void setResearchBucketName(String researchBucketName) throws IllegalArgumentException {
        if (researchBucketName == null || StringUtils.isBlank(researchBucketName) || !objectStorage.doesBucketExist(researchBucketName))
            throw new IllegalArgumentException("Genomic research files S3 bucket name is invalid");
        this.researchBucketName = researchBucketName;
    }

    public String getProcessedBucketName() {
        return processedBucketName;
    }
    public void setProcessedBucketName(String processedBucketName) throws IllegalArgumentException {
        if (processedBucketName == null || StringUtils.isBlank(processedBucketName) || !objectStorage.doesBucketExist(processedBucketName))
            throw new IllegalArgumentException("Genomic processed files S3 bucket name is invalid");
        this.processedBucketName = processedBucketName;
    }

    public String getResultsBucketName() {
        return resultsBucketName;
    }
    public void setResultsBucketName(String resultsBucketName) throws IllegalArgumentException {
        if (resultsBucketName == null || StringUtils.isBlank(resultsBucketName) || !objectStorage.doesBucketExist(resultsBucketName))
            throw new IllegalArgumentException("Genomic processed files S3 bucket name is invalid");
        this.resultsBucketName = resultsBucketName;
    }

    /*
        Private Helper Functions
     */

    private boolean checkSetup(String bucket, Path path) {
        logger.trace("checkSetup('{}','{}')", bucket, path);
        if (archiver == null) {
            logger.error("You must configure your archival settings prior to use");
            return false;
        }
        if (objectStorage == null) {
            logger.error("You must configure your object storage settings prior to use");
            return false;
        }
        if (bucket == null || bucket.equals("") || !objectStorage.doesBucketExist(bucket)) {
            logger.error("You must supply a valid bucket name");
            return false;
        }
        if (path == null) {
            logger.error("You must supply a valid path to work with");
            return false;
        }
        if (!Files.exists(path)) {
            logger.error("Path [{}] does not exist", path);
            return false;
        }
        if (!Files.isDirectory(path)) {
            logger.error("Path [{}] is not a directory", path);
            return false;
        }
        return true;
    }

    private boolean moveToFolder(Path srcFolder, Path dstFolder) {
        try {
            if (!Files.exists(srcFolder)) {
                logger.error("Folder to move [{}] does not exist", srcFolder.toString().replace("\\", "\\\\"));
                return false;
            }
            if (!Files.exists(dstFolder)) {
                logger.error("Destination folder [{}] does not exist", dstFolder.toString().replace("\\", "\\\\"));
                return false;
            }
            try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(srcFolder)) {
                boolean bSuccess = true;
                for (Path path : directoryStream) {
                    if (!movePath(path, dstFolder.resolve(path.getFileName())))
                        bSuccess = false;
                }
                return bSuccess;
            } catch (IOException e) {
                logger.error("Failed to move [{}] to folder [{}] : {}",
                        srcFolder.toString().replace("\\", "\\\\"), dstFolder.toString().replace("\\", "\\\\"),
                        ExceptionUtils.getStackTrace(e).replace("\\", "\\\\"));
                return false;
            }
        } catch (Exception e) {
            logger.error("Failed to move [{}] to folder [{}] : {}",
                    srcFolder.toString().replace("\\", "\\\\"), dstFolder.toString().replace("\\", "\\\\"),
                    ExceptionUtils.getStackTrace(e).replace("\\", "\\\\"));
            return false;
        }
    }

    private boolean movePath(Path srcPath, Path dstPath) {
        try {
            if (!Files.exists(srcPath)) {
                logger.error("Folder to move [{}] does not exist", srcPath.toString().replace("\\", "\\\\"));
                return false;
            }
            Files.deleteIfExists(dstPath);
            long started = System.currentTimeMillis();
            Files.move(srcPath, dstPath, ATOMIC_MOVE);
            return true;
        } catch (IOException e) {
            logger.error("Failed to move [{}] to [{}] : {}",
                    srcPath.toString().replace("\\", "\\\\"), dstPath.toString().replace("\\", "\\\\"),
                    ExceptionUtils.getStackTrace(e).replace("\\", "\\\\"));
            return false;
        }
    }

    /**
     * Deletes an entire folder structure
     * @param folder Path of the folder to delete
     * @return Whether the folder was successfully deleted
     */
    private boolean deleteDirectory(Path folder) {
        logger.trace("deleteFolder({})", folder);
        try {
            Files.walkFileTree(folder, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
            return true;
        } catch (IOException e) {
            logger.error("Failed to delete directory [{}]", folder);
            return false;
        }
    }

    private static String humanReadableByteCount(long bytes/*, boolean si*/) {
        int unit = /*si ? */1000/* : 1024*/;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (/*si ? */"kMGTPE"/* : "KMGTPE"*/).charAt(exp-1) + (/*si ? */""/* : "i"*/);
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }
}
