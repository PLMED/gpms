package edu.uky.pml.gpms.data;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import edu.uky.pml.gpms.data.encapsulation.Archiver;
import edu.uky.pml.gpms.data.transfer.ObjectStorage;
import edu.uky.pml.gpms.utilities.logging.BasicGPMSLogger;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.ion.NullValueException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    private static final Logger simpleLogger = LoggerFactory.getLogger("message-only");
    private static final int RAW_UPLOAD_STAGE = 1;
    private static final int PREPROCESSING_STAGE = 2;
    private static final int PROCESSING_STAGE = 4;
    private static final int POSTPROCESSING_STAGE = 5;
    private static final int RESULTS_STAGE = 6;

    public static void main(String[] args) {
        ArgumentParser parser = ArgumentParsers.newFor("Genomic Pipeline Management System Data Extractor").build()
                .defaultHelp(true)
                .description("Downloads and restores genomics pipeline data to the local filesystem.");
        parser.addArgument("-C", "--credentials")
                .setDefault("config.properties")
                .help("Configuration file holding credential information.");
        parser.addArgument("-A", "--accesskey");
        parser.addArgument("-S", "--secretkey");
        parser.addArgument("-E", "--endpoint");
        parser.addArgument("-R", "--region");
        parser.addArgument("--raw").help("Genomic raw file S3 bucket name");
        parser.addArgument("--clinical").help("Genomic clinical file S3 bucket name");
        parser.addArgument("--research").help("Genomic research file S3 bucket name");
        parser.addArgument("--processed").help("Genomic processed file S3 bucket name");
        parser.addArgument("--results").help("Genomic results file S3 bucket name");
        parser.addArgument("command").nargs("?");
        parser.addArgument("directories").nargs("*");
        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        String command = ns.getString("command"), credentials = ns.getString("credentials"),
                accessKey = ns.getString("accesskey"), secretKey = ns.getString("secretkey"),
                endpoint = ns.getString("endpoint"), region = ns.getString("region"),
                rawBucketName = ns.getString("raw"), clinicalBucketName = ns.getString("clinical"),
                researchBucketName = ns.getString("research"), processedBucketName = ns.getString("processed"),
                resultsBucketName = ns.getString("results")
                ;
        List<String> directories = ns.getList("directories");

        if (command == null) {
            simpleLogger.info("Commands:");
            simpleLogger.info("\t- list-raw");
            simpleLogger.info("\t- list-clinical [flow_cell_id]");
            simpleLogger.info("\t- list-research");
            simpleLogger.info("\t- list-processed [flow_cell_id]");
            simpleLogger.info("\t- list-processed-complete [flow_cell_id]");
            simpleLogger.info("\t- list-results");
            simpleLogger.info("\t-   upload-raw <flow_cell_id> <upload_dir>");
            simpleLogger.info("\t- download-raw <flow_cell_id> <download_dir>");
            simpleLogger.info("\t-   upload-clinical <flow_cell_id> <upload_dir>");
            simpleLogger.info("\t- download-clinical <flow_cell_id> <download_dir>");
            simpleLogger.info("\t-   upload-clinical-sample <flow_cell_id> <sample_id> <upload_dir>");
            simpleLogger.info("\t- download-clinical-sample <flow_cell_id> <sample_id> <download_dir>");
            simpleLogger.info("\t-   upload-research <flow_cell_id> <upload_dir>");
            simpleLogger.info("\t- download-research <flow_cell_id> <download_dir>");
            simpleLogger.info("\t-   upload-processed-sample <flow_cell_id> <sample_id> <upload_dir>");
            simpleLogger.info("\t- download-processed-sample <flow_cell_id> <sample_id> <download_dir>");
            simpleLogger.info("\t- download-complete-processed-sample <flow_cell_id> <sample_id> <download_dir>");
            simpleLogger.info("\t-   upload-results <flow_cell_id> <upload_dir>");
            simpleLogger.info("\t- download-results <flow_cell_id> <download_dir>");
            simpleLogger.info("\t- restore <bagged_archive_file> <target_dir>");
            return;
        }

        logger.trace("Checking for config.properties file");
        File configFile = new File(credentials);
        if (configFile.exists()) {
            Configurations configs = new Configurations();
            try {
                Configuration config = configs.properties(configFile);
                if (config.containsKey("log.level")) {
                    LoggerContext loggerContext = (LoggerContext)LoggerFactory.getILoggerFactory();
                    ch.qos.logback.classic.Logger tmpLogger = loggerContext.getLogger("root");
                    tmpLogger.setLevel(Level.toLevel(config.getString("log.level")));
                }
                if (config.containsKey("access.key"))
                    accessKey = config.getString("access.key");
                if (config.containsKey("secret.key"))
                    secretKey = config.getString("secret.key");
                if (config.containsKey("endpoint"))
                    endpoint = config.getString("endpoint");
                if (config.containsKey("region"))
                    region = config.getString("region");
                if (config.containsKey("bucket.raw"))
                    rawBucketName = config.getString("bucket.raw");
                if (config.containsKey("bucket.clinical"))
                    clinicalBucketName = config.getString("bucket.clinical");
                if (config.containsKey("bucket.research"))
                    researchBucketName = config.getString("bucket.research");
                if (config.containsKey("bucket.processed"))
                    processedBucketName = config.getString("bucket.processed");
                if (config.containsKey("bucket.results"))
                    resultsBucketName = config.getString("bucket.results");
            } catch (ConfigurationException e) {
                logger.error("Error with your config.properties file.");
            }
        }
        ObjectStorage objectStorage;
        try {
            objectStorage = new ObjectStorage(accessKey, secretKey, endpoint, region);
        } catch (NullValueException | IllegalArgumentException e) {
            logger.error("Error building objectStorage : {}", e.getMessage());
            return;
        }
        Archiver archiver;
        try {
            archiver = new Archiver();
        } catch (NullValueException | IllegalArgumentException e) {
            logger.error("Error building archiver : {}", e.getMessage());
            return;
        }
        DataManager dataManager;
        try {
            dataManager = new DataManager(objectStorage, archiver, rawBucketName, clinicalBucketName, researchBucketName,
                    processedBucketName, resultsBucketName);
        } catch (NullValueException | IllegalArgumentException e) {
            logger.error("Error building data manager : {}", e.getMessage());
            return;
        }
        String requestID = "command-line";
        switch (command) {
            case "list-raw":
                for (S3ObjectSummary flowCellObject : objectStorage.listBucketObjects(rawBucketName))
                    simpleLogger.info(flowCellObject.getKey().replace(".tar.gz", "").replace(".tar", "").replace(".tgz", ""));
                break;
            case "list-clinical":
                if (directories.size() > 0) {
                    for (String flowCellObject : objectStorage.listBucketObjects(clinicalBucketName, directories.get(0)))
                        simpleLogger.info(flowCellObject.replace(".tar.gz", "").replace(".tar", "").replace(".tgz", ""));
                } else {
                    for (String flowCell : objectStorage.listBucketDirectories(clinicalBucketName))
                        simpleLogger.info(flowCell.replace("/", ""));
                }
                break;
            case "list-research":
                for (S3ObjectSummary flowCellObject : objectStorage.listBucketObjects(researchBucketName))
                    simpleLogger.info(flowCellObject.getKey().replace(".tar.gz", "").replace(".tar", "").replace(".tgz", ""));
                break;
            case "list-processed":
                if (directories.size() > 0) {
                    String flowCellID = directories.get(0);
                    if (!flowCellID.endsWith("/"))
                        flowCellID += "/";
                    for (String flowCellObject : objectStorage.listBucketObjects(processedBucketName, flowCellID))
                        simpleLogger.info(flowCellObject.replace(".tar.gz", "").replace(".tar", "").replace(".tgz", ""));
                } else {
                    for (String flowCell : objectStorage.listBucketDirectories(processedBucketName))
                        simpleLogger.info(flowCell.replace("/", ""));
                }
                break;
            case "list-processed-complete":
                if (directories.size() > 0) {
                    String flowCellID = directories.get(0);
                    flowCellID += "-complete";
                    if (!flowCellID.endsWith("/"))
                        flowCellID += "/";
                    for (String flowCellObject : objectStorage.listBucketObjects(processedBucketName, flowCellID))
                        simpleLogger.info(flowCellObject.replace(".tar.gz", "").replace(".tar", "").replace(".tgz", ""));
                } else {
                    for (String flowCell : objectStorage.listBucketDirectories(processedBucketName))
                        simpleLogger.info(flowCell.replace("/", ""));
                }
                break;
            case "list-results":
                for (S3ObjectSummary flowCellObject : objectStorage.listBucketObjects(resultsBucketName))
                    simpleLogger.info(flowCellObject.getKey().replace(".tar.gz", "").replace(".tar", "").replace(".tgz", ""));
                break;
            case "download-raw":
                {
                    if (directories.size() < 2) {
                        simpleLogger.info("download-raw usage: <flow_cell_id> <download_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    Path downloadDirectory = Paths.get(directories.get(1));
                    dataManager.setLogger(new BasicGPMSLogger(flowCellID, requestID, PREPROCESSING_STAGE));
                    if (!dataManager.downloadFlowCellRawFiles(flowCellID, downloadDirectory)) {
                        logger.error("Failed to download raw sequence file(s)");
                        return;
                    }
                    simpleLogger.info("Flow cell [{}] raw file(s) restored to [{}]", flowCellID,
                            downloadDirectory.toAbsolutePath());
                }
                break;
            case "download-clinical":
                {
                    if (directories.size() < 2) {
                        simpleLogger.info("download-clinical usage: <flow_cell_id> <download_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    Path downloadDirectory = Paths.get(directories.get(1));
                    dataManager.setLogger(new BasicGPMSLogger(flowCellID, requestID, PROCESSING_STAGE));
                    if (!dataManager.downloadFlowCellClinicalFiles(flowCellID, downloadDirectory)) {
                        logger.error("Failed to download raw flow cell file(s)");
                        return;
                    }
                    simpleLogger.info("Flow cell [{}] preprocessed clinical data restored to [{}]", flowCellID,
                            downloadDirectory.resolve(flowCellID).toAbsolutePath());
                }
                break;
            case "download-clinical-sample":
                {
                    if (directories.size() < 3) {
                        simpleLogger.info("download-clinical-sample usage: <flowcell_id> <sample_id> <download_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    String sampleID = directories.get(1);
                    Path downloadDirectory = Paths.get(directories.get(2));
                    dataManager.setLogger(new BasicGPMSLogger(flowCellID, sampleID, requestID, PROCESSING_STAGE));
                    if (!dataManager.downloadSampleClinicalFiles(flowCellID, sampleID, downloadDirectory)) {
                        logger.error("Failed to download raw flow cell file(s)");
                        return;
                    }
                    simpleLogger.info("Sample [{}] of flowcell [{}] preprocessed clinical data restored to [{}]",
                            sampleID, flowCellID, downloadDirectory.resolve(flowCellID).resolve(sampleID));
                }
                break;
            case "download-research":
                {
                    if (directories.size() < 2) {
                        simpleLogger.info("download-research usage: <flow_cell_id> <download_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    Path downloadDirectory = Paths.get(directories.get(1));
                    dataManager.setLogger(new BasicGPMSLogger(flowCellID, requestID, PROCESSING_STAGE));
                    if (!dataManager.downloadFlowCellResearchFiles(flowCellID, downloadDirectory)) {
                        logger.error("Failed to download research flow cell file(s)");
                        return;
                    }
                    simpleLogger.info("Flow cell [{}] research data restored to [{}]", flowCellID,
                            downloadDirectory.resolve(flowCellID).toAbsolutePath());
                }
                break;
            case "download-processed":
                {
                    if (directories.size() < 2) {
                        simpleLogger.info("download-processed usage: <flow_cell_id> <download_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    Path downloadDirectory = Paths.get(directories.get(1));
                    dataManager.setLogger(new BasicGPMSLogger(flowCellID, requestID, POSTPROCESSING_STAGE));
                    if (!dataManager.downloadFlowCellProcessedFiles(flowCellID, downloadDirectory)) {
                        logger.error("Failed to download raw flow cell file(s)");
                        return;
                    }
                    simpleLogger.info("Flow cell [{}] results restored to [{}]", flowCellID,
                            downloadDirectory.resolve(flowCellID).toAbsolutePath());
                }
                break;
            case "download-processed-sample":
                {
                    if (directories.size() < 3) {
                        simpleLogger.info("download-processed-sample usage: <flow_cell_id> <sample_id> <download_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    String sampleID = directories.get(1);
                    Path downloadDirectory = Paths.get(directories.get(2));
                    dataManager.setLogger(new BasicGPMSLogger(flowCellID, sampleID, requestID, PROCESSING_STAGE));
                    if (dataManager.downloadSampleProcessedFiles(flowCellID, sampleID, downloadDirectory))
                        simpleLogger.info("Processed sample file(s) downloaded successfully");
                    else
                        simpleLogger.error("Failed to download processed sample file(s)");
                }
                break;
            case "download-complete-processed-sample":
                {
                    if (directories.size() < 3) {
                        simpleLogger.info("download-processed-sample usage: <flow_cell_id> <sample_id> <download_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    flowCellID += "-complete";
                    String sampleID = directories.get(1);
                    Path downloadDirectory = Paths.get(directories.get(2));
                    dataManager.setLogger(new BasicGPMSLogger(flowCellID, sampleID, requestID, PROCESSING_STAGE));
                    if (dataManager.downloadSampleProcessedFiles(flowCellID, sampleID, downloadDirectory))
                        simpleLogger.info("Processed sample file(s) downloaded successfully");
                    else
                        simpleLogger.error("Failed to download processed sample file(s)");
                }
                break;
            case "download-results":
                {
                    if (directories.size() < 2) {
                        simpleLogger.info("download-results usage: <flow_cell_id> <download_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    Path downloadDirectory = Paths.get(directories.get(1));
                    dataManager.setLogger(new BasicGPMSLogger(flowCellID, requestID, RESULTS_STAGE));
                    if (!dataManager.downloadFlowCellResultsFiles(flowCellID, downloadDirectory)) {
                        logger.error("Failed to download raw flow cell file(s)");
                        return;
                    }
                    simpleLogger.info("Flow cell [{}] results restored to [{}]", flowCellID,
                            downloadDirectory.resolve(flowCellID).toAbsolutePath());
                }
                break;
            case "upload-raw":
                {
                    if (directories.size() < 2) {
                        simpleLogger.info("upload-raw usage: <flow_cell_id> <upload_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    Path uploadDirectory = Paths.get(directories.get(1));
                    dataManager.setLogger(new BasicGPMSLogger(flowCellID, requestID, RAW_UPLOAD_STAGE));
                    if (dataManager.uploadFlowCellRawFiles(flowCellID, uploadDirectory))
                        simpleLogger.info("Raw flow cell file(s) uploaded successfully");
                    else
                        simpleLogger.error("Failed to upload raw flow cell file(s)");
                }
                break;
            case "upload-clinical":
                {
                    if (directories.size() < 2) {
                        simpleLogger.info("upload-clinical usage: <flow_cell_id> <upload_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    Path uploadDirectory = Paths.get(directories.get(1));
                    if (!Files.exists(uploadDirectory)) {
                        simpleLogger.info("Upload directory [{}] does not exist", uploadDirectory);
                        return;
                    }
                    if (!Files.isDirectory(uploadDirectory)) {
                        simpleLogger.info("Upload directory [{}] is not a directory", uploadDirectory);
                        return;
                    }
                    try {
                        boolean success = true;
                        for (Path sampleDirectory : Files.walk(uploadDirectory, 1).filter(Files::isDirectory).collect(Collectors.toList())) {
                            if (sampleDirectory.equals(uploadDirectory)) continue;
                            String sampleID = sampleDirectory.getFileName().toString();
                            dataManager.setLogger(new BasicGPMSLogger(flowCellID, sampleID, requestID, PREPROCESSING_STAGE));
                            logger.info("Found Sample: ({}, {}, {})", flowCellID, sampleID, sampleDirectory);
                            if (!dataManager.uploadSampleClinicalFiles(flowCellID, sampleID, sampleDirectory))
                                success = false;
                        }
                        if (success)
                            simpleLogger.info("Clinical sample(s) uploaded successfully");
                        else
                            simpleLogger.error("Failed to upload one or more clinical samples");
                    } catch (IOException e) {
                        logger.error("IOException encountered: {}", ExceptionUtils.getStackTrace(e));
                    }
                }
                break;
            case "upload-clinical-sample":
                {
                    if (directories.size() < 2) {
                        simpleLogger.info("upload-clinical-sample usage: <flow_cell_id> <sample_id> <upload_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    String sampleID = directories.get(1);
                    Path uploadDirectory = Paths.get(directories.get(2));
                    dataManager.setLogger(new BasicGPMSLogger(flowCellID, sampleID, requestID, PREPROCESSING_STAGE));
                    if (dataManager.uploadSampleClinicalFiles(flowCellID, sampleID, uploadDirectory))
                        simpleLogger.info("Clinical sample file(s) uploaded successfully");
                    else
                        simpleLogger.error("Failed to upload clinical sample file(s)");
                }
                break;
            case "upload-research":
                {
                    if (directories.size() < 2) {
                        simpleLogger.info("upload-clinical usage: <flow_cell_id> <upload_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    Path uploadDirectory = Paths.get(directories.get(1));
                    dataManager.setLogger(new BasicGPMSLogger(flowCellID, requestID, PREPROCESSING_STAGE));
                    if (dataManager.uploadFlowCellResearchFiles(flowCellID, uploadDirectory))
                        simpleLogger.info("Research flow cell file(s) uploaded successfully");
                    else
                        simpleLogger.error("Failed to upload research flow cell file(s)");
                }
                break;
            case "upload-processed":
                {
                    if (directories.size() < 2) {
                        simpleLogger.info("upload-processed usage: <flow_cell_id> <upload_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    Path uploadDirectory = Paths.get(directories.get(1));
                    if (!Files.exists(uploadDirectory)) {
                        simpleLogger.info("Upload directory [{}] does not exist", uploadDirectory);
                        return;
                    }
                    if (!Files.isDirectory(uploadDirectory)) {
                        simpleLogger.info("Upload directory [{}] is not a directory", uploadDirectory);
                        return;
                    }
                    try {
                        boolean success = true;
                        for (Path sampleDirectory : Files.walk(uploadDirectory, 1).filter(Files::isDirectory).collect(Collectors.toList())) {
                            if (sampleDirectory.equals(uploadDirectory)) continue;
                            String sampleID = sampleDirectory.getFileName().toString();
                            dataManager.setLogger(new BasicGPMSLogger(flowCellID, sampleID, requestID, PROCESSING_STAGE));
                            logger.info("Found Sample: ({}, {}, {})", flowCellID, sampleID, sampleDirectory);
                            if (!dataManager.uploadSampleProcessedFiles(flowCellID, sampleID, sampleDirectory, new ArrayList<>()))
                                success = false;
                        }
                        if (success)
                            simpleLogger.info("Processed sample(s) uploaded successfully");
                        else
                            simpleLogger.error("Failed to upload one or more processed samples");
                    } catch (IOException e) {
                        logger.error("IOException encountered: {}", ExceptionUtils.getStackTrace(e));
                    }
                }
                break;
            case "upload-processed-sample":
                {
                    if (directories.size() < 2) {
                        simpleLogger.info("upload-processed-sample usage: <flow_cell_id> <sample_id> <upload_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    String sampleID = directories.get(1);
                    Path uploadDirectory = Paths.get(directories.get(2));
                    dataManager.setLogger(new BasicGPMSLogger(flowCellID, sampleID, requestID, PROCESSING_STAGE));
                    if (dataManager.uploadSampleProcessedFiles(flowCellID, sampleID, uploadDirectory, new ArrayList<>()))
                        simpleLogger.info("Processed sample file(s) uploaded successfully");
                    else
                        simpleLogger.error("Failed to upload processed sample file(s)");
                }
                break;
            case "upload-results":
                {
                    if (directories.size() < 2) {
                        simpleLogger.info("upload-results usage: <flow_cell_id> <upload_dir>");
                        return;
                    }
                    String flowCellID = directories.get(0);
                    Path uploadDirectory = Paths.get(directories.get(1));
                    if (!Files.exists(uploadDirectory)) {
                        simpleLogger.info("Upload directory [{}] does not exist", uploadDirectory);
                        return;
                    }
                    if (!Files.isDirectory(uploadDirectory)) {
                        simpleLogger.info("Upload directory [{}] is not a directory", uploadDirectory);
                        return;
                    }
                    if (dataManager.uploadFlowCellResultsFiles(flowCellID, uploadDirectory))
                        simpleLogger.info("Results uploaded successfully");
                    else
                        simpleLogger.error("Failed to upload results");
                }
                break;
            case "restore":
                {
                    if (directories.size() < 2) {
                        simpleLogger.info("restore usage: <filename> <target_dir>");
                        return;
                    }
                    Path boxedPath = Paths.get(directories.get(0));
                    if (!Files.exists(boxedPath)) {
                        simpleLogger.error("File to restore does not exist");
                        return;
                    }
                    Path outPath = Paths.get(directories.get(1));
                    if (!Files.exists(outPath)) {
                        simpleLogger.error("Path in which to restore does not exist");
                        return;
                    }
                    String objectName = boxedPath.getFileName().toString();
                    int prefix = objectName.lastIndexOf("/") + 1;
                    String folder = objectName;
                    int suffix = objectName.lastIndexOf(".tar");
                    if (suffix == -1)
                        suffix = objectName.lastIndexOf(".tgz");
                    if (suffix > 0)
                        folder = objectName.substring(prefix, suffix);
                    Path unboxedPath = outPath.resolve(folder);
                    try {
                        simpleLogger.info("Unboxing [{}]", boxedPath);
                        if (!archiver.unarchive(boxedPath, outPath) || !Files.exists(unboxedPath) ||
                                !Files.isDirectory(unboxedPath)) {
                            simpleLogger.error("Failed to unarchive [{}] to [{}]", boxedPath, unboxedPath);
                            return;
                        }
                        simpleLogger.info("Verifying [{}] using BagIt data", unboxedPath);
                        if (!archiver.isBag(unboxedPath) || !archiver.verifyBag(unboxedPath)) {
                            simpleLogger.error("[{}] contains missing or invalid BagIt data", unboxedPath);
                            return;
                        }
                        simpleLogger.info("Reverting [{}] to original format", unboxedPath);
                        archiver.debagify(unboxedPath);
                    } catch (Exception e) {
                        simpleLogger.error("{}", ExceptionUtils.getStackTrace(e));
                    }
                }
                break;
            case "bag":
                {
                    if (directories.size() < 1) {
                        simpleLogger.info("bag usage: <bag_dir>");
                        return;
                    }
                    Path bag = Paths.get(directories.get(0));
                    Path baggedUp = dataManager.getArchiver().bagItUpAndVerify(bag);
                    if (baggedUp != null)
                        simpleLogger.info("Successfully bagged up [{}]", baggedUp);
                    else
                        simpleLogger.error("Failed to bag up [{}]", bag);
                }
                break;
            case "debag":
                {
                    if (directories.size() < 1) {
                        simpleLogger.info("debag usage: <bag_dir>");
                        return;
                    }
                    Path bag = Paths.get(directories.get(0));
                    if (dataManager.getArchiver().isBag(bag) || dataManager.getArchiver().isPartialBag(bag)) {
                        dataManager.getArchiver().debagify(bag);
                        if (dataManager.getArchiver().isBag(bag) || dataManager.getArchiver().isPartialBag(bag)) {
                            simpleLogger.info("Successfully debagged [{}]", bag);
                        } else
                            simpleLogger.error("Failed to debag [{}]", bag);
                    } else
                        simpleLogger.info("[{}] does not contain any BagIt archiving", bag);
                }
                break;
            case "check-bag":
                {
                    if (directories.size() < 1) {
                        simpleLogger.info("check-bag usage: <bag_dir>");
                        return;
                    }
                    Path bag = Paths.get(directories.get(0));
                    if (dataManager.getArchiver().isBag(bag))
                        simpleLogger.info("[{}] contains a valid BagIt archive", bag);
                    else if (dataManager.getArchiver().isPartialBag(bag))
                        simpleLogger.info("[{}] contains a malformed or partial BagIt archive", bag);
                    else
                        simpleLogger.info("[{}] does not appear to be used for BagIt archiving", bag);
                }
                break;
            default:
                simpleLogger.info("Unknown command [{}]", command);
                break;
        }
    }
}
