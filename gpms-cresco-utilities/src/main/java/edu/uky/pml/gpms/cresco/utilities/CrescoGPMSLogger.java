package edu.uky.pml.gpms.cresco.utilities;

import com.researchworx.cresco.library.messaging.MsgEvent;
import com.researchworx.cresco.library.plugin.core.CPlugin;
import com.researchworx.cresco.library.utilities.CLogger;
import edu.uky.pml.gpms.utilities.logging.GPMSLogger;

import java.util.Map;

@SuppressWarnings({"unused", "WeakerAccess"})
public class CrescoGPMSLogger implements GPMSLogger {
    private CLogger logger;
    private CPlugin plugin;
    private String flowCellID;
    private String sampleID;
    private String requestID;
    private int stage;
    private int step;
    private CLogger.Level level;

    // Plugin-only constructors

    public CrescoGPMSLogger(CPlugin plugin) {
        this(CrescoGPMSLogger.class, plugin, null, null, null, 0, 0, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(CPlugin plugin, CLogger.Level level) {
        this(CrescoGPMSLogger.class, plugin, null, null, null, 0, 0, level);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin) {
        this(clazz, plugin, null, null, null, 0, 0, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, CLogger.Level level) {
        this(clazz, plugin, null, null, null, 0, 0, level);
    }

    public CrescoGPMSLogger(CPlugin plugin, int stage) {
        this(CrescoGPMSLogger.class, plugin, null, null, null, stage, 0, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(CPlugin plugin, int stage, CLogger.Level level) {
        this(CrescoGPMSLogger.class, plugin, null, null, null, stage, 0, level);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, int stage) {
        this(clazz, plugin, null, null, null, stage, 0, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, int stage, CLogger.Level level) {
        this(clazz, plugin, null, null, null, stage, 0, level);
    }

    public CrescoGPMSLogger(CPlugin plugin, int stage, int step) {
        this(CrescoGPMSLogger.class, plugin, null, null, null, stage, step, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(CPlugin plugin, int stage, int step, CLogger.Level level) {
        this(CrescoGPMSLogger.class, plugin, null, null, null, stage, step, level);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, int stage, int step) {
        this(clazz, plugin, null, null, null, stage, step, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, int stage, int step, CLogger.Level level) {
        this(clazz, plugin, null, null, null, stage, step, level);
    }

    // Flow cell constructors

    public CrescoGPMSLogger(CPlugin plugin, String flowCellID, String requestID) {
        this(CrescoGPMSLogger.class, plugin, flowCellID, null, requestID, 0, 0, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(CPlugin plugin, String flowCellID, String requestID, CLogger.Level level) {
        this(CrescoGPMSLogger.class, plugin, flowCellID, null, requestID, 0, 0, level);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, String flowCellID, String requestID) {
        this(clazz, plugin, flowCellID, null, requestID, 0, 0, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, String flowCellID, String requestID, CLogger.Level level) {
        this(clazz, plugin, flowCellID, null, requestID, 0, 0, level);
    }

    public CrescoGPMSLogger(CPlugin plugin, String flowCellID, String requestID, int stage) {
        this(CrescoGPMSLogger.class, plugin, flowCellID, null, requestID, stage, 0, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(CPlugin plugin, String flowCellID, String requestID, int stage, CLogger.Level level) {
        this(CrescoGPMSLogger.class, plugin, flowCellID, null, requestID, stage, 0, level);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, String flowCellID, String requestID, int stage) {
        this(clazz, plugin, flowCellID, null, requestID, stage, 0, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, String flowCellID, String requestID, int stage, CLogger.Level level) {
        this(clazz, plugin, flowCellID, null, requestID, stage, 0, level);
    }

    public CrescoGPMSLogger(CPlugin plugin, String flowCellID, String requestID, int stage, int step) {
        this(CrescoGPMSLogger.class, plugin, flowCellID, null, requestID, stage, step, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(CPlugin plugin, String flowCellID, String requestID, int stage, int step, CLogger.Level level) {
        this(CrescoGPMSLogger.class, plugin, flowCellID, null, requestID, stage, step, level);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, String flowCellID, String requestID, int stage, int step) {
        this(clazz, plugin, flowCellID, null, requestID, stage, step, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, String flowCellID, String requestID, int stage, int step, CLogger.Level level) {
        this(clazz, plugin, flowCellID, null, requestID, stage, step, level);
    }

    // Sample constructors

    public CrescoGPMSLogger(CPlugin plugin, String flowCellID, String requestID, String sampleID) {
        this(CrescoGPMSLogger.class, plugin, flowCellID, sampleID, requestID, 0, 0, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(CPlugin plugin, String flowCellID, String requestID, String sampleID, CLogger.Level level) {
        this(CrescoGPMSLogger.class, plugin, flowCellID, sampleID, requestID, 0, 0, level);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, String flowCellID, String requestID, String sampleID) {
        this(clazz, plugin, flowCellID, sampleID, requestID, 0, 0, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, String flowCellID, String requestID, String sampleID, CLogger.Level level) {
        this(clazz, plugin, flowCellID, sampleID, requestID, 0, 0, level);
    }

    public CrescoGPMSLogger(CPlugin plugin, String flowCellID, String requestID, String sampleID, int stage) {
        this(CrescoGPMSLogger.class, plugin, flowCellID, sampleID, requestID, stage, 0, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(CPlugin plugin, String flowCellID, String requestID, String sampleID, int stage, CLogger.Level level) {
        this(CrescoGPMSLogger.class, plugin, flowCellID, sampleID, requestID, stage, 0, level);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, String flowCellID, String requestID, String sampleID, int stage) {
        this(clazz, plugin, flowCellID, sampleID, requestID, stage, 0, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, String flowCellID, String requestID, String sampleID, int stage, CLogger.Level level) {
        this(clazz, plugin, flowCellID, sampleID, requestID, stage, 0, level);
    }

    public CrescoGPMSLogger(CPlugin plugin, String flowCellID, String sampleID, String requestID, int stage, int step) {
        this(CrescoGPMSLogger.class, plugin, flowCellID, sampleID, requestID, stage, step, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(CPlugin plugin, String flowCellID, String sampleID, String requestID, int stage, int step, CLogger.Level level) {
        this(CrescoGPMSLogger.class, plugin, flowCellID, sampleID, requestID, stage, step, level);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, String flowCellID, String sampleID, String requestID, int stage, int step) {
        this(clazz, plugin, flowCellID, requestID, sampleID, stage, step, CLogger.Level.Info);
    }

    public CrescoGPMSLogger(Class clazz, CPlugin plugin, String flowCellID, String sampleID, String requestID, int stage, int step, CLogger.Level level) {
        this.logger = new CLogger(clazz, plugin.getMsgOutQueue(), plugin.getRegion(), plugin.getAgent(), plugin.getPluginID(), level);
        setPlugin(plugin);
        setFlowCellID(flowCellID);
        setSampleID(sampleID);
        setRequestID(requestID);
        setStage(stage);
        setStep(step);
        setLevel(level);
    }

    public void trace(String message) {
        logger.trace(message);
    }
    public void trace(String message, Object... objects) {
        logger.trace(message, objects);
    }

    public void debug(String message) {
        logger.debug(message);
    }
    public void debug(String message, Object... objects) {
        logger.debug(message, objects);
    }

    public void info(String message) {
        logger.info(message);
    }
    public void info(String message, Object... objects) {
        logger.info(message, objects);
    }

    public void warn(String message) {
        logger.warn(message);
    }
    public void warn(String message, Object... objects) {
        logger.warn(message, objects);
    }

    public void error(String message) {
        logger.error(message);
    }
    public void error(String message, Object... objects) {
        logger.error(message, objects);
    }

    public void gpmsInfo(Map<String, String> customParams) {
        MsgEvent infoMsg = genGPMSMessage(MsgEvent.Type.INFO);
        infoMsg.getParams().putAll(customParams);
        plugin.sendMsgEvent(infoMsg);
    }

    public void gpmsInfo(String message) {
        if (!message.equals(CrescoGPMSStatics.PROCESSOR_IDLE_MESSAGE))
            logger.info(message);
        plugin.sendMsgEvent(genGPMSMessage(MsgEvent.Type.INFO, message));
    }

    public void gpmsInfo(Map<String, String> customParams, String message) {
        logger.info(message);
        MsgEvent infoMsg = genGPMSMessage(MsgEvent.Type.INFO, message);
        infoMsg.getParams().putAll(customParams);
        plugin.sendMsgEvent(infoMsg);
    }

    public void gpmsInfo(String message, Object... objects) {
        logger.info(message, objects);
        plugin.sendMsgEvent(genGPMSMessage(MsgEvent.Type.INFO, replaceBrackets(message, objects)));
    }

    public void gpmsInfo(Map<String, String> customParams, String message, Object... objects) {
        logger.info(message, objects);
        MsgEvent infoMsg = genGPMSMessage(MsgEvent.Type.INFO, replaceBrackets(message, objects));
        infoMsg.getParams().putAll(customParams);
        plugin.sendMsgEvent(infoMsg);
    }

    public void gpmsError(Map<String, String> customParams) {
        MsgEvent errMsg = genGPMSMessage(MsgEvent.Type.ERROR);
        errMsg.getParams().putAll(customParams);
        plugin.sendMsgEvent(errMsg);
    }

    public void gpmsError(String message) {
        logger.error(message);
        plugin.sendMsgEvent(genGPMSMessage(MsgEvent.Type.ERROR, message));
    }

    public void gpmsError(Map<String, String> customParams, String message) {
        logger.error(message);
        MsgEvent errMsg = genGPMSMessage(MsgEvent.Type.ERROR, message);
        errMsg.getParams().putAll(customParams);
        plugin.sendMsgEvent(errMsg);
    }

    public void gpmsError(String message, Object... objects) {
        logger.error(message, objects);
        plugin.sendMsgEvent(genGPMSMessage(MsgEvent.Type.ERROR, replaceBrackets(message, objects)));
    }

    public void gpmsError(Map<String, String> customParams, String message, Object... objects) {
        logger.error(message, objects);
        MsgEvent errMsg = genGPMSMessage(MsgEvent.Type.ERROR, replaceBrackets(message, objects));
        errMsg.getParams().putAll(customParams);
        plugin.sendMsgEvent(errMsg);
    }

    public void gpmsFailure(String message) {
        logger.error("FAILURE: {}", message);
        MsgEvent failMsg = genGPMSMessage(MsgEvent.Type.ERROR, message);
        failMsg.setParam(CrescoGPMSStatics.FAILURE_MESSAGE_PARAM_NAME, message);
        plugin.sendMsgEvent(failMsg);
    }

    public void gpmsFailure(String message, Object... objects) {
        logger.error("FAILURE: {}", replaceBrackets(message, objects));
        MsgEvent failMsg = genGPMSMessage(MsgEvent.Type.ERROR, replaceBrackets(message, objects));
        failMsg.setParam(CrescoGPMSStatics.FAILURE_MESSAGE_PARAM_NAME, replaceBrackets(message, objects));
        plugin.sendMsgEvent(failMsg);
    }

    public GPMSLogger cloneLogger(Class clazz) {
        return new CrescoGPMSLogger(clazz, getPlugin(), getFlowCellID(), getSampleID(), getRequestID(), getStage(), getStep(), getLevel());
    }

    public CPlugin getPlugin() {
        return plugin;
    }

    public void setPlugin(CPlugin plugin) {
        this.plugin = plugin;
    }

    public String getFlowCellID() {
        return flowCellID;
    }

    public void setFlowCellID(String flowCellID) {
        this.flowCellID = flowCellID;
    }

    public String getSampleID() {
        return sampleID;
    }

    public void setSampleID(String sampleID) {
        this.sampleID = sampleID;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public void incrementStep() {
        this.step++;
    }

    public CLogger.Level getLevel() {
        return level;
    }

    public void setLevel(CLogger.Level level) {
        this.level = level;
    }

    private MsgEvent genGPMSMessage(MsgEvent.Type type) {
        return genGPMSMessage(type, "");
    }

    private MsgEvent genGPMSMessage(MsgEvent.Type type, String msgBody) {
        MsgEvent me = null;
        try {
            me = new MsgEvent(MsgEvent.Type.EXEC, plugin.getRegion(), plugin.getAgent(), plugin.getPluginID(),
                    (type != MsgEvent.Type.ERROR) ? msgBody : "");
            me.setParam(CrescoGPMSStatics.SOURCE_REGION_PARAM_NAME, plugin.getRegion());
            me.setParam(CrescoGPMSStatics.SOURCE_AGENT_PARAM_NAME, plugin.getAgent());
            me.setParam(CrescoGPMSStatics.SOURCE_PLUGIN_PARAM_NAME, plugin.getPluginID());
            me.setParam(CrescoGPMSStatics.DESTINATION_REGION_PARAM_NAME,
                    plugin.getConfig().getStringParam(CrescoGPMSStatics.GENOMICS_CONTROLLER_REGION_PARAM_NAME, plugin.getRegion()));
            me.setParam(CrescoGPMSStatics.DESTINATION_AGENT_PARAM_NAME,
                    plugin.getConfig().getStringParam(CrescoGPMSStatics.GENOMICS_CONTROLLER_AGENT_PARAM_NAME, plugin.getAgent()));
            me.setParam(CrescoGPMSStatics.DESTINATION_PLUGIN_PARAM_NAME,
                    plugin.getConfig().getStringParam(CrescoGPMSStatics.GENOMICS_CONTROLLER_PLUGIN_PARAM_NAME, plugin.getPluginID()));
            me.setParam(CrescoGPMSStatics.GPMS_MESSAGE_TYPE_PARAM_NAME, type.name());
            me.setParam(CrescoGPMSStatics.PATHSTAGE_PARAM_NAME, Integer.toString(getStage()));
            if (getFlowCellID() != null)
                me.setParam(CrescoGPMSStatics.FLOW_CELL_ID_PARAM_NAME, getFlowCellID());
            if (getSampleID() != null)
                me.setParam(CrescoGPMSStatics.SAMPLE_ID_PARAM_NAME, getSampleID());
            if (getRequestID() != null)
                me.setParam(CrescoGPMSStatics.REQUEST_ID_PARAM_NAME, getRequestID());
            if (getSampleID() != null)
                me.setParam(CrescoGPMSStatics.SAMPLE_STEP_PARAM_NAME, Integer.toString(getStep()));
            else if (getFlowCellID() != null)
                me.setParam(CrescoGPMSStatics.FLOW_CELL_STEP_PARAM_NAME, Integer.toString(getStep()));
            else
                me.setParam(CrescoGPMSStatics.PLUGIN_STEP_PARAM_NAME, Integer.toString(getStep()));
            if (type == MsgEvent.Type.ERROR)
                me.setParam(CrescoGPMSStatics.ERROR_MESSAGE_PARAM_NAME, msgBody);
        } catch(Exception ex) {
            logger.error(ex.getMessage());
        }
        return me;
    }

    private void setFailed(MsgEvent failMsg, String message) {
        failMsg.getParams().put(CrescoGPMSStatics.FAILURE_MESSAGE_PARAM_NAME, message);
    }

    private String replaceBrackets(String logMessage, Object... params) {
        int replaced = 0;
        while (logMessage.contains("{}") && replaced < params.length) {
            logMessage = logMessage.replaceFirst("\\{}", String.valueOf(params[replaced]).replace("\\", "\\\\"));
            replaced++;
        }
        return logMessage;
    }
}
