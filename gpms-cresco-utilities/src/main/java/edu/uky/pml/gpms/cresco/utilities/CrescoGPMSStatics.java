package edu.uky.pml.gpms.cresco.utilities;

@SuppressWarnings("WeakerAccess")
public class CrescoGPMSStatics {
    // Config parameter names
    public static final String PATHSTAGE_CONFIG_PARAM_NAME = "pathstage";
    public static final String INSTANCE_ID_URL_CONFIG_PARAM_NAME = "instance_id_url";
    public static final String SINGLE_USE_INSTANCE_CONFIG_PARAM_NAME = "single_use_instance";
    public static final String GENOMICS_CONTROLLER_REGION_CONFIG_PARAM_NAME = "genomic_controller_region";
    public static final String GENOMICS_CONTROLLER_AGENT_CONFIG_PARAM_NAME = "genomic_controller_agent";
    public static final String GENOMICS_CONTROLLER_PLUGIN_CONFIG_PARAM_NAME = "genomic_controller_plugin";
    public static final String INCOMING_DIRECTORY_CONFIG_PARAM_NAME = "incoming_directory";
    public static final String OUTGOING_DIRECTORY_CONFIG_PARAM_NAME = "outgoing_directory";
    public static final String GPACKAGE_DIRECTORY_CONFIG_PARAM_NAME = "gpackage_directory";
    public static final String WATCH_DIRECTORY_CONFIG_PARAM_NAME = "watch_directory";
    public static final String STAGING_DIRECTORY_CONFIG_PARAM_NAME = "staging_directory";
    public static final String PREPROCESSING_DOCKER_CONTAINER_NAME_CONFIG_PARAM_NAME = "preprocessing_container_name";
    public static final String CONFIG_GENERATION_DOCKER_CONTAINER_NAME_CONFIG_PARAM_NAME =
            "config_generation_container_name";
    public static final String PROCESSING_DOCKER_CONTAINER_NAME_CONFIG_PARAM_NAME = "processing_container_name";
    public static final String DOCKER_REGISTRY_CONFIG_PARAM_NAME = "docker_registry";
    public static final String DOCKER_CONTAINER_VERSION_PARAM_NAME = "docker_container_version";
    public static final String S3_RAW_BUCKET_CONFIG_PARAM_NAME = "raw_bucket";
    public static final String S3_CLINICAL_BUCKET_CONFIG_PARAM_NAME = "clinical_bucket";
    public static final String S3_RESEARCH_BUCKET_CONFIG_PARAM_NAME = "research_bucket";
    public static final String S3_PROCESSED_BUCKET_CONFIG_PARAM_NAME = "processed_bucket";
    public static final String S3_RESULTS_BUCKET_CONFIG_PARAM_NAME = "results_bucket";
    public static final String S3_ACCESS_KEY_CONFIG_PARAM_NAME = "accesskey";
    public static final String S3_SECRET_KEY_CONFIG_PARAM_NAME = "secretkey";
    public static final String S3_ENDPOINT_CONFIG_PARAM_NAME = "endpoint";
    public static final String S3_REGION_CONFIG_PARAM_NAME = "s3region";
    public static final String S3_UPLOAD_PART_SIZE_CONFIG_PARAM_NAME = "uploadpartsizemb";
    public static final String BAGIT_TYPE_CONFIG_PARAM_NAME = "bagit_type";
    public static final String BAGIT_HASH_CONFIG_PARAM_NAME = "bagit_hashing";
    public static final String BAGIT_INCLUDE_HIDDEN_CONFIG_PARAM_NAME = "bagit_include_hidden";
    public static final String COMPRESSION_TYPE_CONFIG_PARAM_NAME = "compression_type";

    // MsgEvent parameter names
    public static final String SOURCE_REGION_PARAM_NAME = "src_region";
    public static final String SOURCE_AGENT_PARAM_NAME = "src_agent";
    public static final String SOURCE_PLUGIN_PARAM_NAME = "src_plugin";
    public static final String DESTINATION_REGION_PARAM_NAME = "dst_region";
    public static final String DESTINATION_AGENT_PARAM_NAME = "dst_agent";
    public static final String DESTINATION_PLUGIN_PARAM_NAME = "dst_plugin";
    public static final String GPMS_MESSAGE_TYPE_PARAM_NAME = "gmsg_type";
    public static final String GENOMICS_CONTROLLER_REGION_PARAM_NAME = "genomic_controller_region";
    public static final String GENOMICS_CONTROLLER_AGENT_PARAM_NAME = "genomic_controller_agent";
    public static final String GENOMICS_CONTROLLER_PLUGIN_PARAM_NAME = "genomic_controller_plugin";
    public static final String REQUEST_ID_PARAM_NAME = "req_id";
    public static final String PROCESSOR_EXEC_ACTION_PARAM_NAME = "exec_action";
    public static final String PROCESSOR_EXEC_ACTION_RESULT_PARAM_NAME = "exec_action_result";
    public static final String PROCESSOR_EXEC_ACTION_MESSAGE_PARAM_NAME = "exec_action_message";
    public static final String PATHSTAGE_PARAM_NAME = "pathstage";
    public static final String FLOW_CELL_ID_PARAM_NAME = "seq_id";
    public static final String SAMPLE_ID_PARAM_NAME = "sample_id";
    public static final String PLUGIN_STEP_PARAM_NAME = "pstep";
    public static final String FLOW_CELL_STEP_PARAM_NAME = "sstep";
    public static final String SAMPLE_STEP_PARAM_NAME = "ssstep";
    public static final String ERROR_MESSAGE_PARAM_NAME = "error_message";
    public static final String FAILURE_MESSAGE_PARAM_NAME = "failure_message";
    public static final String INSTANCE_ID_PARAM_NAME = "ec2_instance_id";
    public static final String SINGLE_USE_INSTANCE_PARAM_NAME = "single_use_instance";
    public static final String SAMPLE_SHEET_PARAM_NAME = "sample_sheet_contents";
    public static final String SAMPLE_MRD_VARIANTS_PARAM_NAME = "sample_coordinates";

    // GPMS Info Request
    public static final String GPMS_INFO_REQUEST_TYPE_PARAM_NAME = "gmsg_info_request";
    public static final String GPMS_INFO_REQUEST_REQUEST_ID_PARAM_NAME = "gpms_info_request_request_id";
    public static final String GPMS_INFO_REQUEST_RESULT_PARAM_NAME = "gmsg_info_request_result";
    public static final String GPMS_INFO_REQUEST_ERRORS_PARAM_NAME = "gmsg_info_request_errors";
    public static final String GPMS_INFO_REQUEST_TYPE_OUTPUT = "docker_output_log";
    public static final String GPMS_INFO_REQUEST_TYPE_OUTPUT_LINES = "docker_output_log_line_count";
    public static final int    GPMS_INFO_REQUEST_OUTPUT_LINES_DEFAULT = 50;
    public static final String GPMS_INFO_REQUEST_TYPE_WATCH_FILE = "watch_file";

    // GPMS Exec Request
    public static final String GPMS_EXEC_REQUEST_PARAM_NAME = "exec_action";
    public static final String GPMS_EXEC_REQUEST_UPLOAD_RAW_FLOW_CELL = "upload_raw_flow_cell";
    public static final String GPMS_EXEC_REQUEST_UPLOAD_RAW_FLOW_CELL_ID = "upload_raw_flow_cell_id";

    // GPMS message headers
    public static final String PROCESSOR_CONFIG_PUSH_MESSAGE = "Sending plugin configuration info";
    public static final String PROCESSOR_IDLE_MESSAGE = "Idle";
    public static final String PROCESSOR_SHUTDOWN_MESSAGE = "Shutting down";

    // Miscellaneous
    public static final String RAW_WATCHER_WATCH_DIRECTORY = "raw_watcher_directory";
    public static final String RAW_WATCHER_CHECK_FILE_NAME = "raw_watcher_check_file_name";
    public static final String DEFAULT_INSTANCE_ID_URL = "http://169.254.169.254/latest/meta-data/instance-id";
    public static final int    PROCESSOR_IDLE_STEP = 2;

}
