package edu.uky.pml.gpms.cresco.plugins;

import com.google.auto.service.AutoService;
import com.researchworx.cresco.library.plugin.core.CPlugin;
import com.researchworx.cresco.library.utilities.CLogger;
import edu.uky.pml.gpms.cresco.utilities.CrescoGPMSLogger;
import edu.uky.pml.gpms.cresco.utilities.CrescoGPMSStatics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

@SuppressWarnings({"WeakerAccess", "unused"})
@AutoService(CPlugin.class)
public class ProcessorPlugin extends CPlugin {
    private static final CLogger.Level LOGGING_LEVEL = CLogger.Level.Trace;

    private CrescoGPMSLogger gpmsLogger;

    private Timer heartBeatTimer;
    private int pathStage;

    private String processorInstanceID;
    private boolean isSingleUseInstance = false;

    public void start() {
        setPathStage(getConfig().getIntegerParam(CrescoGPMSStatics.PATHSTAGE_CONFIG_PARAM_NAME, 0));
        this.gpmsLogger = new CrescoGPMSLogger(ProcessorPlugin.class, this, getPathStage(), LOGGING_LEVEL);
        gpmsLogger.info("Starting plugin");
        setProcessorInstanceID(getAWSInstanceID());
        setIsSingleUseInstance(getConfig().getBooleanParam(CrescoGPMSStatics.SINGLE_USE_INSTANCE_CONFIG_PARAM_NAME, false));
        setExec(new ProcessorExecutor(this));
        gpmsLogger.info("Processor configuration: [ID: {}, SingleUse: {}]", getAWSInstanceID(), getIsSingleUseInstance());
        Map<String, String> instanceConfigInfo = new HashMap<String, String>()
        {{
            put(CrescoGPMSStatics.INSTANCE_ID_PARAM_NAME, getProcessorInstanceID());
            put(CrescoGPMSStatics.SINGLE_USE_INSTANCE_PARAM_NAME, Boolean.toString(getIsSingleUseInstance()));
        }};
        gpmsLogger.gpmsInfo(instanceConfigInfo, CrescoGPMSStatics.PROCESSOR_CONFIG_PUSH_MESSAGE);
        gpmsLogger.setStep(2);
        heartBeatTimer = new Timer();
        heartBeatTimer.scheduleAtFixedRate(new ProcessorPluginHeartBeat(this), 100, 5000);
    }

    @Override
    public void cleanUp() {
        heartBeatTimer.cancel();
        gpmsLogger.setStep(0);
        gpmsLogger.gpmsInfo(CrescoGPMSStatics.PROCESSOR_SHUTDOWN_MESSAGE);
    }

    public CrescoGPMSLogger getCrescoGPMSLogger() {
        return gpmsLogger;
    }

    public int getPathStage() {
        return pathStage;
    }
    public void setPathStage(int pathStage) {
        this.pathStage = pathStage;
    }

    public void setProcessorInstanceID(String processorInstanceID) {
        this.processorInstanceID = processorInstanceID;
    }
    public String getProcessorInstanceID() {
        return processorInstanceID;
    }

    public void setIsSingleUseInstance(boolean isSingleUseInstance) {
        this.isSingleUseInstance = isSingleUseInstance;
    }
    public boolean getIsSingleUseInstance() {
        return isSingleUseInstance;
    }

    public int getPluginPathStage() {
        return gpmsLogger.getStage();
    }
    public void setPluginPathStage(int stage) {
        gpmsLogger.setStage(stage);
    }

    public int getPluginStep() {
        return gpmsLogger.getStep();
    }
    public void incrementPluginStep() {
        gpmsLogger.incrementStep();
    }
    public void setPluginStep(int step) {
        gpmsLogger.setStep(step);
    }

    public void sendHeartBeat() {
        gpmsLogger.gpmsInfo(CrescoGPMSStatics.PROCESSOR_IDLE_MESSAGE);
    }

    private String getAWSInstanceID() {
        try {
            URL instanceIDURL = new URL(getConfig().getStringParam(CrescoGPMSStatics.INSTANCE_ID_URL_CONFIG_PARAM_NAME, CrescoGPMSStatics.DEFAULT_INSTANCE_ID_URL));
            HttpURLConnection conn = (HttpURLConnection) instanceIDURL.openConnection();
            conn.setConnectTimeout(1000);
            conn.setReadTimeout(1000);
            conn.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            return in.readLine();
        } catch (ProtocolException e) {
            logger.error("Protocol exception when getting instance ID: {}", e.getMessage());
            return null;
        } catch (SocketTimeoutException e) {
            logger.warn("Connection to local metadata server timed out");
            return null;
        } catch (IOException e) {
            logger.error("I/O exception when getting instance ID: {}", e.getMessage());
            return null;
        }
    }

    class ProcessorPluginHeartBeat extends TimerTask {
        private ProcessorPlugin processorPlugin;
        ProcessorPluginHeartBeat(ProcessorPlugin processorPlugin) {
            this.processorPlugin = processorPlugin;
        }
        public void run() {
            processorPlugin.sendHeartBeat();
        }
    }
}
