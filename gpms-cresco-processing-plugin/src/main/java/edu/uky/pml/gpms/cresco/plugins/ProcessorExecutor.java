package edu.uky.pml.gpms.cresco.plugins;

import com.researchworx.cresco.library.messaging.MsgEvent;
import com.researchworx.cresco.library.plugin.core.CExecutor;
import com.researchworx.cresco.library.utilities.CLogger;
import edu.uky.pml.gpms.cresco.utilities.CrescoGPMSLogger;
import edu.uky.pml.gpms.cresco.utilities.CrescoGPMSStatics;
import edu.uky.pml.gpms.data.DataManager;
import edu.uky.pml.gpms.data.encapsulation.Archiver;
import edu.uky.pml.gpms.data.transfer.ObjectStorage;
import edu.uky.pml.gpms.processing.ProcessManager;
import edu.uky.pml.gpms.utilities.helpers.GPMSStatics;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import software.amazon.ion.NullValueException;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ProcessorExecutor extends CExecutor {
    private static final CLogger.Level LOGGING_LEVEL = CLogger.Level.Trace;
    private ProcessorPlugin processorPlugin;
    private ProcessManager processManager = null;
    private String runningRequestID = null;

    ProcessorExecutor(ProcessorPlugin plugin) {
        super(plugin);
        this.processorPlugin = plugin;
    }

    @Override
    public MsgEvent processExec(MsgEvent msg) {
        CrescoGPMSLogger logger = new CrescoGPMSLogger(ProcessorExecutor.class, processorPlugin, processorPlugin.getPathStage(), LOGGING_LEVEL);
        try {
            logger.trace("Processing [EXEC] message");
            logger.debug("Params: " + msg.getParams());
            logger.trace("Checking pathstage");
            int pathStage;
            String pathStageParam = msg.getParam(CrescoGPMSStatics.PATHSTAGE_PARAM_NAME);
            if (pathStageParam == null) {
                logger.gpmsError("No [{}] provided", CrescoGPMSStatics.PATHSTAGE_PARAM_NAME);
                return null;
            }
            logger.trace("Getting pathstage");
            try {
                pathStage = Integer.parseInt(pathStageParam);
            } catch (NumberFormatException e) {
                logger.gpmsError("Invalid path stage [{}] provided: {}", CrescoGPMSStatics.PATHSTAGE_PARAM_NAME, pathStageParam);
                return null;
            }
            if (processorPlugin.getPathStage() != pathStage) {
                logger.gpmsError("Wrong path stage request sent [current: {}, received: {}]", processorPlugin.getPathStage(), pathStage);
                return null;
            }
            logger.setStage(pathStage);
            logger.trace("Getting flowCell");
            String flowCellID = msg.getParam(CrescoGPMSStatics.FLOW_CELL_ID_PARAM_NAME);
            logger.setFlowCellID(flowCellID);
            logger.trace("Getting sample");
            String sampleID = msg.getParam(CrescoGPMSStatics.SAMPLE_ID_PARAM_NAME);
            logger.setSampleID(sampleID);
            logger.trace("Getting request");
            String requestID = msg.getParam(CrescoGPMSStatics.REQUEST_ID_PARAM_NAME);
            logger.setRequestID(requestID);
            logger.trace("Getting action");
            String execAction = msg.getParam(CrescoGPMSStatics.PROCESSOR_EXEC_ACTION_PARAM_NAME);
            if (execAction == null || StringUtils.isBlank(execAction)) {
                logger.gpmsFailure("No valid [{}] provided", CrescoGPMSStatics.PROCESSOR_EXEC_ACTION_PARAM_NAME);
                return null;
            }
            switch (execAction) {
                case "start": {
                    logger.trace("Grabbing config file params");
                    String s3AccessKey = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.S3_ACCESS_KEY_CONFIG_PARAM_NAME);
                    String s3SecretKey = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.S3_SECRET_KEY_CONFIG_PARAM_NAME);
                    String s3Endpoint = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.S3_ENDPOINT_CONFIG_PARAM_NAME);
                    String s3Region = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.S3_REGION_CONFIG_PARAM_NAME, "");
                    String archiverBagItType = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.BAGIT_TYPE_CONFIG_PARAM_NAME, GPMSStatics.DEFAULT_BAGIT_TYPE);
                    String archiverBagItHash = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.BAGIT_HASH_CONFIG_PARAM_NAME, GPMSStatics.DEFAULT_BAGIT_HASHING);
                    boolean archiverBagItHidden = processorPlugin.getConfig().getBooleanParam(CrescoGPMSStatics.BAGIT_INCLUDE_HIDDEN_CONFIG_PARAM_NAME, GPMSStatics.DEFAULT_HIDDEN_FILES);
                    String archiverCompression = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.COMPRESSION_TYPE_CONFIG_PARAM_NAME, GPMSStatics.DEFAULT_ARCHIVE_COMPRESSION);
                    String rawBucketName = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.S3_RAW_BUCKET_CONFIG_PARAM_NAME);
                    String clinicalBucketName = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.S3_CLINICAL_BUCKET_CONFIG_PARAM_NAME);
                    String researchBucketName = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.S3_RESEARCH_BUCKET_CONFIG_PARAM_NAME);
                    String processedBucketName = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.S3_PROCESSED_BUCKET_CONFIG_PARAM_NAME);
                    String resultsBucketName = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.S3_RESULTS_BUCKET_CONFIG_PARAM_NAME);
                    String inputDirParam = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.INCOMING_DIRECTORY_CONFIG_PARAM_NAME);
                    String dockerRegistry = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.DOCKER_REGISTRY_CONFIG_PARAM_NAME, GPMSStatics.DEFAULT_DOCKER_REGISTRY);
                    String dockerContainerVersion = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.DOCKER_CONTAINER_VERSION_PARAM_NAME, GPMSStatics.DEFAULT_DOCKER_CONTAINER_VERSION);
                    if (inputDirParam == null) {
                        logger.gpmsFailure("No valid input working directory supplied");
                        return null;
                    }
                    Path inputDir = Paths.get(inputDirParam);
                    String outputDirParam = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.OUTGOING_DIRECTORY_CONFIG_PARAM_NAME);
                    if (outputDirParam == null) {
                        logger.gpmsFailure("No valid output working directory supplied");
                        return null;
                    }
                    Path outputDir = Paths.get(outputDirParam);
                    String preprocessingContainerName = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.PREPROCESSING_DOCKER_CONTAINER_NAME_CONFIG_PARAM_NAME);
                    String configGenerationContainerName = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.CONFIG_GENERATION_DOCKER_CONTAINER_NAME_CONFIG_PARAM_NAME);
                    String processingContainerName = processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.PROCESSING_DOCKER_CONTAINER_NAME_CONFIG_PARAM_NAME);
                    Path gPackageDir = Paths.get(processorPlugin.getConfig().getStringParam(CrescoGPMSStatics.GPACKAGE_DIRECTORY_CONFIG_PARAM_NAME));
                    String sampleMRDVarinats = msg.getParam(CrescoGPMSStatics.SAMPLE_MRD_VARIANTS_PARAM_NAME);
                    if (sampleMRDVarinats == null)
                        sampleMRDVarinats = GPMSStatics.DEFAULT_MRD_VARIANTS;
                    logger.trace("Pathstage switch case");
                    switch (pathStage) {
                        case GPMSStatics.PREPROCESSING_STAGE: {
                            if (flowCellID == null || StringUtils.isBlank(flowCellID)) {
                                logger.gpmsFailure("No valid flow cell ID supplied");
                                return null;
                            }
                            if (requestID == null || StringUtils.isBlank(requestID)) {
                                logger.gpmsFailure("No valid request ID supplied");
                                return null;
                            }
                            logger.setFlowCellID(flowCellID);
                            logger.setRequestID(requestID);
                            processorPlugin.getCrescoGPMSLogger().setRequestID(requestID);
                            processorPlugin.incrementPluginStep();
                            logger.trace("Building object storage engine");
                            ObjectStorage objectStorage;
                            try {
                                objectStorage = new ObjectStorage(s3AccessKey, s3SecretKey, s3Endpoint, s3Region, logger);
                            } catch (NullValueException | IllegalArgumentException e) {
                                logger.gpmsFailure("Error building object storage manager: {}", e.getMessage());
                                return null;
                            }
                            logger.trace("Building archiver engine");
                            Archiver archiver;
                            try {
                                archiver = new Archiver(archiverBagItType, archiverBagItHash, archiverBagItHidden, archiverCompression, logger);
                            } catch (NullValueException | IllegalArgumentException e) {
                                logger.gpmsFailure("Error building archiver: {}", e.getMessage());
                                return null;
                            }
                            logger.trace("Building data management engine");
                            DataManager dataManager;
                            try {
                                dataManager = new DataManager(objectStorage, archiver, rawBucketName, clinicalBucketName, researchBucketName,
                                        processedBucketName, resultsBucketName, logger);
                            } catch (IllegalArgumentException e) {
                                logger.gpmsFailure("Error building data manager: {}", e.getMessage());
                                return null;
                            } catch (Exception e) {
                                logger.gpmsFailure("Exception encountered:\n{}:{}", e.getCause().getClass().getCanonicalName(), e.getCause().getMessage());
                                return null;
                            }
                            logger.trace("Building processor management engine");
                            try {
                                processManager = new ProcessManager(dataManager, logger);
                                processManager.setDockerRegistry(dockerRegistry);
                                processManager.setDockerContainerVersion(dockerContainerVersion);
                            } catch (NullValueException | IllegalArgumentException e) {
                                logger.gpmsFailure("Error building genomics processor: {}", e.getMessage());
                                return null;
                            }
                            logger.info("Received preprocess request [flowCellID: {}, requestID: {}]",
                                    flowCellID, requestID);
                            runningRequestID = requestID;
                            processManager.preprocessBaggedFlowCell(flowCellID, requestID, inputDir, outputDir,
                                    preprocessingContainerName, GPMSStatics.PREPROCESSING_CONTAINER_COMMAND,
                                    configGenerationContainerName, GPMSStatics.CONFIG_GENERATION_CONTAINER_COMMAND);
                            runningRequestID = null;
                            processManager = null;
                            processorPlugin.getCrescoGPMSLogger().setRequestID(null);
                            logger.setFlowCellID(null);
                            logger.setRequestID(null);
                            if (!processorPlugin.getIsSingleUseInstance())
                                processorPlugin.setPluginStep(CrescoGPMSStatics.PROCESSOR_IDLE_STEP);
                        }
                        break;
                        case GPMSStatics.PROCESSING_STAGE: {
                            if (flowCellID == null || StringUtils.isBlank(flowCellID)) {
                                logger.gpmsFailure("No valid flow cell ID supplied");
                                return null;
                            }
                            if (sampleID == null || StringUtils.isBlank(sampleID)) {
                                logger.gpmsFailure("No valid sample ID supplied");
                                return null;
                            }
                            if (requestID == null || StringUtils.isBlank(requestID)) {
                                logger.gpmsFailure("No valid request ID supplied");
                                return null;
                            }
                            logger.setFlowCellID(flowCellID);
                            logger.setSampleID(sampleID);
                            logger.setRequestID(requestID);
                            processorPlugin.getCrescoGPMSLogger().setRequestID(requestID);
                            processorPlugin.incrementPluginStep();
                            logger.trace("Building object storage engine");
                            ObjectStorage objectStorage;
                            try {
                                objectStorage = new ObjectStorage(s3AccessKey, s3SecretKey, s3Endpoint, s3Region, logger);
                            } catch (NullValueException | IllegalArgumentException e) {
                                logger.gpmsFailure("Error building object storage manager: {}", e.getMessage());
                                return null;
                            }
                            logger.trace("Building archiver engine");
                            Archiver archiver;
                            try {
                                archiver = new Archiver(archiverBagItType, archiverBagItHash, archiverBagItHidden, archiverCompression, logger);
                            } catch (NullValueException | IllegalArgumentException e) {
                                logger.gpmsFailure("Error building archiver: {}", e.getMessage());
                                return null;
                            }
                            logger.trace("Building data management engine");
                            DataManager dataManager;
                            try {
                                dataManager = new DataManager(objectStorage, archiver, rawBucketName, clinicalBucketName, researchBucketName,
                                        processedBucketName, resultsBucketName, logger);
                            } catch (IllegalArgumentException e) {
                                logger.gpmsFailure("Error building data manager: {}", e.getMessage());
                                return null;
                            } catch (Exception e) {
                                logger.gpmsFailure("Exception encountered:\n{}:{}", e.getCause().getClass().getCanonicalName(), e.getCause().getMessage());
                                return null;
                            }
                            logger.trace("Building processor management engine");
                            try {
                                processManager = new ProcessManager(dataManager, logger);
                                processManager.setDockerRegistry(dockerRegistry);
                                processManager.setDockerContainerVersion(dockerContainerVersion);
                            } catch (NullValueException | IllegalArgumentException e) {
                                logger.gpmsFailure("Error building genomics processor: {}", e.getMessage());
                                return null;
                            }
                            logger.info("Received process request [flowCellID: {}, sampleID: {}, requestID: {}]",
                                    flowCellID, sampleID, requestID);
                            runningRequestID = requestID;
                            if (processingContainerName == null)
                                processManager.processBaggedSample(flowCellID, sampleID, requestID, inputDir, outputDir, gPackageDir, sampleMRDVarinats);
                            else
                                processManager.processBaggedSample(flowCellID, sampleID, requestID, inputDir, outputDir, gPackageDir, sampleMRDVarinats, processingContainerName);
                            runningRequestID = null;
                            processManager = null;
                            processorPlugin.getCrescoGPMSLogger().setRequestID(null);
                            logger.setFlowCellID(null);
                            logger.setRequestID(null);
                            if (!processorPlugin.getIsSingleUseInstance())
                                processorPlugin.setPluginStep(CrescoGPMSStatics.PROCESSOR_IDLE_STEP);
                        }
                        break;
                        case GPMSStatics.POSTPROCESSING_STAGE: {
                            if (flowCellID == null || StringUtils.isBlank(flowCellID)) {
                                logger.gpmsFailure("No valid flow cell ID supplied");
                                return null;
                            }
                            if (requestID == null || StringUtils.isBlank(requestID)) {
                                logger.gpmsFailure("No valid request ID supplied");
                                return null;
                            }
                            logger.setFlowCellID(flowCellID);
                            logger.setSampleID(sampleID);
                            logger.setRequestID(requestID);
                            processorPlugin.getCrescoGPMSLogger().setRequestID(requestID);
                            processorPlugin.incrementPluginStep();
                            logger.trace("Building object storage engine");
                            ObjectStorage objectStorage;
                            try {
                                objectStorage = new ObjectStorage(s3AccessKey, s3SecretKey, s3Endpoint, s3Region, logger);
                            } catch (NullValueException | IllegalArgumentException e) {
                                logger.gpmsFailure("Error building object storage manager: {}", e.getMessage());
                                return null;
                            }
                            logger.trace("Building archiver engine");
                            Archiver archiver;
                            try {
                                archiver = new Archiver(archiverBagItType, archiverBagItHash, archiverBagItHidden, archiverCompression, logger);
                            } catch (NullValueException | IllegalArgumentException e) {
                                logger.gpmsFailure("Error building archiver: {}", e.getMessage());
                                return null;
                            }
                            logger.trace("Building data management engine");
                            DataManager dataManager;
                            try {
                                dataManager = new DataManager(objectStorage, archiver, rawBucketName, clinicalBucketName, researchBucketName,
                                        processedBucketName, resultsBucketName, logger);
                            } catch (IllegalArgumentException e) {
                                logger.error("Error building data manager: {}", e.getMessage());
                                logger.gpmsFailure("Error building data manager: {}", e.getMessage());
                                return null;
                            } catch (Exception e) {
                                logger.gpmsFailure("Exception encountered:\n{}:{}", e.getCause().getClass().getCanonicalName(), e.getCause().getMessage());
                                return null;
                            }
                            logger.trace("Building processor management engine");
                            try {
                                processManager = new ProcessManager(dataManager, logger);
                                processManager.setDockerRegistry(dockerRegistry);
                                processManager.setDockerContainerVersion(dockerContainerVersion);
                            } catch (NullValueException | IllegalArgumentException e) {
                                logger.gpmsFailure("Error building genomics processor: {}", e.getMessage());
                                return null;
                            }
                            logger.info("Received postprocess request [flowCellID: {}, requestID: {}]",
                                    flowCellID, requestID);
                            runningRequestID = requestID;
                            processManager.postprocessFlowCell(flowCellID, requestID, inputDir, gPackageDir);
                            runningRequestID = null;
                            processManager = null;
                            processorPlugin.getCrescoGPMSLogger().setRequestID(null);
                            logger.setFlowCellID(null);
                            logger.setRequestID(null);
                            if (!processorPlugin.getIsSingleUseInstance())
                                processorPlugin.setPluginStep(CrescoGPMSStatics.PROCESSOR_IDLE_STEP);
                        }
                        break;
                        case GPMSStatics.RESULTS_DOWNLOAD_STAGE: {
                            if (flowCellID == null || StringUtils.isBlank(flowCellID)) {
                                logger.gpmsFailure("No valid flow cell ID supplied");
                                return null;
                            }
                            if (requestID == null || StringUtils.isBlank(requestID)) {
                                logger.gpmsFailure("No valid request ID supplied");
                                return null;
                            }
                            logger.setFlowCellID(flowCellID);
                            logger.setSampleID(sampleID);
                            logger.setRequestID(requestID);
                            processorPlugin.getCrescoGPMSLogger().setRequestID(requestID);
                            processorPlugin.incrementPluginStep();
                            logger.trace("Building object storage engine");
                            ObjectStorage objectStorage;
                            try {
                                objectStorage = new ObjectStorage(s3AccessKey, s3SecretKey, s3Endpoint, s3Region, logger);
                            } catch (NullValueException | IllegalArgumentException e) {
                                logger.gpmsFailure("Error building object storage manager: {}", e.getMessage());
                                return null;
                            }
                            logger.trace("Building archiver engine");
                            Archiver archiver;
                            try {
                                archiver = new Archiver(archiverBagItType, archiverBagItHash, archiverBagItHidden, archiverCompression, logger);
                            } catch (NullValueException | IllegalArgumentException e) {
                                logger.gpmsFailure("Error building archiver: {}", e.getMessage());
                                return null;
                            }
                            logger.trace("Building data management engine");
                            DataManager dataManager;
                            try {
                                dataManager = new DataManager(objectStorage, archiver, rawBucketName, clinicalBucketName, researchBucketName,
                                        processedBucketName, resultsBucketName, logger);
                            } catch (IllegalArgumentException e) {
                                logger.gpmsFailure("Error building data manager: {}", e.getMessage());
                                return null;
                            } catch (Exception e) {
                                logger.gpmsFailure("Exception encountered:\n{}:{}", e.getCause().getClass().getCanonicalName(), e.getCause().getMessage());
                                return null;
                            }
                            logger.trace("Building processor management engine");
                            try {
                                processManager = new ProcessManager(dataManager, logger);
                                processManager.setDockerRegistry(dockerRegistry);
                                processManager.setDockerContainerVersion(dockerContainerVersion);
                            } catch (NullValueException | IllegalArgumentException e) {
                                logger.gpmsFailure("Error building genomics processor: {}", e.getMessage());
                                return null;
                            }
                            logger.info("Received postprocess request [flowCellID: {}, requestID: {}]",
                                    flowCellID, requestID);
                            runningRequestID = requestID;
                            processManager.downloadFlowCellResults(flowCellID, requestID, inputDir);
                            runningRequestID = null;
                            processManager = null;
                            processorPlugin.getCrescoGPMSLogger().setRequestID(null);
                            logger.setFlowCellID(null);
                            logger.setRequestID(null);
                            if (!processorPlugin.getIsSingleUseInstance())
                                processorPlugin.setPluginStep(CrescoGPMSStatics.PROCESSOR_IDLE_STEP);
                        }
                        break;
                        default: {
                            logger.gpmsFailure("Invalid path stage request received [{}]", pathStage);
                        }
                        break;
                    }
                } break;
                case "stop": {
                    msg.setParam(CrescoGPMSStatics.PROCESSOR_EXEC_ACTION_RESULT_PARAM_NAME, String.valueOf(false));
                    try {
                        if (processManager != null) {
                            if (runningRequestID != null) {
                                if (runningRequestID.equals(requestID)) {
                                    if (processManager.isDockerRunning()) {
                                        logger.gpmsInfo("Stopping Docker container");
                                        if (processManager.stopDockerContainer()) {
                                            msg.setParam(CrescoGPMSStatics.PROCESSOR_EXEC_ACTION_RESULT_PARAM_NAME,
                                                    String.valueOf(true));
                                            msg.setParam(CrescoGPMSStatics.PROCESSOR_EXEC_ACTION_MESSAGE_PARAM_NAME,
                                                    "Docker container was stopped successfully");
                                            logger.gpmsInfo("Docker container was manually stopped");
                                        }
                                    } else {
                                        msg.setParam(CrescoGPMSStatics.PROCESSOR_EXEC_ACTION_MESSAGE_PARAM_NAME,
                                                "Docker container is not currently running, cannot currently cancel");
                                    }
                                } else {
                                    msg.setParam(CrescoGPMSStatics.PROCESSOR_EXEC_ACTION_MESSAGE_PARAM_NAME,
                                            String.format("Received request ID [%s] does not match currently running request ID [%s]",
                                            requestID, runningRequestID));
                                    logger.gpmsError("Received request ID [{}] does not match currently running request ID [{}]",
                                            requestID, runningRequestID);
                                }
                            } else {
                                msg.setParam(CrescoGPMSStatics.PROCESSOR_EXEC_ACTION_MESSAGE_PARAM_NAME,
                                        "No running request ID found");
                                logger.gpmsError("No running request ID found, cannot currently cancel");
                            }
                        } else {
                            msg.setParam(CrescoGPMSStatics.PROCESSOR_EXEC_ACTION_MESSAGE_PARAM_NAME,
                                    "No process manager is running");
                            logger.gpmsError("No process manager is running, cannot currently cancel");
                        }
                    } catch (Exception e) {
                        msg.setParam(CrescoGPMSStatics.PROCESSOR_EXEC_ACTION_MESSAGE_PARAM_NAME,
                                String.format("An exception occurred while stopping this container - %s:%s",
                                e.getClass().getCanonicalName(), e.getMessage()));
                        logger.gpmsError("An exception occurred while stopping this container - {}:{}",
                                e.getClass().getCanonicalName(), e.getMessage());
                    }
                    return msg;
                }
                default: {
                    logger.gpmsFailure("Action [{}] is not currently support in GPMS", execAction);
                } break;
            }
            return null;
        } catch (Exception e) {
            logger.gpmsError("Executor exception encountered\n" + ExceptionUtils.getStackTrace(e));
            return null;
        }
    }

    @SuppressWarnings("SwitchStatementWithTooFewBranches")
    @Override
    public MsgEvent processInfo(MsgEvent msg) {
        CrescoGPMSLogger logger = new CrescoGPMSLogger(ProcessorExecutor.class, processorPlugin, processorPlugin.getPathStage(), LOGGING_LEVEL);
        logger.trace("Processing [INFO] message");
        logger.debug("Params: {}", msg.getParams());
        List<String> infoRequestErrors = new ArrayList<>();
        String gMsgInfoRequestType = msg.getParam(CrescoGPMSStatics.GPMS_INFO_REQUEST_TYPE_PARAM_NAME);
        if (gMsgInfoRequestType != null) {
            switch (gMsgInfoRequestType) {
                case CrescoGPMSStatics.GPMS_INFO_REQUEST_TYPE_OUTPUT: {
                    if (processManager == null) {
                        infoRequestErrors.add("No ProcessManager is running from which to get logs");
                    } else {
                        String requestID = msg.getParam(CrescoGPMSStatics.GPMS_INFO_REQUEST_REQUEST_ID_PARAM_NAME);
                        if (requestID != null) {
                            String processManagerRequestID = processManager.getLogger().getRequestID();
                            if (processManagerRequestID == null || !processManagerRequestID.equals(requestID)) {
                                infoRequestErrors.add(String.format("Invalid request id received [%s], working on request [%s]",
                                        requestID, (processManagerRequestID != null) ? processManagerRequestID : "NULL"));
                                break;
                            }
                        }
                        String dockerOutput;
                        String lineCountParam = msg.getParam(CrescoGPMSStatics.GPMS_INFO_REQUEST_TYPE_OUTPUT_LINES);
                        if (lineCountParam != null) {
                            int lineCount = CrescoGPMSStatics.GPMS_INFO_REQUEST_OUTPUT_LINES_DEFAULT;
                            try {
                                lineCount = Integer.parseInt(lineCountParam);
                            } catch (NumberFormatException e) {
                                infoRequestErrors.add(String.format("Invalid output line count received [%s], defaulting to %d lines",
                                        lineCountParam, CrescoGPMSStatics.GPMS_INFO_REQUEST_OUTPUT_LINES_DEFAULT));
                            }
                            dockerOutput = processManager.getOutputTail(lineCount);
                        } else {
                            dockerOutput = processManager.getOutput();
                        }
                        msg.setCompressedParam(CrescoGPMSStatics.GPMS_INFO_REQUEST_RESULT_PARAM_NAME, dockerOutput);
                    }
                } break;
                default: {
                    infoRequestErrors.add(String.format("Info request type [%s] is currently unsupported", gMsgInfoRequestType));
                } break;
            }
        }
        msg.setParam(CrescoGPMSStatics.GPMS_INFO_REQUEST_ERRORS_PARAM_NAME, String.join(";", infoRequestErrors));
        return msg;
    }
}
