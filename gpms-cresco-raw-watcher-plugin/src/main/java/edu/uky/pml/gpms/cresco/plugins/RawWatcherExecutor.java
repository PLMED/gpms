package edu.uky.pml.gpms.cresco.plugins;

import com.researchworx.cresco.library.messaging.MsgEvent;
import com.researchworx.cresco.library.plugin.core.CExecutor;
import com.researchworx.cresco.library.utilities.CLogger;
import edu.uky.pml.gpms.cresco.utilities.CrescoGPMSLogger;
import edu.uky.pml.gpms.cresco.utilities.CrescoGPMSStatics;
import edu.uky.pml.gpms.watcher.TransferStatus;
import edu.uky.pml.gpms.watcher.WatchFolder;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class RawWatcherExecutor extends CExecutor {
    private static final CLogger.Level LOGGING_LEVEL = CLogger.Level.Trace;
    private RawWatcherPlugin rawWatcherPlugin;

    RawWatcherExecutor(RawWatcherPlugin plugin) {
        super(plugin);
        this.rawWatcherPlugin = plugin;
    }

    @SuppressWarnings("SwitchStatementWithTooFewBranches")
    @Override
    public MsgEvent processExec(MsgEvent msg) {
        CrescoGPMSLogger logger = new CrescoGPMSLogger(RawWatcherExecutor.class, rawWatcherPlugin,
                rawWatcherPlugin.getPathStage(), LOGGING_LEVEL);
        logger.trace("Processing [EXEC] message");
        logger.debug("Params: {}", msg.getParams());
        List<String> execRequestErrors = new ArrayList<>();
        String gMsgExecRequestType = msg.getParam(CrescoGPMSStatics.GPMS_EXEC_REQUEST_PARAM_NAME);
        if (gMsgExecRequestType != null) {
            switch (gMsgExecRequestType) {
                case CrescoGPMSStatics.GPMS_EXEC_REQUEST_UPLOAD_RAW_FLOW_CELL: {
                    String flowCellID = msg.getParam(CrescoGPMSStatics.GPMS_EXEC_REQUEST_UPLOAD_RAW_FLOW_CELL_ID);
                    if (flowCellID == null || StringUtils.isBlank(flowCellID)) {
                        execRequestErrors.add(String.format("No valid flow cell ID given in [%s] param",
                                CrescoGPMSStatics.GPMS_EXEC_REQUEST_UPLOAD_RAW_FLOW_CELL_ID));
                    } else {
                        if (rawWatcherPlugin == null) {
                            execRequestErrors.add("No raw watcher plugin is running.");
                        } else {
                            WatchFolder watchFolder = rawWatcherPlugin.getWatchFolder();
                            if (watchFolder == null) {
                                execRequestErrors.add("No watch folder currently exists.");
                            } else {
                                logger.trace("Updating flow cell [{}] to state [READY]", flowCellID);
                                watchFolder.updateFlowCellFolderStatus(flowCellID, TransferStatus.READY);
                            }
                        }
                    }
                } break;
                default: {
                    execRequestErrors.add(String.format("Execute request type [%s] is currently unsupported",
                            gMsgExecRequestType));
                } break;
            }
        }
        msg.setParam(CrescoGPMSStatics.GPMS_INFO_REQUEST_ERRORS_PARAM_NAME, String.join(";", execRequestErrors));
        return msg;
    }

    @SuppressWarnings("SwitchStatementWithTooFewBranches")
    @Override
    public MsgEvent processInfo(MsgEvent msg) {
        CrescoGPMSLogger logger = new CrescoGPMSLogger(RawWatcherExecutor.class, rawWatcherPlugin,
                rawWatcherPlugin.getPathStage(), LOGGING_LEVEL);
        logger.trace("Processing [INFO] message");
        logger.debug("Params: {}", msg.getParams());
        List<String> infoRequestErrors = new ArrayList<>();
        String gMsgInfoRequestType = msg.getParam(CrescoGPMSStatics.GPMS_INFO_REQUEST_TYPE_PARAM_NAME);
        if (gMsgInfoRequestType != null) {
            switch (gMsgInfoRequestType) {
                case CrescoGPMSStatics.GPMS_INFO_REQUEST_TYPE_WATCH_FILE: {
                    String watchFileContents = "";
                    if (rawWatcherPlugin == null) {
                        infoRequestErrors.add("No raw watcher plugin is running.");
                    } else {
                        WatchFolder watchFolder = rawWatcherPlugin.getWatchFolder();
                        if (watchFolder == null) {
                            infoRequestErrors.add("No watch folder currently exists.");
                        } else {
                            watchFileContents = watchFolder.getWatchFileContents();
                        }
                        msg.setCompressedParam(CrescoGPMSStatics.GPMS_INFO_REQUEST_RESULT_PARAM_NAME, watchFileContents);
                    }
                } break;
                default: {
                    infoRequestErrors.add(String.format("Info request type [%s] is currently unsupported",
                            gMsgInfoRequestType));
                } break;
            }
        }
        msg.setParam(CrescoGPMSStatics.GPMS_INFO_REQUEST_ERRORS_PARAM_NAME, String.join(";", infoRequestErrors));
        return msg;
    }
}
