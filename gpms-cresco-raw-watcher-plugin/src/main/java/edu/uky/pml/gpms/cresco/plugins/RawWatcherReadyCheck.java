package edu.uky.pml.gpms.cresco.plugins;

import edu.uky.pml.gpms.cresco.utilities.CrescoGPMSStatics;
import edu.uky.pml.gpms.utilities.logging.GPMSLogger;
import edu.uky.pml.gpms.watcher.ReadyCheck;
import org.apache.commons.lang.StringUtils;

import java.nio.file.Files;
import java.nio.file.Path;

public class RawWatcherReadyCheck implements ReadyCheck {
    private RawWatcherPlugin plugin;
    private GPMSLogger logger;
    private String checkFileName;

    public RawWatcherReadyCheck(RawWatcherPlugin plugin, String checkFileName) throws IllegalArgumentException {
        this.plugin = plugin;
        this.logger = plugin.getCrescoGPMSLogger().cloneLogger(RawWatcherReadyCheck.class);
        logger.trace("RawWatcherReadyCheck('{}')", checkFileName);
        if (checkFileName == null || StringUtils.isBlank(checkFileName))
            throw new IllegalArgumentException(String.format("Check file name [%s] cannot be empty",
                    CrescoGPMSStatics.RAW_WATCHER_CHECK_FILE_NAME));
        this.checkFileName = checkFileName;
    }

    @Override
    public boolean isReady(Path toCheck) {
        logger.trace("isReady('{}')", toCheck);
        return Files.exists(toCheck.resolve(checkFileName));
    }
}
