package edu.uky.pml.gpms.cresco.plugins;

import edu.uky.pml.gpms.cresco.utilities.CrescoGPMSStatics;
import edu.uky.pml.gpms.data.DataManager;
import edu.uky.pml.gpms.utilities.logging.GPMSLogger;
import edu.uky.pml.gpms.watcher.*;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class RawWatcherReadyAction implements ReadyAction {
    private RawWatcherPlugin plugin;
    private GPMSLogger logger;
    private Handler handler;
    private WatchFolder watchFolder;
    private DataManager dataManager;
    private ExecutorService exec = Executors.newFixedThreadPool(1);

    public RawWatcherReadyAction(RawWatcherPlugin plugin, WatchFolder watchFolder, DataManager dataManager) {
        this.plugin = plugin;
        this.logger = plugin.getCrescoGPMSLogger().cloneLogger(RawWatcherReadyAction.class);
        this.watchFolder = watchFolder;
        this.dataManager = dataManager;
    }

    @Override
    public void actOn(FlowCellFolder toActOn) {
        logger.trace("actOn('{}')", toActOn.getFlowCellID());
        exec.submit(new RawWatcherReadyActionWorker(toActOn));
    }

    @Override
    public void setHandler(Handler handler) {
        logger.trace("setHandler()");
        this.handler = handler;
    }

    @Override
    public void stop() {
        logger.trace("stop()");
        exec.shutdownNow();
    }

    private class RawWatcherReadyActionWorker implements Runnable {
        private FlowCellFolder folder;

        public RawWatcherReadyActionWorker(FlowCellFolder folder) {
            logger.info("Adding transfer task for [{}]", folder.getFlowCellID());
            this.folder = folder;
        }

        @Override
        public void run() {
            String flowCellID = folder.getFlowCellID();
            folder = watchFolder.getFolder(flowCellID);
            if (folder == null || folder.getStatus() != TransferStatus.READY) {
                if (folder == null)
                    logger.error("Folder for flow cell [{}] does not exist", flowCellID);
                if (folder.getStatus() != TransferStatus.READY)
                    logger.error("Folder for flow cell [{}] is not ready, but is in status [{}]", flowCellID, folder.getStatus().name());
                return;
            }
            logger.setFlowCellID(flowCellID);
            logger.setStep(1);
            plugin.setPluginStep(3);
            dataManager.updateLogger(logger);
            try {
                logger.trace("Running: {}", flowCellID);
                watchFolder.updateFlowCellFolderStatus(flowCellID, TransferStatus.TRANSFERRING);
                Path flowCellPath = watchFolder.getWatchFolder().resolve(flowCellID);
                if (dataManager.uploadFlowCellRawFiles(flowCellID, flowCellPath)) {
                    watchFolder.updateFlowCellFolderStatus(flowCellID, TransferStatus.COMPLETE);
                    Thread.sleep(1000);
                    logger.incrementStep();
                    Path sampleSheetPath = flowCellPath.resolve("SampleSheet.csv");
                    if (Files.exists(sampleSheetPath)) {
                        Map<String, String> rawUploadInfo = new HashMap<>();
                        try (BufferedReader buffer = Files.newBufferedReader(sampleSheetPath,
                                Charset.forName("UTF-8"))) {
                            rawUploadInfo.put(CrescoGPMSStatics.SAMPLE_SHEET_PARAM_NAME,
                                    buffer.lines().collect(Collectors.joining("\n")));
                        } catch (IOException e) {
                            logger.gpmsError("Failed to read sample sheet [{}]", sampleSheetPath);
                        }
                        logger.gpmsInfo(rawUploadInfo, "Successfully uploaded raw files");
                    } else {
                        logger.gpmsInfo("Successfully uploaded raw files");
                    }
                } else {
                    watchFolder.updateFlowCellFolderStatus(flowCellID, TransferStatus.FAILED);
                    logger.gpmsFailure("Failed to upload raw files");
                }
                if (handler != null)
                    handler.handled(folder);
                logger.setFlowCellID(null);
                dataManager.updateLogger(logger);
                plugin.setPluginStep(2);
            } catch (Exception e) {
                logger.error("[{}] transfer was interrupted\n{}", flowCellID, ExceptionUtils.getStackTrace(e));
            }
        }
    }
}
