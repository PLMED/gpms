package edu.uky.pml.gpms.cresco.plugins;

import com.google.auto.service.AutoService;
import com.researchworx.cresco.library.plugin.core.CPlugin;
import com.researchworx.cresco.library.utilities.CLogger;
import edu.uky.pml.gpms.cresco.utilities.CrescoGPMSLogger;
import edu.uky.pml.gpms.cresco.utilities.CrescoGPMSStatics;
import edu.uky.pml.gpms.data.DataManager;
import edu.uky.pml.gpms.data.encapsulation.Archiver;
import edu.uky.pml.gpms.data.transfer.ObjectStorage;
import edu.uky.pml.gpms.utilities.helpers.GPMSStatics;
import edu.uky.pml.gpms.watcher.*;
import software.amazon.ion.NullValueException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Timer;
import java.util.TimerTask;

@SuppressWarnings({"WeakerAccess", "unused"})
@AutoService(CPlugin.class)
public class RawWatcherPlugin extends CPlugin {
    private static final CLogger.Level LOGGING_LEVEL = CLogger.Level.Info;

    private CrescoGPMSLogger gpmsLogger;

    private Timer heartBeatTimer;
    private int pathStage;

    private WatchFolder watchFolder;
    private Watcher watcher;
    private Handler handler;

    public RawWatcherPlugin() {
    }

    public void start() {
        setPathStage(getConfig().getIntegerParam(CrescoGPMSStatics.PATHSTAGE_CONFIG_PARAM_NAME, 0));
        this.gpmsLogger = new CrescoGPMSLogger(RawWatcherPlugin.class, this, getPathStage(), LOGGING_LEVEL);

        gpmsLogger.info("Starting plugin");
        setExec(new RawWatcherExecutor(this));

        gpmsLogger.trace("Grabbing config file params");
        // Object Storage params
        String s3AccessKey = getConfig().getStringParam(CrescoGPMSStatics.S3_ACCESS_KEY_CONFIG_PARAM_NAME);
        String s3SecretKey = getConfig().getStringParam(CrescoGPMSStatics.S3_SECRET_KEY_CONFIG_PARAM_NAME);
        String s3Endpoint = getConfig().getStringParam(CrescoGPMSStatics.S3_ENDPOINT_CONFIG_PARAM_NAME);
        String s3Region = getConfig().getStringParam(CrescoGPMSStatics.S3_REGION_CONFIG_PARAM_NAME, "");
        // Archiver params
        String archiverBagItType = getConfig().getStringParam(CrescoGPMSStatics.BAGIT_TYPE_CONFIG_PARAM_NAME,
                GPMSStatics.DEFAULT_BAGIT_TYPE);
        String archiverBagItHash = getConfig().getStringParam(CrescoGPMSStatics.BAGIT_HASH_CONFIG_PARAM_NAME,
                GPMSStatics.DEFAULT_BAGIT_HASHING);
        boolean archiverBagItHidden = getConfig().getBooleanParam(CrescoGPMSStatics.BAGIT_INCLUDE_HIDDEN_CONFIG_PARAM_NAME,
                GPMSStatics.DEFAULT_HIDDEN_FILES);
        String archiverCompression = getConfig().getStringParam(CrescoGPMSStatics.COMPRESSION_TYPE_CONFIG_PARAM_NAME,
                GPMSStatics.DEFAULT_ARCHIVE_COMPRESSION);
        // Data Manager params
        String rawBucketName = getConfig().getStringParam(CrescoGPMSStatics.S3_RAW_BUCKET_CONFIG_PARAM_NAME);
        // Raw Watcher Plugin params
        String watchDirectory = getConfig().getStringParam(CrescoGPMSStatics.RAW_WATCHER_WATCH_DIRECTORY);
        String checkFileName = getConfig().getStringParam(CrescoGPMSStatics.RAW_WATCHER_CHECK_FILE_NAME);

        gpmsLogger.trace("Checking watcher params");
        if (watchDirectory == null) {
            gpmsLogger.gpmsError("No watch directory [{}] configured on this plugin",
                    CrescoGPMSStatics.RAW_WATCHER_WATCH_DIRECTORY);
            return;
        }
        Path watchPath = Paths.get(watchDirectory);
        if (!Files.exists(watchPath)) {
            gpmsLogger.gpmsError("Watch directory [{}] does not exist", watchDirectory);
            return;
        }

        gpmsLogger.trace("Building helpers");
        ReadyCheck rawWatcherReadyCheck;
        try {
            rawWatcherReadyCheck = new RawWatcherReadyCheck(this, checkFileName);
        } catch (IllegalArgumentException e) {
            gpmsLogger.gpmsError("Failed to build ready check: {}", e.getMessage());
            return;
        }
        ObjectStorage objectStorage;
        try {
            objectStorage = new ObjectStorage(s3AccessKey, s3SecretKey, s3Endpoint, s3Region, gpmsLogger);
        } catch (NullValueException | IllegalArgumentException e) {
            gpmsLogger.gpmsError("Error building objectStorage : {}", e.getMessage());
            return;
        }
        Archiver archiver;
        try {
            archiver = new Archiver(archiverBagItType, archiverBagItHash, archiverBagItHidden, archiverCompression,
                    gpmsLogger);
        } catch (NullValueException | IllegalArgumentException e) {
            gpmsLogger.gpmsError("Error building archiver : {}", e.getMessage());
            return;
        }
        DataManager dataManager;
        try {
            dataManager = new DataManager(objectStorage, archiver, gpmsLogger);
            dataManager.setRawBucketName(rawBucketName);
        } catch (NullValueException | IllegalArgumentException e) {
            gpmsLogger.gpmsError("Error building data manager : {}", e.getMessage());
            return;
        }

        gpmsLogger.setStep(2);
        gpmsLogger.trace("Building watcher");
        watchFolder = new WatchFolder(Paths.get(""), watchPath, rawWatcherReadyCheck, gpmsLogger);
        watcher = new Watcher(watchFolder, gpmsLogger);
        ReadyAction rawWatcherReadyAction = new RawWatcherReadyAction(this, watchFolder, dataManager);
        handler = new Handler(watchFolder, rawWatcherReadyAction, gpmsLogger);

        gpmsLogger.gpmsInfo("Starting Raw Watcher");
        watcher.start();
        handler.start();

        gpmsLogger.trace("Starting heart beat");
        heartBeatTimer = new Timer();
        heartBeatTimer.scheduleAtFixedRate(new PluginHeartBeat(this), 100, 5000);
    }

    @Override
    public void cleanUp() {
        handler.stop();
        watcher.stop();
        heartBeatTimer.cancel();
        gpmsLogger.setStep(0);
        gpmsLogger.gpmsInfo(CrescoGPMSStatics.PROCESSOR_SHUTDOWN_MESSAGE);
    }

    public CrescoGPMSLogger getCrescoGPMSLogger() {
        return gpmsLogger;
    }

    public int getPathStage() {
        return pathStage;
    }
    public void setPathStage(int pathStage) {
        this.pathStage = pathStage;
    }

    public int getPluginPathStage() {
        return gpmsLogger.getStage();
    }
    public void setPluginPathStage(int stage) {
        gpmsLogger.setStage(stage);
    }

    public int getPluginStep() {
        return gpmsLogger.getStep();
    }
    public void incrementPluginStep() {
        gpmsLogger.incrementStep();
    }
    public void setPluginStep(int step) {
        gpmsLogger.setStep(step);
    }

    public WatchFolder getWatchFolder() {
        return watchFolder;
    }

    public Watcher getWatcher() {
        return watcher;
    }

    public Handler getHandler() {
        return handler;
    }

    public void sendHeartBeat() {
        gpmsLogger.gpmsInfo(CrescoGPMSStatics.PROCESSOR_IDLE_MESSAGE);
    }

    class PluginHeartBeat extends TimerTask {
        private RawWatcherPlugin rawWatcherPlugin;
        PluginHeartBeat(RawWatcherPlugin rawWatcherPlugin) {
            this.rawWatcherPlugin = rawWatcherPlugin;
        }
        public void run() {
            rawWatcherPlugin.sendHeartBeat();
        }
    }
}
